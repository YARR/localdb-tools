#!/usr/bin/env python3
# -*- coding: utf-8 -*
##################################
## Author1: Eunchong Kim (eunchong.kim at cern.ch)
## Copyright: Copyright 2019, ldbtools
## Date: Nov. 2019
## Project: Local Database Tools
## Description: Get client
##################################

from configs.development import *  # Omajinai


def getClient(
    host,
    port,
    authSource=None,
    username=None,
    password=None,
    db_tls=None,
    db_ca_certs=None,
    db_certfile=None,
    auth_mechanism=None,
    keyFile=None,
):
    max_server_delay = 10
    url = "mongodb://{0}:{1}".format(host, port)

    ### check tls
    if db_tls:
        url += "/?ssl=true&ssl_ca_certs={0}&ssl_certfile={1}&ssl_match_hostname=false".format(
            db_ca_certs, db_certfile
        )
        ### check auth mechanism
        if auth_mechanism == "x509":
            url += "&authMechanism=MONGODB-X509"
            authSource = "$external"

    client = MongoClient(url, serverSelectionTimeoutMS=max_server_delay)

    #    try:
    #        localdb.list_collection_names()
    #
    #    except errors.ServerSelectionTimeoutError as err:
    #        ### Connection failed
    #        print('The connection of Local DB {} is BAD.'.format(url))
    #        print(err)
    #        sys.exit(1)
    #
    #    except errors.OperationFailure as err:
    #        ### Need user authentication
    #        print('Need user authentication.')
    #        ### check user and password
    #        if keyFile:
    #            keys = readKey(keyFile)
    #            username = keys['username']
    #            password = keys['password']
    #        if username:
    #            username = username
    #        if password:
    #            password = password
    #        through = False
    #        while through==False:
    #            if not username or not password:
    #                answer = input('Continue? [y/n(skip)]\n> ')
    #                print('')
    #                if answer.lower()=='y':
    #                    username = None
    #                    password = None
    #                else:
    #                    sys.exit(1)
    #                username = input('User name > ')
    #                print('')
    #                password = getpass('Password > ')
    #                print('')
    #            try:
    #                localdb.authenticate(username, password)
    #                through = True
    #            except errors.OperationFailure as err:
    #                print('Authentication failed.')
    #                answer = input('Try again? [y/n(skip)]\n> ')
    #                print('')
    #                if answer.lower()=='y':
    #                    username = input('User name > ')
    #                    print('')
    #                    password = getpass('Password > ')
    #                    print('')
    #                else:
    #                    sys.exit(1)

    if authSource:
        client = MongoClient(
            url, username=username, password=password, authSource=authSource
        )
    else:
        client = MongoClient(url)

    return client
