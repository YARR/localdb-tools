const config = {
  plugins: [require.resolve("prettier-plugin-jinja-template")],
  overrides: [
    {
      files: ["*.html"],
      options: { parser: "jinja-template", printWidth: 140 },
    },
  ],
};

module.exports = config;
