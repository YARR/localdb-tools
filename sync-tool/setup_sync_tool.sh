#!/bin/bash
#################################
# Author: Eunchong Kim, Arisa Kubota
# Email: eunchong.kim at cern.ch, arisa.kubota at cern.ch
# Date: Oct 2019
# Project: Local Database for YARR
# Description: Setup sync tool for local use
#################################
set -e

ITSNAME="[LDB]"

# Usage
function usage {
    cat <<EOF

Usage:
    ./setup_sync_tool.sh

EOF
}

if [ `echo ${0} | grep bash` ]; then
    echo -e "${ITSNAME} DO NOT 'source'"
    usage
    return
fi

TOOLS_DIR=$(cd $(dirname $0); pwd)

# Start
echo -e "$ITSNAME Welcome!"
echo -e "$ITSNAME"

# Check python modules
/usr/bin/env python3 ${TOOLS_DIR}/../setting/check_python_modules.py || exit
echo -e "$ITSNAME"

# Copy bin
cp -r ${TOOLS_DIR}/src/bin ${TOOLS_DIR}/
chmod +x ${TOOLS_DIR}/bin/*

# Set editor command
if [ -z "$EDITOR" ]; then
    read -p "$ITSNAME Set editor command ... > " ANSWER
    while [ -z $ANSWER ];
    do
        read -p "$ITSNAME Set editor command ... > " ANSWER
    done
    EDITOR=$ANSWER
fi
echo -e "$ITSNAME"

# Copy yml configure
if [ ! -f ${TOOLS_DIR}/my_configure.yml ]; then
    cp ${TOOLS_DIR}/src/etc/localdbtools/default.yml ${TOOLS_DIR}/my_configure.yml
fi
$EDITOR ${TOOLS_DIR}/my_configure.yml

echo -e "$ITSNAME"
echo -e "$ITSNAME Finish!"
echo -e "$ITSNAME"
echo -e "$ITSNAME Enable bash completion by ..."
echo -e "$ITSNAME   source ${TOOLS_DIR}/src/share/bash-completion/completions/localdbtool-sync"
echo -e "$ITSNAME"
echo -e "$ITSNAME Start synchronization by ..."
echo -e "$ITSNAME   cd ${TOOLS_DIR}"
echo -e "$ITSNAME   ./bin/localdbtool-sync.py --sync-opt <option> --config my_configure.yml"
echo -e "$ITSNAME"
echo -e "$ITSNAME More information: https://localdb-docs.readthedocs.io/en/master/"
echo -e ""
