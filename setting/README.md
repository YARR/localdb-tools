# PIP Modules

|   module name   |            requirement            |
| :-------------: | :-------------------------------: |
|    arguments    |  Get arguments from command line  |
|   coloredlogs   |          Colored logging          |
|      Flask      |           Web platform            |
|  Flask-PyMongo  |                                   |
| Flask-HTTPAuth  |                                   |
|  Flask-Session  |                                   |
|    pdf2image    |          PDF to JPG/PNG           |
|     Pillow      |                                   |
|   prettytable   | Output a pretty table on terminal |
|     pymongo     |       Python MongoDB client       |
| python-dateutil |                                   |
|     PyYAML      |          Input YAML file          |
|      pytz       |                                   |
|     plotly      |        Plot a pretty plot         |
|   matplotlib    |         Plot a dirty plot         |
|      numpy      |                                   |
|    requests     |   Get/Post via http/https host    |
|     tzlocal     |                                   |

# Create Admin.sh

in edit.
