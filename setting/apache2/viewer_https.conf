########################################
# /etc/httpd/conf.d/viewer_https.conf
# Apache2 configuration for viewer https
# Eunchong Kim
# Arisa Kubota
# Jul. 2020
########################################

# Load WSGI for python3.6
LoadModule wsgi_module /opt/rh/httpd24/root/etc/httpd/modules/mod_rh-python36-wsgi.so

<VirtualHost *:443>
    ServerName HOSTNAME:443

    WSGIDaemonProcess app user=apache group=apache threads=5
    WSGIScriptAlias / /var/www/localdb-tools/viewer/.app.wsgi

    # Support for Automatic Reloading
    WSGIScriptReloading On

    <Directory /var/www/localdb-tools/viewer/>
        WSGIProcessGroup app
        WSGIApplicationGroup %{GLOBAL}
        WSGIPassAuthorization On
        Order deny,allow
        Require all granted
    </Directory>
</VirtualHost>

#=============================================================================================
#                                       SSL
#
# https://www.digitalocean.com/community/tutorials/how-to-create-an-ssl-certificate-on-apache-for-centos-7
#=============================================================================================
# Certificate
SSLCertificateFile /etc/pki/tls/certs/localdb-viewer.crt
SSLCertificateKeyFile /etc/pki/tls/private/localdb-viewer.key
SSLCACertificatePath /etc/pki/tls/certs/
SSLCertificateChainFile /etc/pki/tls/certs/localdb-viewer.crt
#SSLCertificateChainFile /etc/pki/tls/certs/localdb-viewer.p12

# from https://cipherli.st/
# and https://raymii.org/s/tutorials/Strong_SSL_Security_On_Apache2.html
SSLCipherSuite EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH
SSLProtocol All -SSLv2 -SSLv3
SSLHonorCipherOrder On

# Disable preloading HSTS for now.  You can use the commented out header line that includes
# the "preload" directive if you understand the implications.
#Header always set Strict-Transport-Security "max-age=63072000; includeSubdomains; preload"
Header always set Strict-Transport-Security "max-age=63072000; includeSubdomains"
Header always set X-Frame-Options DENY
Header always set X-Content-Type-Options nosniff

# Requires Apache >= 2.4
SSLCompression off
SSLUseStapling on
SSLStaplingCache "shmcb:logs/stapling-cache(150000)"
# Requires Apache >= 2.4.11
# SSLSessionTickets Off
