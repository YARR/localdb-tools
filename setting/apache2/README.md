# Basic: Apache2 for LocalDB Viewer application

Run LocalDB viewer application with apache2 and WSGI

## 1. Install Apache2 and WSGI

### 1.1 Install apache2

```
$ sudo yum install httpd -y
```

### 1.2 Install WSGI

#### For CentOS 7

- Install WSGI for python3.6 followed
  [here](https://stackoverflow.com/questions/42004986/how-to-install-mod-wgsi-for-apache-2-4-with-python3-5-on-centos-7)

```
$ sudo yum install -y centos-release-scl
$ sudo yum install -y rh-python36-mod_wsgi
$ sudo yum remove -y mod_wsgi.x86_64
```

Do not install by `sudo yum install mod_wsgi` because it will install for
python2, not for python3!

#### For Alma 9

```
sudo yum install -y mod_wsgi
```

### 1.3 Firewall

Make sure port 80 and 443 are open

#### a. Open firewall port

```
$ sudo firewall-cmd --add-service=http --permanent
$ sudo firewall-cmd --add-service=https --permanent
$ sudo firewall-cmd --reload
```

#### b. Allow httpd make network request

This allows apache2 connect to mongo server

```
$ sudo /usr/sbin/setsebool -P httpd_can_network_connect 1
```

## 2. Download viewer application

- Clone LocalDB tools

```
$ cd /var/www
$ sudo git clone https://gitlab.cern.ch/yarr/localdb-tools.git
$ sudo chown -R ${USER} localdb-tools
```

- Setup viewer

```
$ cd /var/www/localdb-tools/setting
$ ./create_admin.sh
$ cd /var/www/localdb-tools/viewer
$ ./setup_viewer.sh
$ sudo chmod +x admin_conf.yml .KeyFile
```

- Set followed
  [here](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/security-enhanced_linux/sect-security-enhanced_linux-working_with_selinux-selinux_contexts_labeling_files)

```
$ sudo semanage fcontext -a -t httpd_sys_content_t '/var/www/localdb-tools/viewer(/.*)?'
$ sudo semanage fcontext -a -t httpd_sys_script_exec_t '/var/www/localdb-tools/viewer/plotting-tool/bin(/.*)?'
$ sudo semanage fcontext -a -t httpd_sys_script_exec_t '/var/www/localdb-tools/viewer/analysis-tool/bin(/.*)?'
$ sudo restorecon -R /var/www/localdb-tools/
$ sudo setsebool -P httpd_execmem on
```

## 3. Start apache2

- Setup apache2

```
$ sudo cp /var/www/localdb-tools/setting/apache2/viewer_http.conf /etc/httpd/conf.d/.
$ sudo sed -i -e "s/HOSTNAME/${HOSTNAME}/g" /etc/httpd/conf.d/viewer_http.conf
```

For Alma 9, remove the line starting with `LoadModule` in
`/etc/httpd/conf.d/viewer_http.conf`

- Run apache

```
$ sudo chown -R apache:apache localdb-tools
$ sudo systemctl restart httpd.service
```

## 4. Check

Open browser and type your viewer address. e.g.
http://pcatlidxray01.cern.ch/localdb

# Advanced: Apache2 for LocalDB Viewer application with certificate

Run LocalDB viewer application with apache2 and WSGI and certificate

## 1. Install Apache2 and WSGI

-> Same as the previous section.

## 2. Download viewer application

-> Same as the previous section.

## 3. Create SSL authentication certificate

### a. for CERN host (CERN LocalDB)

Go to https://ca.cern.ch/ca/, to generate CERN HOST CERTIFICATE. It will be
hostservername.p12 (PKCS12 file).

- Extract PKCS12 file to certifiactes private key

```
$ openssl pkcs12 -nodes -in hostservername.p12 -clcerts -nokeys -out localdb-viewer.crt
$ openssl pkcs12 -nodes -in hostservername.p12 -nocerts -out localdb-viewer.key
```

- Move certificate and private key to directory /etc/pki/tls

```
$ sudo mv localdb-viewer.crt /etc/pki/tls/certs/.
$ sudo mv localdb-viewer.key /etc/pki/tls/private/.
$ sudo mv hostservername.p12 /etc/pki/tls/certs/localdb-viewer.p12
$ sudo chmod 600 /etc/pki/tls/private/localdb-viewer.key
```

### b. NOT for CERN host

```
$ sudo yum install mod_ssl
$ sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/pki/tls/private/localdb-viewer.key -out /etc/pki/tls/certs/localdb-viewer.crt

#   Country Name (2 letter code) [XX]:US
#   State or Province Name (full name) []:Example
#   Locality Name (eg, city) [Default City]:Example
#   Organization Name (eg, company) [Default Company Ltd]:Example Inc
#   Organizational Unit Name (eg, section) []:Example Dept
#   Common Name (eg, your name or your server's hostname) []:example.com
#   Email Address []:webmaster@example.com

$ sudo openssl dhparam -out /etc/pki/tls/certs/dhparam.pem 2048
$ cat /etc/pki/tls/certs/dhparam.pem | sudo tee -a /etc/pki/tls/certs/localdb-viewer.crt
```

## 4. Start apache2

- Setup apache2

```
$ sudo cp /var/www/localdb-tools/setting/apache2/viewer_https.conf /etc/httpd/conf.d/.
$ sudo sed -i -e "s/HOSTNAME/${HOSTNAME}/" /etc/httpd/conf.d/viewer_https.conf
$ sudo cp /var/www/localdb-tools/setting/apache2/viewer_http_https.conf /etc/httpd/conf.d/viewer_http.conf
$ sudo sed -i -e "s/HOSTNAME/${HOSTNAME}/g" /etc/httpd/conf.d/viewer_http.conf
```

- Run apache

```
$ sudo systemctl restart httpd.service
```

## 5. Check

Open browser and type your viewer address. e.g.
https://pcatlidxray01.cern.ch/localdb

# Additional: Apache2 for proxy server of LocalDB Viewer application

Run LocalDB viewer application and run apache2 for proxy server

## 1. Install Apache2

-> Same as the previous section.

## 2. Download viewer application

- Clone LocalDB tools

```
$ cd ${HOME}
$ git clone https://gitlab.cern.ch/yarr/localdb-tools.git
```

- Setup viewer

```
$ cd ${HOME}/localdb-tools/setting
$ ./create_admin.sh
$ cd /var/www/localdb-tools/viewer
$ ./setup_viewer.sh
$ sudo chmod +x admin_conf.yml .KeyFile
```

## 3. Run viewer application

```
$ cd ${HOME}/localdb-tools/viewer
$ python3 app.py --config admin_conf.yml &
```

## 4. Start apache2

- Setup apache2

```
$ sudo cp /var/www/localdb-tools/setting/apache2/viewer_http_proxy.conf /etc/httpd/conf.d/viewer_http.conf
```

- Run apache

```
$ sudo systemctl restart httpd.service
```

## 5. Check

Open browser and type your viewer address. e.g.
http://pcatlidxray01.cern.ch/localdb

# Errors and warnings

- Unable to configure server certificate for stapling

- Your connection is not private

# References

- [How to add modules to the apache server on centOS7](https://hostadvice.com/how-to/how-to-add-modules-to-the-apache-server-on-centos/)
- [How to create an SSL certificate on apache for centOS7](https://www.digitalocean.com/community/tutorials/how-to-create-an-ssl-certificate-on-apache-for-centos-7)
- [How to install WSGI with pip](https://pypi.org/project/mod-wsgi/)
