#!/bin/zsh
#################################
# Contacts: Arisa Kubota
# Email: arisa.kubota at cern.ch
# Date: April 2020
# Project: Local Database Tools
# Description: Setup DB Server
# Usage: ./installer_macos.sh
################################

#############
### Usage ###
#############

function usage {
    cat <<EOF

Usage:
    ./installer_macos.sh

    - h    Show this usage
EOF
}

#############
### Start ###
#############

if [ `echo ${0} | grep bash` ]; then
    echo -e "[LDB] DO NOT 'source'"
    usage
    return
fi

while getopts h OPT
do
    case ${OPT} in
        h ) usage
            exit ;;
        * ) usage
            exit ;;
    esac
done

shell_dir=$(cd $(dirname ${BASH_SOURCE:-0}); pwd)

which brew > /dev/null 2>&1
if [ $? = 1 ]; then
    printf '\033[31m%s\033[m\n' "[LDB ERROR] 'brew' command is required."
    exit 1
fi

#################################################
### Confirmation before starting installation ###
#################################################

echo -e "[LDB] Looking for missing packages for Local DB and Tools ..."

############################
### Check python version ###
############################

echo -e "[LDB]     Checking Python version ..."
pytver=false
if which python3 > /dev/null 2>&1; then
    python3 <<EOF
import sys
if not sys.version_info[0]==3: sys.exit(1)
if not sys.version_info[1]>=4: sys.exit(1)
sys.exit(0)
EOF
    if [ $? = 0 ]; then
        pytver=true
    fi
fi

############################
### Check python package ###
############################

echo -e "[LDB]     Checking Python packages ..."
if "${pytver}"; then
    piparray=()
    while read -r pac req ver; do
        pac_mod=$( echo ${pac} | sed -e "s/\[.*//g" )
        if ! python3 -m pip list 2>&1 | grep ${pac_mod} 2>&1 > /dev/null; then
            piparray+=(${pac}${req}${ver})
        fi
    done < ${shell_dir}/../requirements-pip.txt
    pippackages=("${piparray[@]}")
fi

##########################
### Check brew package ###
##########################

echo -e "[LDB]     Checking brew packages ..."

### gcc
bool_gcc=false
if g++ --version > /dev/null 2>&1; then
    bool_gcc=true
fi

### cmake
bool_cmake=false
if cmake --version > /dev/null 2>&1; then
    bool_cmake=true
fi

### others
bool_gawk=false
if which gawk > /dev/null 2>&1; then
    bool_gawk=true
fi
bool_gnuplot=false
if which gnuplot > /dev/null 2>&1; then
    bool_gnuplot=true
fi
bool_gs=false
if which gs > /dev/null 2>&1; then
    bool_gs=true
fi
bool_mactex=false
if which platex > /dev/null 2>&1; then
    bool_mactex=true
fi

### MongoDB
bool_mongo=false
if which mongo > /dev/null 2>&1; then
    bool_mongo=true
fi

### InfluxDB
bool_influx=false
if which influx > /dev/null 2>&1; then
    bool_influx=true
fi

### Grafana
bool_grafana=false
if which grafana-server > /dev/null 2>&1; then
    bool_grafana=true
fi

echo "[LDB] Done."

####################
### Confirmation ###
####################

package_install=false
initialize_db=false
if ! "${bool_gcc}" || ! "${bool_cmake}" || ! "${bool_gawk}" || ! "${bool_gnuplot}" || ! "${bool_gs}" || ! "${bool_mactex}" || ! "${bool_mongo}" || ! "${bool_influx}" || ! "${bool_grafana}" || [ ${#pippackages} != 0 ]; then
    printf '\033[33m%s\033[m\n' '------------------------------------'
    printf '\033[33m%s\033[m\n' '          Missing packages          '
    printf '\033[33m%s\033[m\n' '------------------------------------'
    if ! "${bool_gcc}"; then
        printf '\033[33m%s\033[m\n' " brew   : g++"
    fi
    if ! "${bool_cmake}"; then
        printf '\033[33m%s\033[m\n' " brew   : cmake"
    fi
    if ! "${bool_gawk}"; then
        printf '\033[33m%s\033[m\n' " brew   : gawk"
    fi
    if ! "${bool_gnuplot}"; then
        printf '\033[33m%s\033[m\n' " brew   : gnuplot"
    fi
    if ! "${bool_gs}"; then
        printf '\033[33m%s\033[m\n' " brew   : ghostscript"
    fi
    if ! "${bool_mactex}"; then
        printf '\033[33m%s\033[m\n' " brew   : mactex"
    fi
    if ! "${bool_mongo}"; then
        printf '\033[33m%s\033[m\n' " brew   : MongoDB"
    fi
    if ! "${bool_influx}"; then
        printf '\033[33m%s\033[m\n' " brew   : InfluxDB"
    fi
    if ! "${bool_grafana}"; then
        printf '\033[33m%s\033[m\n' " brew   : Grafana"
    fi
    if [ ${#pippackages} != 0 ]; then
        for pac in ${pippackages[@]}; do
            printf '\033[33m%s\033[m\n' " python : ${pac}"
        done
    fi
    printf '\033[33m%s\033[m\n' '------------------------------------'
    echo -e ""
    echo -e "[LDB] Install these package? [y/n]"
    unset answer
    while [ -z ${answer} ]; do
        read answer
    done
    echo -e ""
    if [[ ${answer} == "y" ]]; then
        package_install=true
    fi
else
    echo -e "[LDB]"
    echo -e "[LDB] Requirement already satisfied."
    echo -e "[LDB]"
fi

### Confirmation

echo -e "[LDB] Following steps will be done ..."
if "${package_install}" || "${initialize_db}"; then
    if "${package_install}"; then
        echo -e "[LDB] - Install required packages by brew & pip."
    fi
    echo -e "[LDB]"
    echo -e "[LDB] You need to be root to perform this command."
    sudo echo "[LDB] OK!"
    if [ $? = 1 ]; then
        printf '\033[31m%s\033[m\n' "[LDB ERROR] Failed, exit..."
        exit 1
    fi

    ### Install necessary packages if not yet installed
    echo -e "[LDB] Start."

    ### Install packages
    if "${package_install}"; then
        ### Install brew packages
        if ! "${bool_gcc}"; then
            echo -e "[LDB]     Installing g++ ..."
            brew install gcc > /dev/null
        fi
        if ! "${bool_cmake}"; then
            echo -e "[LDB]     Installing cmake ..."
            brew install cmake > /dev/null
        fi
        if ! "${bool_gawk}"; then
            echo -e "[LDB]     Installing gawk ..."
            brew install gawk > /dev/null
        fi
        if ! "${bool_gnuplot}"; then
            echo -e "[LDB]     Installing gnuplot ..."
            brew install gnuplot > /dev/null
        fi
        if ! "${bool_gs}"; then
            echo -e "[LDB]     Installing gs ..."
            brew install ghostscript > /dev/null
        fi
        if ! "${bool_mactex}"; then
            echo -e "[LDB]     Installing mactex ..."
            brew cask install mactex > /dev/null
            sudo tlmgr update --self --all > /dev/null
            sudo tlmgr paper a4 > /dev/null
        fi
        if ! "${bool_mongo}"; then
            echo -e "[LDB]     Installing MongoDB ..."
            brew tap mongodb/brew > /dev/null
            brew install mongodb-community@4.2 > /dev/null
        fi
        if ! "${bool_influx}"; then
            echo -e "[LDB]     Installing InfluxDB ..."
            brew install influxdb > /dev/null
        fi
        if ! "${bool_grafana}"; then
            echo -e "[LDB]     Installing Grafana ..."
            brew install grafana > /dev/null
            brew tap homebrew/services > /dev/null
        fi
        if ! "${pytver}"; then
            echo -e "[LDB]     Installing Python3 ..."
            brew install python3 > /dev/null
        fi
        ### Install python packages by pip for the DB viewer
        for pac in ${pippackages[@]}; do
            echo -e "[LDB]     Installing ${pac} ..."
            sudo -H python3 -m pip install ${pac} > /dev/null
        done
    fi

    echo -e "[LDB] Done."
    echo -e "[LDB]"
    echo -e "[LDB] Finished installation!!"
    echo -e "[LDB]"
else
    echo -e "[LDB] - Nothings to do."
    echo -e "[LDB]"
fi

### Start MongoDB
monver=false
if which mongo > /dev/null 2>&1; then
    mongo_version=`awk '{print $4}' <<< $( mongo --version )`
    test ${mongo_version:1:1} -ge 4
    if [ $? = 0 ]; then
        test ${mongo_version:3:1} -ge 2
        if [ $? = 0 ]; then
            monver=true
        fi
    fi
fi

if "${monver}"; then
    echo -e "[LDB] Setting up mongod..."
    if brew services list | grep mongodb > /dev/null; then
        echo -e "[LDB] mongod is already running. Nothing to do."
    else
        echo -e "[LDB] Starting mongod on your local machine."
        brew services start mongodb-community@4.2 > /dev/null
    fi
    echo -e "[LDB]"
    echo -e "[LDB] Set Local DB Server:"
    echo -e "[LDB]     IP address: 127.0.0.1"
    echo -e "[LDB]     port      : 27017"
    echo -e "[LDB] Done."
    echo -e "[LDB]"
fi

### Start InfluxDB
influx_exist=false
if which influx > /dev/null 2>&1; then
    influx_exist=true
fi


if "${influx_exist}"; then
    echo -e "[LDB] Setting up influxd..."
    if launchctl list | grep influxdb > /dev/null; then
        echo -e "[LDB] influxdb is already running. Nothing to do."
    else
        echo -e "[LDB] Starting influxdb on your local machine."
        ln -sfv /usr/local/opt/influxdb/*.plist ~/Library/LaunchAgents > /dev/null
        launchctl load ~/Library/LaunchAgents/homebrew.mxcl.influxdb.plist > /dev/null
    fi
    echo -e "[LDB]"
    echo -e "[LDB] Set influxDB Server:"
    echo -e "[LDB]     IP address: 127.0.0.1"
    echo -e "[LDB]     port      : 8086"
    echo -e "[LDB] Done."
    echo -e "[LDB]"
fi

### Start Grafana
grafana_exist=false
if which grafana-server > /dev/null 2>&1; then
    grafana_exist=true
fi

if "${grafana_exist}"; then
    echo -e "[LDB] Setting up grafana-server..."
    if brew services list | grep grafana > /dev/null; then
        echo -e "[LDB] grafana-server is already running. Nothing to do."
    else
        echo -e "[LDB] Starting grafana-server on your local machine."
        brew services start grafana > /dev/null
    fi
    echo -e "[LDB]"
    echo -e "[LDB] Set Grafana Server:"
    echo -e "[LDB]     IP address: 127.0.0.1"
    echo -e "[LDB]     port      : 3000"
    echo -e "[LDB] Done."
    echo -e "[LDB]"
fi

echo -e "[LDB]"
echo -e "[LDB] Finished!!"
echo -e "[LDB]"

##########################
### Final Confirmation ###
##########################
echo -e "[LDB] Final confirming ..."

pytver=false
if which python3 > /dev/null 2>&1; then
    python3 <<EOF
import sys
if not sys.version_info[0]==3: sys.exit(1)
if not sys.version_info[1]>=4: sys.exit(1)
sys.exit(0)
EOF
    if [ $? = 0 ]; then
        pytver=true
    fi
fi

monver=false
if which mongo > /dev/null 2>&1; then
    mongo_version=`awk '{print $4}' <<< $( mongo --version )`
    test ${mongo_version:1:1} -ge 4
    if [ $? = 0 ]; then
        test ${mongo_version:3:1} -ge 2
        if [ $? = 0 ]; then
            monver=true
        fi
    fi
fi

if which mongosh > /dev/null 2>&1; then
    monver=true
fi

if "${pytver}"; then
    piparray=()
    while read -r pac req ver; do
        pac_mod=$( echo ${pac} | sed -e "s/\[.*//g" )
        if ! python3 -m pip list 2>&1 | grep ${pac_mod} 2>&1 > /dev/null; then
            piparray+=(${pac}${req}${ver})
        fi
    done < ${shell_dir}/../requirements-pip.txt
    pippackages=("${piparray[@]}")
fi

brewpackages=( `cat ${shell_dir}/requirements.txt` )
unset brewarray
for pac in ${brewpackages[@]}; do
    if ! which ${pac} > /dev/null 2>&1; then
        brewarray+=(${pac})
    fi
done
brewpackages=${brewarray[@]}
echo $brewpackages

if ! "${pytver}" || [ ${#brewpackages} != 0 ] || [ ${#pippackages} != 0 ] || ! "${monver}"; then
    if ! "${pytver}"; then
        printf '\033[31m%s\033[m\n' '[LDB ERROR] Python version 3.4 or greater is required.'
    fi
    if [ ${#brewpackages} != 0 ]; then
        printf '\033[31m%s\033[m\n' '[LDB ERROR] brew packages not be installed: '
        for pac in ${brewpackages[@]}; do
            printf '\033[31m%s\033[m\n' "[LDB ERROR]     - ${pac}"
        done
        printf '\033[31m%s\033[m\n' "[LDB ERROR] You can install them by brew. (detail: https://localdb-docs.readthedocs.io/en/master/manual-install-macos/)"
    fi
    if [ ${#pippackages} != 0 ]; then
        printf '\033[31m%s\033[m\n' '[LDB ERROR] python packages not be installed: '
        for pac in ${pippackages[@]}; do
            printf '\033[31m%s\033[m\n' "[LDB ERROR]     - ${pac}"
        done
        printf '\033[31m%s\033[m\n' "[LDB ERROR] You can install them by 'python3 -m pip install <package>'"
    fi
    if ! "${monver}"; then
        printf '\033[31m%s\033[m\n' '[LDB ERROR] MongoDB version 4.2 or greater is required.'
        printf '\033[31m%s\033[m\n' "[LDB ERROR] You can install/upgrade using brew. (detail: https://docs.mongodb.com/manual/installation/)"
    fi
    echo -e ""
    printf '\033[31m%s\033[m\n' "[LDB] If you want to setup them manually, the page 'https://localdb-docs.readthedocs.io/en/master/' should be helpful!"
else
    echo -e "[LDB] Success!!"
    echo -e ""
    echo -e "----------------------------------------"
    echo -e "# Quick tutorial"
    echo -e ""
    echo -e "## 1. Setup Viewer Application"
    echo -e "$ cd localdb-tools/viewer"
    echo -e "$ ./setup_viewer.sh"
    echo -e "$ python3 app.py --config conf.yml"
    echo -e ""
    echo -e "## 2. Access Viewer Application"
    echo -e "- From the DB machine: http://localhost:5000/localdb/"
    echo -e ""
    echo -e "## 3.Check more detail"
    echo -e "- https://localdb-docs.readthedocs.io/en/master/"
    echo -e "----------------------------------------"
    echo -e "Enjoy!!"
fi
