#!/usr/bin/env python3
#################################
# Author: Eunchong Kim
# Email: eunchong.kim at cern.ch
# Date: April 2019
# Project: Local Database for YARR
# Description: Check python modules
#################################

from pathlib import Path

import pkg_resources

module_names = []
requirement_file = Path(__file__).parent.joinpath("requirements-pip.txt").absolute()


if requirement_file.is_file():
    for req in pkg_resources.parse_requirements(
        requirement_file.read_text().splitlines()
    ):
        module_names.append(pkg_resources.to_filename(req.key))
else:
    print("[LDB] ERROR! Cannot open 'requirements-pip.txt'")
    exit(1)

print("[LDB] Welcome to Local Database Tools!")


# Check python modules
def checkPythonModule():
    print("[LDB] Check python modules... ")
    founds = []

    packages = []
    for dist in pkg_resources.working_set:
        packages.append(dist.project_name)

    for module_name in module_names:
        if module_name in packages:
            founds.append(True)
        else:
            founds.append(False)

    if False in founds:
        print("[LDB] Packages not be installed:")
        for idx, module_name in enumerate(module_names):
            if not founds[idx]:
                print(f"\t- {module_name}")
        print('[LDB] Install them by "python3 -m pip install <package name>"')
        print(f'[LDB] or " python3 -m pip install -r {requirement_file}"')
        exit(1)
    else:
        print("[LDB] Requirements already satisfied")


if __name__ == "__main__":
    checkPythonModule()
