import argparse
import datetime
from hashlib import md5

from pymongo import MongoClient

parser = argparse.ArgumentParser(
    prog="Create Admin", description="Creates admin login for the LocalDB in MongoDB."
)


parser.add_argument("admin_username")
parser.add_argument("admin_password")
parser.add_argument(
    "--address", default="localhost", help="ip address of MongoDB (default: localhost)"
)
parser.add_argument(
    "--port", default=27017, type=int, help="port of MongoDB (default: 27017)"
)

args = parser.parse_args()


client = MongoClient(args.address, args.port)

localdb = client["localdb"]
localdbTools = client["localdbtools"]

user = args.admin_username
password = args.admin_password
user_hashed = md5(user.encode("utf-8")).hexdigest()
password_hashed = md5(password.encode("utf-8")).hexdigest()


localdb.command("dropUser", user, check=False)
localdb.command(
    "createUser",
    user,
    pwd=password,
    roles=[
        {"role": "readWrite", "db": "localdb"},
        {"role": "readWrite", "db": "localdbtools"},
        {"role": "userAdmin", "db": "localdb"},
        {"role": "userAdmin", "db": "localdbtools"},
    ],
)


localdb.command("dropUser", user_hashed, check=False)
localdb.command(
    "createUser",
    user_hashed,
    pwd=password_hashed,
    roles=[
        {"role": "readWrite", "db": "localdb"},
        {"role": "readWrite", "db": "localdbtools"},
        {"role": "userAdmin", "db": "localdb"},
        {"role": "userAdmin", "db": "localdbtools"},
    ],
)


localdbTools.viewer.user.delete_one({"username": user})

localdbTools.viewer.user.insert_one(
    {
        "sys": {
            "rev": 0,
            "cts": datetime.datetime.now(tz=datetime.timezone.utc),
            "mts": datetime.datetime.now(tz=datetime.timezone.utc),
        },
        "username": user,
        "name": user,
        "auth": "adminViewer",
        "institution": "",
        "Email": "",
        "password": password_hashed,
    }
)
