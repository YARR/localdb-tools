import argparse
import datetime
import os
import sys

from bson.objectid import ObjectId
from pymongo import MongoClient


def connectDB():
    client = MongoClient(host="127.0.0.1", port=27017)
    return client


def main():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument("--module", "-m", help="Module name", type=str)
    parser.add_argument("--user", "-u", help="User name", type=str)
    parser.add_argument("--password", "-p", help="password", type=str)
    parser.add_argument("--test", "-t", help="Test name", type=str)

    args = parser.parse_args()

    c = connectDB()
    localdb = c["localdb"]
    userdb = c["localdbtools"]
    localdb.authenticate(args.user, args.password)

    this_cp = localdb.component.find_one({"name": args.module}, {"proDB": True})
    if this_cp is None:
        print("This module is not registered.")
        sys.exit()
    qc_doc = localdb.QC.module.status.find_one({"component": str(this_cp["_id"])})
    user_doc = userdb.viewer.user.find_one({"username": args.user})

    doc = {
        "component": qc_doc["component"],
        "user": args.user,
        "address": user_doc["institution"],
        "stage": qc_doc["stage"],
        "sys": {
            "mts": datetime.datetime.now(),
            "cts": datetime.datetime.now(),
            "rev": 0,
        },
        "dbVersion": 1.01,
        "testType": args.test,
        "results": {},
    }

    localdb.QC.result.insert_one(doc)
    print("Generated and inserted !")


if __name__ == "__main__":
    main()
