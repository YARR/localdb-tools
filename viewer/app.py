import logging
import os
import ssl
import threading

import pytz
from flask import (
    Flask,
    request,
)
from flask_mail import Mail
from flask_session import Session
from functions.common import (
    PrefixMiddleware,
    args,
    check_gitlab_announcements,
    check_latest_localdb_version,
    localdb_url,
    readKey,
    run_db_migrations,
)
from pages.route import (
    checkout_api,
    component_api,
    config_api,
    error_api,
    history_api,
    picture_data_api,
    plot_data_api,
    plot_dcs_api,
    qc_api,
    register_api,
    retrieve_api,
    site_api,
    static_api,
    sync_statistics_api,
    tag_api,
    toppage_api,
    user_api,
)
from pymongo import (
    MongoClient,
)

logger = logging.getLogger("localdb")

# temporarily disabled
# startup.startup()

# ==============================
# Setup logging
# ==============================
# setupLogging("logs/development.log")

# ==============================
# Setup SSL Certificate
# ==============================
if args.fcert and args.fkey:
    context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
    context.load_cert_chain(args.fcert, args.fkey)
    localdb_url = "https" + localdb_url
else:
    context = None
    localdb_url = "http" + localdb_url

# app
app = Flask(__name__)
logger.info(f"Viewer Application URL: {localdb_url}/")

with app.app_context():
    app.wsgi_app = PrefixMiddleware(app.wsgi_app, prefix="/localdb")
    app.config["SECRET_KEY"] = os.urandom(24)
    # app.config['SECRET_KEY'] = 'key'

    ## for spescific users
    # app.config['MAIL_SERVER'] = ''
    # app.config['MAIL_PORT'] = 465
    # app.config['MAIL_USERNAME'] = ''
    # app.config['MAIL_PASSWORD'] = ''
    # app.config['MAIL_USE_TLS'] = False
    # app.config['MAIL_USE_SSL'] = True

    mail = Mail(app)

    if args.fsession:
        if args.fskey:
            fskeys = readKey(args.fskey)
            username = fskeys["username"]
            password = fskeys["password"]
        else:
            username = args.fsuser
            password = args.fspass
        app.config["SESSION_TYPE"] = "mongodb"
        app.config["SESSION_MONGODB"] = MongoClient(
            f"mongodb://{args.host}:{args.port}",
            authSource=args.fsdb,
            username=username,
            password=password,
        )
        app.config["SESSION_MONGODB_DB"] = args.fsdb
        app.config["SESSION_MONGODB_COLLECTION"] = "sessions"
        Session(app)

    # Register Blue Prints
    app.register_blueprint(sync_statistics_api)
    app.register_blueprint(plot_data_api)
    app.register_blueprint(picture_data_api)
    app.register_blueprint(retrieve_api)
    app.register_blueprint(static_api)

    ### Pages
    ## Top Page
    app.register_blueprint(toppage_api)
    ## Component/Result Page
    app.register_blueprint(component_api)
    ## History Page
    app.register_blueprint(history_api)
    ## FE configs
    app.register_blueprint(config_api)
    ## QC
    app.register_blueprint(qc_api)
    ## Checkout
    app.register_blueprint(checkout_api)
    ## Register
    app.register_blueprint(register_api)
    ## Tag
    app.register_blueprint(tag_api)
    ## User
    app.register_blueprint(user_api)
    ## DCS
    app.register_blueprint(plot_dcs_api)
    ## Error handlers
    app.register_blueprint(error_api)

    app.register_blueprint(site_api)

    logging.getLogger("werkzeug").setLevel(logging.ERROR)
    logging.getLogger("module_qc_database_tools").setLevel(logging.WARNING)

    run_db_migrations()


@app.before_request
def incoming_request_set_thread_name():
    threading.current_thread().name = f"{request.method} {request.path}"


@app.context_processor
def inject_timezone():
    return {"timezones": pytz.all_timezones}


if __name__ == "__main__":
    check_latest_localdb_version()
    check_gitlab_announcements()
    app.run(
        host="0.0.0.0", port=args.fport, threaded=True, debug=False, ssl_context=context
    )
