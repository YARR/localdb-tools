import json
import logging
import os
import sys

from ..common import localdb, process_request
from . import PDInterface

logger = logging.getLogger("localdb")


class ModuleRegistration(PDInterface.PDInterface):
    def user_institutions(self, user):
        userinfo = self.pd_client.get("getUser", json={"userIdentity": user.identity})
        return [i["code"] for i in (userinfo.get("institutions") or [])]

    def get_component(self, serialNum):
        return super().getCompFromProdDB(serialNum)

    def check_assemble(self, module_type, child_type, serialnumber, look_up_table):
        check = -1
        if look_up_table["MODULE_TYPES"][module_type][child_type] == serialnumber[3:7]:
            check = 1
        else:
            check = 0
        return check

    def check_exist(self, component_sn):
        check = "-1"
        try:
            self.pd_client.get("getComponent", json={"component": component_sn})
            check = "1"
        except Exception:
            check = "2"
        return check

    def make_table(self, config):
        look_up_table = {}
        look_up_table["MODULE_TYPES"] = {}
        look_up_table["FECHIP_VERSION"] = {}
        # module types
        for i in range(len(config["types"])):
            look_up_table["MODULE_TYPES"][config["types"][i]["code"]] = {
                "XX": config["types"][i]["subprojects"][0]["code"],
                "YY": config["types"][i]["snComponentIdentifier"],
                "BARE_MODULE": config["children"][
                    config["types"][i]["code"].replace(".", "")
                ]["BARE_MODULE"],
                "PCB": config["children"][config["types"][i]["code"].replace(".", "")][
                    "PCB"
                ],
            }
        # FE chips
        for i in range(len(config["FEchip"])):
            look_up_table["FECHIP_VERSION"][config["FEchip"][i]["code"]] = config[
                "FEchip"
            ][i]["value"]
        return look_up_table

    def register_Module(self, config, look_up_table, location):
        check = 0

        data = {
            "project": "P",
            "subproject": look_up_table["MODULE_TYPES"][config["type"]]["XX"],
            "institution": location,
            "componentType": "MODULE",
            "type": config["type"],
            "properties": {
                "FECHIP_VERSION": config["FECHIP"],
                "ORIENTATION": True,
            },
            "serialNumber": config["serialNumber"],
        }

        module_ins = self.pd_client.post("registerComponent", json=data)
        logger.info("Register module!!")

        module_doc = self.pd_client.get(
            "getComponent", json={"component": module_ins["component"]["code"]}
        )

        if "BARE_MODULE" in config["child"]:
            if len(config["child"]["BARE_MODULE"]) > 1:
                for j in range(len(config["child"]["BARE_MODULE"])):
                    data = {
                        "parent": module_ins["component"]["code"],
                        "slot": module_doc["children"][j + 1]["id"],
                        "child": config["child"]["BARE_MODULE"][j],
                    }

                    if not data.get("child"):
                        continue

                    if (
                        self.check_assemble(
                            config["type"], "BARE_MODULE", data["child"], look_up_table
                        )
                        == 0
                    ):
                        logger.info(
                            "Couldn't assemble BARE_MODULE because the BARE_MODULE and the module are not the correct combination. Please check them and assemble on the Web page."
                        )
                        check = 1
                    else:
                        # assemble the bare module to the module
                        self.pd_client.post("assembleComponentBySlot", json=data)
                        logger.info("Assemble BARE_MODULE!!")
            else:
                data = {
                    "parent": module_ins["component"]["code"],
                    "slot": module_doc["children"][1]["id"],
                    "child": config["child"]["BARE_MODULE"][0],
                }
                if (
                    self.check_assemble(
                        config["type"], "BARE_MODULE", data["child"], look_up_table
                    )
                    == 0
                ):
                    logger.info(
                        "Couldn't assemble BARE_MODULE because the BARE_MODULE and the module are not the correct combination. Please check them and assemble on the Web page."
                    )
                    check = 1
                else:
                    # assemble the bare module to the module
                    self.pd_client.post("assembleComponentBySlot", json=data)
                    logger.info("Assembled BARE_MODULE!!")

        if "PCB" in config["child"]:
            data = {
                "parent": module_ins["component"]["code"],
                "child": config["child"]["PCB"],
            }
            if (
                self.check_assemble(config["type"], "PCB", data["child"], look_up_table)
                == 0
            ):
                logger.info(
                    "Couldn't assemble PCB because the PCB and the module are not the correct combination. Please check them and assemble on the Web page."
                )
                if check == 0:
                    check = 2
                if check == 1:
                    check = 3
            else:
                # assemble the PCB to the module
                self.pd_client.post("assembleComponent", json=data)
                logger.info("Assembled PCB!!")

        if "CARRIER" in config["child"]:
            data = {
                "parent": module_ins["component"]["code"],
                "child": config["child"]["CARRIER"],
            }
            if data.get("child"):
                # assemble the module carrier to the module if need it
                self.pd_client.post("assembleComponent", json=data)
                logger.info("Assembled MODULE_CARRIER!!")
            else:
                logger.info("Skipped to assemble MODULE_CARRIER")


if __name__ == "__main__":
    code1 = os.environ.get("ITKDB_ACCESS_CODE1", " ")
    code2 = os.environ.get("ITKDB_ACCESS_CODE2", " ")
    success, main_pd_client = process_request(code1, code2)
    if success == 0:
        sys.exit(1)
    else:
        with open("module_registerCfg.json", encoding="utf-8") as f:
            _config = json.load(f)
        _module_config = localdb.QC.module.types.find_one()

        mod_reg = ModuleRegistration(main_pd_client)
        mod_reg.register_Module(_config, mod_reg.make_table(_module_config))
