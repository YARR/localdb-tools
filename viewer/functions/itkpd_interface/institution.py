import logging
from datetime import datetime

import itkdb
from bson.objectid import ObjectId

from ..common import dbv, localdb, mount_retry_adapter

logger = logging.getLogger("localdb")

##############
## functions


def query_institution(doc):
    query = {"code": doc["code"]}
    if localdb.institution.find_one(query) is None:
        institution_info = {
            "code": doc["code"],
            "institution": doc["institution"],
            "address": "...",
            "HOSTNAME": "...",
            "sys": {"mts": datetime.utcnow(), "cts": datetime.utcnow(), "rev": 0},
            "dbVersion": dbv,
        }
        localdb.institution.insert_one(institution_info)

    return localdb.institution.find_one(query)


def create_institute(institute_doc):
    return {
        "sys": {"mts": datetime.utcnow(), "cts": datetime.utcnow(), "rev": 0},
        "dbVersion": dbv,
        "_id": ObjectId(institute_doc["id"]),
        "code": institute_doc["code"],
        "institution": institute_doc["name"],
    }


def download_institution(code1, code2):
    u = itkdb.core.User(access_code1=code1, access_code2=code2)
    pd_client = itkdb.Client(user=u, use_eos=True)
    mount_retry_adapter(pd_client)

    # download institute information
    logger.info("Start downloading institution info...")
    for institute_doc in pd_client.get("listInstitutions", json={}):
        doc = create_institute(institute_doc)
        query_institution(doc)
    logger.info("Successfully finished downloading institutions.\n")
