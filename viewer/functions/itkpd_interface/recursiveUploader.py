import concurrent.futures
import contextlib
import io
import json
import logging
import os
import pprint
import shutil
import sys
import traceback
import zipfile

import itkdb
from bson.objectid import ObjectId

from ..common import (
    IF_DIR,
    THUMBNAIL_DIR,
    args,
    component_type_code_map,
    create_message,
    fs,
    localdb,
    mount_retry_adapter,
    pd_client,
    process_request,
    upload_chip_config,
    userdb,
)
from . import PDInterface

logger = logging.getLogger("localdb")


def make_archive_threadsafe(zip_name: str, path: str):
    with zipfile.ZipFile(zip_name, "w", zipfile.ZIP_DEFLATED) as zfile:
        for root, _dirs, files in os.walk(path):
            for file in files:
                zfile.write(
                    os.path.join(root, file),
                    os.path.relpath(os.path.join(root, file), path),
                )


def _stagesToProcess(topComponent):
    qcStatus = localdb.QC.module.status.find_one(
        {"component": str(topComponent["_id"])}
    )

    stageInfo = userdb.QC.stages.find_one(
        {"code": component_type_code_map[topComponent["componentType"]]}
    )

    # logger.info( pprint.pformat( topComponent ) )
    # logger.info( pprint.pformat( qcStatus ) )

    currentStage = qcStatus["stage"]
    stages = stageInfo["stage_flow"]

    assert (
        currentStage in stages
    ), f"Current stage {currentStage} is not in the list of stages I am aware of: {stages}."

    currentStageIndex = stages.index(currentStage)

    stagesToProcess = []

    for stage in stages[:currentStageIndex]:
        if qcStatus["upload_status"].get(stage) in ["1", "alternative"]:
            continue

        stagesToProcess += [stage]

    logger.info(str(stagesToProcess))

    return stagesToProcess, currentStage


class RecursiveUploader(PDInterface.PDInterface):
    def __submitTest(self, serialNumber, stage, test):
        # logger.info( pprint.pformat( test ) )

        component = localdb.component.find_one({"serialNumber": serialNumber})
        if component is None:
            logger.warning(
                f"component for serialNumber {serialNumber} not found on LocalDB!"
            )

        componentType = component_type_code_map[component["componentType"]]

        testFormat = userdb.QC.tests.find_one(
            {"code": test["testType"], "componentType.code": componentType}
        )

        if testFormat is None:
            msg = f"testFormat for {test['testType']} of {componentType} not found in LocalDB!"
            logger.warning(msg)

        # logger.info( pprint.pformat( testFormat ) )
        project = testFormat["componentType"]["project"]
        componentType = testFormat["componentType"]["code"]
        testTypeCode = testFormat["code"]

        try:
            cpt_doc = self.pd_client.get(
                "getComponent", json={"component": component["proID"]}
            )
        except itkdb.exceptions.BadRequest as exc:
            if "Component does not exist." not in str(exc):
                raise

            msg = f"component doc for serialNumber {serialNumber} not found on ITkPD!"
            logger.exception(msg)
            create_message(
                f"ERROR: {msg}",
                function="RecursiveUploader.__submitTest()",
                code="LOOKUP_FAIL",
                component=serialNumber,
            )

            cpt_doc = None

        # try to get institution using the following priority:
        # 1) test['results']['Metadata']['Institution']
        # 2) test['results']['metadata']['Institution']
        # 3) cpt_doc['currentLocation']['code']
        # 4) cpt_doc['locations'][-1]['institution'] -> map to code via mongoDB query
        # 5) cpt_doc['institution']['code']

        institutionCode = test["results"].get("Metadata", {}).get(
            "Institution"
        ) or test["results"].get("metadata", {}).get("Institution")
        if institutionCode:
            msg = "test location successfully extracted from Metadata/metadata field"
            logger.info(msg)
        elif cpt_doc:
            institutionCode = cpt_doc.get("currentLocation", {}).get("code")

            if institutionCode:
                msg = "test location missing from metadata field, component currentLocation used."
                logger.warning(msg)
            else:
                lastInstitution = cpt_doc.get("locations", [{}])[-1].get("institution")
                if lastInstitution:
                    query = {"_id": ObjectId(lastInstitution)}
                    institution = localdb.institution.find_one(query)
                    institutionCode = institution.get("code")

            if not institutionCode:
                institutionCode = cpt_doc.get("institution").get("code")
        else:
            msg = "failed in getting institution code from cpt_doc. Substuting with the local test record as a fallback..."
            logger.warning(msg)
            institutionCode = localdb.institution.find_one(
                {"_id": ObjectId(test["address"])}
            ).get("code")

        out = self.pd_client.get(
            "generateTestTypeDtoSample",
            json={
                "project": project,
                "componentType": componentType,
                "code": testTypeCode,
            },
        )

        out.update(
            {
                "component": serialNumber,
                "institution": institutionCode,
                "date": test["sys"]["mts"].strftime("%Y-%m-%dT%H:%MZ"),
                "runNumber": str(test["_id"]),
            }
        )

        logger.debug(pprint.pformat(out))

        properties = {}
        for key in ["properties", "property", "Metadata", "metadata"]:
            properties.update(test["results"].pop(key, {}))

        out["properties"].update(properties)

        # in general, just copy the LocalDB results field...
        out.update({"results": test.get("results")})
        out.update({"passed": test.get("passed", False)})

        # ... but in case the result field is testRun, a special format is required.
        for field, value in out["results"].items():
            try:
                fieldDef = next(
                    f for f in testFormat["parameters"] if f["code"] == field
                )
            except Exception as e:
                logger.warning(str(e))
                logger.warning(traceback.format_exc())
                msg = f"ERROR: fieldDef for {field} was not found in the TestRun definition! ==> Contact the experts."
                raise RuntimeError(msg) from e

            if fieldDef is None:
                logger.info(pprint.pformat(testFormat["parameters"]))

            if fieldDef["dataType"] == "testRun":
                if value is None:
                    out["results"][field] = None
                    continue

                runNumber = value

                # acquire test in localdb
                test = localdb.QC.result.find_one({"_id": ObjectId(runNumber)})
                subComponentId = test["component"]

                subComponent = localdb.component.find_one(
                    {"_id": ObjectId(subComponentId)}
                )

                try:
                    testRunCandidates = self.pd_client.get(
                        "listTestRunsByComponent",
                        json={
                            "filterMap": {
                                "code": subComponent["proID"],
                                "runNumber": runNumber,
                                "state": "ready",
                            }
                        },
                    )
                except Exception:
                    msg = "Failed in fetching testRunCandidates from PDB"
                    logger.exception(
                        "failed in fetching testRunCandidates from PDB for {subComponent['serialNumber'] and runNumber={runNumber}."
                    )
                    create_message(
                        f"ERROR: {msg}",
                        function="RecursiveUploader.__submitTest()",
                        code="SUBMIT_FAIL",
                        component=subComponent["serialNumber"],
                    )

                    continue

                checkFlag = False

                for tr in reversed(list(testRunCandidates)):
                    for key in [
                        "stage",
                        "institution",
                        "cts",
                        "stateTs",
                        "stateUserIdentity",
                        "config_id",
                        "config_revision_current",
                        "config_revision_prev",
                    ]:
                        tr.pop(key, None)

                    # logger.info( pprint.pformat(tr) )
                    out["results"][field] = tr["id"]
                    checkFlag = True
                    break

                if not checkFlag:
                    logger.warning(
                        "the corresponding test record specified by runNumber not found on ProductionDB!"
                    )

                logger.info(f'succeeded in filling a TestRun link {fieldDef["code"]}')

        logger.info("submit a new TestRun...")

        # submit a new TestRun
        tr_new = self.pd_client.post("uploadTestRunResults", json=out)
        logger.info("done submission of a new TestRun")

        # attach config if config_id is present on test.
        if "config_id" in test and test.get("testType") == "TUNING":
            assert "config_revision" in test, "config_revision not in test record"
            assert "layer_option" in test, "layer_option not in test record"
            assert "component" in test, "component not in test record"

            # Clear up attachments under component for the case obsolete EOS attachments are found.
            cpt_doc_noeos = self.pd_client.get(
                "getComponent", json={"component": component["proID"]}
            )
            for att in cpt_doc_noeos.get("attachments"):
                data = {"component": component["proID"], "code": att["code"]}

                if "url" not in att:
                    continue

                if "/eos/atlas/test" in att["url"]:
                    logger.info(
                        f'Broken EOS attachment detected: {att["url"]} ==> Deleting'
                    )
                    try:
                        tr_new = pd_client.post("deleteComponentAttachment", json=data)
                        logger.info("deleted broken EOS attachment successfully")
                    except Exception:
                        msg = f"unable to delete broken EOS attachment code={att['code']} for {serialNumber}"
                        logger.exception(msg)
                        create_message(
                            f"ERROR: {msg}",
                            function="RecursiveUploader.__submitTest()",
                            code="DELETE_FAIL",
                            component=serialNumber,
                        )

                    continue

            upload_chip_config(
                component["proID"],
                test["config_id"],
                test["config_revision"],
                test["layer_option"],
                tr_new["testRun"]["id"],
                stage,
            )

        # logger.info( pprint.pformat(tr_new) )
        return tr_new

    def __clearPreviousTestRuns(self, serialNumber, stage, testType):
        # logger.info( pprint.pformat( test ) )

        component = localdb.component.find_one({"serialNumber": serialNumber})
        if component is None:
            logger.warning(
                f"component for serialNumber {serialNumber} not found on LocalDB!"
            )

        componentType = component_type_code_map[component["componentType"]]

        testFormat = userdb.QC.tests.find_one(
            {"code": testType, "componentType.code": componentType}
        )

        if testFormat is None:
            msg = f"testFormat for {testType} of {componentType} not found in LocalDB!"
            logger.warning(msg)

        # logger.info( pprint.pformat( testFormat ) )
        componentType = testFormat["componentType"]["code"]
        testTypeCode = testFormat["code"]

        try:
            self.pd_client.get("getComponent", json={"component": component["proID"]})
        except itkdb.exceptions.BadRequest as exc:
            if "Component does not exist." in str(exc):
                msg = (
                    f"component doc for serialNumber {serialNumber} not found on ITkPD!"
                )
                raise ValueError(msg) from exc

        logger.info("delete existing testRuns on PDB..")

        try:
            # delete existing testRuns on PDB
            pastTestRuns = self.pd_client.get(
                "listTestRunsByComponent",
                json={
                    "filterMap": {
                        "code": component["proID"],
                        "stage": [stage],
                        "testType": testTypeCode,
                        "state": "ready",
                    }
                },
            )
        except Exception as e:
            logger.warning(str(e))
            pastTestRuns = []

        for tr in pastTestRuns:
            try:
                # first delete testRun attachments
                tr_doc = self.pd_client.get(
                    "getTestRun", json={"testRun": tr["id"], "noEosToken": False}
                )
            except itkdb.exceptions.BadRequest as exc:
                if "Granting token from EoS failed" not in str(exc):
                    msg = f'deletion request of TestRun {tr["id"]} for component {serialNumber} stage {stage} testType {testTypeCode} failed. This is permissible if the TestRun is already requested to delete.'
                    logger.exception(msg)
                    create_message(
                        f"WARNING: {msg}",
                        function="RecursiveUploader.__clearPreviousTestRuns()",
                        code="GRANT_TOKEN_FAIL",
                        component=serialNumber,
                    )

                    continue

                logger.warning(
                    "Granting token from EoS failed ==> retry with noEosToken = True"
                )
                tr_doc = self.pd_client.get(
                    "getTestRun", json={"testRun": tr["id"], "noEosToken": True}
                )

            for att in tr_doc.get("attachments"):
                data = {"testRun": tr["id"], "code": att.get("code")}

                try:
                    self.pd_client.post("deleteTestRunAttachment", json=data)
                except Exception:
                    msg = f'on the way of removing testRun {tr["id"]}, failed in deleting attachment {att.get("code")} --> leaving as is'
                    logger.exception(msg)
                    create_message(
                        f"WARNING: {msg}",
                        function="RecursiveUploader.__clearPreviousTestRuns()",
                        code="DELETE_FAIL",
                        component=serialNumber,
                    )

            logger.info(
                f'requested deletion of TestRun {tr["id"]} for component {serialNumber} stage {stage} testType {testTypeCode}'
            )
            self.pd_client.post(
                "deleteTestRun",
                json={
                    "testRun": tr["id"],
                    "reason": "Reprocessing via localDB so obsoleting existing test runs first.",
                },
            )

        logger.info(f"done clearing TestRuns for {serialNumber} {stage} {testType}")

    def __attachBinaries(self, serialNumber, _stage, testRunId, test):
        gridfs_attachments = test.get("gridfs_attachments")

        if gridfs_attachments is None:
            return

        if len(gridfs_attachments) == 0:
            return

        zip_dir = f"{THUMBNAIL_DIR}/{testRunId}"
        os.makedirs(zip_dir, exist_ok=True)

        for oid in gridfs_attachments.values():
            cache_file = f"{THUMBNAIL_DIR}/{testRunId}/{oid!s}"

            if os.path.exists(cache_file):
                pass
            else:
                data = fs.get(oid).read()

                with open(cache_file, "wb") as f:
                    f.write(data)

        info = []
        for filename, oid in gridfs_attachments.items():
            data = {"testRun": testRunId, "title": filename, "data_id": str(oid)}
            info += [data]

        with open(
            f"{THUMBNAIL_DIR}/{testRunId}/attachments_info.json", "w", encoding="utf-8"
        ) as f:
            json.dump(info, f, indent=4)

        # Zipping the attachment pack
        zip_filename = f"{THUMBNAIL_DIR}/TestRun_{testRunId}_Attachment_Pack.zip"
        make_archive_threadsafe(zip_filename, zip_dir)

        logger.info(f"Created attachment pack zip: {zip_filename}")

        zip_data = {
            "title": f"TestRun_{testRunId}_Attachment_Pack.zip",
            "url": zip_filename,
            "type": "file",
            "testRun": testRunId,
            "description": zip_filename,
        }

        shutil.rmtree(zip_dir)

        with concurrent.futures.ThreadPoolExecutor() as executor:
            executor.submit(
                self.__attachBinaryWorker, zip_data, zip_filename, serialNumber
            )
            logger.info(
                f"requested to attach a binary file {zip_filename} to TestRun {testRunId} on Production DB."
            )

        os.remove(zip_filename)

    def __attachBinaryWorker(self, data, cache_file, serialNumber):
        tmp_client = itkdb.Client(user=self.pd_client.user, use_eos=True)
        mount_retry_adapter(tmp_client)

        with open(cache_file, "rb") as attachment_fpointer:
            files = {
                "data": itkdb.utils.get_file_components({"data": attachment_fpointer})
            }

            logger.info(
                f'will try to attach file {data.get("title")} to test run {data.get("testRun")}'
            )

            try:
                res = tmp_client.post("createTestRunAttachment", data=data, files=files)
            except Exception as exc:
                res = None
                if "necessary data rewind" in str(exc):
                    logger.warning("cleaning up garbage records")

                    tr_doc = self.pd_client.get(
                        "getTestRun",
                        json={
                            "testRun": data.get("testRun"),
                            "noEosToken": False,
                        },
                    )

                    for att in tr_doc.get("attachments"):
                        if data.get("title") != att.get("title"):
                            continue

                        rm_data = {
                            "testRun": data.get("testRun"),
                            "code": att.get("code"),
                        }

                        try:
                            self.pd_client.post("deleteTestRunAttachment", json=rm_data)
                        except Exception:
                            msg = f'failed in deleting attachment {att.get("code")} --> leaving as is'
                            logger.exception(msg)
                            create_message(
                                f"ERROR: {msg}",
                                function="RecursiveUploader.__attachBinaryWorker()",
                                code="DELETE_FAIL",
                                component=serialNumber,
                            )

                        break

            if res is None:
                try:
                    # try one more time
                    res = tmp_client.post(
                        "createTestRunAttachment", data=data, files=files
                    )
                except Exception:
                    msg = f'failed in attaching a file {data.get("title")}'
                    logger.exception(msg)
                    create_message(
                        f"ERROR: {msg}",
                        function="RecursiveUploader.__attachBinaryWorker()",
                        code="CREATE_FAIL",
                        component=serialNumber,
                    )

        return res

    def __attachRAW(self, serialNumber, stage, testRunId, raw):
        if raw is None:
            return

        raw["_id"] = str(raw["_id"])

        for r in raw["raw"]:
            r["component"] = str(r["component"])
            r["sys"]["cts"] = r["sys"]["cts"].isoformat()
            r["sys"]["mts"] = r["sys"]["mts"].isoformat()
            if ObjectId.is_valid(r["address"]):
                institution = localdb.institution.find_one(
                    {"_id": ObjectId(r["address"])}
                )
                if institution:
                    r["address"] = institution["code"]
                else:
                    create_message(
                        f"WARNING: RAW address ObjectId(\"{r['address']}\") does not correspond to an institution code for test run (id={testRunId}) RAW (id={raw['_id']})",
                        function="RecursiveUploader.__attachRAW()",
                        code="MISSING_INSTITUTION",
                        component=serialNumber,
                    )
            else:
                create_message(
                    f"WARNING: RAW address {r['address']} is not a valid address for test run (id={testRunId}) RAW (id={raw['_id']})",
                    function="RecursiveUploader.__attachRAW()",
                    code="INVALID_ADDRESS",
                    component=serialNumber,
                )

        stage_alt = stage.replace("/", "__")
        attachment_fname = f'{IF_DIR}/{serialNumber}_{stage_alt}_{raw["raw"][0]["testType"]}_{testRunId}_{raw["_id"]}.json'

        try:
            data = {
                "title": "RAW",
                "url": attachment_fname,
                "type": "file",
                "testRun": testRunId,
                "description": "RAW",
            }

            with io.TextIOWrapper(
                io.BytesIO(),
                encoding="utf-8",
                line_buffering=True,
            ) as attachment_fpointer:
                json.dump(raw, attachment_fpointer)
                attachment_fpointer.seek(0, 0)

                files = {
                    "data": itkdb.utils.get_file_components(
                        {"data": (attachment_fname, attachment_fpointer)}
                    )
                }

                self.pd_client.post("createTestRunAttachment", data=data, files=files)

            logger.info(
                f"managed to attach RAW record to TestRun {testRunId} on Production DB."
            )

        except Exception as e:
            logger.warning(
                f'attachment attempt failed for TestRun {testRunId} for component {serialNumber} stage {stage} testType {raw["raw"][0]["testType"]}.'
            )
            logger.warning(str(e))

        if raw["raw"][0]["testType"] == "VISUAL_INSPECTION":
            metadata = raw["raw"][0].get("results").get("metadata") or raw["raw"][
                0
            ].get("results").get("Metadata")

            for name in ["front_image", "back_image"]:
                if name in metadata:
                    image_id = metadata[name]
                    self.upload_vi_image(testRunId, name, image_id, serialNumber)

            front_defect_images = metadata.get("front_defect_images")
            back_defect_images = metadata.get("back_defect_images")

            if front_defect_images is not None:
                for tile, image_id in front_defect_images.items():
                    name = "front_tile" + tile

                    self.upload_vi_image(testRunId, name, image_id, serialNumber)

            if back_defect_images is not None:
                for tile, image_id in back_defect_images.items():
                    name = "back_tile" + tile

                    self.upload_vi_image(testRunId, name, image_id, serialNumber)

            # Defects and comments
            front_defects = metadata.get("front_defects")
            back_defects = metadata.get("back_defects")

            if front_defects:
                for tile, defects in front_defects.items():
                    self.create_vi_defects(testRunId, "front_tile" + tile, defects)

            if back_defects:
                for tile, defects in back_defects.items():
                    self.create_vi_defects(testRunId, "back_tile" + tile, defects)

            front_comments = metadata.get("front_comments")
            back_comments = metadata.get("back_comments")

            if front_comments:
                self.create_vi_comments(testRunId, front_comments, "front")

            if back_comments:
                self.create_vi_comments(testRunId, back_comments, "back")

    def upload_vi_image(self, testRunId, name, image_id, serialNumber):
        os.makedirs(IF_DIR, exist_ok=True)
        cache_file = f"{IF_DIR}/{image_id}"

        if not os.path.exists(cache_file):
            try:
                data = fs.get(ObjectId(image_id)).read()

                with open(cache_file, "wb") as f:
                    f.write(data)
            except Exception:
                msg = f"Failed in dumping attachment to {cache_file} for testRunId={testRunId}, name={name}, and image_id={image_id}. This record is not uploaded to ITkPD."
                logger.exception(msg)
                create_message(
                    f"ERROR: {msg}",
                    function="RecursiveUploader.upload_vi_image()",
                    code="UPLOAD_FAIL",
                    component=serialNumber,
                )

                return

        data = {
            "testRun": testRunId,
            "url": cache_file,
            "title": name,
            "description": "Visual Inspection Image",
            "type": "file",
        }

        res = self.__attachBinaryWorker(data, cache_file, serialNumber)
        if res is None:
            tr_doc = self.pd_client.get(
                "getTestRun", json={"testRun": testRunId, "noEosToken": False}
            )

            for att in tr_doc.get("attachments"):
                data = {"testRun": testRunId, "code": att.get("code")}
                with contextlib.suppress(Exception):
                    self.pd_client.post("deleteTestRunAttachment", json=data)

            with contextlib.suppress(Exception):
                self.pd_client.post(
                    "deleteTestRun",
                    json={
                        "testRun": testRunId,
                        "reason": "Uploading from localDB failed, so cleaning up.",
                    },
                )

            msg = f"failed to attach an image {name} (id: {image_id}) to TestRun {testRunId} on Production DB. Cleaning up by deleting the test run given that the uploading process failed."
            raise RuntimeError(msg)

        logger.info(
            f"managed to attach an image {name} (id: {image_id}) to TestRun {testRunId} on Production DB."
        )

        os.remove(cache_file)

    def create_vi_defects(self, testRunId, name, defects):
        defects_submit = []

        for d in defects:
            defects_submit.append({"name": name, "description": d})

        self.pd_client.post(
            "createTestRunDefect",
            json={"testRun": testRunId, "defects": defects_submit},
        )

    def create_vi_comments(self, testRunId, comments, side):
        comments_submit = []

        for tile, comment in comments.items():
            if comment != "":
                comments_submit.append(f"tile_comment::{side}_tile{tile} : {comment}")

        self.pd_client.post(
            "createTestRunComment",
            json={"testRun": testRunId, "comments": comments_submit},
        )

    def __processCore(self, stage, componentId, doSubmit, record_stage_history=True):
        # acquire the test rcord of the test
        component = localdb.component.find_one({"_id": ObjectId(componentId)})
        qcStatus = localdb.QC.module.status.find_one({"component": componentId})

        if stage not in qcStatus["QC_results"]:
            logger.warning(
                f'Stage {stage} is not present for this component (componentType: {component["componentType"]})'
            )
            return

        stageResults = qcStatus["QC_results"][stage]

        # initialize ProdDB link for the case of previous record being present
        for testType in stageResults:
            localdb.QC.module.status.update_one(
                {"component": componentId},
                {"$set": {f"QC_results_pdb.{stage}.{testType}": "-1"}},
            )

        # set the component stage on ProdDB to this stage
        try:
            serialNumber = component["name"]
            self.pd_client.post(
                "setComponentStage",
                json={
                    "component": component["proID"],
                    "stage": stage,
                    "history": record_stage_history,
                },
            )

        except Exception as e:
            logger.warning(f"{e}")

        if not doSubmit:
            return

        for testType, resultId in stageResults.items():
            logger.info(f"testType = {testType}, resultId = {resultId}")

            # clean up past records of the test
            self.__clearPreviousTestRuns(serialNumber, stage, testType)

            # acquire the test summary record
            try:
                test = localdb.QC.result.find_one({"_id": ObjectId(resultId)})
                if test is None:
                    continue
            except Exception:
                continue

            # acquire the raw record if present
            if "raw_id" in test:
                raw = (
                    localdb.QC.testRAW.find_one({"_id": test["raw_id"]})
                    if test["raw_id"] is not None
                    else None
                )
            else:
                raw = None

            # first, push the summary record to PD
            res = self.__submitTest(serialNumber, stage, test)

            if res is None:
                msg = f"Null response in __submitTest( {serialNumber}, {stage}, {pprint.pformat(test)} )"
                raise RuntimeError(msg)

            testRunId = res["testRun"]["id"]

            # next, attach the gridfs records to PD
            self.__attachBinaries(serialNumber, stage, testRunId, test)

            # next, attach the raw result to PD
            if raw:
                self.__attachRAW(serialNumber, stage, testRunId, raw)

            # next, write the receipt record to LocalDB
            tr_doc = self.pd_client.get("getTestRun", json={"testRun": testRunId})

            localdb.QC.result.update_one(
                {"_id": ObjectId(resultId)}, {"$set": {"prodDB_record": tr_doc}}
            )
            localdb.QC.module.status.update_one(
                {"component": componentId},
                {"$set": {f"QC_results_pdb.{stage}.{testType}": testRunId}},
            )

        # update the upload status on LocalDB
        localdb.QC.module.status.update_one(
            {"component": componentId}, {"$set": {f"upload_status.{stage}": "1"}}
        )

    def __recursiveProcess(self, stage, componentId, doSubmit, record_stage_history):
        parent = localdb.component.find_one({"_id": ObjectId(componentId)})

        # first, process sub-components
        cprs = list(localdb.childParentRelation.find({"parent": componentId}))

        with concurrent.futures.ThreadPoolExecutor() as executor:
            futures = {}
            for cpr in cprs:
                child = localdb.component.find_one({"_id": ObjectId(cpr["child"])})

                if (
                    parent["componentType"] == "module"
                    and child["componentType"] == "front-end_chip"
                ):
                    continue

                futures[
                    executor.submit(
                        self.__recursiveProcess,
                        stage,
                        str(child["_id"]),
                        doSubmit,
                        record_stage_history,
                    )
                ] = {"id": str(child["_id"]), "serialNumber": child["serialNumber"]}

            for future in concurrent.futures.as_completed(futures):
                child = futures[future]
                try:
                    future.result()
                except Exception:
                    msg = f"There was an error in processing the upload of child {child['serialNumber']} (id=={child['id']}) at {stage} for parent {parent['serialNumber']} (id={componentId})."
                    logger.exception(msg)
                    create_message(
                        f"ERROR: {msg}",
                        function="RecursiveUploader.__recursiveProcess()",
                        code="PROCESS_FAIL",
                        component=parent["serialNumber"],
                    )
                    raise

            # process main part
            if parent.get("name"):
                self.__processCore(stage, componentId, doSubmit, record_stage_history)
                msg = f"processed {parent['name']} ({parent['componentType']})"
                logger.info(msg)

    def __recursiveComponentPropertiesUpdate(self, componentId):
        parent = localdb.component.find_one({"_id": ObjectId(componentId)})

        if not parent.get("name"):
            logger.warning("no serial number defined, skipping")
            return

        # first, process sub-components
        cprs = list(localdb.childParentRelation.find({"parent": componentId}))

        # loop over children
        for cpr in cprs:
            child = localdb.component.find_one({"_id": ObjectId(cpr["child"])})

            if (
                parent["componentType"] == "module"
                and child["componentType"] == "front-end_chip"
            ):
                continue

            self.__recursiveComponentPropertiesUpdate(str(child["_id"]))

        ctype_mapping = {"front-end_chip": "fe_chip", "module_pcb": "pcb"}
        ctype_code = (
            ctype_mapping.get(parent["componentType"]) or parent["componentType"]
        )
        # only set properties that do not impact serial number
        ctype = userdb.componentType.find_one({"code": ctype_code.upper()})
        ctype_props = {}
        if ctype:
            ctype_props = {prop["code"]: prop for prop in ctype["properties"]}

        # process main part
        for prop in parent["properties"]:
            if prop["value"] is None or prop["value"] == "" or prop["value"] == "N/A":
                continue

            # skip properties that have snPosition defined
            if ctype_props.get(prop["code"], {}).get("snPosition") is not None:
                continue

            try:
                self.pd_client.post(
                    "setComponentProperty",
                    json={
                        "component": parent["proID"],
                        "code": prop["code"],
                        "value": prop["value"],
                    },
                )
            except Exception:
                msg = f'Property {prop["code"]}={prop["value"]} was not able to be updated. One such way this can happen is if this property was deleted from the component type definition in PDB.'
                logger.exception(msg)
                create_message(
                    f"ERROR: {msg}",
                    function="RecursiveUploader.__recursiveComponentPropertiesUpdate()",
                    code="UPDATE_FAIL",
                    component=parent["serialNumber"],
                )

        # comments processing
        lComments = localdb.comments.find({"component": componentId})

        componentDoc = self.pd_client.get(
            "getComponent", json={"component": parent["proID"]}
        )
        pComments = componentDoc["comments"]

        newComments = []

        for lComment in lComments:
            recorded = False

            if pComments is not None:
                for pComment in pComments:
                    if pComment["comment"] == lComment["comment"]:
                        recorded = True

            if recorded:
                continue

            newComments.append(lComment["comment"])

        self.pd_client.post(
            "createComponentComment",
            json={"component": parent["proID"], "comments": newComments},
        )

        msg = f"processed {parent['name']} ({parent['componentType']})"
        logger.info(msg)

    # public

    def exec(self, serialNumber):
        # Activate processing indication
        if not os.path.exists(IF_DIR):
            os.makedirs(IF_DIR)

        # Get component info
        topComponent = localdb.component.find_one({"name": serialNumber})

        # First, update components properties
        self.__recursiveComponentPropertiesUpdate(str(topComponent["_id"]))

        stages, currentStage = _stagesToProcess(topComponent)

        for stage in stages:
            logger.info(
                f'processing to submit component "{serialNumber}" / stage "{stage}"'
            )

            doSubmit = stage != currentStage

            # record stage history
            record_stage_history = True

            self.__recursiveProcess(
                stage, str(topComponent["_id"]), doSubmit, record_stage_history
            )


if __name__ == "__main__":
    code1 = os.environ.get("ITKDB_ACCESS_CODE1", " ")
    code2 = os.environ.get("ITKDB_ACCESS_CODE2", " ")
    success, main_pd_client = process_request(code1, code2)
    if success == 0:
        sys.exit(1)
    else:
        RecursiveUploader(main_pd_client).exec(args.component_id)
