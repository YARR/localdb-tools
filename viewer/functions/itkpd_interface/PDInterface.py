import json
import logging

from bson.objectid import ObjectId

from ..common import VIEWER_DIR, localdb

logger = logging.getLogger("localdb")


class PDInterface:
    # constructor
    def __init__(self, pd_client):
        self.pd_client = pd_client

        self.ComponentTypeCodes = [
            "MODULE",
            "BARE_MODULE",
            "SENSOR_TILE",
            "PCB",
            "FE_CHIP",
            "OB_BARE_MODULE_CELL",
            "OB_LOADED_MODULE_CELL",
        ]

        with open(
            f"{VIEWER_DIR}/itkpd-interface/config/stage_test.json", encoding="utf-8"
        ) as f:
            self.stage_test_map = json.load(f)

        self.resetStage = None

    def getCompFromLocalDB(self, sn):
        query = {"serialNumber": sn, "proDB": True}
        return localdb.component.find_one(query)

    def getFEchipsDocs(self, msn):
        m_doc = self.getCompFromLocalDB(msn)
        cprs = localdb.childParentRelation.find({"parent": str(m_doc["_id"])})
        chip_ids = [chip["child"] for chip in cprs]
        docs = []
        for chip_id in chip_ids:
            doc = localdb.component.find_one(
                {"_id": ObjectId(chip_id), "componentType": "front-end_chip"}
            )
            if doc is not None:
                logger.info(f"{doc}")
                docs.append(doc)
        return docs

    def getQmsFromLocalDB(self, oid):
        query = {"component": oid}
        return localdb.QC.module.status.find_one(query)

    def getPmsFromLocalDB(self, oid):
        query = {"component": oid}
        return localdb.QC.prop.status.find_one(query)

    def getQrFromLocalDB(self, oid):
        query = {"_id": ObjectId(str(oid))}
        return localdb.QC.result.find_one(query)

    def getPrFromLocalDB(self, oid):
        query = {"_id": ObjectId(str(oid))}
        return localdb.QC.module.prop.find_one(query)

    def getTrFromLocalDB(self, oid):
        query = {"_id": ObjectId(str(oid))}
        return localdb.testRun.find_one(query)

    def getCtrFromLocalDB(self, tr_id):
        query = {"testRun": str(tr_id)}
        return localdb.componentTestRun.find(query)

    def getCprFromLocalDB(self, poid, coid):
        query = {"parent": poid, "child": coid}
        return localdb.childParentRelation.find_one(query)

    def getCompFromProdDB(self, component_id):
        return self.pd_client.get("getComponent", json={"component": component_id})

    def insertDocToLdb(self, col, doc):
        localdb[col].insert(doc)

    def writeBinaryData(self, path, data):
        with open(path, "wb") as f:
            f.write(data)
