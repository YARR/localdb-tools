from __future__ import annotations

import concurrent.futures
import contextlib
import json
import logging
import os
import pprint
import sys
import threading
import traceback
from datetime import datetime

import dateutil.parser
import dateutil.tz
import gridfs  # gridfs system
from bson.objectid import ObjectId

from ..common import (
    add_disabled_stage,
    args,
    create_message,
    dbv,
    developer_flag,
    download_itkpd_attachment,
    localdb,
    process_request,
    proddbv,
    userdb,
)
from . import PDInterface

logger = logging.getLogger("localdb")


class RecursiveComponentsSynchronizer(PDInterface.PDInterface):
    error_list = []

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.skip_synched = True
        self.skip_attachments = None
        self.stageFlows = {}
        self.component_list = []
        self.cpr_list = []
        self.cpt_sn = []

        self._executors = {}
        self.thread_name_prefix_global = ""

    def thread_name(self, suffix):
        return (
            f"{self.thread_name_prefix_global}/{suffix}"
            if self.thread_name_prefix_global
            else suffix
        )

    @property
    def thread_names(self):
        return list(map(self.thread_name, self._executors))

    def executor(self, suffix):
        return self._executors[suffix]

    def create_executor(self, suffix):
        if suffix in self._executors:
            msg = f"{suffix} was already created"
            raise ValueError(msg)
        self._executors[suffix] = concurrent.futures.ThreadPoolExecutor(
            thread_name_prefix=self.thread_name(suffix), max_workers=args.nthreads
        )

    def shutdown_executors(self, wait=False):
        for suffix in list(self._executors):
            executor = self._executors.pop(suffix)
            executor.shutdown(wait=wait)

    def threads(self):
        return [
            thread.name
            for thread in threading.enumerate()
            if any(thread_name in thread.name for thread_name in self.thread_names)
        ]

    def as_completed(self, futures):
        for future in concurrent.futures.as_completed(futures):
            context = futures[future]
            try:
                future.result()
            except Exception as exc:
                if context["action"] == "loopSubComponents":
                    msg = f"There was an error in looping over subcomponents, context={context}. {exc}"
                elif context["action"] == "processTestRun":
                    msg = f'There was an error in processing localDB componentId={context["ldb_componentId"]} for {context["testRun"]["testType"]["code"]} testRun={context["testRun"]["id"]}. {exc}'
                elif context["action"] == "processAttachment":
                    msg = f'There was an error in processing attachment type={context["attachment"].get("type")} for a test run type={context["ldb_tr_doc"]["testType"]} (ITkPD code: {context["pd_tr_doc"].get("id")}, LDB code: {context["ldb_tr_doc"].get("_id")}): {context["attachment"]}. {exc}'
                else:
                    msg = f"There was an unknown error in processing. We don't know what happened. Context: {context}. {exc}"

                self.error_list.append(msg)
                logger.error(msg)
                logger.error(traceback.format_exc())

    def __getInstitutionId(self, code):
        res = localdb.institution.find_one({"code": code})
        if res is None:
            return None
        return str(res["_id"])

    def __createComponentDoc(self, cpt_doc):
        address_id = self.__getInstitutionId(cpt_doc["institution"]["code"])
        componentType = cpt_doc["componentType"]["code"]
        chipType = ""
        if componentType == "MODULE":
            for prop in cpt_doc["properties"]:
                if prop["code"] == "FECHIP_VERSION":
                    chipType = prop["value"]
        else:
            chipType = cpt_doc["type"]["name"]

        # Overwrite the address by the PD component's current location
        currentLocation = cpt_doc.get("currentLocation", {}).get("code")
        lastLocation = (cpt_doc.get("locations") or [{}])[-1].get("institution")

        currentLocation_id = self.__getInstitutionId(currentLocation)
        lastLocation_id = self.__getInstitutionId(lastLocation)
        address = currentLocation_id or lastLocation_id or address_id

        if not currentLocation:
            msg = f"component with SN \"{cpt_doc['serialNumber']}\" has no currentLocation in PDB, falling back to last shipment destination"
            logger.warning(msg)

        if not currentLocation and not lastLocation:
            msg2 = f"component with SN \"{cpt_doc['serialNumber']}\" has no last shipment destination in PDB, address will not be updated (stick to home institute: \"{address}\")"
            logger.warning(msg2)

        ctype_mapping = {"fe_chip": "front-end_chip", "pcb": "module_pcb"}

        return {
            "_id": ObjectId(cpt_doc["id"]),
            "name": cpt_doc["serialNumber"],
            "chipType": chipType,
            "serialNumber": cpt_doc["serialNumber"],
            "chipId": -1,
            "componentType": ctype_mapping.get(
                componentType.lower(), componentType.lower()
            ),  # NB: important as we use lowercase for filtering in /components
            "address": address,
            "children": -1,
            "sys": {"mts": datetime.utcnow(), "cts": datetime.utcnow(), "rev": 0},
            "dbVersion": dbv,
            "user_id": -1,
            "proID": cpt_doc["code"],
            "proDB": True,
            "properties": cpt_doc["properties"],
            "flags": [
                flag.get("code")
                for flag in (cpt_doc.get("flags", []) or [])
                if flag.get("state") != "deleted"
            ],
            "currentGrade": cpt_doc["currentGrade"],
        }

    def __createCprDoc(self, module_doc, child_doc):
        return {
            "sys": {"mts": datetime.utcnow(), "cts": datetime.utcnow(), "rev": 0},
            "dbVersion": dbv,
            "parent": str(module_doc["_id"]),
            "child": str(child_doc["_id"]),
            "chipId": child_doc["chipId"],
            "status": "active",
        }

    def __createStageDefinitionDoc(self, cpt_doc):
        stage_flow = []
        stage_vs_test = {}
        stage_map = {}
        test_item_map = {}
        alternatives = []

        for stage in cpt_doc["stages"]:
            if stage["code"].startswith("_") and not developer_flag:
                continue
            stage_map[stage["code"]] = stage["name"]
            test_items = []

            logger.debug(pprint.pformat(stage))

            if "testTypes" in stage:
                if stage["testTypes"] is None:
                    continue

                for testType in stage["testTypes"]:
                    logger.debug(pprint.pformat(testType))

                    try:
                        code = testType["testType"]["code"]
                        name = testType["testType"]["name"]
                    except Exception:
                        testID = testType["testType"]

                        test_doc_local = userdb.QC.tests.find_one({"id": testID})

                        if "code" in test_doc_local:
                            code = test_doc_local["code"]

                        if "name" in test_doc_local:
                            name = test_doc_local["name"]

                    # 1 TestRun per Test
                    test_item_map[code] = name
                    test_items.append(code)

            stage_flow.append(stage["code"])
            stage_vs_test[stage["code"]] = test_items

            if stage.get("alternative", False):
                alternatives.append(stage["code"])

        return {
            "code": cpt_doc["code"],
            "name": cpt_doc["name"],
            "sys": {"mts": datetime.utcnow(), "cts": datetime.utcnow(), "rev": 0},
            "dbVersion": dbv,
            "proddbVersion": proddbv,
            "stage_flow": stage_flow,
            "stage_test": stage_vs_test,
            "stages": stage_map,
            "alternatives": alternatives,
            "test_items": test_item_map,
        }

    def __createComponentQCStatusDoc(self, cpt_doc):
        # Below is the skeleton
        doc = {
            "sys": {"mts": datetime.utcnow(), "cts": datetime.utcnow(), "rev": 0},
            "dbVersion": dbv,
            "proddbVersion": proddbv,
            "componentType": cpt_doc["componentType"]["code"],
            "component": cpt_doc["id"],
            "stage": cpt_doc["currentStage"]["code"],
            "status": "created",
            "rework_stage": [],
            "QC_results": {},
            "QC_results_pdb": {},
        }

        if self.resetStage:
            doc.update({"stage": self.resetStage})

        # Checkout the QC stage-test flow from localdbtools.QC.stages
        qcStatus = userdb.QC.stages.find_one({"code": cpt_doc["componentType"]["code"]})
        if qcStatus is None:
            msg = f"localdbtools.QC.stages does not contain a QC status skeleton for {cpt_doc['componentType']['code']}"
            logger.warning(msg)
            raise RuntimeError(msg)

        # Fetch
        for stage in qcStatus["stage_flow"]:
            tests = qcStatus["stage_test"][stage]
            doc["QC_results"][stage] = {}
            doc["QC_results_pdb"][stage] = {}
            for test in tests:
                doc["QC_results"][stage][test] = "-1"
                doc["QC_results_pdb"][stage][test] = "-1"

        return doc

    def __downloadStages(self, componentTypeDoc):
        # create an example of a document of QC status
        if userdb.QC.stages.find_one({"code": componentTypeDoc["code"]}) is None:
            userdb.QC.stages.insert_one(
                self.__createStageDefinitionDoc(componentTypeDoc)
            )
            add_disabled_stage(componentTypeDoc["code"])
        else:
            userdb.QC.stages.update_one(
                {"code": componentTypeDoc["code"]},
                {"$set": self.__createStageDefinitionDoc(componentTypeDoc)},
            )

        msg = f"Downloaded info of stages of {componentTypeDoc['code']}"
        logger.info(msg)

        return 0

    def __checkTypeAndSerialNumber(self, d):
        # logger.info("Module document ID: " + d["code"])
        with contextlib.suppress(Exception):
            return d["serialNumber"] is not None and d["type"]["name"] is not None

        return False

    def __isFeChipsValid(self, module_doc):
        for child_info in module_doc["children"]:
            if child_info["componentType"]["code"] == "BARE_MODULE":
                if child_info["component"] is None:
                    return False
                child_doc = super().getCompFromProdDB(child_info["component"]["code"])
                for child_child_info in child_doc["children"]:
                    if child_child_info["componentType"]["code"] == "FE_CHIP":
                        if child_child_info["component"] is None:
                            return False
                        if child_child_info["component"]["dummy"]:
                            return False

        return True

    def __getUserName(self, user_info):
        if user_info["middleName"]:
            return (
                user_info["firstName"] + user_info["middleName"] + user_info["lastName"]
            )
        return user_info["firstName"] + user_info["lastName"]

    def __syncTestRun(self, componentId, _tr_runNumber, pd_tr_doc, testTypeLocal):
        # tr_runNumber: LocalDB ObjectId string of the TestRun
        # pd_tr_doc: ProdDB TestRun document
        logger.info(
            f"Syncing test run (runNumber={pd_tr_doc['runNumber']}, id={pd_tr_doc['id']}) for component (localDB id={componentId})"
        )

        tr_oid = (
            ObjectId(pd_tr_doc["runNumber"])
            if ObjectId.is_valid(pd_tr_doc["runNumber"])
            else ObjectId(pd_tr_doc["id"])
        )

        ldb_tr_doc = {
            "_id": tr_oid,
            "component": componentId,
            "user": self.__getUserName(pd_tr_doc["user"]),
            "address": str(
                localdb.institution.find_one(
                    {"code": pd_tr_doc["institution"]["code"]}
                )["_id"]
            ),
            "stage": pd_tr_doc["components"][0]["testedAtStage"]["code"],
            "sys": {
                "mts": dateutil.parser.parse(pd_tr_doc["sys"]["mts"]),
                "cts": dateutil.parser.parse(pd_tr_doc["sys"]["cts"]),
                "rev": pd_tr_doc["sys"]["rev"],
            },
            "dbVersion": dbv,
            "testType": testTypeLocal,
            "results": {},
            "raw_id": None,
            "gridfs_attachments": {},
            "pdb_runNumber": pd_tr_doc["runNumber"],
        }

        if "passed" in pd_tr_doc:
            ldb_tr_doc["flags"] = {"passed": pd_tr_doc["passed"]}
            ldb_tr_doc["passed"] = pd_tr_doc["passed"]

        ldb_tr_doc["results"]["properties"] = {}
        for field in pd_tr_doc.get("properties") or []:
            ldb_tr_doc["results"]["properties"][field["code"]] = field["value"]

        for field in pd_tr_doc.get("results") or []:
            if field["dataType"] == "testRun":
                # This is a link to TestRun ==> Extract RunNumber and save it.

                if field["value"] not in ["", None]:
                    # search linked doc
                    linked_doc = None
                    with contextlib.suppress(Exception):
                        linked_doc = localdb.QC.result.find_one(
                            {"prodDB_record.id": str(field["value"]["id"])}
                        )

                    if linked_doc:
                        ldb_tr_doc["results"].update(
                            {field["code"]: str(linked_doc.get("_id"))}
                        )
                    else:
                        ldb_tr_doc["results"].update(
                            {field["code"]: field["value"]["runNumber"]}
                        )
                else:
                    ldb_tr_doc["results"].update({field["code"]: None})

            else:
                # Normal variable
                ldb_tr_doc["results"].update({field["code"]: field["value"]})

        if not pd_tr_doc["attachments"]:
            msg = f"No attachments for testRun (runNumber={pd_tr_doc['runNumber']}, id={pd_tr_doc['id']})"
            logger.info(msg)
        else:
            self.__processAttachments(pd_tr_doc, ldb_tr_doc)

        # upsert TestRun record to LocalDB
        localdb.QC.result.replace_one(
            {"_id": ldb_tr_doc["_id"]}, ldb_tr_doc, upsert=True
        )

    def __processAttachments(self, pd_tr_doc, ldb_tr_doc):
        futures = {}

        attachments = {att["title"]: att for att in pd_tr_doc["attachments"]}
        assert (
            len(attachments) == len(pd_tr_doc["attachments"])
        ), f"Some of your attachments have the same title (runNumber={pd_tr_doc['runNumber']}, id={pd_tr_doc['id']})."

        # complex analyses do not have RAW attachment
        # FECHIP_TEST does not have RAW attachment
        if ldb_tr_doc["testType"] not in [
            "MIN_HEALTH_TEST",
            "PIXEL_FAILURE_ANALYSIS",
            "TUNING",
            "FECHIP_TEST",
        ]:
            assert (
                "RAW" in attachments
            ), f"There is no RAW attachment for this test run (runNumber={pd_tr_doc['runNumber']}, id={pd_tr_doc['id']})."

            self.__processAttachmentRaw(attachments.pop("RAW"), pd_tr_doc, ldb_tr_doc)

        for attachment_title, attachment in attachments.items():
            logger.info(
                f"Submitting thread to process attachment `{attachment_title}` (type={attachment.get('type')}, code={attachment.get('code')}) for (runNumber={pd_tr_doc['runNumber']}, id={pd_tr_doc['id']})"
            )
            if attachment_title.endswith("Attachment_Pack.zip"):
                futures[
                    self.executor("processAttachment").submit(
                        self.__processAttachmentPack,
                        attachment,
                        pd_tr_doc,
                        ldb_tr_doc,
                    )
                ] = {
                    "action": "processAttachment",
                    "attachment": attachment,
                    "pd_tr_doc": pd_tr_doc,
                    "ldb_tr_doc": ldb_tr_doc,
                }
            else:
                # not an attachment pack -- compatibility for old style < v2.2.5
                # other attachments should go to gridfs, except visual inspection.
                # for visual inspection, book-keeping is made in raw (which is
                # why RAW is processed first).
                futures[
                    self.executor("processAttachment").submit(
                        self.__processAttachment,
                        attachment,
                        pd_tr_doc,
                        ldb_tr_doc,
                    )
                ] = {
                    "action": "processAttachment",
                    "attachment": attachment,
                    "pd_tr_doc": pd_tr_doc,
                    "ldb_tr_doc": ldb_tr_doc,
                }

        self.as_completed(futures)

        fs = gridfs.GridFS(localdb)

        # delete LocalDB gridfs chunk of the identical run number
        for doc in localdb.QC.result.find({"_id": ldb_tr_doc["_id"]}):
            attachments = doc.get("gridfs_attachments") or {}

            for value in attachments.values():
                if fs.exists(value):
                    fs.delete(value)

        # attach the prodDB testRun record
        ldb_tr_doc["prodDB_record"] = pd_tr_doc

        return

    def __processAttachmentRaw(self, attachment, pd_tr_doc, ldb_tr_doc):
        logger.debug("Attachment RAW mode: " + pprint.pformat(attachment))
        logger.info(
            f"Processing RAW attachment (type={attachment.get('type')}, code={attachment.get('code')}) for (runNumber={pd_tr_doc['runNumber']}, id={pd_tr_doc['id']})"
        )

        doc, error_list = download_itkpd_attachment(
            self.pd_client, "testRun", pd_tr_doc["id"], attachment
        )
        self.error_list.extend(error_list)

        if doc is None:
            return

        raw = json.loads(doc.fptr.read())
        raw["_id"] = ObjectId(raw["_id"])

        for r in raw["raw"]:
            r["component"] = ObjectId(r["component"])
            r["sys"]["mts"] = dateutil.parser.parse(r["sys"]["mts"])
            r["sys"]["cts"] = dateutil.parser.parse(r["sys"]["cts"])
            institution = localdb.institution.find_one({"code": r["address"]})
            if institution:
                r["address"] = str(institution["_id"])
            else:
                create_message(
                    f"WARNING: RAW address {r['address']} is not a valid institution code for test run (runNumber={pd_tr_doc['runNumber']}, id={pd_tr_doc['id']}) RAW (id={raw['_id']})",
                    function="RecursiveUploader.__attachRAW()",
                    code="INVALID_ADDRESS",
                    component=ldb_tr_doc["component"],
                )

        # insert RAW record to LocalDB
        localdb.QC.testRAW.replace_one({"_id": raw["_id"]}, raw, upsert=True)

        msg = f"Upserted a new RAW record {raw['_id']} to LocalDB"
        logger.info(msg)

        # link this RAW record to the TestRun record
        ldb_tr_doc["raw_id"] = raw["_id"]

    def __processAttachmentPack(self, attachment, pd_tr_doc, ldb_tr_doc):
        logger.debug("Attachment pack mode: " + pprint.pformat(attachment))
        logger.info(
            f"Processing packed attachment (type={attachment.get('type')}, code={attachment.get('code')}) for (runNumber={pd_tr_doc['runNumber']}, id={pd_tr_doc['id']})"
        )

        doc, error_list = download_itkpd_attachment(
            self.pd_client, "testRun", pd_tr_doc["id"], attachment
        )
        self.error_list.extend(error_list)

        try:
            subattachments_info = doc.getinfo("attachments_info.json")
        except KeyError as exc:
            msg = f'attachment (id={attachment["id"]}, title={attachment.get("title")}, type={attachment.get("type")}) for test run (ITkPD code: {pd_tr_doc.get("id")}) is missing "attachments_info.json"'
            raise KeyError(msg) from exc

        subattachments_data = json.loads(doc.read(subattachments_info))

        for subattachment_data in subattachments_data:
            subattachment_title = subattachment_data.get("title")
            subattachment_id = subattachment_data.get("data_id")

            try:
                subattachment_info = doc.getinfo(subattachment_id)
            except KeyError:
                msg = f'file (id={subattachment_id}, title={subattachment_title}) is missing from attachment (id={attachment["id"]}, title={attachment.get("title")}, type={attachment.get("type")}) for test run (ITkPD code: {pd_tr_doc.get("id")})'
                logger.warning(msg)
                self.error_list.append(msg)
                continue

            fs = gridfs.GridFS(localdb)
            binary_id = fs.put(doc.read(subattachment_info))

            ldb_tr_doc["gridfs_attachments"][subattachment_title] = binary_id

            logger.info(
                f"deployed {subattachment_title} / {subattachment_id} to gridfs_attachments"
            )

    def __processAttachment(self, attachment, pd_tr_doc, ldb_tr_doc):
        logger.info(
            f"Processing raw attachment (type={attachment.get('type')}, code={attachment.get('code')}) for (runNumber={pd_tr_doc['runNumber']}, id={pd_tr_doc['id']})"
        )

        if self.skip_attachments:
            if (
                attachment.get("title").lower().find("zip") >= 0
                or ldb_tr_doc["testType"] == "VISUAL_INSPECTION"
            ):
                pass
            else:
                return

        try:
            ObjectId(attachment.get("title"))
            keyname = attachment.get("description")
        except Exception:
            keyname = attachment.get("title")

            if keyname is None:
                logger.warning(
                    "keyname is None for attachment " + pprint.pformat(attachment)
                )
                return

        doc, error_list = download_itkpd_attachment(
            self.pd_client, "testRun", pd_tr_doc["id"], attachment
        )
        self.error_list.extend(error_list)

        if not doc:
            msg = f'{attachment.get("title")}: the type={attachment.get("type")} attachment was not possible to acquire'
            logger.warning(msg)
            self.error_list.append(msg)
            return

        if doc.size == 0:
            msg = f'{attachment.get("title")}: zero-size type={attachment.get("type")} attachment was detected!'
            logger.warning(msg)
            self.error_list.append(msg)
            return

        fs = gridfs.GridFS(localdb)

        try:
            binary_id = fs.put(doc.fptr)

            if ldb_tr_doc["testType"] != "VISUAL_INSPECTION":
                ldb_tr_doc["gridfs_attachments"][keyname] = binary_id
            else:
                # logger.info( f'visual inspection attachment: keyname = {keyname}')

                raw = localdb.QC.testRAW.find_one(ldb_tr_doc["raw_id"])
                if raw is None:
                    return

                metadata = raw["raw"][0]["results"].get("metadata", {})
                metadata.update(raw["raw"][0]["results"].get("Metadata", {}))

                if keyname in ["front_image", "back_image"]:
                    metadata[keyname] = str(binary_id)
                    localdb.QC.testRAW.replace_one(
                        {"_id": raw["_id"]}, raw, upsert=True
                    )

                elif keyname.find("tile") > 0:
                    if keyname.find("front") == 0:
                        tile = keyname[len("front_tile") :]
                        metadata["front_defect_images"][tile] = str(binary_id)
                    elif keyname.find("back") == 0:
                        tile = keyname[len("back_tile") :]
                        metadata["back_defect_images"][tile] = str(binary_id)

                    localdb.QC.testRAW.replace_one(
                        {"_id": raw["_id"]}, raw, upsert=True
                    )

        except Exception:
            msg = f"failed to save attachment to localDB (db.QC.testRAW._id={ldb_tr_doc['raw_id']}, attachment.code={attachment['code']})"
            logger.exception(msg)
            self.error_list.append(msg)

    def __bookComponent(self, cpt_doc):
        logger.info(
            f"Registering {cpt_doc['componentType']['code']} component {cpt_doc['serialNumber']}"
        )

        # Component skeleton
        cpt_localdb_doc = self.__createComponentDoc(cpt_doc)
        self.component_list.append(cpt_localdb_doc)

        # QC status skeleton
        cpt_qc_doc = self.__createComponentQCStatusDoc(cpt_doc)

        stageFlow = self.stageFlows[cpt_doc["componentType"]["code"]]
        stage_history = {stage["code"] for stage in cpt_doc["stages"]}

        # set all stages up to the current PDB stage as uploaded (assume all previous were uploaded, including alternative)
        resetUpToStage = cpt_qc_doc["stage"]
        if self.resetStage and self.resetStage in stageFlow["all"]:
            resetUpToStageIndex = max(0, stageFlow["all"].index(self.resetStage) - 1)
            resetUpToStage = stageFlow["all"][resetUpToStageIndex]

        upload_status = {}
        for stage in stageFlow["all"]:
            # the resetUpToStage will typically represent the "next" stage, but
            # we also don't want to "reset" the last stage if module complete
            if (
                stageFlow["all"].index(stage) <= stageFlow["all"].index(resetUpToStage)
                and stage in stage_history
            ):
                status = "1"
            else:
                status = "-1" if stage in stageFlow["required"] else "alternative"

            upload_status[stage] = status

        cpt_qc_doc.setdefault("upload_status", upload_status)

        # Set the current stage to the next one (if not at the end)
        currStage = cpt_qc_doc["stage"]
        while True:
            nextStageIndex = min(
                len(stageFlow["all"]) - 1,
                stageFlow["all"].index(currStage) + 1,
            )
            currStage = stageFlow["all"][nextStageIndex]
            if currStage in stageFlow["required"]:
                break

        cpt_qc_doc["stage"] = currStage

        # Synchronize the QC status to ProdDB
        futures = self.__processTestRuns(cpt_doc, cpt_localdb_doc, cpt_qc_doc)

        return cpt_localdb_doc, futures

    def __processTestRuns(self, cpt_doc, cpt_localdb_doc, cpt_qc_doc):
        # Set the sign-off record of the module up to the current stage
        ldb_componentId = str(cpt_localdb_doc["_id"])

        # process test runs now
        for _ in range(3):
            try:
                pd_testRuns = list(
                    self.pd_client.get(
                        "listTestRunsByComponent",
                        json={
                            "filterMap": {"code": cpt_doc["code"], "state": ["ready"]}
                        },
                    )
                )
                break
            except Exception as e:
                logger.warning(str(e))

        logger.info(f"Found {len(pd_testRuns)} test runs to process")

        futures = {}
        # Loop over TestRuns
        for testRun in pd_testRuns:
            tr_runNumber = testRun.get("runNumber")

            if not ObjectId.is_valid(tr_runNumber):
                msg = f"the runNumber recorded on PDB TestRun ({tr_runNumber}) is not an ObjectId format. Perhaps the TestRun was created by other clients than LocalDB. Using the ID of TestRun instead"
                logger.warning(msg)
                tr_runNumber = testRun.get("id")

            stage = testRun["stage"]["code"]
            testType = testRun["testType"]["code"]

            cpt_qc_doc["QC_results"][stage][testType] = tr_runNumber
            cpt_qc_doc["QC_results_pdb"][stage][testType] = testRun.get("id")

            if stage in cpt_qc_doc["QC_results"]:
                logger.info(
                    f"Submitting thread to process testRun (id={testRun.get('id')}, runNumber={testRun.get('runNumber')}) for component (localDB id={ldb_componentId})"
                )
                futures[
                    self.executor("processTestRun").submit(
                        self.__processTestRun, testRun, ldb_componentId
                    )
                ] = {
                    "action": "processTestRun",
                    "testRun": testRun,
                    "ldb_componentId": ldb_componentId,
                }
            else:
                logger.warning(
                    f'This TestRun {testRun["id"]} is pointing to stage {stage}, which is not in the list of valid stages. Possibly the stage was deprecated.'
                )

        msg = f"Creating QC status doc for {cpt_doc['componentType']['code']} component {cpt_doc['serialNumber']}"
        logger.info(msg)
        logger.debug(pprint.pformat(cpt_qc_doc))

        # Replace old record
        localdb.QC.module.status.replace_one(
            {"component": cpt_doc["id"]}, cpt_qc_doc, upsert=True
        )

        msg = f"Created QC status doc for {cpt_doc['componentType']['code']} component {cpt_doc['serialNumber']}"
        logger.info(msg)

        return futures

    def __processTestRun(self, testRun, ldb_componentId):
        stage = testRun["stage"]["code"]
        testType = testRun["testType"]["code"]

        logger.info(
            f"Processing test run (stage={stage}, type={testType}, id={testRun['id']}) for component (localDB id={ldb_componentId})"
        )

        # Clean up obsolete EOS attachment first
        pd_tr_doc_noeos = self.pd_client.get(
            "getTestRun", json={"testRun": testRun["id"]}
        )

        for att in pd_tr_doc_noeos.get("attachments"):
            if "/eos/atlas/test" in att.get("url", ""):
                logger.warning(
                    f'Deleting broken EOS attachment: {pd_tr_doc_noeos.get("code")} {att.get("url")}'
                )
                try:
                    self.pd_client.post(
                        "deleteTestRunAttachment",
                        data={
                            "testRun": pd_tr_doc_noeos.get("id"),
                            "code": att.get("code"),
                        },
                    )
                except Exception as exc:
                    logger.exception("unable to delete broken eos attachment")
                    msg = f"{ type(exc) !s}: {pprint.pformat(str(exc))}"
                    self.error_list.append(msg)

        try:
            pd_tr_doc = self.pd_client.get(
                "getTestRun", json={"testRun": testRun["id"], "noEosToken": False}
            )
        except Exception:
            msg = f'not possible to perform "getTestRun" for test run ID {testRun["id"]} with "noEosToken:False". this may indicate that some broken EOS attachments are present in the test run, but they were not possible to delete by the authenticated user.'
            logger.exception(msg)
            return

        tr_runNumber = pd_tr_doc["runNumber"]

        if not ObjectId.is_valid(tr_runNumber):
            msg = f"the runNumber recorded on PDB TestRun ({tr_runNumber}) is not an ObjectId format. Perhaps the TestRun was created by other clients than LocalDB. Using the ID of TestRun instead."
            logger.warning(msg)
            tr_runNumber = pd_tr_doc["id"]

        localTestRunRecord = (
            localdb.QC.result.find_one({"_id": ObjectId(tr_runNumber)}) or {}
        )

        if (
            localTestRunRecord.get("prodDB_record", {}).get("id") == testRun.get("id")
            and self.skip_synched
        ):
            logger.info(
                f"Skipping test run record {tr_runNumber} as it is already synced"
            )
        else:
            self.__syncTestRun(ldb_componentId, tr_runNumber, pd_tr_doc, testType)
            msg = f"Added TestRun {stage}.{testType} / {testRun['id']}"
            logger.info(msg)

        try:
            localdb.QC.testRuns_pdb_ldb_map.insert_one(
                {"pdbId": testRun["id"], "ldbId": tr_runNumber}
            )

        except Exception as e:
            msg = f"{traceback.format_exc()}\n{e}"
            logger.warning(msg)
            raise e

    def __loopSubComponents(
        self, cpt_doc, cpt_localdb_doc, parent_module_localdb_doc=None, depth=0
    ) -> dict:
        if cpt_doc["children"] is None:
            return {}

        cpt_type = cpt_doc.get("componentType").get("code")
        if parent_module_localdb_doc is None and cpt_type == "MODULE":
            parent_module_localdb_doc = cpt_localdb_doc

        futures = {}
        for child_cpt_metadata in cpt_doc["children"]:
            # Check which child component type
            child_type = child_cpt_metadata["componentType"]["code"]

            # Skip carrier and children of OB Bare Module Cell
            if child_type in ["MODULE_CARRIER", "OB_COOLING_BLOACK", "OB_PG_TYLE"]:
                continue

            logger.debug(pprint.pformat(child_cpt_metadata))

            try:
                child_id = child_cpt_metadata["component"]["code"]
            except Exception:
                logger.warning("child component ID was not found: skipping")
                continue

            # Acquire the child component from ProdDB
            child_doc = self.getCompFromProdDB(child_id)

            child_cpt_sn = child_doc["serialNumber"]

            self.cpt_sn.append(child_cpt_sn)

            child_cpt_localdb_doc, component_futures = self.__bookComponent(child_doc)
            futures.update(component_futures)

            # Create a new child-parent relation

            # For FE chips, make an additional direct link between the chip and the parent module
            if child_type == "FE_CHIP" and parent_module_localdb_doc is not None:
                try:
                    cpr_info = self.__createCprDoc(
                        parent_module_localdb_doc, child_cpt_localdb_doc
                    )

                    logger.info(
                        f"checking to see if parent={parent_module_localdb_doc.get('serialNumber')} and child={child_cpt_sn} are currently related (parent={parent_module_localdb_doc['_id']}, child[FE_CHIP]={child_cpt_localdb_doc['_id']})."
                    )

                    if super().getCprFromLocalDB(
                        str(parent_module_localdb_doc["_id"]),
                        str(child_cpt_localdb_doc["_id"]),
                    ):
                        localdb.childParentRelation.delete_many(
                            {
                                "parent": str(parent_module_localdb_doc["_id"]),
                                "child": str(child_cpt_localdb_doc["_id"]),
                            }
                        )
                        msg = f"{child_type} {child_cpt_sn} cpr relation was reset for parent={parent_module_localdb_doc.get('serialNumber')}."
                        logger.info(msg)

                    self.cpr_list.append(cpr_info)
                    msg = f"{child_type} {child_cpt_sn} cpr relation was created for parent={parent_module_localdb_doc.get('serialNumber')} (parent={cpr_info['parent']}, child={cpr_info['child']})."
                    logger.info(msg)

                except Exception as e:
                    logger.warning(str(e))

            # The following generic linking applies to all sub-components
            cpr_info = self.__createCprDoc(cpt_localdb_doc, child_cpt_localdb_doc)

            logger.info(
                f"checking to see if parent={cpt_localdb_doc['serialNumber']} and child={child_cpt_sn} are currently related (parent={cpt_localdb_doc['_id']}, child[{child_type}]={child_cpt_localdb_doc['_id']})."
            )

            if super().getCprFromLocalDB(
                str(cpt_localdb_doc["_id"]), str(child_cpt_localdb_doc["_id"])
            ):
                localdb.childParentRelation.delete_many(
                    {
                        "parent": str(cpt_localdb_doc["_id"]),
                        "child": str(child_cpt_localdb_doc["_id"]),
                    }
                )
                msg = f"{child_type} {child_cpt_sn} cpr relation was reset for parent={cpt_localdb_doc.get('serialNumber')}."
                logger.info(msg)

            self.cpr_list.append(cpr_info)
            msg = f"{child_type} {child_cpt_sn} cpr relation was created for parent={cpt_localdb_doc.get('serialNumber')} (parent={cpr_info['parent']}, child={cpr_info['child']})."
            logger.info(msg)

            # Recursively apply subcomponents loop
            futures[
                self.executor("loopSubComponents").submit(
                    self.__loopSubComponents,
                    child_doc,
                    child_cpt_localdb_doc,
                    parent_module_localdb_doc,
                    depth=depth + 1,
                )
            ] = {
                "action": "loopSubComponents",
                "child_doc": child_doc,
                "child_cpt_localdb_doc": child_cpt_localdb_doc,
                "parent_module_localdb_doc": parent_module_localdb_doc,
                "depth": depth + 1,
            }

        if depth == 0:
            return futures

        # wait for recursive calls to finish (meaning inside spawned subthread)
        return self.as_completed(futures)

    def recursiveReplace(self, j, a, b, nest=0):
        # TODO: understand this function
        flag = False

        for _ in range(10):
            for key, value in j.items():
                if isinstance(value, dict):
                    self.recursiveReplace(value, a, b, nest + 1)

                new_key = key.replace(a, b)
                if new_key != key:
                    j[new_key] = value
                    del j[key]
                    msg = f"{key} ==> {new_key}"
                    logger.debug(msg)
                    flag = True
                    break

            if not flag:
                break

        return j

    def deployStagesAndTests(self):
        # ComponentTypeID is a map { name, id } defined in PDInterface
        for componentTypeCode in self.ComponentTypeCodes:
            msg = f"Fetching Stage / Test Structure for {componentTypeCode}"
            logger.info(msg)
            logger.debug("===============================")

            # Comment 2022-12-12
            # if the key contains dot, then the object is not possible to store to mongo. Temporarily escaping by @.
            cpt_type = self.pd_client.get(
                "getComponentTypeByCode",
                json={"project": "P", "code": componentTypeCode},
            )
            componentTypeDoc = self.recursiveReplace(cpt_type, ".", "@")

            userdb.componentType.delete_one({"id": componentTypeDoc["id"]})
            userdb.componentType.insert_one(componentTypeDoc)

            logger.debug("  Stages:")
            for stage in componentTypeDoc["stages"]:
                if stage["code"].startswith("_") and not developer_flag:
                    continue
                msg = f"   - {stage['code']}"
                logger.debug(msg)

            self.__downloadStages(componentTypeDoc)

            # listTestTypes dumps all tests defined for the queried componentType
            ttlist = self.pd_client.get(
                "listTestTypesByComponentTypes",
                json={"project": "P", "componentTypes": [componentTypeCode]},
            )

            logger.debug("  Tests:")

            # loop over each test
            for t in ttlist:
                if t["state"] != "closed":
                    msg = f"   - {t['code']} [{t['state']}]"
                    logger.debug(msg)

                # create an example of a document of QC status
                userdb.QC.tests.delete_one(
                    {"code": t["code"], "componentType.code": componentTypeCode}
                )
                userdb.QC.tests.insert_one(t)

        msg = f"Downloaded info of tests of {componentTypeDoc['code']}"
        logger.info(msg)

    # public
    def exec(
        self,
        component_id=None,
        skip_attachments=False,
        skip_synched=True,
    ):
        """
        This function triggers the (sub)threading execution as follows below.
        Anything with an '@' means that it is run threaded / non-blocking.
        Anything wrapped in '<...>' means it is going to spawn multiple times
        (e.g. in a loop).

        @exec()
        |--bookComponent
        | |--processTestRuns
        |   |--<@processTestRun>
        |     |--syncTestRun
        |       |--processAttachments
        |         |-<@processAttachment>
        |--loopSubComponents
          |--bookComponent
          | |--<@processTestRun>
          |   |--syncTestRun
          |     |--processAttachments
          |       |-<@processAttachment>
          |--<@loopSubComponents>
            |-- (recursive call)

        Then wait for all currently spawned threads to finish.  Note that
        certain spawned threads take some time to query some information from
        the database before spawning more (sub)threads. This is why we have a
        bit of a complicated structure here.

        Some extra notes:
        - bookComponent does not wait on <@processTestRun>, @exec() does.
        - top-level loopSubComponents does not wait on <@loopSubComponents>, @exec() does.
        - loopSubComponents waits on @processTestRun if it was recursively called.
        """
        self.error_list = []

        self.thread_name_prefix_global = component_id
        self.create_executor("loopSubComponents")
        self.create_executor("processTestRun")
        self.create_executor("processAttachment")

        self.resetStage = None
        self.skip_attachments = skip_attachments
        self.skip_synched = skip_synched

        self.deployStagesAndTests()

        ######################################
        # Task:
        # 1. list up components to download
        # 2. trace-down all sub-components tree
        # 3. register components and all parent-child relations

        ######################################
        ## Step-1
        ## list up components to download

        for cpt_code in self.ComponentTypeCodes:
            cpt_doc = self.pd_client.get(
                "getComponentTypeByCode", json={"project": "P", "code": cpt_code}
            )

            self.stageFlows[cpt_code] = {"all": [], "required": []}
            for stage in sorted(cpt_doc["stages"], key=lambda x: x["order"]):
                if stage["code"].startswith("_") and not developer_flag:
                    continue
                self.stageFlows[cpt_code]["all"].append(stage["code"])
                if stage.get("alternative", False):
                    continue
                self.stageFlows[cpt_code]["required"].append(stage["code"])

        ######################################
        ## Step-2
        ## Trace-down all sub-components tree

        this_cpt_sn = component_id

        # try looking up the PDB Identifier if we know it, don't rely on
        # serialNumber if we don't have to
        existing_component = localdb.component.find_one({"serialNumber": component_id})
        pdb_identifier = (
            existing_component["proID"] if existing_component else component_id
        )

        # Acquire the component doc from ProdDB
        try:
            this_cpt_doc = super().getCompFromProdDB(pdb_identifier)

            cpt_type = this_cpt_doc.get("componentType").get("code")
            cpt_stage = this_cpt_doc["currentStage"]["code"]

            # In case of MODULE, skip if chips are incomplete or blank
            if cpt_type == "MODULE" and not self.__isFeChipsValid(this_cpt_doc):
                logger.warning(
                    f"Detected blank or incomplete FE chips in {cpt_type} {this_cpt_sn}, Skipping to pull."
                )
                create_message(
                    f"WARNING: Detected blank or incomplete FE chips in {cpt_type} {this_cpt_sn}.<br />This component was not pulled to LocalDB",
                    function="RecursiveComponentSynchronizer.deployStagesAndTests()",
                    code="INCOMPLETE_FE_CHIPS_ON_BARE_MODULE",
                )
                # continue

            if (
                not self.resetStage
                and cpt_stage not in self.stageFlows[cpt_type]["all"]
            ):
                logger.warning(
                    f"The stage on PDB record is obsolete detected ({cpt_stage})"
                )
                if cpt_type == "MODULE":
                    self.resetStage = "MODULE/ASSEMBLY"
                elif cpt_type == "PCB":
                    self.resetStage = "PCB_RECEPTION"
                elif cpt_type == "BARE_MODULE":
                    self.resetStage = "BAREMODULERECEPTION"

                if self.resetStage:
                    logger.warning(f"==> Switching to stage {self.resetStage}")

            this_cpt_localdb_doc, component_futures = self.__bookComponent(this_cpt_doc)

        except Exception as e:
            logger.warning(str(e))
            logger.warning("error in acquiring component {this_cpt_sn}")
            raise

        # Loop over sub-components
        # Note: you may be tempted to submit this in the loopSubComponents
        # executor, but we need to have all children threads made first before
        # waiting below
        subcomponent_futures = self.__loopSubComponents(
            this_cpt_doc, this_cpt_localdb_doc
        )

        futures = {**component_futures, **subcomponent_futures}
        logger.info(f"Waiting for {len(futures)} futures to finish")
        self.as_completed(futures)
        self.shutdown_executors(wait=False)

        ######################################
        ## Step-3
        ## register components and all parent-child relations

        for component_item in self.component_list:
            localdb.component.delete_one(
                {"_id": ObjectId(component_item["_id"])},
            )

            try:
                localdb.component.insert_one(
                    component_item,
                )
                logger.info(f'inserted component {component_item["name"]}.')
                logger.debug(pprint.pformat(component_item))
            except Exception:
                logger.info(
                    "This module's serial number has been changed for some reason."
                )
                old_component_name = localdb.component.find_one(
                    {"_id": ObjectId(component_item["_id"])},
                )["name"]
                localdb.component.update_one(
                    {"_id": ObjectId(component_item["_id"])},
                    {
                        "$set": {
                            "name": component_item["name"],
                            "serialNumber": component_item["serialNumber"],
                            "proID": component_item["proID"],
                            "properties": component_item["properties"],
                        }
                    },
                )

                localdb.componentTestRun.update_many(
                    {"name": old_component_name},
                    {"$set": {"name": component_item["name"]}},
                )

                if component_item["componentType"] == "front-end_chip":
                    localdb.chip.update_one(
                        {"_id": old_component_name},
                        {"$set": {"name": component_item["name"]}},
                    )

        for cpr in self.cpr_list:
            msg = (
                f"Creating new relation (parent={cpr['parent']}, child={cpr['child']})"
            )
            logger.info(msg)

        if self.cpr_list:
            logger.info("Inserting child-parent relations:")
            localdb.childParentRelation.insert_many(self.cpr_list)

        if len(self.error_list) > 0:
            error_messages = (
                "<pre>"
                + "\n".join(
                    [
                        msg.replace("<", "&lt;").replace(">", "&gt;")
                        for msg in self.error_list
                    ]
                )
                + "</pre>"
            )

            create_message(
                f"ERROR: The following errors were detected during pull. {error_messages}",
                function="RecursiveComponentsSynchronizer",
                component=component_id,
                code="END_SYNC_ERRORS",
            )

        return self.cpt_sn


if __name__ == "__main__":
    code1 = os.environ.get("ITKDB_ACCESS_CODE1", " ")
    code2 = os.environ.get("ITKDB_ACCESS_CODE2", " ")
    success, main_pd_client = process_request(code1, code2)
    if success == 0:
        sys.exit(1)
    else:
        RecursiveComponentsSynchronizer(main_pd_client).exec(args.component_id)
