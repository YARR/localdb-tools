import logging
import pprint
from datetime import datetime

import itkdb
from bson.objectid import ObjectId

from ..common import client, dbv, mount_retry_adapter

logger = logging.getLogger("localdb")

##############
## functions


def query_moduletype(doc):
    localdbtools_db = client.localdbtools
    query = {"code": doc["code"]}
    if localdbtools_db["QC.module.types"].find_one(query) is None:
        moduletype_info = {
            "id": doc["id"],
            "code": doc["code"],
            "name": doc["name"],
            "sys": {"mts": datetime.utcnow(), "cts": datetime.utcnow(), "rev": 0},
            "dbVersion": dbv,
        }
        localdbtools_db["QC.module.types"].insert_one(moduletype_info)
    return localdbtools_db["QC.module.types"].find_one(query)


def create_moduletype(moduletype_doc, baretype_doc, pcbtype_doc):
    baretypes = [
        baretype_doc["types"][i]["code"] for i in range(len(baretype_doc["types"]))
    ]
    pcbtypes = [
        pcbtype_doc["types"][i]["code"] for i in range(len(pcbtype_doc["types"]))
    ]

    chipVersionDoc = None
    for prop in moduletype_doc["properties"]:
        if prop["code"] == "FECHIP_VERSION":
            chipVersionDoc = prop["codeTable"]

    doc = {
        "sys": {"mts": datetime.utcnow(), "cts": datetime.utcnow(), "rev": 0},
        "dbVersion": dbv,
        "_id": ObjectId(moduletype_doc["id"]),
        "code": moduletype_doc["code"],
        "name": moduletype_doc["name"],
        "project": moduletype_doc["project"],
        "subprojects": moduletype_doc["subprojects"],
        "FEchip": chipVersionDoc,
    }

    doc["types"] = [
        {
            "code": moduletype["code"],
            "name": moduletype["name"],
            "subprojects": moduletype["subprojects"],
            "snComponentIdentifier": moduletype["snComponentIdentifier"],
        }
        for moduletype in moduletype_doc["types"]
        if moduletype["code"] != "SINGLE_CHIP_MODULE"
    ]

    doc["children"] = {}
    for childtype, contents in moduletype_doc["children"].items():
        childtype_alt = childtype.replace(".", "")

        logger.info(f"composing childtype {childtype_alt}")

        doc["children"][childtype_alt] = {}

        for sub_childtype in contents:
            logger.info(f'  looping for child type {sub_childtype["code"]}')

            if sub_childtype["code"] == "BARE_MODULE":
                bare_type_index = baretypes.index(sub_childtype["type"]["code"])
                typedoc = baretype_doc["types"][bare_type_index]

                try:
                    doc["children"][childtype_alt]["BARE_MODULE"] = (
                        typedoc["subprojects"][0]["code"]
                        + typedoc["snComponentIdentifier"]
                    )
                except Exception as e:
                    logger.warning(str(e))
                    logger.warning(
                        f"create_moduletype(): typedoc {typedoc} has a formatting incompatibility -- skipping for now"
                    )

            elif sub_childtype["code"] == "PCB":
                pcb_type_index = pcbtypes.index(sub_childtype["type"]["code"])
                typedoc = pcbtype_doc["types"][pcb_type_index]
                try:
                    doc["children"][childtype_alt]["PCB"] = (
                        typedoc["subprojects"][0]["code"]
                        + typedoc["snComponentIdentifier"]
                    )
                except Exception as e:
                    logger.warning(str(e))
                    logger.warning(
                        f"create_moduletype(): typedoc {typedoc} has a formatting incompatibility -- skipping for now"
                    )

        logger.info(f"Done composing childtype {childtype_alt}")

    logger.info(pprint.pformat(doc["children"]))

    return doc


def download_ModuleType(code1, code2):
    u = itkdb.core.User(access_code1=code1, access_code2=code2)
    pd_client = itkdb.Client(user=u, use_eos=True)
    mount_retry_adapter(pd_client)

    localdbtools_db = client.localdbtools

    # download module type information from ITkPD
    logger.info("Start downloading module type info...")
    moduletype_doc = pd_client.get(
        "getComponentTypeByCode", json={"project": "P", "code": "MODULE"}
    )
    baretype_doc = pd_client.get(
        "getComponentTypeByCode", json={"project": "P", "code": "BARE_MODULE"}
    )
    pcbtype_doc = pd_client.get(
        "getComponentTypeByCode", json={"project": "P", "code": "PCB"}
    )

    try:
        doc = create_moduletype(moduletype_doc, baretype_doc, pcbtype_doc)
    except Exception as e:
        logger.warning(str(e))
        msg = "Exception in create_moduletype()"
        raise RuntimeError(msg) from e

    if (
        localdbtools_db["QC.module.types"].find_one(
            {"_id": ObjectId(moduletype_doc["id"])}
        )
        is None
    ):
        localdbtools_db["QC.module.types"].insert_one(doc)
    else:
        localdbtools_db.drop_collection("QC.module.types")
        localdbtools_db["QC.module.types"].insert_one(doc)
    query_moduletype(doc)
    logger.info("Finished downloading component types!!\n")
