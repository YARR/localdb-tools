import logging
import traceback
from datetime import datetime

from .common import (
    check_and_process,
    clear_message,
    create_message,
    download_configs,
    get_pd_client,
    update_message,
)
from .itkpd_interface.recursiveComponentsSynchronizer import (
    RecursiveComponentsSynchronizer,
)
from .itkpd_interface.recursiveUploader import (
    RecursiveUploader,
)

logger = logging.getLogger("localdb")


def traceback_html():
    return f"""<div style="margin-left: 2%; margin-top: 10px; margin-bottom: 10px;">
    <a class="btn btn-info" data-toggle="collapse" href="#errorlog" role="button" aria-expanded="false" aria-controls="errorlog">Expand Error Log</a>
</div>
<div class="collapse" id="errorlog">
  <div class="card card-body">
    <pre>{traceback.format_exc()}</pre>
  </div>
</div>"""


def download_worker(
    serialNumber,
    code1=None,
    code2=None,
    skip_attachments=False,
    skip_synched=True,
    institution=None,
):
    if (
        code1 is not None
        and code2 is not None
        and not check_and_process(code1, code2, institution)
    ):
        return
    pd_client = get_pd_client()

    logger.info(f"serialNumber = {serialNumber}")

    try:
        synchronizer = RecursiveComponentsSynchronizer(pd_client)
        synchronizer.exec(serialNumber, skip_attachments, skip_synched)

    except Exception as e:
        clear_message(
            {"function": "RecursiveComponentsSynchronizer", "component": serialNumber}
        )
        create_message(
            f"Recursive synchronization of the component {serialNumber} encountered an error:<br /><pre>{e}</pre><br />Contact developers telling the serial number you attempted to pull with the corresponding block of the log.<br />{traceback_html()}",
            function="RecursiveComponentsSynchronizer",
            component=serialNumber,
            code="ERROR_SYNC_COMPONENT",
        )
        logger.warning(traceback.format_exc())
        logger.error(f"{e}")

    logger.info(f"Threads left: {synchronizer.threads()}")
    logger.info("done")

    return


def upload_worker(serialNumber, pd_client):
    logger.info(f"serialNumber = {serialNumber}")

    timestamp = datetime.now()
    ticket_id = create_message(
        f"Recursive upload of the component {serialNumber} started on {timestamp}. This process will take several minutes to complete.",
        function="RecursiveUploader",
        component=serialNumber,
        code="START_UPLOAD_COMPONENT",
        can_dismiss=False,
    )

    try:
        # step 1: upload LocalDB contents to PDB
        RecursiveUploader(pd_client).exec(serialNumber)

        update_message(
            ticket_id, message="Recursive upload complete. Downloading configs now."
        )

        # step 2: sync FE configs
        download_configs(serialNumber, pd_client)

        update_message(
            ticket_id,
            message="Recursive upload complete. Configs downloaded.",
            can_dismiss=True,
        )

    except Exception as e:
        clear_message(
            {"function": "RecursiveUploader", "component": serialNumber}, forced=True
        )
        logger.error(f"{e}")
        logger.error(traceback.format_exc())
        create_message(
            f"Recursive uploading of the component {serialNumber} encountered an error:<br /><pre>{e}</pre><br />Contact developers telling the serial number you attempted to upload with the corresponding block of the log.<br />{traceback_html()}",
            function="RecursiveUploader",
            component=serialNumber,
            code="ERROR_UPLOAD_COMPONENT",
        )

    logger.info("done")
    return
