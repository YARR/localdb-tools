from datetime import datetime

from .common import createScanCache, dbv, localdb, userdb


def startup():
    userdb.viewer.query.delete_many({})
    config_doc = {
        "runId": "config",
        "timeStamp": datetime.utcnow(),
        "sys": {"cts": datetime.utcnow(), "mts": datetime.utcnow(), "rev": 0},
    }
    userdb.viewer.query.insert_one(config_doc)
    query = {"dbVersion": dbv}
    run_entries = localdb.testRun.find(query).sort([("startTime", -1)])
    for entry in run_entries:
        docs = createScanCache(entry)
        userdb.viewer.query.insert_one(docs)
