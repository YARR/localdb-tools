import matplotlib.pyplot as plt


def layout_ModuleQC(sensorIVresult, testname):  # testname = "Sensor_IV" or "SLDO_VI"
    fig = plt.figure()

    # set results

    if testname == "Sensor_IV":
        I_value = [
            sensorIVresult["results"][testname][time]["Current_mean"]
            for time in range(len(sensorIVresult["results"][testname]))
        ]
        V_value = [
            sensorIVresult["results"][testname][time]["Voltage"]
            for time in range(len(sensorIVresult["results"][testname]))
        ]
        I_error = [
            sensorIVresult["results"][testname][time]["Current_sigma"]
            for time in range(len(sensorIVresult["results"][testname]))
        ]
    if testname == "SLDO_VI":
        I_value = [
            sensorIVresult["results"][testname][time]["Current"]
            for time in range(len(sensorIVresult["results"][testname]))
        ]
        V_value = [
            sensorIVresult["results"][testname][time]["Voltage_mean"]
            for time in range(len(sensorIVresult["results"][testname]))
        ]
        V_error = [
            sensorIVresult["results"][testname][time]["Voltage_sigma"]
            for time in range(len(sensorIVresult["results"][testname]))
        ]

    I_unit = "Current [" + sensorIVresult["results"]["unit"]["Current"] + "]"
    V_unit = "Voltage [" + sensorIVresult["results"]["unit"]["Voltage"] + "]"

    fig = plt.figure()
    ax = fig.add_subplot(111)
    if testname == "Sensor_IV":
        ax.scatter(V_value, I_value, c="black")
        ax.errorbar(
            V_value, I_value, yerr=I_error, capsize=3, ecolor="black", c="black"
        )
        ax.set_xlabel(V_unit)
        ax.set_ylabel(I_unit)
    if testname == "SLDO_VI":
        ax.scatter(I_value, V_value, c="black")
        ax.errorbar(
            I_value, V_value, yerr=V_error, capsize=3, ecolor="black", c="black"
        )
        ax.set_xlabel(I_unit)
        ax.set_ylabel(V_unit)
    ax.set_title(sensorIVresult["testType"], pad=35)
    fig.subplots_adjust(bottom=0.15)
    fig.subplots_adjust(left=0.25)

    plt.tight_layout()

    return fig
