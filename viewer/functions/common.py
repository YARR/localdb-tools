import base64
import concurrent.futures
import contextlib
import copy
import io
import json
import logging
import logging.handlers
import os
import pprint
import pwd
import re
import shutil
import sys
import time
import traceback
import unicodedata
import uuid
from binascii import a2b_base64  # convert a block of base64 data back to binary

# use Flask scheme
from datetime import datetime, timezone
from pathlib import Path
from urllib.parse import urlencode

import arrow
import flask
import gridfs
import itkdb
import module_qc_analysis_tools
import module_qc_database_tools
import pytz
import requests
from bson.objectid import ObjectId  # handle bson format
from flask import (
    session,
)
from module_qc_database_tools.chip_config_api import ChipConfigAPI
from module_qc_database_tools.core import Module
from module_qc_database_tools.utils import (
    get_layer_from_serial_number,
)
from packaging.version import Version
from pdf2image import convert_from_path  # convert pdf to image
from pymongo import MongoClient, errors  # use mongodb scheme
from requests.adapters import HTTPAdapter
from rich.console import Console
from rich.logging import RichHandler
from tzlocal import get_localzone
from urllib3.util.retry import Retry

from .commandline import getArgs  # Pass command line arguments into app.py

args = getArgs()

#####################
### Set up logging
logging.getLogger().setLevel(
    logging.NOTSET
)  # Must set root logger to NOTSET (default: WARNING)
logger = logging.getLogger("localdb")  # Rely on the localdb logger everywhere
logger.setLevel(logging.NOTSET)
formatter_stream = logging.Formatter(
    "[%(name)s][%(funcName)s] %(message)s [%(threadName)s]", "%Y-%m-%d %H:%M:%S"
)

handler_stream = RichHandler(
    console=Console(width=160, stderr=True),
    rich_tracebacks=True,
    tracebacks_suppress=[flask],
)
handler_stream.setLevel(logging.INFO)
handler_stream.setFormatter(formatter_stream)
logger.addHandler(handler_stream)
logger.propagate = False

if args.logfile:
    # Make logfile path if it does not exist
    log_file_path = Path(args.logfile).resolve()
    log_file_path.parent.mkdir(parents=True, exist_ok=True)

    # 10 MiB file max (rotated)
    handler_file = logging.handlers.RotatingFileHandler(
        log_file_path, mode="a", maxBytes=10 * 1024 * 1024, backupCount=5
    )
    handler_file.setLevel(logging.DEBUG)
    formatter_file = logging.Formatter(
        "%(asctime)s %(levelname)-8s %(message)s [%(filename)s:%(funcName)s:%(lineno)d] [%(threadName)s]",
        "%Y-%m-%d %H:%M:%S",
    )
    handler_file.setFormatter(formatter_file)
    logger.addHandler(handler_file)

logging.getLogger("matplotlib").setLevel(logging.ERROR)
logging.getLogger("itkdb").setLevel(logging.ERROR)


#####################
### Database Settings
def readKey(i_path):
    with open(os.path.join(VIEWER_DIR, i_path), encoding="utf-8") as file_text:
        file_keys = file_text.read().split()
    return {"username": file_keys[0], "password": file_keys[1]}


def setDb():
    url = f"mongodb://{args.host}:{args.port}"
    auth_kwargs = {"authSource": args.authSource}

    ### check ssl
    db_tls = args.tls
    db_ssl = args.ssl
    if db_tls:
        db_ca_certs = args.tlsCAFile
        db_certfile = args.tlsCertificateKeyFile
    elif db_ssl:
        db_ca_certs = args.sslCAFile
        db_certfile = args.sslPEMKeyFile

    if args.KeyFile:
        keys = readKey(args.KeyFile)
        auth_kwargs["username"] = keys["username"]
        auth_kwargs["password"] = keys["password"]
    if args.username:
        auth_kwargs["username"] = args.username
    if args.password:
        auth_kwargs["password"] = args.password

    ### check tls
    params = {}
    if db_ssl or db_tls:
        params["ssl"] = "true"
        if db_ca_certs:
            params["ssl_ca_certs"] = db_ca_certs
        if db_certfile:
            params["ssl_certfile"] = db_certfile
        if args.matchHostname:
            params["ssl_match_hostname"] = "true"
        url += f"/?{urlencode(params)}"
        ### check auth mechanism
        db_auth = args.auth
        if db_auth == "x509":
            url += "&authMechanism=MONGODB-X509"
            auth_kwargs["authSource"] = "$external"

    mongo_client = MongoClient(
        url,
        serverSelectionTimeoutMS=10,
        **auth_kwargs,
    )

    try:
        mongo_client.admin.command("ping")
    except errors.ServerSelectionTimeoutError as exc:
        logger.exception(f"The connection of Local DB {url} is BAD.\n{exc}")
        sys.exit(1)
    except errors.OperationFailure as exc:
        logger.exception(
            f"Likely incorrect authentication or server is not available.\n{exc}"
        )
        sys.exit(1)

    return mongo_client


def getpwuid():
    euid = os.geteuid()
    try:
        return pwd.getpwuid(euid).pw_name
    except KeyError:
        # the uid can't be resolved by the system
        return str(euid)


def slugify(value, allow_unicode=False):
    """
    Taken from https://github.com/django/django/blob/master/django/utils/text.py.
        - modified to not lowercase the string
    Convert to ASCII if 'allow_unicode' is False. Convert spaces or repeated
    dashes to single dashes. Remove characters that aren't alphanumerics,
    underscores, or hyphens. Convert to lowercase. Also strip leading and
    trailing whitespace, dashes, and underscores.
    """
    value = str(value)
    if allow_unicode:
        value = unicodedata.normalize("NFKC", value)
    else:
        value = (
            unicodedata.normalize("NFKD", value)
            .encode("ascii", "ignore")
            .decode("ascii")
        )
    value = re.sub(r"[^\w\s-]", "", value)
    return re.sub(r"[-\s]+", "-", value).strip("-_")


def fix_naming(value1, value2):
    updateDoc = localdb.component.update_many(
        {"componentType": value1}, {"$set": {"componentType": value2}}
    )
    if updateDoc.modified_count > 0:
        msg = f"Modified {updateDoc.modified_count} components to rename '{value1}' to '{value2}'"
        logger.warning(msg)


def rename_current_stage():
    key1 = "currentStage"
    key2 = "stage"
    for coll in [localdb.QC.result, localdb.QC.module.status, localdb.QC.testRAW]:
        updateDoc = coll.update_many({}, {"$rename": {key1: key2}})
        if updateDoc.modified_count > 0:
            msg = f"Modified {updateDoc.modified_count} entries in {coll} to rename '{key1}' to '{key2}'"
            logger.warning(msg)


def drop_serialNumber_QC_results():
    updateDoc = localdb.QC.result.update_many({}, {"$unset": {"serialNumber": 1}})
    if updateDoc.modified_count > 0:
        msg = f"Modified {updateDoc.modified_count} entries in localdb.QC.result to drop serialNumber key"
        logger.warning(msg)


def migrate_institution(from_inst_id, to_inst_id):
    from_condition = (
        {"$in": [from_inst_id, ObjectId(from_inst_id)]}
        if ObjectId.is_valid(from_inst_id)
        else from_inst_id
    )
    # migrate by updating various documents to map the new id
    queries = [
        ({"address": from_condition}, {"$set": {"address": to_inst_id}}),
        ({"address": from_condition}, {"$set": {"address": to_inst_id}}),
        ({"raw.address": from_condition}, {"$set": {"raw.$[].address": to_inst_id}}),
        ({"address": from_condition}, {"$set": {"address": to_inst_id}}),
    ]

    for collection, queryset in zip(
        [localdb.QC.result, localdb.component, localdb.QC.testRAW, localdb.testRun],
        queries,
    ):
        updateDoc = collection.update_many(*queryset)
        if updateDoc.modified_count > 0:
            msg = f"Modified {updateDoc.modified_count} entries in {collection.full_name} to change {from_inst_id!r} to {to_inst_id!r}"
            logger.warning(msg)


def migrate_pd_institution_to_institution():
    for pd_institution in localdb.pd.institution.find():
        institution = localdb.institution.find_one({"code": pd_institution["code"]})
        if institution:
            from_inst_id = str(pd_institution["_id"])
            to_inst_id = str(institution["_id"])
            migrate_institution(from_inst_id, to_inst_id)
        else:
            localdb.institution.insert_one(pd_institution)


def fix_address_code_to_id():
    addresses = (
        localdb.QC.result.distinct("address")
        + localdb.component.distinct("address")
        + localdb.QC.testRAW.distinct("raw.address")
        + localdb.testRun.distinct("address")
    )
    for address in addresses:
        if not ObjectId.is_valid(address):
            if isinstance(address, dict):
                address_code = address.get("code", address)
            else:
                address_code = address
            res = localdb.institution.find_one({"code": address_code})
            if not res:
                logger.error(
                    f"Address code {address_code} for address {address} is not a valid institution code. Please tell someone."
                )
                continue
            address_id = str(res["_id"])
            migrate_institution(address, address_id)
        else:
            # address is valid, make sure it exists in db.pd.institution and not just db.institution
            if not localdb.institution.find_one({"_id": ObjectId(address)}):
                # does not exist, need to cure it
                institution = localdb.pd.institution.find_one(
                    {"_id": ObjectId(address)}
                )
                if not institution:
                    if isinstance(address, ObjectId):
                        migrate_institution(address, str(address))
                    logger.error(
                        f'Address ID ObjectId("{address}") is unknown. Please report this.'
                    )
                    continue

                institution_code = institution["code"].upper()
                res = localdb.institution.find_one({"code": institution_code})
                if not res:
                    logger.error(
                        f'Address ID ObjectId("{address}") corresponds with code {institution_code} but it is not in localdb.institution. Please contact experts'
                    )
                    continue

                address_id = str(res["_id"])
                migrate_institution(address, address_id)
            elif ObjectId.is_valid(address):
                migrate_institution(address, str(address))


###################
## global variables
if "set_variables" not in globals():
    localdb_url = f"://{args.fhost}:{args.fport}/localdb"

    dbv = 1.01
    proddbv = 1.02
    qcAnalysisVersion = "2.2.39-rc9"

    itkdb_version = itkdb.__version__
    mqat_version = module_qc_analysis_tools.__version__
    mqdbt_version = module_qc_database_tools.__version__

    itkdb_version_req = "0.6.12"
    mqdbt_version_req = "2.4.9"
    mqat_version_req = "2.3.0"

    if Version(itkdb_version) < Version(itkdb_version_req):
        logger.error(
            f"Server cannot launch: itkdb >= v{itkdb_version_req} is required!"
        )
        raise RuntimeError()

    if Version(mqat_version) < Version(mqat_version_req):
        logger.error(
            f"Server cannot launch: module-qc-analysis-tools >= v{mqat_version_req} is required!"
        )
        raise RuntimeError()

    if Version(mqdbt_version) < Version(mqdbt_version_req):
        logger.error(
            f"Server cannot launch: module-qc-database-tools >= v{mqdbt_version_req} is required!"
        )
        raise RuntimeError()

    logger.info("This LocalDB server runs with:")
    logger.info(f"  * itkdb {itkdb_version}")
    logger.info(f"  * module-qc-database-tools {mqdbt_version}")
    logger.info(f"  * module-qc-analysis-tools {mqat_version}")
    logger.info(f"  * nthreads {args.nthreads}")

    VIEWER_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

    STATIC_DIR = Path(VIEWER_DIR).joinpath("static")
    CACHE_DIR = STATIC_DIR.joinpath("cache")
    THUMBNAIL_DIR = CACHE_DIR.joinpath("thumbnail")

    TMP_DIR = "/var/tmp/localdb"
    JSON_DIR = f"{TMP_DIR}/json"
    IF_DIR = f"{TMP_DIR}/itkpd-interface"
    JSROOT_DIR = f"{TMP_DIR}/jsroot"
    AT_DIR = f"{TMP_DIR}/module-qc-analysis-tools"

    STATIC_DIR.mkdir(exist_ok=True)
    CACHE_DIR.mkdir(exist_ok=True)
    THUMBNAIL_DIR.mkdir(exist_ok=True)
    os.makedirs(TMP_DIR, exist_ok=True)
    os.makedirs(JSON_DIR, exist_ok=True)
    os.makedirs(IF_DIR, exist_ok=True)
    os.makedirs(AT_DIR, exist_ok=True)

    client = setDb()
    localdb = client[args.db]
    userdb = client[args.userdb]
    fs = gridfs.GridFS(localdb)

    pd_client = None

    # if os.path.isdir(TMP_DIR):
    #    shutil.rmtree(TMP_DIR)

    with open(
        f"{VIEWER_DIR}/json-lists/scan_datafile_list.json", encoding="utf-8"
    ) as f_scan_datafile_list:
        DATA_SELECTION_LIST = json.load(f_scan_datafile_list)

    EXTENSIONS = ["png", "jpeg", "jpg", "JPEG", "jpe", "jfif", "pjpeg", "pjp", "gif"]

    set_variables = True

    plotting_mutex_lock = False
    plotting_mutex_lock_timestamp = datetime.now()

    developer_flag = False

    # left: LDB, right: PDB code
    component_type_code_map = {
        "module": "MODULE",
        "module_pcb": "PCB",
        "bare_module": "BARE_MODULE",
        "sensor_tile": "SENSOR_TILE",
        "front-end_chip": "FE_CHIP",
        "ob_bare_module_cell": "OB_BARE_MODULE_CELL",
        "ob_loaded_module_cell": "OB_LOADED_MODULE_CELL",
    }


# Prefix
class PrefixMiddleware:
    def __init__(self, app, prefix=""):
        self.app = app
        self.prefix = prefix

    def __call__(self, environ, start_response):
        if environ["REQUEST_URI"].startswith(self.prefix):
            if environ["PATH_INFO"].startswith(self.prefix):
                environ["PATH_INFO"] = environ["PATH_INFO"][len(self.prefix) :]
            environ["SCRIPT_NAME"] = self.prefix
            return self.app(environ, start_response)
        start_response("404", [("Content-Type", "text/plain")])
        return [b"This url does not belong to the app."]


#################
### Functions ###
#################
def makeDir():
    if not os.path.isdir(TMP_DIR):
        os.mkdir(TMP_DIR)
    user_dir = f"{TMP_DIR}/{session['uuid']}"
    _DIRS = [THUMBNAIL_DIR, JSON_DIR, user_dir, JSROOT_DIR]
    for dir_ in _DIRS:
        if not os.path.isdir(dir_):
            os.makedirs(dir_)


def cleanDir(dir_name):
    if os.path.isdir(dir_name):
        shutil.rmtree(dir_name)
    os.makedirs(dir_name)


def cleanDirs(dir_name):
    if os.path.isdir(dir_name):
        shutil.rmtree(dir_name)
    os.makedirs(dir_name)


def bin2image(i_type, i_binary):
    if i_type in EXTENSIONS:
        data = f"data:image/{i_type};base64,{i_binary}"
    elif i_type == "pdf":
        with open(f"{TMP_DIR}/image.pdf", "wb") as pdf_file:
            bin_data = a2b_base64(i_binary)
            pdf_file.write(bin_data)
        path = f"{TMP_DIR}/image.pdf"
        image = convert_from_path(path)
        image[0].save(f"{TMP_DIR}/image.png", "png")
        with open(f"{TMP_DIR}/image.png", "rb") as png_file:
            binary = base64.b64encode(png_file.read()).decode()
        data = f"data:image/png;base64,{binary}"
    return data


def initPage():
    if "uuid" not in session:
        session["uuid"] = str(uuid.uuid4())
    if "timezone" not in session:
        # session['timezone'] = 'UTC'
        session["timezone"] = str(get_localzone())
    makeDir()


#############################################################################
# itkdb


def process_request(code1, code2):
    global pd_client

    success = 0
    try:
        user = itkdb.core.User(access_code1=code1, access_code2=code2)
        pd_client = itkdb.Client(user=user, use_eos=True)
        mount_retry_adapter(pd_client)

        if not pd_client.user.authenticate():
            logger.warning("process_request(): failure in itkdb authentication")
            return success, None

        logger.info(f"process_request(): itkdb authenticated as {pd_client.user.name}")

        success = 1

    except Exception:
        pd_client = None
        success = 0
        logger.warning("process_request(): failure in itkdb authentication")

    return success, pd_client


def get_pd_client():
    if pd_client and pd_client.user.is_authenticated():
        return pd_client

    return None


def check_and_process(code1, code2, institution):
    global pd_client

    if not get_pd_client():
        process_request(code1, code2)

    pd_client = get_pd_client()

    if not pd_client:
        logger.error("Failed in ITkPD authentication!")
        return False

    user = pd_client.get("getUser", json={"userIdentity": pd_client.user.identity})

    if institution is not None and not (
        any(
            element["name"] == institution
            for element in (user.get("institutions") or [])
        )
    ):
        logger.warning(
            "Originally authenticated in ITkPD with user from different institute than in localDB! Updating this now with latest entered user codes."
        )
        process_request(code1, code2)

        pd_client = get_pd_client()

        if not get_pd_client():
            logger.error("Failed in ITkPD authentication!")
            return False

        user = pd_client.get("getUser", json={"userIdentity": pd_client.user.identity})

        if not (
            any(
                element["name"] == institution
                for element in (user.get("institutions") or [])
            )
        ):
            logger.error(
                "Failed in ITkPD authentication! ITkPD user institute still different than localDB!"
            )
            return False

    return True


def mount_retry_adapter(session, total=5, backoff_factor=1):
    adapter = HTTPAdapter(
        pool_connections=10,
        pool_maxsize=args.nthreads,
        max_retries=Retry(
            total=total,
            backoff_factor=backoff_factor,  # {backoff factor} * (2 ** ({number of previous retries}))
        ),
    )
    session.mount("http://", adapter)
    session.mount("https://", adapter)


#############################################################################
# Scan


def createScanCache(entry):
    this_run = entry

    ### user
    query = {"_id": ObjectId(this_run["user_id"])}
    this_user = localdb.user.find_one(query)
    user_name = this_user["userName"]

    ### site
    query = {"_id": ObjectId(this_run["address"])}
    this_site = localdb.institution.find_one(query)
    site_name = this_site["institution"]

    ### put tags
    query = {"runId": str(entry["_id"])}
    testRun_tags = userdb.viewer.tag.docs.find(query)
    tags = []
    for testRun_tag in testRun_tags:
        tags.append(testRun_tag)

    ### set Temperature
    # setT = 0
    # if "setT" in this_run:

    #    query = {"_id": ObjectId(this_run["setT"])}
    #    # temporary solution
    #    setT = -15

    ### component
    query = {"testRun": str(entry["_id"])}
    ctr_entries = localdb.componentTestRun.find(query)
    components = []
    for this_ctr in ctr_entries:
        components.append(
            {
                "name": this_ctr.get("name", "NONAME"),
            }
        )

    query_targets = []
    for component in components:
        query_targets.append(component["name"])
    query_targets.append(this_run["testType"])
    query_targets.append(user_name)
    query_targets.append(site_name)
    query_targets.append(this_run["startTime"].strftime("%Y/%m/%d"))
    if "setT" in this_run:
        query_targets.append(this_run["setT"])
    for tag in tags:
        query_targets.append(tag["name"])
    # query_targets.append()

    return {
        "runId": str(entry["_id"]),
        "timeStamp": this_run["startTime"],
        "data": query_targets,
    }


def getScanSummary(run_id):
    query = {"_id": ObjectId(run_id)}
    this_run = localdb.testRun.find_one(query)

    ### user
    query = {"_id": ObjectId(this_run["user_id"])}
    this_user = localdb.user.find_one(query)
    user_name = this_user["userName"]

    ### site
    query = {"_id": ObjectId(this_run["address"])}
    this_site = localdb.institution.find_one(query)
    site_name = this_site["institution"]

    ### tags
    query = {}
    testRun_tag_candidates = userdb.viewer.tag.categories.find(query)
    tag_candidate = []
    for testRun_tag in testRun_tag_candidates:
        tag_candidate.append(testRun_tag)
    ### put tags
    query = {"runId": run_id}
    testRun_tags = userdb.viewer.tag.docs.find(query)
    tag = []
    for testRun_tag in testRun_tags:
        tag.append(testRun_tag)

    ### component
    query = {"testRun": run_id}
    ctr_entries = localdb.componentTestRun.find(query)
    components = []
    for this_ctr in ctr_entries:
        components.append(
            {
                "name": this_ctr.get("name", "NONAME"),
                "enabled": this_ctr.get("enable", 1) == 1,
                "_id": this_ctr["component"],
                "chip_id": this_ctr["chip"],
            }
        )

    run_data = {
        "_id": run_id,
        "datetime": get_localtime_str(this_run["startTime"]),
        "testType": this_run["testType"],
        "runNumber": this_run["runNumber"],
        "stage": this_run["stage"],
        "plots": this_run["plots"] != [],
        "components": components,
        "user": user_name,
        "site": site_name,
        "testRun_tag_candidate": tag_candidate,
        "testRun_tag": tag,
    }
    if "setT" in this_run:
        TempVal = {"setT": this_run["setT"]}  # setT added
        run_data.update(TempVal)

    return run_data


#############################################################################
# Serial Numbers


def delim_SN(sn):
    if not sn:
        return ""

    if sn.find("20U") == 0 and len(sn) == 14:
        return " ".join([sn[0:3], sn[3:5], sn[5:7], sn[7:9], sn[9:]])
    return sn


def SN_tokens(sn):
    return {
        "head": sn[0:3],
        "project": sn[3],
        "XX": sn[3:5],
        "YY": sn[5:7],
        "XXYY": sn[3:7],
        "1st": sn[7],
        "2nd": sn[8],
        "3rd": sn[9],
        "4th": sn[10],
        "number": sn[-7:],
        "hex": f"{int(sn[-7:]):#07x}",
    }


def SN_typeinfo(sn, verbose=0):
    if not sn:
        return "Invalid Serial Number"

    if len(sn) != 14:
        return "Invalid Serial Number"

    sn_tokens = SN_tokens(sn)

    if sn_tokens.get("head") != "20U":
        return "Invalid Serial Number"

    if sn_tokens.get("project") != "P":
        return "Wrong project SN"

    code_table = {
        "FE_CHIP": {"PGFC": "Undef", "PGXF": "Tutorial-FE"},
        "SENSOR_TILE": {
            "PIS6": "Half-100um-Sensor",
            "PGS6": "Half-100um-Sensor",
            "PGS7": "Half-150um-Sensor",
            "PIS8": "Full-100um-Sensor",
            "PGS8": "Full-100um-Sensor",
            "PGS9": "Full-150um-Sensor",
            "PISG": "Half-3D-25x100-Sensor",
            "PGSG": "Half-3D-25x100-Sensor",
            "PISH": "FUll-3D-25x100-Sensor",
            "PGSH": "FUll-3D-25x100-Sensor",
            "PISI": "Half-3D-50x50-Sensor",
            "PGSI": "Half-3D-50x50-Sensor",
            "PISJ": "FUll-3D-50x50-Sensor",
            "PGSJ": "FUll-3D-50x50-Sensor",
            "PIS0": "L0-3D-25x100-Sensor",
            "PIS1": "L0-3D-50x50-Sensor",
            "PIS2": "L1-Quad-Sensor",
            "PGS3": "Outer-Quad-Sensor",
        },
        "BARE_MODULE": {
            "PGB1": "Single-BareModule",
            "PGB2": "Double-BareModule",
            "PGB4": "Quad-BareModule",
            "PGBS": "Single-Digital-BareModule",
            "PGBQ": "Quad-Digital-BareModule",
            "PGBT": "Single-Dummy-BareModule",
            "PGBR": "Quad-Dummy-BareModule",
            "PGXB": "Tutorial-BareModule",
        },
        "PCB": {
            "PIPT": "L0-Stave-PCB",
            "PIP0": "L0-R0-PCB",
            "PIP5": "L0-R0.5-PCB",
            "PGPQ": "Quad-PCB",
            "PGPD": "Dual-PCB",
            "PGXP": "Tutorial-PCB",
        },
        "MODULE": {
            "PIMS": "L0-Stave-Module",
            "PIM0": "L0-R0-Module",
            "PIM5": "L0-R0.5-Module",
            "PIM1": "L1-Quad-Module",
            "PGM2": "Outer-Quad-Module",
            "PGR0": "Single-Chip-Module",
            "PGR2": "Dual-Chip-Module",
            "PIR6": "Digital-L0-Stave-Module",
            "PIR7": "Digital-L0-R0-Module",
            "PIR8": "Digital-L0-R0.5-Module",
            "PGR9": "Digital-Quad-Module",
            "PIRB": "Digital-L1-Quad-Module",
            "PIX3": "Dummy-L0-Stave-Module",
            "PIX4": "Dummy-L0-R0-Module",
            "PIX5": "Dummy-L0-R0.5-Module",
            "PGX1": "Dummy-Quad-Module",
            "PGXA": "Dummy-L1-Quad-Module",
            "PGXM": "Tutorial-Module",
        },
        "OB_LOADED_MODULE_CELL": {
            "PBLM": "Loaded-Module-Cell",
            "PBZS": "Dummy-Loaded-Module-Cell",
        },
        "OB_BARE_MODULE_CELL": {
            "PBBC": "Bare-Module-Cell",
            "PBZP": "Dummy-Bare-Module-Cell",
        },
    }

    cpt_type = None
    typeinfo = "Others"

    for _type, table in code_table.items():
        if sn_tokens.get("XXYY") in table:
            cpt_type = _type
            typeinfo = table.get(sn_tokens.get("XXYY"))

    if cpt_type == "FE_CHIP":
        batchMap = {"0": "RD53A", "1": "ITkPixV1", "2": "ITkPixV2"}

        typeinfo = "Undef"
        with contextlib.suppress(Exception):
            typeinfo = batchMap[sn_tokens.get("hex")[2]]

        wafer = f"-{sn_tokens.get('hex')[3:5]}-({int(sn_tokens.get('hex')[5], 16)},{int(sn_tokens.get('hex')[6], 16)})"

        typeinfo += wafer

    elif cpt_type in ["MODULE", "BARE_MODULE"]:
        fe_types = {
            "0": "RD53A",
            "1": "ITkPix_v1.0",
            "2": "ITkPix_v1.1",
            "3": "ITkPix_v2",
            "9": "No FEs",
        }

        if sn_tokens.get("1st") in fe_types:
            typeinfo = "/".join([typeinfo, fe_types.get(sn_tokens.get("1st"))])

    elif cpt_type == "PCB":
        pcb_types = {
            "0": "RD53A",
            "1": "Prototype",
            "2": "Outer Pre-prod.",
            "3": "Inner Pre-prod.",
            "4": "Outer Prod.",
            "5": "Inner Prod.",
            "9": "No FEs",
        }

        if sn_tokens.get("1st") in pcb_types:
            typeinfo = "/".join([typeinfo, pcb_types.get(sn_tokens.get("1st"))])

    if verbose > 0:
        if cpt_type == "BARE_MODULE":
            vendors = {
                "0": "Advacam",
                "1": "Leonardo",
                "2": "IZM",
                "3": "HPK",
                "4": "RAL",
                "5": "Glasgow",
                "6": "ITk institutes",
            }

            if sn_tokens.get("2nd") in vendors:
                typeinfo = "/".join([typeinfo, vendors.get(sn_tokens.get("2nd"))])

        elif cpt_type == "PCB":
            pcb_vendors = {
                "1": "EPEC-100um",
                "2": "NCAB-100um",
                "3": "ATLAFLEX-75um",
                "4": "SFCircuits-100um",
                "5": "PHOENIX-100um",
                "6": "Yamashita-Mat-75um",
            }

            if sn_tokens.get("2nd") in pcb_vendors:
                typeinfo = "/".join([typeinfo, pcb_vendors.get(sn_tokens.get("2nd"))])

    return typeinfo


#############################################################################
# Timestamp


def setTime(date, zone):
    # zone = session.get('timezone','UTC')
    # zone = session.get('timezone',str(get_localzone()))
    converted_time = date.replace(tzinfo=timezone.utc).astimezone(pytz.timezone(zone))
    return converted_time.strftime("%Y/%m/%d %H:%M:%S")


def setDatetime(date):
    zone = session.get("timezone", "UTC")
    return date.replace(tzinfo=timezone.utc).astimezone(pytz.timezone(zone))


def get_localtime_str(isoformat):
    gmt = pytz.timezone("GMT")
    date_gmt = gmt.localize(datetime.fromisoformat(str(isoformat)))
    tz_local = get_localzone()
    date_local = date_gmt.astimezone(tz_local)

    return date_local.strftime("%Y-%m-%d %H:%M:%S %Z(%z)")


def dt_tz_to_timestamp(dt, *, tzinfo=None):
    return arrow.get(dt, tzinfo=tzinfo).int_timestamp


#############################################################################
# Messaging


def create_message(message, function="", component=None, code="", can_dismiss=True):
    query = {
        "message": message,
        "function": function,
        "component": component,
        "code": code,
    }

    result = userdb.message.find_one(query)
    if not result:
        ret = userdb.message.insert_one(
            {**query, "timestamp": datetime.now(), "can_dismiss": can_dismiss}
        )
        return ret.inserted_id
    return result["_id"]


def message_exists(*, function="", component="", code=""):
    query = {}
    if function:
        query["function"] = function

    if component:
        query["component"] = component

    if code:
        query["code"] = code

    return userdb.message.count_documents(query, limit=1) != 0


def clear_message(query, forced=False):
    the_query = copy.deepcopy(query)
    the_query.update({"can_dismiss": {"$in": [None, True]}})
    if forced:
        the_query["can_dismiss"]["$in"].append(False)

    deleted_messages = list(userdb.message.find(the_query))
    result = userdb.message.delete_many(the_query)
    if result.deleted_count:
        msg = f"Cleared {result.deleted_count} messages for {query}:"
        logger.info(msg)

        for message in deleted_messages:
            logger.info(message)
    return result


def format_messages(query=None):
    queries = [
        {"code": "INFO_NEW_LOCALDB_VERSION"},
        {"code": "INFO_GITLAB_ANNOUNCEMENT"},
    ]
    if query:
        queries.append(query)

    return list(
        userdb.message.aggregate(
            [
                {"$match": {"$or": queries}},
                {
                    "$addFields": {
                        "sortKey": {
                            "$cond": {
                                "if": {"$eq": ["$code", "GITLAB_ANNOUNCEMENT"]},
                                "then": 0,
                                "else": 1,
                            }
                        }
                    }
                },
                {"$sort": {"sortKey": 1, "code": 1}},
                {"$project": {"sortKey": 0}},
            ]
        )
    )


def update_message(message_id, *, message=None, can_dismiss=None):
    if ObjectId.is_valid(message_id):
        kwargs = {}
        if message:
            kwargs["message"] = message

        if can_dismiss:
            kwargs["can_dismiss"] = can_dismiss

        userdb.message.update_one({"_id": ObjectId(message_id)}, {"$set": kwargs})
    else:
        msg = f"cannot update message as message_id={message_id} is invalid"
        raise ValueError(msg)


#############################################################################
# Chip config tools


def upload_chip_config(
    component_id,
    config_id,
    config_revision,
    layer_option,
    test_run_id=None,
    stage=None,
):
    pd_client_ok = get_pd_client()

    if not pd_client_ok:
        msg = "upload_chip_config(): pd_client is None"
        raise ValueError(msg)

    if test_run_id and not stage:
        msg = "upload_chip_config(): stage needs to be specified when test_run_id is provided"
        raise ValueError(msg)

    component = localdb.component.find_one({"proID": component_id})
    if not component:
        msg = (
            f"upload_chip_config(): could not find component with proID={component_id}."
        )
        raise ValueError(msg)

    chip_api = ChipConfigAPI(client)

    config_info = chip_api.get_info(config_id)
    chip_api.info(config_id)

    config_data = chip_api.get_config(config_id, config_revision, True)
    if not config_data:
        msg = f"upload_chip_config(): config_data is null! config_id = {config_id}, revision = {config_revision}"
        raise ValueError(msg)

    for var in ["_id", "current_revision_id"]:
        config_info[var] = str(config_info[var])

    hexSN = hex(int(component["serialNumber"][-7:]))
    branch = config_info.get("branch")

    config_fname = f"{hexSN}_{layer_option}_{branch}.json"

    # delete existing attachment on the Component
    cpt_doc = pd_client.get("getComponent", json={"component": component_id})

    attachments = cpt_doc.get("attachments")
    for att in attachments:
        if att.get("title") == config_fname:
            data = {"component": cpt_doc.get("id"), "code": att.get("code")}
            try:
                pd_client.post("deleteComponentAttachment", data=data)
            except Exception:
                logger.exception(
                    "pd_client deleteComponentAttachment failed, giving up."
                )

    config_info_ser = json.dumps(config_info, separators=(",", ":"))

    with io.TextIOWrapper(
        io.BytesIO(),
        encoding="utf-8",
        line_buffering=True,
    ) as config_fpointer:
        json.dump(config_data, config_fpointer)
        config_fpointer.seek(0, 0)

        files = {
            "data": itkdb.utils.get_file_components(
                {"data": (config_fname, config_fpointer)}
            )
        }

        config_data_component = {
            "component": cpt_doc["code"],
            "title": config_fname,
            "description": config_info_ser,
            "url": f"{config_revision}.json",
            "type": "file",
        }

        logger.info(config_data_component)
        try:
            pd_client.post(
                "createComponentAttachment", data=config_data_component, files=files
            )
        except Exception:
            msg = f"failed in attaching {config_fname} with revision {config_revision} to component {cpt_doc['code']}"
            logger.exception(msg)
            raise

        if test_run_id:
            config_data_testrun = {
                "testRun": test_run_id,
                "title": config_fname,
                "description": config_info_ser,
                "url": f"{config_revision}.json",
                "type": "file",
            }

            logger.info(config_data_testrun)
            try:
                pd_client.post(
                    "createTestRunAttachment", data=config_data_testrun, files=files
                )
            except Exception:
                msg = f"failed in attaching {config_fname} with revision {config_revision} to test run {test_run_id}"
                logger.exception(msg)


def download_itkpd_attachment(client, kind, identifier, attachment):
    doc = None
    error_list = []

    if attachment["type"] not in ["file", "eos"]:
        logger.warning(
            f'RAW attachment is not eos or file type, do not know how to download it: id={attachment["id"]}, type={attachment["type"]}'
        )
        return doc, error_list

    routes = {
        "component": "getComponentAttachment",
        "testRun": "getTestRunAttachment",
        "shipment": "getShipmentAttachment",
        "batch": "getBatchAttachment",
    }
    if kind not in routes:
        msg = f"{kind} is not one of {list(routes)}."
        raise ValueError(msg)

    attachment_type = attachment.get("type")

    try:
        if attachment_type == "eos":
            code = attachment.get("url")
            doc = client.get(attachment.get("url"))
        else:
            code = attachment["code"]
            doc = client.get(
                routes[kind],
                json={kind: identifier, "code": code},
            )

    except Exception as exc:
        logger.exception(
            f"failed in getting {attachment_type} attachment for {kind} (identifier: {identifier}, code: {code})"
        )

        if isinstance(exc, itkdb.exceptions.Forbidden):
            msg = "received 403 HTTP response in getting the attachment"
        elif isinstance(exc, itkdb.exceptions.NotFound):
            msg = "received 404 HTTP response in getting the attachment"
        else:
            msg = f"{ type(exc) !s}: {pprint.pformat(str(exc))}"

        error_list.append(msg)
        logger.error(msg)
        return None, error_list

    msg = f"succeeded in getting {attachment_type} attachment for {kind} (identifier: {identifier}, code: {code})"
    logger.info(msg)
    return doc, error_list


def get_module_children(module):
    return [
        localdb.component.find_one({"_id": ObjectId(cpr.get("child"))})
        for cpr in localdb.childParentRelation.find({"parent": str(module.get("_id"))})
    ]


def get_module_bare_modules(module):
    return filter(
        lambda cpt: cpt.get("componentType") == "bare_module",
        get_module_children(module),
    )


def get_module_sensors(module):
    bm_cprs_list = [
        list(localdb.childParentRelation.find({"parent": str(bm.get("_id"))}))
        for bm in get_module_bare_modules(module)
    ]

    sensors = []

    for bm_cprs in bm_cprs_list:
        bm_children = [
            localdb.component.find_one({"_id": ObjectId(cpr.get("child"))})
            for cpr in bm_cprs
        ]
        bm_sensor = filter(
            lambda cpt: cpt.get("componentType") == "sensor_tile", bm_children
        )

        sensors += bm_sensor

    return sensors


def get_component_property_codes(component):
    return [prop.get("code") for prop in component.get("properties")]


def get_component_property_value(component, code):
    values = list(
        filter(lambda prop: prop.get("code") == code, component.get("properties"))
    )

    return values[0]["value"] if len(values) == 1 else None


def get_component_property_codetable(component, code):
    _property = filter(
        lambda prop: prop.get("code") == code, component.get("properties")
    )
    _codetable = _property["codeTable"]
    return _codetable if len(_codetable) > 0 else None


def get_layer_bare_module(component):
    """
    {'code': '0', 'value': 'No sensor'},
    {'code': '1', 'value': 'Market survey sensor tile'},
    {'code': '2', 'value': 'L0 inner pixel 3D sensor tile'},
    {'code': '3', 'value': 'L0 inner pixel planar sensor tile'},
    {'code': '4', 'value': 'L1 inner pixel quad sensor tile'},
    {'code': '5', 'value': 'Outer pixel quad sensor tile'},
    {'code': '9', 'value': 'Dummy sensor tile'}
    """
    compcode = get_component_property_value(component, "SENSOR_TYPE")
    codetable = get_component_property_codetable(component, "SENSOR_TYPE")
    _sensorcode = filter(lambda item: item.get("code") == compcode, codetable)
    _sensorvalue = _sensorcode.get("value")
    if _sensorvalue.startswith("L"):
        return _sensorvalue.split(" ")[0]
    if _sensorvalue.startswith("Outer"):
        return "L2"
    logger.error(f"No layer found for this bare module {component['serialNumber']}")
    return None


def get_latest_localdb_version(pre=False):
    # curl -s https://gitlab.cern.ch/api/v4/projects/75787/repository/tags | jq -r '.[0].name'
    response = requests.get(
        "https://gitlab.cern.ch/api/v4/projects/75787/repository/tags"
    )
    try:
        response.raise_for_status()

        tags = response.json()
        for tag in tags:
            if not pre and "rc" in tag["name"]:
                continue
            return tag["name"]
    except Exception:
        return ""


def check_latest_localdb_version():
    executor = concurrent.futures.ThreadPoolExecutor(
        thread_name_prefix="check_latest_localdb_version", max_workers=1
    )

    def check_latest_localdb_version_job():
        while True:
            # if we want to check for release candidates, or we're currently using a release candidate...
            latest_tag = get_latest_localdb_version(
                args.check_rc_tags or ("rc" in qcAnalysisVersion)
            )

            if latest_tag and Version(f"v{qcAnalysisVersion}") < Version(latest_tag):
                logger.warning(f"v{qcAnalysisVersion} < {latest_tag}")
                existing_message = (
                    userdb.message.find_one(
                        {"function": "check_latest_localdb_version"}
                    )
                    or {}
                )

                # current message has a different "latest_tag", let's clear it
                if existing_message and existing_message["component"] != latest_tag:
                    logger.warning(
                        f"clearing existing message for version={existing_message['component']}"
                    )
                    clear_message({"_id": existing_message["_id"]}, forced=True)

                if not existing_message:
                    logger.warning(f"creating new message for version={latest_tag}")
                    create_message(
                        f"My current version is at v{qcAnalysisVersion}, but the latest version is {latest_tag}. Please update me.",
                        function="check_latest_localdb_version",
                        component=latest_tag,
                        code="INFO_NEW_LOCALDB_VERSION",
                        can_dismiss=False,
                    )
            else:
                # clear all existing messages
                clear_message({"function": "check_latest_localdb_version"}, forced=True)
                logger.info(
                    "I appear to be on the latest tag. Checking again in an hour."
                )

            time.sleep(60 * 60)  # check daily

    executor.submit(check_latest_localdb_version_job)


def get_issue_with_iid(iid: int):
    response = requests.get(
        f"https://gitlab.cern.ch/api/v4/projects/75787/issues/{iid}",
    )
    try:
        response.raise_for_status()
        return response.json()
    except Exception as e:
        logger.error(f"Failed to fetch issues: {e}")
        return {}


def get_open_issues_with_label(label: str = "announcement"):
    response = requests.get(
        "https://gitlab.cern.ch/api/v4/projects/75787/issues",
        params={"labels": label, "state": "opened"},
    )
    try:
        response.raise_for_status()
        return response.json()
    except Exception as e:
        logger.error(f"Failed to fetch issues: {e}")
        return []


def clear_announcements_from_closed_issues():
    for announcement in userdb.message.find(
        {"function": "check_gitlab_announcements", "code": "INFO_GITLAB_ANNOUNCEMENT"}
    ):
        iid = announcement["component"]
        issue = get_issue_with_iid(iid)
        if issue["state"] == "opened":
            continue

        clear_message(
            {"function": "check_gitlab_announcements", "component": iid}, forced=True
        )


def check_gitlab_announcements():
    executor = concurrent.futures.ThreadPoolExecutor(
        thread_name_prefix="check_gitlab_announcements", max_workers=1
    )

    def check_gitlab_announcements_job():
        while True:
            clear_announcements_from_closed_issues()

            issues = get_open_issues_with_label()

            if issues:
                for issue in issues:
                    issue_id = issue["iid"]
                    title = issue["title"]

                    if message_exists(
                        function="check_gitlab_announcements", component=issue_id
                    ):
                        message = userdb.message.find_one(
                            {
                                "function": "check_gitlab_announcements",
                                "component": issue_id,
                            }
                        )
                        update_message(message["_id"], message=title)
                    else:
                        create_message(
                            title,
                            function="check_gitlab_announcements",
                            component=issue_id,
                            code="INFO_GITLAB_ANNOUNCEMENT",
                            can_dismiss=False,
                        )
            else:
                # Clear any existing messages if there are no announcements
                clear_message({"function": "check_gitlab_announcements"}, forced=True)
                logger.info("No open announcements found. Checking again in an hour.")

            time.sleep(60 * 60)  # Check every hour

    executor.submit(check_gitlab_announcements_job)


def download_configs(serialNumber, pd_client):
    try:
        cpt_doc = localdb.component.find_one({"serialNumber": serialNumber})
        ctype = cpt_doc["componentType"]

        if ctype != "module":
            logger.warning(f"Not downloading configs for component of type {ctype}.")
            return

        qc_status_doc = localdb.QC.module.status.find_one(
            {"component": str(cpt_doc.get("_id"))}
        )
        stage_flow = userdb.QC.stages.find_one({"code": "MODULE"})["stage_flow"]

        stage = qc_status_doc["stage"]
        stage_index = stage_flow.index(stage)
        ref_index = stage_flow.index("MODULE/INITIAL_WARM")

        if stage_index <= ref_index:
            logger.warning(
                f"Not downloading configs for component in {stage} <= MODULE/INITIAL_WARM"
            )
            return

    except Exception as e:
        clear_message(
            {"component": serialNumber, "code": "DOWNLOAD_CONFIG_INFO"}, forced=True
        )
        create_message(
            f"ERROR: {e} <pre>{traceback.format_exc()}</pre> --> contact experts",
            function="download_configs_worker",
            component=serialNumber,
            code="ERROR_DOWNLOAD_CONFIG_COMPONENT_INFO",
        )
        logger.warning(f"Failed in getting the component info for {serialNumber}")
        logger.error(str(e))
        return

    logger.info(f"serialNumber = {serialNumber}, stage = {stage}")

    create_message(
        f"Started to download FE configs for module {serialNumber}.<br />This may take several minutes.<br />Try to access to this module later.",
        function="download_configs_worker",
        component=serialNumber,
        code="DOWNLOAD_CONFIG_INFO",
        can_dismiss=False,
    )

    # Call config generation alg
    try:
        Module(pd_client, serialNumber)
        logger.info("create_config(): Getting layer-dependent config from module SN...")
    except Exception as e:
        clear_message(
            {"component": serialNumber, "code": "DOWNLOAD_CONFIG_INFO"}, forced=True
        )
        create_message(
            f"ERROR: {e} <pre>{traceback.format_exc()}</pre> --> contact experts",
            function="download_configs_worker",
            component=serialNumber,
            code="ERROR_DOWNLOAD_CONFIG_MODULE_INSTANCE",
        )
        logger.error(str(e))
        return

    try:
        layer_config = get_layer_from_serial_number(serialNumber)
    except Exception as e:
        clear_message(
            {"component": serialNumber, "code": "DOWNLOAD_CONFIG_INFO"}, forced=True
        )
        create_message(
            f"ERROR: {e} <pre>{traceback.format_exc()}</pre> --> contact experts",
            function="download_configs_worker",
            component=serialNumber,
            code="ERROR_DOWNLOAD_CONFIG_MODULE_LAYER_OPTION",
        )
        logger.error(str(e))
        return

    chip_api = ChipConfigAPI(client)

    chips = []
    cprs = list(localdb.childParentRelation.find({"parent": str(cpt_doc["_id"])}))
    for cpr in cprs:
        subcpt = localdb.component.find_one({"_id": ObjectId(cpr.get("child"))})
        if subcpt.get("componentType") == "front-end_chip":
            try:
                chip = pd_client.get(
                    "getComponent",
                    json={
                        "component": subcpt["proID"],
                        "noEosToken": False,
                    },
                )
                chips.append(chip)
            except Exception as e:
                clear_message(
                    {"component": serialNumber, "code": "DOWNLOAD_CONFIG_INFO"},
                    forced=True,
                )
                create_message(
                    f"ERROR: {e} <pre>{traceback.format_exc()}</pre> --> contact experts",
                    function="download_configs_worker",
                    component=serialNumber,
                    code="ERROR_DOWNLOAD_CONFIG_CHIP_DOC_DOWNLOAD",
                )
                logger.exception(
                    "Please contact the experts about this error in downloading the chip configs"
                )
                return

    # check if the config is present on PD
    for chip in chips:
        configs = {}

        for att in chip.get("attachments") or []:
            hexSN = hex(int(chip["serialNumber"][-7:]))
            title = att.get("title") or ""
            if title.find(f"{hexSN}_{layer_config}") == 0 and title.endswith(".json"):
                branch = title.replace(".json", "").split("_")[-1]
                configs.update({branch: att})

        required_branches = ["warm", "cold", "LP"]

        for branch in required_branches:
            if branch in configs:
                att = configs[branch]
                # config was found.

                doc, _ = download_itkpd_attachment(
                    pd_client, "component", chip.get("code"), att
                )
                if not doc:
                    msg = (
                        f"ERROR: Could not download chip config for {chip['serialNumber']} (component={chip.get('code')}, att={att}",
                    )
                    create_message(
                        msg,
                        function="download_configs_worker",
                        component=serialNumber,
                        code="ERROR_DOWNLOAD_CONFIG_SEEK",
                    )
                    logger.error(msg)
                    continue

                data = doc.fptr.read()

                try:
                    config_data = json.loads(data)
                except json.JSONDecodeError as exc:
                    msg = (
                        f"ERROR: Could not parse chip config attachment as JSON for {chip['serialNumber']} (component={chip.get('code')}, att={att}. {exc}",
                    )
                    create_message(
                        function="download_configs_worker",
                        component=serialNumber,
                        code="ERROR_DOWNLOAD_CONFIG_SEEK",
                    )
                    logger.exception(msg)
                    continue

                try:
                    config_id = chip_api.checkout(
                        chip.get("serialNumber"), stage, branch
                    )

                    if not config_id:
                        config_id = chip_api.create_config(
                            chip.get("serialNumber"), stage, branch
                        )

                    new_revision = chip_api.commit(
                        config_id,
                        config_data,
                        f"downloaded from ITkPD : code = {att.get('code')}, description = {att.get('description')}",
                    )

                    logger.info(
                        f'committed the contents of config_data in {chip.get("serialNumber")} to config_id {config_id}. New revision = {new_revision}'
                    )

                except Exception as e:
                    clear_message(
                        {"component": serialNumber, "code": "DOWNLOAD_CONFIG_INFO"},
                        forced=True,
                    )
                    create_message(
                        f"ERROR: {e} <pre>{traceback.format_exc()}</pre> --> contact experts",
                        function="download_configs_worker",
                        component=serialNumber,
                        code="ERROR_DOWNLOAD_CONFIG_SEEK",
                    )
                    logger.error(str(e))
                    continue

            else:
                logger.warning(
                    f"The config for {branch} is missing on ITkPD. Attempting to complement it now..."
                )

                try:
                    stage_flow = userdb.QC.stages.find_one({"code": "MODULE"}).get(
                        "stage_flow"
                    )
                    stage = qc_status_doc["stage"]
                    stage_index = stage_flow.index(stage)

                    ref_config_id = None
                    # initialized to current stage index
                    prev_index = stage_index

                    while not ref_config_id and prev_index > stage_flow.index(
                        "MODULE/INITIAL_WARM"
                    ):
                        # get the previous stage
                        prev_index -= 1
                        prev_stage = stage_flow[prev_index]

                        # fetch the latest config of the branch
                        ref_config_id = chip_api.checkout(
                            chip["serialNumber"], prev_stage, branch
                        )

                        if not ref_config_id:
                            clear_message(
                                {
                                    "component": serialNumber,
                                    "code": "DOWNLOAD_CONFIG_INFO",
                                    "forced": True,
                                }
                            )
                            create_message(
                                f'WARNING: no matched chip config was found for ( {chip.get("serial_number")}, {prev_stage}, {branch} ), checking earlier stages ...',
                                function="download_configs_worker",
                                component=serialNumber,
                                code="WARNING_DOWNLOAD_CONFIG_REF",
                            )

                    if not ref_config_id:
                        clear_message(
                            {"component": serialNumber, "code": "DOWNLOAD_CONFIG_INFO"},
                            forced=True,
                        )
                        create_message(
                            f'ERROR: no matched chip config was found for ( {chip.get("serial_number")}, {prev_stage}, {branch} ) and any of the following stages',
                            function="download_configs_worker",
                            component=serialNumber,
                            code="ERROR_DOWNLOAD_CONFIG_REF",
                        )
                        return

                    config_info = chip_api.get_info(ref_config_id)

                    logger.info(
                        "reference config info = " + pprint.pformat(config_info)
                    )

                    upload_chip_config(
                        chip["code"],
                        ref_config_id,
                        str(config_info.get("current_revision_id")),
                        layer_config,
                    )

                    new_config_id = chip_api.copy_config(
                        ref_config_id, chip.get("serialNumber"), stage, branch
                    )
                    chip_api.info(new_config_id)

                    config_data = chip_api.get_config(new_config_id, None, True)
                    chip_api.commit(
                        new_config_id,
                        config_data,
                        f"copied from serialNumber {config_info.get('serialNumber')}, stage {config_info.get('stage')}, branch {config_info.get('branch')}, revision {config_info.get('current_revision_id')}, and the config_data was uploaded to ITkPD",
                    )

                except Exception as e:
                    clear_message(
                        {"component": serialNumber, "code": "DOWNLOAD_CONFIG_INFO"},
                        forced=True,
                    )
                    create_message(
                        f"ERROR: {e} <pre>{traceback.format_exc()}</pre> --> contact experts",
                        function="download_configs_worker",
                        component=serialNumber,
                        code="ERROR_DOWNLOAD_CONFIG_REGISTER",
                    )
                    logger.error(str(e))
                    return

    clear_message(
        {"component": serialNumber, "code": "DOWNLOAD_CONFIG_INFO"}, forced=True
    )
    create_message(
        f"Downloading FE configs for module {serialNumber} is complete.",
        function="download_configs_worker",
        component=serialNumber,
        code="DOWNLOAD_CONFIG_COMPLETE",
    )
    logger.info("done.")


def add_disabled_stage(component_type):
    stage_doc = userdb.QC.stages.find_one({"code": component_type})
    disable_doc = stage_doc.get("disabled_tests", {})

    for stage in stage_doc.get("stage_flow"):
        if stage in disable_doc:
            continue  # Keep the user setting in the previous session
        if ("/" in stage) and (
            stage.partition("/")[0].lower() not in ["module"]
        ):  # TODO: expansion for OB
            disable_doc[stage] = {"disabled": 1, "tests": []}

    userdb.QC.stages.update_one(
        {"_id": stage_doc.get("_id")}, {"$set": {"disabled_tests": disable_doc}}
    )


def add_qcflow_customization():
    for stage_doc in userdb.QC.stages.find():
        add_disabled_stage(stage_doc.get("code"))


def run_db_migrations():
    clear_message({}, forced=True)
    fix_naming("fe_chip", "front-end_chip")
    fix_naming("pcb", "module_pcb")
    rename_current_stage()
    drop_serialNumber_QC_results()
    migrate_pd_institution_to_institution()
    fix_address_code_to_id()
    add_qcflow_customization()
