### import
import base64
import io
import json
import logging
import os
import pickle
import subprocess
import traceback

from bson.objectid import ObjectId  # handle bson format
from module_qc_database_tools.chip_config_api import ChipConfigAPI

from .common import (
    CACHE_DIR,
    DATA_SELECTION_LIST,
    bin2image,
    client,
    fs,
    localdb,
)

logger = logging.getLogger("localdb")


def retrieveFiles(run_oid, selection=None):
    logger.info(f"retrieveFiles(): testRun id = {run_oid}, selection = {selection}")

    query = {"_id": ObjectId(run_oid)}
    this_run = localdb.testRun.find_one(query)
    if not this_run:
        return None

    # logger.info( f'retrieveFiles(): testRun = ' + pprint.pformat( this_run ) )

    run_dir = f"{CACHE_DIR}/{run_oid}"
    if not os.path.isdir(run_dir):
        os.makedirs(run_dir)

    # scanLog
    scan_log = {
        "connectivity": [],
        "testType": this_run["testType"],
        "targetCharge": this_run["targetCharge"],
        "targetTot": this_run["targetTot"],
    }

    # scanCfg
    file_path = f"{run_dir}/{this_run['testType']}.json"
    if not os.path.isfile(file_path) and this_run.get("scanCfg", "...") != "...":
        query = {"_id": ObjectId(this_run["scanCfg"])}
        this_config = localdb.config.find_one(query)

        pickle_data = fs.get(ObjectId(this_config["data_id"])).read()
        data = pickle.load(io.BytesIO(pickle_data))

        file_path = file_path.replace("pickle", "json")

        with open(file_path, "w", encoding="utf-8") as f:
            json.dump(data, f, separators=(",", ":"))

    # data files
    query = {"testRun": run_oid}
    entries = localdb.componentTestRun.find(query)
    ctr_oids = []
    for entry in entries:
        ctr_oids.append(str(entry["_id"]))
    connectivity = {"chipType": this_run["chipType"], "chips": []}
    for ctr_oid in ctr_oids:
        query = {"_id": ObjectId(ctr_oid)}
        this_ctr = localdb.componentTestRun.find_one(query)

        try:
            hexSN = hex(int(this_ctr["name"][-7:]))
        except Exception:
            hexSN = ""

        if this_ctr.get("enable", 1) == 0:
            continue
        if this_ctr["chip"] == "module":
            continue

        # retrieve chip config
        config_api = ChipConfigAPI(client)
        config_id = this_ctr.get("config_id")

        try:
            config_revision_before = config_api.get_config(
                config_id, this_ctr.get("config_revision_prev"), True
            )
        except Exception as e:
            logger.warning(str(e))
            config_revision_before = {}

        try:
            config_revision_after = config_api.get_config(
                config_id, this_ctr.get("config_revision_current"), True
            )
        except Exception as e:
            logger.warning(str(e))
            config_revision_after = {}

        try:
            with open(f"{run_dir}/{hexSN}.json.before", "w", encoding="utf-8") as f:
                json.dump(config_revision_before, f)

            with open(f"{run_dir}/{hexSN}.json.after", "w", encoding="utf-8") as f:
                json.dump(config_revision_after, f)
        except Exception as e:
            logger.error(str(e))
            logger.error("Failed in writing chip configs!")

        if "scanSN" not in this_ctr:
            connectivity["chips"].append({"config": f"{hexSN}.json"})
        else:
            connectivity["chips"].append({"config": f"{this_ctr['scanSN']}.json"})
        for key in this_ctr:
            if "scanSN" not in this_ctr:
                file_path = f"{run_dir}/{hexSN}.json.{key[:-3]}"
            else:
                file_path = f"{run_dir}/{this_ctr['scanSN']}.json.{key[:-3]}"

            file_path = file_path.replace("pickle", "json")

            if not os.path.isfile(file_path):
                if "Cfg" not in key or this_ctr.get(key, "...") == "...":
                    continue
                query = {"_id": ObjectId(this_ctr[key])}
                this_config = localdb.config.find_one(query)
                pickle_data = fs.get(ObjectId(this_config["data_id"])).read()
                data = pickle.load(io.BytesIO(pickle_data))
                with open(file_path, "w", encoding="utf-8") as f:
                    json.dump(data, f, separators=(",", ":"))

        for data in this_ctr.get("attachments", []):
            try:
                if (
                    not selection
                    or data["filename"].split(".")[0].split("-")[0]
                    in DATA_SELECTION_LIST[this_run["testType"]]
                ):
                    if "scanSN" not in this_ctr:
                        file_path = f"{run_dir}/{hexSN}_{data['filename']}"
                    else:
                        file_path = f"{run_dir}/{this_ctr['scanSN']}_{data['filename']}"

                    file_path = file_path.replace("pickle", "json")

                    if not os.path.isfile(file_path):
                        pickled_data = fs.get(ObjectId(data.get("code"))).read()
                        unpickled_data = pickle.load(io.BytesIO(pickled_data))
                        with open(file_path, "w", encoding="utf-8") as f:
                            json.dump(unpickled_data, f, separators=(",", ":"))
            except Exception as e:
                logger.warning(str(e))
                logger.warning(traceback.print_exc())

        scan_log["connectivity"].append(connectivity)

    file_path = f"{run_dir}/scanLog.json"
    if not os.path.isfile(file_path):
        with open(file_path, "w", encoding="utf-8") as f:
            json.dump(scan_log, f, separators=(",", ":"))

    return True


def plotRoot(plot_tool, run_oid, force_regenerate=False, expand=False):
    run_dir = f"{CACHE_DIR}/{run_oid}"
    if not os.path.isdir(run_dir):
        return None

    command = [f"{plot_tool}/bin/plotFromDir", "-i", run_dir, "-P", "-e", "png"]
    if not expand:
        command.append("-X")

    logger.info(" ".join(command))

    if not force_regenerate and os.path.isfile(f"{run_dir}/rootfile.root"):
        return 0

    try:
        subprocess.check_call(command, timeout=30)
    except subprocess.CalledProcessError:
        msg = f"plotting tool did not run successfully. Tried to run the command: {command}"
        logger.error(msg)
        logger.error(traceback.format_exc())
        return 1
    except FileNotFoundError:
        msg = f"Are the plotting tools compiled and installed correctly? Tried to run the command: {command}"
        logger.error(msg)
        logger.error(traceback.format_exc())
        return 1

    return 0


def setRootResult(run_oid, chip_oid=None):
    thum_dir = f"{CACHE_DIR}/{run_oid}/thumbnail"
    if not os.path.isdir(thum_dir):
        return None

    chip_name = None
    plots = {}
    filepath = f"{CACHE_DIR}/{run_oid}/plotLog.json"
    with open(filepath, encoding="utf-8") as f:
        plotLog = json.load(f)
    if chip_oid:
        query = {"testRun": run_oid, "component": chip_oid}
        this_ctr = localdb.componentTestRun.find_one(query)
        chip_name = this_ctr["name"]
    for name in plotLog["chips"]:
        if chip_name and chip_name != name:
            continue
        for plotname in plotLog["chips"][name]:
            plots.update({plotname: {"plot": []}})
            for filename in plotLog["chips"][name][plotname]:
                filepath = f"{thum_dir}/{filename}_root.png"
                with open(filepath, "rb") as binary_image:
                    code_base64 = base64.b64encode(binary_image.read()).decode()
                plots[plotname]["plot"].append(
                    {"name": filename, "url": bin2image("png", code_base64)}
                )
            plots[plotname]["num"] = len(plots[plotname]["plot"])
    return plots


def setPlots(i_oid, i_tr_oid):  # TODO
    docs = {}
    if i_tr_oid:
        retrieveFiles(i_tr_oid)
        plotRoot("./plotting-tool", i_tr_oid)
        docs = setRootResult(i_tr_oid, i_oid)
    return docs
