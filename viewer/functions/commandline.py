import argparse  # Pass command line arguments into python script
import multiprocessing

import yaml  # Read YAML config file


def readConfig(conf_path):
    with open(conf_path) as f:
        return yaml.safe_load(f)


def getArgs():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument("--config", "-f", help="Config file path", type=str)
    parser.add_argument("--host", help="Host", type=str)
    parser.add_argument("--port", help="Port", type=int)
    parser.add_argument("--db", help="Db", type=str)
    parser.add_argument(
        "--authSource", help="MongoDB authentication database", type=str
    )
    parser.add_argument("--username", "-u", help="User name", type=str)
    parser.add_argument("--password", "-p", help="User password", type=str)
    parser.add_argument("--KeyFile", help="Path to user key file", type=str)
    parser.add_argument("--fhost", help="Flask Host", type=str)
    parser.add_argument("--fport", help="Flask Port", type=int)
    parser.add_argument("--fcert", help="Path to Flask Certification File", type=int)
    parser.add_argument("--fkey", help="Path to Flask Private Key File", type=int)
    parser.add_argument("--fmail", help="Use mail function", type=bool)
    parser.add_argument("--fsession", help="Use Flask-Session", type=bool)
    parser.add_argument(
        "--fsuser", "-fu", help="Flask-Session MongoDb user name", type=str
    )
    parser.add_argument(
        "--fspass", "-fp", help="Flask-Session MongoDb password", type=str
    )
    parser.add_argument("--fsdb", "-fd", help="Flask-Session MongoDb", type=str)
    parser.add_argument("--fskey", help="Path to Flask-Session key file", type=str)
    parser.add_argument("--userdb", help="userdb", type=str)
    parser.add_argument("--ssl", help="Enable ssl", action="store_true")
    parser.add_argument("--sslPEMKeyFile", help="Path to certificate", type=str)
    parser.add_argument("--sslCAFile", help="Path to CA file", type=str)
    parser.add_argument("--tls", help="Enable tls", action="store_true")
    parser.add_argument("--tlsCertificateKeyFile", help="Path to certificate", type=str)
    parser.add_argument("--tlsCAFile", help="Path to CA file", type=str)
    parser.add_argument(
        "--matchHostname", help="Whether to match hostname", action="store_true"
    )
    parser.add_argument(
        "--nthreads",
        help="max # of threads (defaults to number of CPU cores)",
        type=int,
        default=multiprocessing.cpu_count(),
    )
    parser.add_argument(
        "--check-rc-tags",
        help="Enable to include release candidate tags in the automatic version check",
        action="store_true",
    )
    parser.add_argument("--auth", help="Set authentication mechanism", type=str)
    parser.add_argument("--overwrite", help="Set PD test overwrite flag", type=bool)
    parser.add_argument(
        "--component_id",
        help="Either code, or SN for the component in question",
        type=str,
    )
    parser.add_argument(
        "--logfile", help="Log file path", type=str, default="logs/production.log"
    )
    parser.add_argument("--option", help="Select download info", type=str)
    parser.add_argument("--stage", help="Input stage", type=str)

    args = parser.parse_args()

    # Overwrite arguments from config file
    if args.config is not None:
        conf = readConfig(args.config)  # Read from config file
        if "mongoDB" in conf:
            if "host" in conf["mongoDB"] and not args.host:
                args.host = conf["mongoDB"]["host"]
            if "port" in conf["mongoDB"] and not args.port:
                args.port = conf["mongoDB"]["port"]
            if "db" in conf["mongoDB"] and not args.db:
                args.db = conf["mongoDB"]["db"]
            if "authSource" in conf["mongoDB"] and not args.authSource:
                args.authSource = conf["mongoDB"]["authSource"]
            if "username" in conf["mongoDB"] and not args.username:
                args.username = conf["mongoDB"]["username"]
            if "password" in conf["mongoDB"] and not args.password:
                args.password = conf["mongoDB"]["password"]
            if "KeyFile" in conf["mongoDB"] and not args.KeyFile:
                args.KeyFile = conf["mongoDB"]["KeyFile"]
            if "ssl" in conf["mongoDB"]:
                if "enabled" in conf["mongoDB"]["ssl"] and not args.ssl:
                    args.ssl = conf["mongoDB"]["ssl"]["enabled"]
                if "PEMKeyFile" in conf["mongoDB"]["ssl"] and not args.sslPEMKeyFile:
                    args.sslPEMKeyFile = conf["mongoDB"]["ssl"]["PEMKeyFile"]
                if "CAFile" in conf["mongoDB"]["ssl"] and not args.sslCAFile:
                    args.sslCAFile = conf["mongoDB"]["ssl"]["CAFile"]
                if (
                    "match_hostname" in conf["mongoDB"]["ssl"]
                    and not args.matchHostname
                ):
                    args.matchHostname = conf["mongoDB"]["ssl"]["matchHostname"]
            if "tls" in conf["mongoDB"]:
                if "enabled" in conf["mongoDB"]["tls"] and not args.tls:
                    args.tls = conf["mongoDB"]["tls"]["enabled"]
                if (
                    "CertificateKeyFile" in conf["mongoDB"]["tls"]
                    and not args.tlsCertificateKeyFile
                ):
                    args.tlsCertificateKeyFile = conf["mongoDB"]["tls"][
                        "CertificateKeyFile"
                    ]
                if "CAFile" in conf["mongoDB"]["tls"] and not args.tlsCAFile:
                    args.tlsCAFile = conf["mongoDB"]["tls"]["CAFile"]
                if (
                    "match_hostname" in conf["mongoDB"]["tls"]
                    and not args.matchHostname
                ):
                    args.matchHostname = conf["mongoDB"]["tls"]["matchHostname"]
            if "auth" in conf["mongoDB"] and not args.auth:
                args.auth = conf["mongoDB"]["auth"]
        if "flask" in conf:
            if "host" in conf["flask"] and not args.fhost:
                args.fhost = conf["flask"]["host"]
            if "port" in conf["flask"] and not args.fport:
                args.fport = conf["flask"]["port"]
            if "cert" in conf["flask"] and not args.fcert:
                args.fcert = conf["flask"]["cert"]
            if "key" in conf["flask"] and not args.fkey:
                args.fkey = conf["flask"]["key"]
            if "mail" in conf["flask"] and not args.fmail:
                args.fmail = conf["flask"]["mail"]
            if "session" in conf["flask"] and not args.fsession:
                args.fsession = conf["flask"]["session"]
                if args.fsession:
                    if "username" in conf["flask"] and not args.fsuser:
                        args.fsuser = conf["flask"]["username"]
                    if "password" in conf["flask"] and not args.fspass:
                        args.fspass = conf["flask"]["password"]
                    if "db" in conf["flask"] and not args.fsdb:
                        args.fsdb = conf["flask"]["db"]
                    if "KeyFile" in conf["flask"] and not args.fskey:
                        args.fskey = conf["flask"]["KeyFile"]
        if "userDB" in conf and "db" in conf.get("userDB", {}) and not args.userdb:
            args.userdb = conf["userDB"]["db"]

        args.nthreads = conf.get("localDB", {}).get("nthreads", args.nthreads)

        args.check_rc_tags = conf.get("localDB", {}).get(
            "check_rc_tags", args.check_rc_tags
        )

    # default
    if not args.host:
        args.host = "localhost"
    if not args.port:
        args.port = 27017
    if not args.db:
        args.db = "localdb"
    if not args.authSource:
        args.authSource = "localdb"
    if not args.fhost:
        args.fhost = "localhost"
    if not args.fport:
        args.fport = 5000
    if not args.userdb:
        args.userdb = "localdbtools"
    if not args.nthreads:
        args.nthreads = multiprocessing.cpu_count()

    return args
