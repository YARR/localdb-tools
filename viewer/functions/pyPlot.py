import matplotlib as mpl
import numpy as np

mpl.use("Agg")  # set batch mode
import logging

import matplotlib.pyplot as plt
from flask import session
from matplotlib import dates as mdates

logger = logging.getLogger("localdb")


class pyBasic:
    def __init__(self):
        self.__name = None
        self.__Title = None
        self.__xTitle = None
        self.__yTitle = None
        self.xMin = None
        self.xMax = None
        self.yMin = None
        self.yMax = None
        self.name = None

        self.canvas = gPad

    def SetName(self, string):
        self.__name = string
        self.name = string

    def SetTitle(self, string):
        title = string.split(";")
        if len(title) == 3:
            self.__Title = title[0]
            self.__xTitle = title[1]
            self.__yTitle = title[2]
        else:
            self.__Title = title[0]

    def SetXAxisTitle(self, string):
        self.__xTitle = string

    def SetYAxisTitle(self, string):
        self.__xTitle = string

    def SetXRange(self, Xmin, Xmax):
        self.xMin = Xmin
        self.xMax = Xmax

    def SetYRange(self, yMin, yMax):
        self.yMin = yMin
        self.yMax = yMax

    def SetCanvas(self, canvas):
        self.canvas = canvas

    def CheckParX(self):
        return not (self.xMin is None and self.xMax is None)

    def CheckParY(self):
        return not (self.yMin is None and self.yMax is None)

    def SetDrawStatus(self):
        if self.__Title is not None:
            self.canvas.ax.set_title(self.__Title)
        if self.__xTitle is not None:
            self.canvas.ax.set_xlabel(self.__xTitle)
        if self.__yTitle is not None:
            self.canvas.ax.set_ylabel(self.__yTitle)
        if self.xMin is not None and self.xMax is not None:
            self.canvas.ax.set_xlim(self.xMin, self.xMax)
        if self.yMin is not None and self.yMax is not None:
            self.canvas.ax.set_ylim(self.yMin, self.yMax)


class pyH1F(pyBasic):
    def __init__(self, name, title, binNum, Min, Max):
        super().__init__()
        self.SetName(name)
        self.SetTitle(title)
        self.Bin = int(binNum)
        self.Min = Min
        self.Max = Max
        self.Binweight = float((self.Max - self.Min) / self.Bin)

        super().SetXRange(Min, Max)

        if isinstance(self.Bin, int):
            self.Hist = np.zeros(self.Bin)
            self.Xaxis = np.zeros(self.Bin)
            for i in range(self.Bin):
                self.Xaxis[i] = self.Min + self.Binweight * i + self.Binweight / 2

    def usage(self):
        # print('pyH1F(name, title, bin<int>, min , max)')
        logger.debug("pyH1F(name, title, bin<int>, min , max)")
        return 1

    def Fill(self, x, weight=1):
        if x > self.Min and x < self.Max:
            nbin = int((x - self.Min) / self.Binweight)
            return self.FillBins(nbin, weight)
        return 1

    def FillBins(self, nbin, weight=1):
        self.Hist[nbin] += weight

    def outputHist(self):
        # print(self.Xaxis)
        logger.debug(self.Xaxis)
        # print(self.Hist)
        logger.debug(self.Hist)

    def Draw(self):
        super().SetDrawStatus()
        p = self.canvas.ax.bar(self.Xaxis, self.Hist, width=self.Binweight)
        self.canvas.output(p)


class pyGraph(pyBasic):
    def __init__(self, title=None):
        super().__init__()
        self.__setRegion = False
        self.__RegionXmin = None
        self.__RegionXmax = None
        self.__RegionYmin = None
        self.__RegionYmax = None

        self.__setXAxis_time = False

        self.__x_list = np.array([])
        self.__y_list = np.array([])

        self.SetTitle(title)

    def SetPoint(self, x, y):
        self.__x_list = np.append(self.__x_list, x)
        self.__y_list = np.append(self.__y_list, y)

    def GetxList(self):
        return self.__x_list

    def GetyList(self):
        return self.__y_list

    def GetEntries(self):
        return len(self.__x_list)

    """
    def SetTitle(self, string) :
        self.__Title=string
    def SetXAxisTitle(self, string) :
        self.__xTitle=string
    def SetYAxisTitle(self, string) :
        self.__yTitle=string
    def SetXRange(self, x_min ,x_max) :
        self.__x_min=x_min
        self.__x_max=x_max
    def SetYRange(self, y_min , y_max) :
        self.__y_min=y_min
        self.__y_max=y_max
    """

    def SetXAxis_Time(self):
        self.__setXAxis_time = True

    def SetXRegion(self, x_min, x_max):
        self.__setRegion = True
        self.__RegionXmin = x_min
        self.__RegionXmax = x_max

    def Draw(self, mode=""):
        try:
            mpl.rcParams["timezone"] = session.get("timezone", "UTC")
        except Exception:
            # print('timezone : UTC')
            logger.debug("timezone : UTC")
            mpl.rcParams["timezone"] = "UTC"
        if mode == "same":
            self.__setRegion = False
        if not super().CheckParX():
            super().SetXRange(np.amin(self.__x_list), np.amax(self.__x_list))
        if not super().CheckParY():
            super().SetYRange(np.amin(self.__y_list), np.amax(self.__y_list))

        super().SetDrawStatus()
        if self.__setXAxis_time:
            self.canvas.ax.xaxis.set_major_formatter(mdates.DateFormatter("%H:%M:%S"))

        if self.__setRegion:
            if self.__RegionXmin is None:
                self.__RegionXmin = self.xMin
            if self.__RegionXmax is None:
                self.__RegionXmax = self.xMax
            if self.__RegionYmin is None:
                self.__RegionYmin = self.yMin
            if self.__RegionYmax is None:
                self.__RegionYmax = self.yMax
            rangeX = [
                self.__RegionXmin,
                self.__RegionXmin,
                self.__RegionXmax,
                self.__RegionXmax,
            ]
            rangeY = [
                self.__RegionYmin,
                self.__RegionYmax,
                self.__RegionYmax,
                self.__RegionYmin,
            ]
            self.canvas.ax.fill(rangeX, rangeY, facecolor="r", alpha=0.3)

        p = self.canvas.ax.plot(
            self.__x_list, self.__y_list, marker="o", label=self.name
        )
        self.canvas.output(p)


class pyCanvas:
    def __init__(self):
        self.__plotlist = []
        self.__old_plot = None
        self.fig = plt.figure(figsize=(10, 8))
        self.ax = self.fig.add_subplot(111)
        self.permit_show = False
        plt.rcParams["font.size"] = 24
        plt.tight_layout()

    def output(self, p):
        self.__old_plot = None
        self.__old_plot = p
        if self.permit_show:
            plt.show()

    def SetLegend(self):
        self.ax.legend()

    def output_withregion(self, p):
        plt.legend((self.__old_plot[0], p[0]), ("Plot1", "Plot2"), loc=4)

    def Print(self, filename):
        plt.savefig(filename)


#### global instance #############
gPad = pyCanvas()


def main():
    # hist=pyH1F('hist','hist',10,0,20)
    # hist.Fill(5)
    # hist.Fill(7)
    # hist.Fill(6)

    # hist.Draw()
    c = pyCanvas()

    g = pyGraph()
    g.SetCanvas(c)
    g.SetPoint(2, 3)
    g.SetPoint(3, 4)
    g.SetPoint(4, 5)

    g1 = pyGraph()
    g1.SetCanvas(c)
    g1.SetPoint(2, 4)
    g1.SetPoint(3, 5)
    g1.SetPoint(4, 3)

    g.Draw()
    c.Print("gaya.png")
    g1.Draw()

    c.Print("gaya2.png")


if __name__ == "__main__":
    gPad.permit_show = True
    main()
