#!/bin/bash

if [ ! $(echo "${0}" | grep bash) ]; then
    echo -e "Use 'source'"
    exit
fi

read -sp "Input Access Code 1 for ITkPD: " pass1
echo ""
read -sp "Input Access Code 2 for ITkPD: " pass2
echo ""
export ITKDB_ACCESS_CODE1=${pass1}
export ITKDB_ACCESS_CODE2=${pass2}

itkdb authenticate
