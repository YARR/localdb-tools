function setHide(query, boolEnable, display = "table-row") {
  const elems = document.querySelectorAll(query);
  elems.forEach((elem) => {
    console.log(elem);
    if (!boolEnable) {
      elem.style.display = display;
    } else {
      elem.style.display = "none";
    }
  });
}

var jqOther = jQuery.noConflict(true);
jqOther(function () {
  jqOther("#elTable").tablesorter({
    widgets: ["zebra", "columns"],
    usNumberFormat: false,
    sortReset: true,
    sortRestart: true,
    theme: "bootstrap",
  });
  jqOther("#qcTable").tablesorter({
    widgets: ["zebra", "columns"],
    usNumberFormat: false,
    sortReset: true,
    sortRestart: true,
    theme: "bootstrap",
  });
  jqOther("#propTable").tablesorter({
    widgets: ["zebra", "columns"],
    usNumberFormat: false,
    sortReset: true,
    sortRestart: true,
    theme: "bootstrap",
  });
});


const cleaveZen = window.cleaveZen;
const { formatGeneral, unformatGeneral, registerCursorTracker } = cleaveZen;

const inputs_serialNumber = document.querySelectorAll("input.serialNumber");
const spans_serialNumber = document.querySelectorAll("span.serialNumber");
const anchors_serialNumber = document.querySelectorAll("a.serialNumber");

const formatOptions_ATLAS_SN = {
    blocks: [2, 1, 2, 2, 7],
    uppercase: true,
    delimiter: " ",
};

inputs_serialNumber.forEach((el) => {
  registerCursorTracker({
    input: el,
    delimiter: " ",
  });

  el.addEventListener("input", (e) => {
    e.target.value = formatGeneral(e.target.value, formatOptions_ATLAS_SN);
  });
});

spans_serialNumber.forEach((el) => {
  el.innerHTML = formatGeneral(el.innerHTML, formatOptions_ATLAS_SN);

  el.addEventListener("mouseenter", (e) => {
    e.target.innerHTML = unformatGeneral(e.target.innerHTML, formatOptions_ATLAS_SN);
  });

  el.addEventListener("mouseleave", (e) => {
    e.target.innerHTML = formatGeneral(e.target.innerHTML, formatOptions_ATLAS_SN);
  });
});

anchors_serialNumber.forEach((el) => {
  el.innerHTML = formatGeneral(el.innerHTML, formatOptions_ATLAS_SN);
});
