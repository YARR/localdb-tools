import json
import logging

import requests
from bson.objectid import ObjectId
from flask import (
    Blueprint,
    jsonify,
    request,
    session,
)
from functions.common import (
    dbv,
    fs,
    localdb,
    setTime,
)
from pymongo import (
    DESCENDING,
)
from tzlocal import get_localzone

logger = logging.getLogger("localdb")

retrieve_api = Blueprint("retrieve_api", __name__)


@retrieve_api.route("/retrieve/config", methods=["GET"])
def retrieve_config():
    oid = request.args["oid"]
    data_type = request.args["type"]
    if data_type == "json":
        data = json.loads(fs.get(ObjectId(oid)).read().decode("ascii"))
    else:
        data = fs.get(ObjectId(oid)).read().decode("ascii")
    return_json = {"data": data}

    return jsonify(return_json)


@retrieve_api.route("/retrieve/log", methods=["GET"])
def retrieve_log():
    return_json = {}

    run_query = {}
    log_query = {"$and": []}

    chip_name = request.args.get("chip")
    if chip_name:
        query = {"name": chip_name}
        chip_entries = localdb.chip.find(query)
        chip_query = []
        for this_chip in chip_entries:
            chip_query.append({"chip": str(this_chip["_id"])})
        if chip_query:
            return_json = {
                "message": f"Not found chip data: {chip_name}",
                "error": True,
            }
            return jsonify(return_json)

        run_query.update({"$or": chip_query})

    if not run_query:
        run_entries = localdb.componentTestRun.find(run_query)
        run_oids = []
        for run_entry in run_entries:
            run_oids.append({"_id": ObjectId(run_entry["testRun"])})
        log_query["$and"].append({"$or": run_oids})

    if request.args.get("user"):
        query = {"userName": {"$regex": request.args["user"].lower().replace(" ", "_")}}
        entries = localdb.user.find(query)
        if entries.estimated_document_count() == 0:
            return_json = {
                "message": f"Not found user data: {request.args['user']}",
                "error": True,
            }
            return jsonify(return_json)
        user_oids = []
        for entry in entries:
            user_oids.append({"user_id": str(entry["_id"])})
        log_query["$and"].append({"$or": user_oids})
    if request.args.get("site"):
        query = {
            "institution": {"$regex": request.args["site"].lower().replace(" ", "_")}
        }
        entries = localdb.institution.find(query)
        if entries.estimated_document_count() == 0:
            return_json = {
                "message": f"Not found site data: {request.args['site']}",
                "error": True,
            }
            return jsonify(return_json)
        site_oids = []
        for entry in entries:
            site_oids.append({"address": str(entry["_id"])})
        log_query["$and"].append({"$or": site_oids})

    if not log_query["$and"]:
        log_query = {}

    run_entries = localdb.testRun.find(log_query).sort([("startTime", DESCENDING)])

    return_json = {"log": []}
    for this_run in run_entries:
        query = {"_id": ObjectId(this_run["user_id"])}
        this_user = localdb.user.find_one(query)
        query = {"_id": ObjectId(this_run["address"])}
        this_site = localdb.institution.find_one(query)
        if not this_site:
            logger.info(query)
        query = {"testRun": str(this_run["_id"])}
        ctr_entries = localdb.componentTestRun.find(query)
        chips = []
        this_dcs = {}
        for this_ctr in ctr_entries:
            if chip_name and this_ctr["name"] == chip_name:
                chips.append(f"\033[1;31m{this_ctr['name']}\033[0m")
            else:
                chips.append(this_ctr["name"])
            if this_ctr.get("environment", "...") != "...":
                this_dcs.update({this_ctr["name"]: []})
                query = {"_id": ObjectId(this_ctr["environment"])}
                this_env = localdb.environment.find_one(query)
                for key in this_env:
                    if key not in ["_id", "dbVersion", "sys"]:
                        this_dcs[this_ctr["name"]].append(key)
        test_data = {
            "user": this_user["userName"],
            "site": this_site["institution"],
            "datetime": setTime(
                this_run["startTime"], session.get("timezone", str(get_localzone()))
            ),
            "runNumber": this_run["runNumber"],
            "testType": this_run["testType"],
            "runId": str(this_run["_id"]),
            "chips": chips,
            "dbVersion": this_run["dbVersion"],
            "environment": this_dcs,
        }
        return_json["log"].append(test_data)

    return jsonify(return_json)


@retrieve_api.route("/retrieve/data", methods=["GET"])
def retrieve_data():
    ###################################
    ### Pull data files from testRun ID
    def __pull_test_run(tr_oid, i_chip):
        data_entries = []
        query = {"_id": ObjectId(tr_oid), "dbVersion": dbv}
        this_tr = localdb.testRun.find_one(query)
        chip_type = (
            this_tr.get("chipType", "NULL")
            if this_tr.get("chipType", "NULL") != "FE-I4B"
            else "FEI4B"
        )

        ### scanLog
        log_json = {}
        for key in this_tr:
            if "Cfg" in key and this_tr[key] != "...":
                ### config file
                query = {"_id": ObjectId(this_tr[key])}
                this_cfg = localdb.config.find_one(query)
                docs = getData(
                    "json", dir_path, this_cfg["filename"], this_cfg["data_id"]
                )
                data_entries.append(docs)
            elif key == "_id":
                log_json.update({key: str(this_tr[key])})
            elif key in ["startTime", "finishTime"]:
                log_json.update({key: this_tr[key].timestamp()})
            elif key not in ["address", "user_id", "sys"]:
                log_json.update({key: this_tr[key]})

        ### connectivity
        query = {"testRun": tr_oid, "dbVersion": dbv}
        entries = localdb.componentTestRun.find(query)
        conn_json = {
            "stage": this_tr.get("stage", "..."),
            "chipType": chip_type,
            "chips": [],
        }
        chips = []
        for this_ctr in entries:
            if this_ctr["chip"] == "module":
                query = {"_id": ObjectId(this_ctr["component"])}
                this_cmp = localdb.component.find_one(query)
                conn_json.update(
                    {
                        "module": {
                            "serialNumber": this_cmp["name"],
                            "componentType": this_cmp["componentType"],
                        }
                    }
                )
                continue
            chip_conn = {}
            for key in this_ctr:
                if key == "config":
                    chip_conn.update({key: f"{dir_path}/{this_ctr[key]}"})
                elif "Cfg" in key and this_ctr[key] != "...":
                    ### chip config
                    query = {"_id": ObjectId(this_ctr[key])}
                    this_cfg = localdb.config.find_one(query)

                    config_name = this_ctr.get("config", f"{this_ctr['name']}.json")

                    if key == "beforeCfg":
                        docs = getData(
                            "json",
                            dir_path,
                            config_name,
                            this_cfg["data_id"],
                        )
                        data_entries.append(docs)

                        docs = getData(
                            "json",
                            dir_path,
                            f"{config_name}.before",
                            this_cfg["data_id"],
                        )
                        data_entries.append(docs)
                    elif key == "afterCfg":
                        docs = getData(
                            "json",
                            dir_path,
                            f"{config_name}.after",
                            this_cfg["data_id"],
                        )
                        data_entries.append(docs)
                    else:
                        docs = getData(
                            "json",
                            dir_path,
                            f"{this_ctr['name']}_{key}.json",
                            this_cfg["data_id"],
                        )
                        data_entries.append(docs)
                elif key == "attachments":
                    ### attachment
                    for attachment in this_ctr[key]:
                        docs = getData(
                            attachment["contentType"],
                            dir_path,
                            f"{this_ctr['name']}_{attachment['filename']}",
                            attachment["code"],
                        )
                        data_entries.append(docs)
                elif key not in [
                    "_id",
                    "component",
                    "sys",
                    "chip",
                    "testRun",
                    "environment",
                    "dbVersion",
                ]:
                    chip_conn.update({key: this_ctr[key]})
            conn_json["chips"].append(chip_conn)
            ### output texts
            if i_chip and this_ctr["name"] == i_chip:
                chips.append(f"\033[1;31m{this_ctr['name']}\033[0m")
            else:
                chips.append(this_ctr["name"])
        docs = getData("json", dir_path, "connectivity.json", conn_json, True)
        data_entries.append(docs)

        log_json["connectivity"] = []
        log_json["connectivity"].append(conn_json)
        docs = getData("json", dir_path, "scanLog.json", log_json, True)
        data_entries.append(docs)

        ### user data
        query = {"_id": ObjectId(this_tr["user_id"])}
        this_user = localdb.user.find_one(query)

        ### site data
        query = {"_id": ObjectId(this_tr["address"])}
        this_site = localdb.institution.find_one(query)

        ### output texts
        return {
            "_id": str(this_tr["_id"]),
            "col": "testRun",
            "log": {
                "User": f"{this_user['userName']} at {this_site['institution']}",
                "Date": setTime(
                    this_tr["startTime"], session.get("timezone", str(get_localzone()))
                ),
                "Chips": ", ".join(chips),
                "Run Number": this_tr["runNumber"],
                "Test Type": this_tr["testType"],
            },
            "data": data_entries,
        }

    ##############################################
    ### Pull data files from component information
    def __pull_component(i_chip):
        data_entries = []
        query = {"name": i_chip, "dbVersion": dbv}
        this_cmp = localdb.component.find_one(query)
        this_ch = localdb.chip.find_one(query)
        if not this_cmp and not this_ch:
            return {
                "message": f"Not found chip data: {i_chip}",
                "error": True,
            }

        this_chip = this_cmp if this_cmp else this_ch
        ### connectivity
        conn_json = {"stage": "Testing", "chips": []}
        module = False
        children = []
        query = {"parent": str(this_chip["_id"]), "dbVersion": dbv}
        entries = localdb.childParentRelation.find(query)
        if entries.estimated_document_count() == 0:
            children.append(str(this_chip["_id"]))
        else:
            conn_json.update(
                {
                    "module": {
                        "serialNumber": this_chip["serialNumber"],
                        "componentType": this_chip["componentType"],
                    }
                }
            )
            for entry in entries:
                children.append(entry["child"])
            module = True

        chips = []
        for i, chip in enumerate(children):
            query = {"_id": ObjectId(chip)}
            this = localdb.component.find_one(query)
            if not this:
                continue

            chip_type = (
                this.get("chipType", "NULL")
                if this.get("chipType", "NULL") != "FE-I4B"
                else "FEI4B"
            )
            cfg_json = requests.get(
                f"https://gitlab.cern.ch/YARR/YARR/raw/master/configs/defaults/default_{chip_type.lower()}.json",
                timeout=60,
            )
            if cfg_json.status_code == 404:
                return {
                    "message": f"Not found default chip config: {cfg_json.url}",
                    "error": True,
                }
            cfg_json = cfg_json.json()
            if chip_type == "FEI4B":
                cfg_json["FE-I4B"]["name"] = this["name"]
                cfg_json["FE-I4B"]["Parameter"]["chipId"] = this.get("chipId", 0)
            else:
                cfg_json[chip_type]["Parameter"]["Name"] = this["name"]
                cfg_json[chip_type]["Parameter"]["ChipId"] = this.get("chipId", 0)
            ### chip config
            docs = getData("json", dir_path, f"{this['name']}.json", cfg_json, True)
            data_entries.append(docs)

            conn_json["chips"].append(
                {
                    "config": "/".join([dir_path, this["name"]]),
                    "tx": i,
                    "rx": i,
                }
            )
            conn_json.update({"chipType": chip_type})
            ### output texts
            if chip == str(this_chip["_id"]):
                chips.append(f"\033[1;31m{this['name']}\033[0m")
            else:
                chips.append(this["name"])
        docs = getData("json", dir_path, "connectivity.json", conn_json, True)
        data_entries.append(docs)

        ### output texts
        return {
            "_id": str(this_chip["_id"]),
            "col": "component",
            "log": {
                "Parent": (
                    f"\033[1;31m{this_chip['serialNumber']} ({this_chip['componentType']})\033[0m"
                    if module
                    else None
                ),
                "Chip Type": chip_type,
                "Chips": ", ".join(chips),
            },
            "data": data_entries,
        }

    #################
    ### main function
    return_json = {}
    dir_path = request.args.get("dir")

    tr_oid = None
    if request.args.get("test"):
        tr_oid = request.args["test"]
    elif request.args.get("chip"):
        query = {"name": request.args["chip"], "dbVersion": dbv}
        entries = localdb.componentTestRun.find(query)
        if entries.estimated_document_count() != 0:
            query = {"$or": [], "dbVersion": dbv}
            for entry in entries:
                query["$or"].append({"_id": ObjectId(entry["testRun"])})
            entry = (
                localdb.testRun.find(query).sort([("startTime", DESCENDING)]).limit(1)
            )
            if entry.estimated_document_count() != 0:
                tr_oid = str(entry[0]["_id"])
    else:
        query = {"dbVersion": dbv}
        entry = localdb.testRun.find(query).sort([("startTime", DESCENDING)]).limit(1)
        if entry.estimated_document_count() != 0:
            tr_oid = str(entry[0]["_id"])

    if tr_oid:
        query = {"testRun": tr_oid, "dbVersion": dbv}
        entries = localdb.componentTestRun.find(query)
        if entries.estimated_document_count() == 0:
            if request.args.get("chip"):
                return_json.update({"warning": f"Not found test data ( ID: {tr_oid} )"})
                console_data = __pull_component(request.args["chip"])
            else:
                return_json = {
                    "message": f"Not found test data ( ID: {tr_oid} )",
                    "error": True,
                }
                return jsonify(return_json)
        else:
            console_data = __pull_test_run(tr_oid, request.args.get("chip"))
    elif request.args.get("chip"):
        return_json.update(
            {"warning": f"Not found test data of the component: {request.args['chip']}"}
        )
        console_data = __pull_component(request.args["chip"])
    else:
        return_json = {"message": "Not found test data", "error": True}
        return jsonify(return_json)

    if console_data.get("error", False):
        return_json = console_data
    else:
        return_json.update({"console_data": console_data})

    return jsonify(return_json)


@retrieve_api.route("/retrieve/list", methods=["GET"])
def retrieve_list():
    opt = request.args["opt"]
    return_json = {}

    if opt == "component":
        return_json.update({"parent": [], "child": {}})
        entries = localdb.childParentRelation.find()
        oids = []
        for entry in entries:
            oids.append(entry["parent"])
        oids = list(set(oids))
        for oid in oids:
            query = {"_id": ObjectId(oid)}
            this = localdb.component.find_one(query)
            if ObjectId.is_valid(this["user_id"]):
                query = {"_id": ObjectId(this["user_id"])}
                this_user = localdb.user.find_one(query)
            else:
                this_user = {"userName": ""}

            if ObjectId.is_valid(this["address"]):
                query = {"_id": ObjectId(this["address"])}
                this_site = localdb.institution.find_one(query)
            else:
                this_site = {"institution": ""}
            docs = {
                "oid": oid,
                "name": this["name"],
                "type": this["componentType"],
                "asic": this["chipType"],
                "chips": [],
                "user": this_user["userName"],
                "site": this_site["institution"],
            }
            query = {"parent": oid}
            entries = localdb.childParentRelation.find(query)
            for entry in entries:
                docs["chips"].append(entry["child"])
            return_json["parent"].append(docs)
        query = {"componentType": "front-end_chip"}
        entries = localdb.component.find(query)
        oids = []
        for entry in entries:
            oids.append(str(entry["_id"]))
        for oid in oids:
            query = {"_id": ObjectId(oid)}
            this = localdb.component.find_one(query)
            if ObjectId.is_valid(this["user_id"]):
                query = {"_id": ObjectId(this["user_id"])}
                this_user = localdb.user.find_one(query)
            else:
                this_user = {"userName": ""}

            if ObjectId.is_valid(this["address"]):
                query = {"_id": ObjectId(this["address"])}
                this_site = localdb.institution.find_one(query)
            else:
                this_site = {"institution": ""}
            return_json["child"].update(
                {
                    oid: {
                        "name": this["name"],
                        "type": this["componentType"],
                        "asic": this["chipType"],
                        "chipId": this["chipId"],
                        "user": this_user["userName"],
                        "site": this_site["institution"],
                    }
                }
            )
        return_json["parent"] = sorted(return_json["parent"], key=lambda x: (x["type"]))
    elif opt == "user":
        entries = localdb.user.find()
        users = []
        for entry in entries:
            users.append(entry["userName"])
        for user in users:
            query = {"userName": user}
            entries = localdb.user.find(query)
            docs = []
            for entry in entries:
                docs.append(entry["institution"])
            docs = list(set(docs))
            return_json.update({user: docs})
    elif opt == "site":
        entries = localdb.institution.find()
        docs_list = []
        for entry in entries:
            docs_list.append(entry["institution"])
        docs_list = list(set(docs_list))
        return_json.update({"site": docs_list})

    return jsonify(return_json)


def getData(i_type, i_dir, i_filename, i_data, i_bool=False):
    return {
        "type": i_type,
        "path": f"{i_dir}/{i_filename}",
        "data": i_data,
        "bool": i_bool,
    }
