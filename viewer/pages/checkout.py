# import module for metrology
import concurrent.futures
import copy
import hashlib
import logging
import pprint
import traceback
from datetime import datetime

import module_qc_database_tools as mqdbt
import module_qc_database_tools.db
from bson.objectid import ObjectId
from flask import (
    Blueprint,
    redirect,
    render_template,
    request,
    session,
    url_for,
)
from functions.checkouter import ScanCheckoutManager
from functions.common import (
    clear_message,
    create_message,
    initPage,
    localdb,
    userdb,
)

logger = logging.getLogger("localdb")
checkout_api = Blueprint("checkout_api", __name__)


@checkout_api.route("/pixel_failure_test", methods=["GET", "POST"])
def pixel_failure_test():
    initPage()

    session["regeneratePlots"] = request.form.get(
        "regeneratePlots", type=bool, default=False
    )
    session["expandPlots"] = request.form.get("expandPlots", type=bool, default=False)
    session["logged_in"] = session.get("logged_in", False)

    # If not logged in, prohibit operation
    if not session["logged_in"]:
        return render_template("401.html")

    # At the beginning, 3 parameters are passed from the browser:
    #  - 'componentID': componentID of LocalDB
    #  - 'stage': stage of the QC
    #  - 'test':  name of the Test

    m_doc = {"page": "pixel_failure_test"}

    # Define 4 states: selection, processing, confirmation, completed
    processFunc = {
        "not_logged_in": processPFT_not_logged_in,
        "selection": processPFT_selection,
        "complete": processPFT_complete,
    }

    # Retrieve the previous record of the checkout record using hash
    docHash = request.form.get("documentHash", "")
    logger.info(f"docHash from session = {docHash}")

    if docHash != "":
        logger.info(f"attempt to retrieve m_doc from hash = {docHash}")

        try:
            m_doc = userdb.QC.checkout.find_one({"hash": docHash})
            logger.info(f"successfully retrieved m_doc, hash = {docHash}")

        except Exception:
            logger.exception()

    # Update the current state of the processing
    m_doc["processState"] = request.form.get("processState", "selection")
    m_doc["componentID"] = request.args.get("componentID", "")

    logger.info(f'processState = {m_doc["processState"]}')

    if m_doc["processState"] == "selection":
        component = localdb.component.find_one({"_id": ObjectId(m_doc["componentID"])})
        m_doc["componentName"] = str(component.get("name"))
        m_doc["address"] = str(component.get("address"))
        m_doc["stage"] = request.args.get("stage", "")
        m_doc["test"] = request.args.get("test", "")
        m_doc["messageText"] = ""

    try:
        module_sn = get_module_sn_from_fe_sn(m_doc["componentName"])
        # MongoDB interface to organize the scan documents and carry out analysis
        checkouter = ScanCheckoutManager(
            m_doc["componentName"], m_doc["stage"], module_sn
        )

        m_doc = processFunc[m_doc["processState"]](m_doc, checkouter)

    except Exception as e:
        logger.error(f"{e}")
        logger.error(traceback.format_exc())

    if m_doc["processState"] == "selection":
        t_today = datetime.now()
        s_today = t_today.strftime("%Y/%m/%d %H:%M:%S")
        m_doc["hash"] = hashlib.sha256(s_today.encode("utf-8")).hexdigest()

        logger.info(f"document hash = {m_doc['hash']}")

    # Record the checkout contents to the server's memory
    if session["logged_in"]:
        userdb.QC.checkout.delete_one({"hash": m_doc["hash"]})
        userdb.QC.checkout.insert_one(m_doc)

    if m_doc["processState"] != "complete":
        return render_template("yarr_scans/select_table.html", m_doc=m_doc)

    return redirect(
        url_for(
            "component_api.show_component",
            id=m_doc["componentID"],
        )
    )


def processPFT_not_logged_in(m_doc, _checkouter):
    return m_doc


def processPFT_selection(m_doc, checkouter):
    logger.info("processPFT_selection")

    m_doc["run"] = []
    m_doc["scans"] = {}
    m_doc["scans"]["analysis_result"] = []
    m_doc["scans"].update(checkouter.getScans(m_doc["test"]))
    m_doc["title"] = "Select scan results for QC result in this stage."
    m_doc["run"] = checkouter.createFullSummary()

    return m_doc


def processPFT_complete(m_doc, checkouter):
    logger.info("processPFT_complete")

    m_doc["title"] = "Analysis process is running in the background. Stay tuned..."
    m_doc["selectedScans"] = {}

    this_run_ids = []
    run_ids_without_blank = []
    for scantype in m_doc["scans"]["scantypes"]:
        tr_oid = request.form.get(scantype + "_id")

        logger.info(f"selected {tr_oid} for scantype {scantype}")
        this_run_ids.append(tr_oid)

        m_doc["selectedScans"][scantype] = tr_oid

    for _id in this_run_ids:
        if _id != "":
            run_ids_without_blank.append(_id)

    applyAllFEs = request.form.get("applyAllFEs", type=bool, default=False)

    docs = []
    if not applyAllFEs:
        m_doc["serialNumber"] = checkouter.component_sn
        docs.append(m_doc)
    else:
        assert checkouter.module_sn
        cpt_module = localdb.component.find_one({"serialNumber": checkouter.module_sn})

        for child in mqdbt.db.local.get_children(
            localdb, cpt_module, component_type="front-end_chip"
        ):
            doc = copy.deepcopy(m_doc)
            doc["componentID"] = str(child["_id"])
            doc["serialNumber"] = mqdbt.db.local.get_serial_number(child)
            docs.append(doc)
            msg = f"Queuing to process complex analysis for {doc['serialNumber']} ({doc['componentID']})"
            logger.info(msg)

    executor = concurrent.futures.ThreadPoolExecutor()
    for doc in docs:
        executor.submit(complex_analysis_worker, doc, run_ids_without_blank)
    executor.shutdown(wait=False)

    logger.info("processPFT_complete(): analysis is done. Switch to confirmation")
    return docs[0]


def complex_analysis_worker(doc, run_ids_without_blank):
    module_sn = get_module_sn_from_fe_sn(doc["serialNumber"])

    checkouter = ScanCheckoutManager(doc["serialNumber"], doc["stage"], module_sn)

    msg_id = create_message(
        f'INFO: {doc.get("test")} processing for {doc.get("serialNumber")} is ongoing',
        function="complex_analysis_worker",
        component=doc.get("serialNumber"),
        code="INFO_COMPLEX_ANALYSIS_PROGRESS",
        can_dismiss=False,
    )

    try:
        checkouter.createElecTestDocs(run_ids_without_blank, doc)
        doc["processState"] = "complete"
        doc["title"] = "Check the analysis result and proceed."
        create_message(
            f'INFO: {doc.get("test")} processing for {doc.get("serialNumber")} is done',
            function="complex_analysis_worker",
            component=doc.get("serialNumber"),
            code="INFO_COMPLEX_ANALYSIS_PROGRESS",
            can_dismiss=True,
        )
    except Exception as exc:
        doc["processState"] = "selection"
        doc["title"] = "Please fix the analysis and retry."
        logger.exception()
        create_message(
            f"ERROR: {exc}. {traceback.format_exc()}",
            function="complex_analysis_worker",
            component=doc.get("serialNumber"),
            code="INFO_COMPLEX_ANALYSIS_PROGRESS",
            can_dismiss=True,
        )

    clear_message({"_id": msg_id}, forced=True)


def get_module_sn_from_fe_sn(fe_sn):
    cpt_doc = localdb.component.find_one({"serialNumber": fe_sn})
    cprs = list(localdb.childParentRelation.find({"child": str(cpt_doc.get("_id"))}))

    logger.info(pprint.pformat(cprs))

    module_sn = None
    for cpr in cprs:
        parent_doc = localdb.component.find_one({"_id": ObjectId(cpr.get("parent"))})
        logger.info(pprint.pformat(parent_doc))
        if parent_doc.get("componentType") == "module":
            module_sn = parent_doc.get("serialNumber")
            break

    return module_sn
