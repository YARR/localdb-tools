def setCount():  # TODO
    docs = {}
    query = {"resultId": str(this_tr["_id"])}
    thisRunInLocal = localdb.localdb.find_one(query)
    if thisRunInLocal:
        docs = thisRunInLocal["count"]
    else:
        writeDat(str(this_tr["_id"]))
        docs = {}
        if DOROOT:
            root.uuid = str(session.get("uuid", "localuser"))
            docs = root.countPix(run.get("test_type"), session["plotList"])
        document = {"resultId": str(this_tr["_id"]), "count": docs}
        localdb.localdb.insert_one(document)
    return docs
