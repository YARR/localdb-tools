import concurrent.futures
import contextlib
import logging
import os
import pprint
import shutil
import traceback
from datetime import datetime

from bson.objectid import ObjectId
from flask import (
    Blueprint,
    redirect,
    render_template,
    request,
    session,
    url_for,
)
from functions.common import (
    TMP_DIR,
    SN_tokens,
    SN_typeinfo,
    args,
    client,
    createScanCache,
    delim_SN,
    format_messages,
    get_localtime_str,
    get_pd_client,
    getScanSummary,
    initPage,
    localdb,
    userdb,
)
from functions.itkpd_interface.module import ModuleRegistration
from functions.workers import download_worker

logger = logging.getLogger("localdb")

toppage_api = Blueprint("toppage_api", __name__)


###############
# Show top page
@toppage_api.route("/", methods=["GET"])
def show_toppage():
    initPage()

    user_dir = "/".join([TMP_DIR, session["uuid"]])
    if os.path.isdir(user_dir):
        shutil.rmtree(user_dir)

    session.pop("signup", None)
    table_docs = {"page": None}

    query = {"auth": "adminViewer"}
    admin_info = (
        client.localdbtools.viewer.user.find_one(query, sort=[("_id", -1)]) or {}
    )
    admin_username = admin_info.get("name", "")

    user_name = session.get("username", "")
    table_docs["admin"] = "admin" if admin_username == user_name else "shomin"

    mail = args.fmail

    # messages
    messages = format_messages({"component": None})

    table_docs["messages"] = messages

    return render_template("toppage.html", table_docs=table_docs, mail=mail)


#####################
# Show component list
@toppage_api.route("/components", methods=["GET", "POST"])
def show_comps():
    initPage()

    table_docs = {"components": []}

    # messages
    messages = format_messages({"component": None})

    table_docs["messages"] = messages

    table_docs["view"] = request.args.get("view", "module")

    view = table_docs["view"]

    logger.debug(f"show_comps(): view = {view}")

    # Action modes
    archive_list = []
    doArchive = False
    if request.form.get("archive"):
        msg = f"show_comps(): archive = {request.form.get('archive')}"
        logger.debug(msg)
        doArchive = True

        for key in request.form:
            flag = request.form.get(key)
            if flag:
                archive_list.append(key[key.find("_") + 1 :])

    doShowAll = False
    if request.form.get("showall"):
        msg = f"show_comps(): showall = {request.form.get('showall')}"
        logger.debug(msg)
        doShowAll = request.form.get("showall") == "true"

    # get keywords
    if request.form.get("keywords"):
        table_docs["keywords"] = request.form.get("keywords", "")
        table_docs["match"] = request.form.get("match", "None")
    else:
        table_docs["keywords"] = request.args.get("keywords", "")
        table_docs["match"] = request.args.get("match", "None")

    if table_docs["keywords"] == "":
        doShowAll = True

    table_docs["doArchive"] = doArchive
    table_docs["doShowAll"] = doShowAll

    compsPerPage = 10

    thistime = datetime.utcnow()

    if not userdb.viewer.tag.categories.find_one({"name": "archived"}):
        userdb.viewer.tag.categories.insert_one(
            {
                "sys": {"rev": 0, "cts": thistime, "mts": thistime},
                "name": "archived",
                "user_id": None,
            }
        )

    page = 1
    with contextlib.suppress(ValueError):
        page = int(request.args.get("page", 1))

    # Define the aggregation pipeline
    pipeline = [
        {"$match": {"componentType": view}},
        {
            "$lookup": {
                "from": "childParentRelation",
                "let": {"componentId": "$_id"},
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$eq": ["$parent", {"$toString": "$$componentId"}]
                            }
                        }
                    }
                ],
                "as": "children_oids",
            }
        },
        {
            "$lookup": {
                "from": "childParentRelation",
                "let": {"componentId": "$_id"},
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {"$eq": ["$child", {"$toString": "$$componentId"}]}
                        }
                    }
                ],
                "as": "parents_oids",
            }
        },
        {
            "$project": {
                "_id": 1,
                "componentType": 1,
                "serialNumber": 1,
                "children_oids": {
                    "$map": {
                        "input": "$children_oids",
                        "as": "relation",
                        "in": {"child": {"$toObjectId": "$$relation.child"}},
                    }
                },
                "parents_oids": {
                    "$map": {
                        "input": "$parents_oids",
                        "as": "relation",
                        "in": {"parent": {"$toObjectId": "$$relation.parent"}},
                    }
                },
            }
        },
        {
            "$lookup": {
                "from": "component",
                "localField": "children_oids.child",
                "foreignField": "_id",
                "as": "children",
            }
        },
        {
            "$lookup": {
                "from": "component",
                "localField": "parents_oids.parent",
                "foreignField": "_id",
                "as": "parents",
            }
        },
        {
            "$project": {
                "_id": 1,
                "serialNumber": 1,
                "componentType": 1,
                "children": {
                    "$map": {
                        "input": "$children",
                        "as": "child",
                        "in": "$$child.serialNumber",
                    }
                },
                "parents": {
                    "$map": {
                        "input": "$parents",
                        "as": "parent",
                        "in": "$$parent.serialNumber",
                    }
                },
            }
        },
    ]

    if table_docs.get("keywords"):
        keywords = table_docs["keywords"].split()
        match_conditions = []

        if table_docs.get("match") == "partial":
            regex_pattern = f"({'|'.join(keywords)})"
            match_conditions.extend(
                [
                    {"serialNumber": {"$regex": regex_pattern, "$options": "i"}},
                    {
                        "children": {
                            "$elemMatch": {"$regex": regex_pattern, "$options": "i"}
                        }
                    },
                    {
                        "parents": {
                            "$elemMatch": {"$regex": regex_pattern, "$options": "i"}
                        }
                    },
                ]
            )
        else:
            match_conditions.extend(
                [
                    {"serialNumber": {"$in": keywords}},
                    {"children": {"$elemMatch": {"$in": keywords}}},
                    {"parents": {"$elemMatch": {"$in": keywords}}},
                ]
            )

        pipeline.append({"$match": {"$or": match_conditions}})

    pipeline.append({"$sort": {"_id": -1}})

    matched_components = list(localdb.component.aggregate(pipeline))

    # Add to matched_components the components associated to tags entered as keywords
    tag_components = []
    if table_docs["keywords"]:
        keywords = table_docs["keywords"].split()
        name_filter = {}

        if table_docs.get("match") == "partial":
            regex_pattern = f"({'|'.join(keywords)})"
            name_filter = {"$regex": regex_pattern, "$options": "i"}
        else:
            name_filter = {"$in": keywords}

        tag_components = list(
            userdb.viewer.tag.docs.find(
                {
                    "name": name_filter,
                    "$and": [
                        {"componentid": {"$exists": True}},
                        {"componentid": {"$ne": None}},
                    ],
                }
            )
        )

        tag_component_ids = [ObjectId(tag["componentid"]) for tag in tag_components]

        tag_components = list(
            localdb.component.find({"_id": {"$in": tag_component_ids}})
        )

    matched_components.extend(tag_components)

    # Post-processing: Fetch tags separately from userdb
    filtered_components = []
    for component in matched_components:
        component["tags"] = []
        for tag in userdb.viewer.tag.docs.find(
            {"componentid": str(component.get("_id"))}
        ):
            component["tags"].append(tag.get("name").upper())

        if not doShowAll and "archived" in component.get("tags", []):
            continue

        filtered_components.append(component)

    nModulesTotal = len(filtered_components)
    maxPage = len(filtered_components) // compsPerPage + 1
    minRange = max(1, page - 2)
    maxRange = min(maxPage, minRange + 3)
    pageRange = list(range(minRange, maxRange + 1))

    pagingInfo = {
        "thisPage": page,
        "range": pageRange,
        "minPage": 1,
        "maxPage": maxPage,
        "entries": nModulesTotal,
    }

    cmp_ids = []
    for component in filtered_components[
        (page - 1) * compsPerPage : page * compsPerPage - 1
    ]:
        logger.debug("component: {}".format(component.get("serialNumber")))
        cmp_id = str(component["_id"])

        ### tags
        tag_candidates = list(userdb.viewer.tag.categories.find({}))

        ### put tags
        tags = list(userdb.viewer.tag.docs.find({"componentid": cmp_id}))

        isArchived = False
        for tag in tags:
            if tag["name"] == "archived":
                isArchived = True

        if (not isArchived) and component["serialNumber"] in archive_list:
            tagsdocs = {
                "sys": {"rev": 0, "cts": thistime, "mts": thistime},
                "name": "archived",
                "componentid": cmp_id,
            }
            userdb.viewer.tag.docs.insert_one(tagsdocs)
            isArchived = True

        if not isArchived:
            cmp_ids.append(cmp_id)

        cps = []

        cprs1 = localdb.childParentRelation.find({"parent": cmp_id})
        cprs2 = localdb.childParentRelation.find({"child": cmp_id})

        for cpr in cprs1:
            if cpr is None:
                continue

            child = localdb.component.find_one({"_id": ObjectId(cpr["child"])})

            if child is None:
                continue

            logger.debug("child = %s", child)

            child_name = child["name"] or ""
            typeinfo = SN_typeinfo(child_name)

            if child_name.find("20UPGFC") == 0:
                hexName = f"{int(child_name[-7:]):#07x}"
            else:
                hexName = ""

            cps.append(
                {
                    "_id": str(child["_id"]),
                    "name": child["name"],
                    "grade": {},
                    "hex": hexName,
                    "typeinfo": typeinfo,
                }
            )

        # endfor

        for cpr in cprs2:
            parent = localdb.component.find_one({"_id": ObjectId(cpr["parent"])})

            # logger.info( 'parent = {}'.format( parent ) )

            if parent:
                parent_name = parent["name"]

                typeinfo = SN_typeinfo(parent_name)

                cps.append(
                    {
                        "_id": str(parent["_id"]),
                        "name": parent["name"],
                        "grade": {},
                        "hex": hexName if parent_name.find("20UPGFC") == 0 else "",
                        "typeinfo": typeinfo,
                    }
                )

        # endfor

        name = component["serialNumber"]

        typeinfo = SN_typeinfo(name)

        qcStatus = localdb.QC.module.status.find_one({"component": cmp_id})

        result = None
        results = localdb.QC.result.find({"component": cmp_id})
        for r in results:
            result = r

        component_data = {
            "_id": cmp_id,
            "name": name,
            "typeinfo": typeinfo,
            "cps": cps,
            "grade": {},
            "proDB": component.get("proDB", False),
            "component_tag_candidate": tag_candidates,
            "component_tag": tags,
        }

        if qcStatus:
            component_data.update({"stage": qcStatus["stage"]})

        if result:
            logger.debug(pprint.pformat(result))
            component_data.update(
                {
                    "runId": str(result["_id"]),
                    "datetime": get_localtime_str(result["sys"]["mts"]),
                    "testType": result.get("testType"),
                    "user": result.get("user"),
                    "site": result.get("address"),
                }
            )

        # component search field
        search_targets = []
        search_targets.append(component_data["name"])
        if "stage" in component_data:
            search_targets.append(component_data["stage"])
        if "user" in component_data:
            search_targets.append(component_data["user"])
        if "site" in component_data:
            search_targets.append(component_data["site"])
        if "datetime" in component_data:
            search_targets.append(component_data["datetime"])

        for tag in component_data["component_tag"]:
            search_targets.append(tag["name"])

        for subcomp in component_data["cps"]:
            search_targets.append(subcomp["name"])

        if doShowAll:
            table_docs["components"].append(component_data)
        else:
            if not isArchived:
                table_docs["components"].append(component_data)

    # end for component in components:

    table_docs.update({"pagingInfo": pagingInfo})
    table_docs["page"] = "components"
    table_docs["title"] = f"{view.upper()} List"

    return render_template(
        "toppage.html",
        table_docs=table_docs,
    )


####################
# Show test run list
@toppage_api.route("/yarr_scans", methods=["GET", "POST"])
def show_scans():
    initPage()
    check_database_update()

    max_num = 15
    sort_cnt = 0
    if request.args.get("p", 0) != "":
        sort_cnt = int(request.args.get("p", 0))

    table_docs = {"run": []}

    # Action modes
    archive_list = []
    doArchive = False
    if request.form.get("archive"):
        msg = f"show_comps(): archive = {request.form.get('archive')}"
        logger.debug(msg)
        doArchive = True

        for key in request.form:
            flag = request.form.get(key)
            if flag:
                archive_list.append(key[key.find("_") + 1 :])

    doShowAll = False
    if request.form.get("showall"):
        msg = f"show_comps(): showall = {request.form.get('showall')}"
        logger.debug(msg)
        doShowAll = request.form.get("showall") == "true"

    # get keywords
    if request.form.get("keywords") is not None:
        table_docs["keywords"] = request.form.get("keywords", "")
        table_docs["match"] = request.form.get("match", "None")
    else:
        table_docs["keywords"] = request.args.get("keywords", "")
        table_docs["match"] = request.args.get("match", "None")

    if table_docs["keywords"] == "":
        doShowAll = True

    table_docs["doArchive"] = doArchive
    table_docs["doShowAll"] = doShowAll

    logger.info(table_docs["keywords"])
    run_ids = [str(tr.get("_id")) for tr in localdb.testRun.find({}).sort("_id", -1)]
    msg = f"[Component.py] show_scans {len(run_ids)}"
    logger.info(msg)

    if "config" in run_ids:
        run_ids.remove("config")

    nrun_entries = len(run_ids)
    run_num_list = []
    if nrun_entries < (sort_cnt + 1) * max_num:
        for i in range(nrun_entries % max_num):
            run_num_list.append(sort_cnt * (max_num) + i)
    else:
        for i in range(max_num):
            run_num_list.append(sort_cnt * (max_num) + i)

    thistime = datetime.utcnow()

    if not userdb.viewer.tag.categories.find_one({"name": "archived"}):
        userdb.viewer.tag.categories.insert_one(
            {
                "sys": {"rev": 0, "cts": thistime, "mts": thistime},
                "name": "archived",
                "user_id": None,
            }
        )

    run_counts = 0
    if nrun_entries != 0:
        for i in run_num_list:
            try:
                run_data = getScanSummary(run_ids[i])

                isArchived = False
                tags = run_data["testRun_tag"]
                for tag in tags:
                    if tag["name"] == "archived":
                        isArchived = True
                        break

                if (not isArchived) and str(run_data["_id"]) in archive_list:
                    tagsdocs = {
                        "sys": {"rev": 0, "cts": thistime, "mts": thistime},
                        "name": "archived",
                        "runId": str(run_data["_id"]),
                    }
                    userdb.viewer.tag.docs.insert_one(tagsdocs)
                    userdb.viewer.query.update_one(
                        {"runId": str(run_data["_id"])},
                        {"$push": {"data": tagsdocs["name"]}},
                    )
                    isArchived = True

                if doShowAll:
                    table_docs["run"].append({"run_data": run_data, "nrun": run_counts})
                    run_counts += 1

                else:
                    if not isArchived:
                        table_docs["run"].append(
                            {"run_data": run_data, "nrun": run_counts}
                        )
                        run_counts += 1
            except Exception as e:
                logger.error(f"{e}")
                logger.error(traceback.format_exc())

    table_docs["run"] = sorted(
        table_docs["run"], key=lambda x: x["run_data"]["datetime"], reverse=True
    )
    cnt = []
    for i in range(((nrun_entries - 1) // max_num) + 1):
        if sort_cnt - (max_num / 2) < i:
            cnt.append(i)
        if len(cnt) == max_num:
            break

    scans_num = {
        "total": run_counts,
        "cnt": cnt,
        "now_cnt": sort_cnt,
        "max_cnt": ((nrun_entries - 1) // max_num),
        "max_num": max_num,
    }
    table_docs.update(scans_num)

    # table_docs['run'] = sorted(table_docs['run'], key=lambda x:x['run_data']['datetime'], reverse=True)

    table_docs["page"] = "yarr_scans"
    table_docs["title"] = "YARR Scan List"

    return render_template(
        "toppage.html",
        table_docs=table_docs,
    )


####################
# Show test run list


@toppage_api.route("/assemble_module", methods=["GET", "POST"])
def assemble_module():
    initPage()

    if not session.get("logged_in", False):
        return render_template("401.html")

    table_docs = {"components": {}}
    table_docs["page"] = "module"
    table_docs["title"] = "Assemble a new Module on ITkPD"

    stage = request.form.get("stage", "input_module")
    moduleinfo = request.form.getlist("moduleinfo")
    tripletinfo = request.form.getlist("tripletinfo")

    check_serialNum = ""
    module_serial_number = ""
    text = ""
    config = {}
    lookup_table = {}
    currentLocation = {}
    typeinfos = {}

    if not get_pd_client():
        message = "No valid ITkPD client found. Please login."
        return redirect(
            url_for(
                "user_api.itkdb_authenticate",
                message=message,
                redirect=url_for("toppage_api.assemble_module"),
            )
        )

    pd_client = get_pd_client()

    user = pd_client.get("getUser", json={"userIdentity": pd_client.user.identity})
    user_institutions = [
        element["name"] for element in (user.get("institutions") or [])
    ]

    if session["institution"] not in user_institutions:
        _institutions_string = "', '".join(user_institutions)
        message = f"LocalDB user institution '{session['institution']}' does not match any any of the authenticated institutions from the authenticated ITkPD user: '{_institutions_string}'. This can be fixed by either using a different production database user account, or changing the institution for the currently logged-in localDB user at the top right."
        return redirect(
            url_for(
                "user_api.itkdb_authenticate",
                message=message,
                redirect=url_for("toppage_api.assemble_module"),
            )
        )

    if pd_client:
        try:
            module_config = userdb["QC.module.types"].find_one()
            lookup_table = ModuleRegistration(pd_client).make_table(module_config)
        except Exception as e:
            logger.error(str(e))
            logger.error(traceback.format_exc())
            stage = "download_moduletypes"

    institution = ModuleRegistration(pd_client).user_institutions(pd_client.user)

    logger.info(f"user's institution: {institution}")

    if stage != "input_module":
        # moduleinfo[0]: bare_module_SNs
        # moduleinfo[1]: PCB_SN

        bare_serial_numbers = [moduleinfo[0]]
        pcb_serial_number = moduleinfo[1]
        carrier_serial_number = moduleinfo[2]

        assembly_doc = {}

        try:
            assembly_doc.update(
                {
                    bare_serial_numbers[0]: check_component(
                        bare_serial_numbers[0],
                        "BARE_MODULE",
                        "Bare Module",
                        institution,
                    )
                }
            )
            assembly_doc.update(
                {
                    pcb_serial_number: check_component(
                        pcb_serial_number, "PCB", "Module PCB", institution
                    )
                }
            )

            bare1_tokens = SN_tokens(bare_serial_numbers[0])
            pcb_tokens = SN_tokens(pcb_serial_number)

            module_type_candidates = {
                typename: data.get("XX") + data.get("YY")
                for typename, data in lookup_table.get("MODULE_TYPES").items()
                if data.get("BARE_MODULE") == bare1_tokens.get("XXYY")
                and data.get("PCB") == pcb_tokens.get("XXYY")
            }

            module_xxyy_candidates = [
                xxyy for typename, xxyy in module_type_candidates.items()
            ]
            module_typename_candidates = [
                name for name, xxyy in module_type_candidates.items()
            ]

            if len(module_xxyy_candidates) == 0:
                msg = f'ERROR: Specified PCB Type "{pcb_tokens.get("XXYY")}" and BareModule Type "{bare1_tokens.get("XXYY")}" are not compatible to deduce a valid Module Type'
                raise ValueError(msg)

            if len(module_xxyy_candidates) > 1:
                logger.info(
                    f'Specified PCB Type "{pcb_tokens.get("XXYY")}" and BareModule Type "{bare1_tokens.get("XXYY")}" do not deduce a unique Module Type: candidates = {module_xxyy_candidates}'
                )
                logger.info("Attempting to resolve it using BareModule property...")

                pd_bare_cpt = pd_client.get(
                    "getComponent", json={"component": bare_serial_numbers[0]}
                )

                bare_subcpts = pd_bare_cpt.get("children")

                sensor_sn = None
                for subcpt in bare_subcpts:
                    if subcpt.get("componentType").get("code") == "SENSOR_TILE":
                        subcpt_info = subcpt.get("component")
                        if not subcpt_info:
                            continue

                        sensor_sn = subcpt.get("component").get("serialNumber")

                if not sensor_sn:
                    msg = f"Bare Module {bare_serial_numbers[0]} does not have its sensor tile!"
                    raise ValueError(msg)

                if sensor_sn:
                    logger.info(
                        "Sensor_tile is found on the bare module. Using the bare module SENSOR_TILE property for module SN resolution"
                    )

                    sensor_xx = sensor_sn[3:5]

                    if sensor_xx == "PG":
                        module_xxyy_candidates = [
                            xxyy for xxyy in module_xxyy_candidates if "PG" in xxyy
                        ]
                        module_typename_candidates = [
                            name
                            for name, xxyy in module_type_candidates.items()
                            if "PG" in xxyy
                        ]
                    else:
                        module_xxyy_candidates = [
                            xxyy for xxyy in module_xxyy_candidates if "PI" in xxyy
                        ]
                        module_typename_candidates = [
                            name
                            for name, xxyy in module_type_candidates.items()
                            if "PI" in xxyy
                        ]

                else:
                    logger.info(
                        "No sensor_tile is found on the bare module. As a fallback, attempt to use the bare module SENSOR_TILE property for module SN resolution"
                    )
                    bare_props = pd_bare_cpt.get("properties")

                    layer_info = next(
                        [
                            prop["value"]
                            for prop in bare_props
                            if prop["code"] == "SENSOR_TYPE"
                        ]
                    )

                    if not layer_info:
                        msg = "No SENSOR_TYPE property is recorded on the bare module property!"
                        raise ValueError(msg)

                    if "Outer" in layer_info:
                        module_xxyy_candidates = [
                            xxyy for xxyy in module_xxyy_candidates if "PG" in xxyy
                        ]
                        module_typename_candidates = [
                            name
                            for name, xxyy in module_type_candidates.items()
                            if "PG" in xxyy
                        ]
                    else:
                        module_xxyy_candidates = [
                            xxyy for xxyy in module_xxyy_candidates if "PI" in xxyy
                        ]
                        module_typename_candidates = [
                            name
                            for name, xxyy in module_type_candidates.items()
                            if "PI" in xxyy
                        ]

            if len(module_xxyy_candidates) > 1:
                msg = f'ERROR: Specified PCB Type "{pcb_tokens.get("XXYY")}" and BareModule Type "{bare1_tokens.get("XXYY")}" do not deduce a unique Module Type: candidates = {module_xxyy_candidates}'
                raise ValueError(msg)

            module_xxyy = module_xxyy_candidates[0]
            module_typename = module_typename_candidates[0]

            if tripletinfo:
                module_serial_number = "".join(
                    [
                        "20",
                        "U",
                        module_xxyy,
                        "2",
                        bare1_tokens.get("1st"),
                        "0",
                        pcb_tokens.get("number")[-4:],
                    ]
                )
            else:
                module_serial_number = "".join(
                    [
                        "20",
                        "U",
                        module_xxyy,
                        bare1_tokens.get("1st"),
                        pcb_tokens.get("number")[-6:],
                    ]
                )

            moduleinfo += [module_typename]

        except Exception as e:
            typeinfos = {
                "BARE_MODULE": [SN_typeinfo(bare, 1) for bare in bare_serial_numbers],
                "PCB": SN_typeinfo(pcb_serial_number, 1),
                "MODULE": SN_typeinfo(module_serial_number, 1),
            }

            text = str(e)
            logger.error(traceback.format_exc())
            stage = "confirm"
            return render_template(
                "assemble_module.html",
                table_docs=table_docs,
                stage=stage,
                text=text,
                moduleInfo=moduleinfo,
                lookup_table=lookup_table,
                module_serial_number=module_serial_number,
                institution=institution,
                tripletInfo=tripletinfo,
                check_serialNum=check_serialNum,
                currentLocation=currentLocation,
                typeinfos=typeinfos,
            )

        if tripletinfo:
            # Triplet Check
            bare_serial_numbers += tripletinfo
            try:
                assembly_doc.update(
                    {
                        bare_serial_numbers[1]: check_component(
                            bare_serial_numbers[1],
                            "BARE_MODULE",
                            "Bare Module",
                            institution,
                        )
                    }
                )
                assembly_doc.update(
                    {
                        bare_serial_numbers[2]: check_component(
                            bare_serial_numbers[2],
                            "BARE_MODULE",
                            "Bare Module",
                            institution,
                        )
                    }
                )

                bare2_tokens = SN_tokens(bare_serial_numbers[1])
                bare3_tokens = SN_tokens(bare_serial_numbers[2])

                if bare1_tokens.get("XXYY") != bare2_tokens.get("XXYY"):
                    msg = f'ERROR: Specified PCB Type "{pcb_tokens.get("XXYY")}" and BareModule Types ["{bare1_tokens.get("XXYY")}", "{bare2_tokens.get("XXYY")}", "{bare2_tokens.get("XXYY")}" ] are not compatible to deduce a valid Module Type'
                    raise ValueError(msg)

                if bare1_tokens.get("XXYY") != bare3_tokens.get("XXYY"):
                    msg = f'ERROR: Specified PCB Type "{pcb_tokens.get("XXYY")}" and BareModule Types ["{bare1_tokens.get("XXYY")}", "{bare2_tokens.get("XXYY")}", "{bare2_tokens.get("XXYY")}" ] are not compatible to deduce a valid Module Type'
                    raise ValueError(msg)

            except Exception as e:
                text = str(e)
                logger.error(traceback.format_exc())
                stage = "confirm"

                typeinfos = {
                    "BARE_MODULE": [
                        SN_typeinfo(bare, 1) for bare in bare_serial_numbers
                    ],
                    "PCB": SN_typeinfo(pcb_serial_number, 1),
                    "MODULE": SN_typeinfo(module_serial_number, 1),
                }

                return render_template(
                    "assemble_module.html",
                    table_docs=table_docs,
                    stage=stage,
                    text=text,
                    moduleInfo=moduleinfo,
                    lookup_table=lookup_table,
                    module_serial_number=module_serial_number,
                    institution=institution,
                    tripletInfo=tripletinfo,
                    check_serialNum=check_serialNum,
                    currentLocation=currentLocation,
                    typeinfos=typeinfos,
                )

        logger.info(f"Synthesized Module Serial Number = {module_serial_number}")

        # logger.info( 'assembly_doc = ' + pprint.pformat( assembly_doc ) )

        current_locations = [doc.get("location") for sn, doc in assembly_doc.items()]

        location = current_locations[0]

        if not all(loc == current_locations[0] for loc in current_locations):
            text = "The current location of sub-components are not all identical!"
            stage = "confirm"
            return render_template(
                "assemble_module.html",
                table_docs=table_docs,
                stage=stage,
                text=text,
                moduleInfo=moduleinfo,
                lookup_table=lookup_table,
                module_serial_number=module_serial_number,
                institution=institution,
                tripletInfo=tripletinfo,
                check_serialNum=check_serialNum,
                currentLocation=currentLocation,
                typeinfos=typeinfos,
            )

        config = {
            "type": module_typename,
            "FECHIP": bare1_tokens.get("1st"),
            "child": {
                "BARE_MODULE": bare_serial_numbers,
                "PCB": pcb_serial_number,
                "CARRIER": (
                    carrier_serial_number if carrier_serial_number != "" else None
                ),
            },
            "serialNumber": module_serial_number,
        }

        # Check the module serial number if available
        check_serialNum = ModuleRegistration(pd_client).check_exist(
            module_serial_number
        )

        if check_serialNum == "2":
            pass
        else:
            stage = "confirm"

        typeinfos = {
            "BARE_MODULE": [SN_typeinfo(bare, 1) for bare in bare_serial_numbers],
            "PCB": SN_typeinfo(pcb_serial_number, 1),
            "MODULE": SN_typeinfo(module_serial_number, 1),
        }

    if stage == "complete":
        ModuleRegistration(pd_client).register_Module(config, lookup_table, location)

        with concurrent.futures.ThreadPoolExecutor() as executor:
            executor.submit(
                download_worker, module_serial_number, None, "MODULE/ASSEMBLY"
            )

    return render_template(
        "assemble_module.html",
        table_docs=table_docs,
        stage=stage,
        text=text,
        moduleInfo=moduleinfo,
        lookup_table=lookup_table,
        module_serial_number=module_serial_number,
        institution=institution,
        tripletInfo=tripletinfo,
        check_serialNum=check_serialNum,
        currentLocation=currentLocation,
        typeinfos=typeinfos,
    )


def check_component(serialNumber, componentType, componentTypeName, user_institution):
    pd_client = get_pd_client()

    try:
        componentInfo = ModuleRegistration(pd_client).get_component(serialNumber)
    except Exception as exc:
        logger.error(traceback.format_exc())
        text = f"{componentTypeName} {serialNumber} does not exist in ITkPD..."
        raise RuntimeError(text) from exc

    checked_component_type = componentInfo.get("componentType")["code"]
    checked_component_location = componentInfo["currentLocation"]["code"]
    if checked_component_type != componentType:
        text = f"{componentTypeName} {serialNumber} component type is not compatible (is a {checked_component_type})..."
        raise ValueError(text)
    if checked_component_location not in user_institution:
        text = f"{componentTypeName} {serialNumber} location is not your institution ({checked_component_location}))..."
        raise ValueError(text)

    doc = {
        "location": checked_component_location,
        "typeInfo": SN_typeinfo(serialNumber),
        "delim_SN": delim_SN(serialNumber),
    }

    logger.info(
        f"check_component(): serialNumber = {serialNumber}, doc = "
        + pprint.pformat(doc)
    )

    return doc


#################
## Query Function
def query_docs(Keywords, Match):
    run_ids = []
    if Keywords in ["None", ""]:
        entries = userdb.viewer.query.find({})
    else:
        Keyword = Keywords.split(" ")
        Keyword_num = len(Keyword)
        AND_flag = 0
        OR_flag = 0

        msg = f"Keyword_num {Keyword_num}"
        logger.info(msg)
        for i in range(Keyword_num):
            if Keyword[i].upper() == "AND":
                AND_flag = 1
            if Keyword[i].upper() == "OR":
                OR_flag = 1

        msg = f"AND_flag {AND_flag!r}\nOR_flag {OR_flag!r}"
        logger.info(msg)

        query_list = []
        if AND_flag == 1 and OR_flag == 1:
            entries = userdb.viewer.query.find({})
        elif AND_flag == 0 and OR_flag == 1:
            for i in range(Keyword_num):
                if Keyword[i] != "":
                    if Match == "partial":
                        query_list.append({"data": {"$regex": Keyword[i]}})
                    if Match == "perfect":
                        query_list.append({"data": Keyword[i]})
            entries = userdb.viewer.query.find({"$or": query_list})
        else:
            for i in range(Keyword_num):
                if Keyword[i].upper() == "AND":
                    pass
                else:
                    if Match == "partial":
                        query_list.append({"data": {"$regex": Keyword[i]}})
                    if Match == "perfect":
                        query_list.append({"data": Keyword[i]})
            # Here also inside the viewer.query so I need to update this
            entries = userdb.viewer.query.find({"$and": query_list})

    components = sorted(entries, key=lambda x: x["timeStamp"], reverse=True)

    for component in components:
        run_ids.append(component["runId"])

    return run_ids


def query_keywords(Keywords, query_targets, Match):
    flag = 1
    if Keywords in ["None", ""]:
        pass
    else:
        Keyword = Keywords.split(" ")
        Keyword_num = len(Keyword)
        AND_flag = 0
        OR_flag = 0
        for i in range(Keyword_num):
            if Keyword[i].upper() == "AND":
                AND_flag = 1
            if Keyword[i].upper() == "OR":
                OR_flag = 1

        if AND_flag == 1 and OR_flag == 1:
            pass
        elif AND_flag == 0 and OR_flag == 1:
            flag = 0
            for i in range(Keyword_num):
                for query_target in query_targets:
                    if (
                        Match == "partial"
                        and Keyword[i].upper() in str(query_target).upper()
                    ):
                        flag = 1
                    if (
                        Match == "perfect"
                        and Keyword[i].upper() == str(query_target).upper()
                    ):
                        flag = 1
        else:
            Keyword_flag = []
            num = []
            delete_num = []
            for i in range(Keyword_num):
                num.append(i)
                if Keyword[i].upper() == "AND":
                    delete_num.append(i)
                Keyword_flag.append(0)
                for query_target in query_targets:
                    if (
                        Match == "partial"
                        and Keyword[i].upper() in str(query_target).upper()
                    ):
                        Keyword_flag[i] = 1
                    if (
                        Match == "perfect"
                        and Keyword[i].upper() == str(query_target).upper()
                    ):
                        Keyword_flag[i] = 1
            query_num = list(set(num) - set(delete_num))
            for i in query_num:
                if Keyword_flag[i] == 0:
                    flag = 0
    return flag


def check_database_update():
    config_doc = userdb.viewer.query.find_one({"runId": "config"})
    #    query_list = [{"sys.cts": {"$gt":config_doc["sys"]["mts"]}},{"sys.mts": {"$gt":config_doc["sys"]["mts"]}}]
    #    append_docs = localdb.testRun.find({'$or':query_list}).sort([('startTime', -1)])
    if config_doc is None:
        config_doc = {
            "runId": "config",
            "timeStamp": datetime.utcnow(),
            "sys": {"cts": datetime.utcnow(), "mts": datetime.utcnow(), "rev": 0},
        }
        userdb.viewer.query.insert_one(config_doc)

    queries = {"sys.cts": {"$gte": config_doc["sys"]["mts"]}}

    append_docs = list(localdb.testRun.find(queries).sort([("startTime", -1)]))

    for entry in append_docs:
        userdb.viewer.query.delete_one({"runId": str(entry["_id"])})
        docs = createScanCache(entry)
        userdb.viewer.query.insert_one(docs)

    userdb.viewer.query.update_one(
        {"runId": "config"}, {"$set": {"sys.mts": datetime.utcnow()}}
    )
    userdb.viewer.query.update_one(
        {"runId": "config"}, {"$set": {"sys.rev": config_doc["sys"]["rev"] + 1}}
    )

    return 0
