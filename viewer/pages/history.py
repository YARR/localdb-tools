import contextlib
import json
import logging
import traceback
from datetime import datetime

import matplotlib.pyplot as plt
import numpy as np
from bson.objectid import ObjectId
from flask import Blueprint, render_template, request, url_for
from functions.common import (
    CACHE_DIR,
    initPage,
    localdb,
    userdb,
)
from module_qc_analysis_tools import data as datadir

logger = logging.getLogger("localdb")
history_api = Blueprint("history_api", __name__)


# display component page
@history_api.route("/history", methods=["GET", "POST"])
def show_history():
    initPage()

    # Get Module ID and Name from initial click on page
    # print("Request:", request)
    moduleId = request.args.get("id")
    moduleName = localdb.component.find_one({"_id": ObjectId(moduleId)})["name"]

    # Use different dbs to get relevant chip info from module ID and ESUM document
    ESUMS = get_ESUMS(moduleId)
    chips = get_chips(moduleId)
    testnames = get_testnames(ESUMS)
    testnames_for_html = get_testnames_for_html(testnames)
    analysis_cuts = get_analysis_cuts()

    # Check if plots need to be remade
    force_remake_plots_string = request.args.get("remake", "")
    force_remake_plots_list = parse_string(force_remake_plots_string, testnames)
    # Check what plots should be displayed
    plot2display_string = request.args.get("display", "")
    display_plots_list = parse_string(plot2display_string, testnames)

    # Generate figures that show history of module parameters
    histories = {}
    for testname in testnames:
        force_remake_plot = False
        if testname in force_remake_plots_list:
            force_remake_plot = True
        if testname in display_plots_list:
            histories[testname] = get_QCtest_results(
                testname, ESUMS, chips, moduleName, analysis_cuts, force_remake_plot
            )
        else:
            histories[testname] = []

    remakeLinks = {}
    buttonNames = {"main": "Make All"}
    for testname in testnames:
        remakeLinks[testname] = generate_remake_link(
            moduleId, force_remake_plots_list, display_plots_list, testname
        )
        buttonNames[testname] = generate_button_name(display_plots_list, testname)

    # Render figures in html document
    return render_template(
        "history.html",
        moduleName=moduleName,
        histories=histories,
        testnames=testnames_for_html,
        remakeLinks=remakeLinks,
        buttonNames=buttonNames,
        mainlink=generateMainButton(moduleId, testnames),
    )


# Gets a string that looks like "SLDO VCAL_CALIBRATION"
# and turns it into an array ["SLDO", "VCAL_CALIBRATION"]
def parse_string(string, testnames):
    if string.lower() == "all":
        return testnames(testnames)
    return string.split(" ")


# Says whether a button name to (re)make plots should be
# make or remake
def generate_button_name(display_plots_list, testname):
    if testname in display_plots_list:
        return "Remake Plots"
    return "Make Plots"


# When clicking on a button on the history page that says "remake", it will
# take you to a url with new arguments that instruct the page to remake some
# number of plots and display some number. This function handles the logic to
# create this url from the moduleId and list of things that are currently
# being made/displayed
def generate_remake_link(moduleId, remake_list, display_list, testname):
    # Add currently displaying to will display
    display_list_new = []
    for item in display_list:
        if len(item) > 0:
            display_list_new.append(item)

    # Remove everything from remake, put in display
    for item in remake_list:
        if item not in display_list_new and len(item) > 0:
            display_list_new.append(item)

    # Only display the requested test
    remake_list_new = [testname]

    # Check if testname in display_list. If not, put it in
    if testname not in display_list_new:
        display_list_new.append(testname)

    return getURLfromlists(moduleId, remake_list_new, display_list_new)


# Generates url for the main "Make All" plots at
# the top of the page
def generateMainButton(moduleId, testnames):
    return getURLfromlists(moduleId, testnames, testnames)


# Handles logic to take lists of things you want to display
# and make plots for to generate URL
def getURLfromlists(moduleId, remake_list, display_list):
    # Generate url components
    startstring = "/localdb/history?"
    id_portion = "id=" + str(moduleId)
    remake_portion = "&remake="
    display_portion = "&display="
    for item in remake_list:
        remake_portion += str(item) + "+"
    for item in display_list:
        display_portion += str(item) + "+"

    # Remove final +
    if display_portion[-1] == "+":
        display_portion = display_portion[:-1]
    if remake_portion[-1] == "+":
        remake_portion = remake_portion[:-1]

    # Create and return  url
    return startstring + id_portion + remake_portion + display_portion


# Returns the results a specific QCtest (i.e. the final reported parameters
# Need to pass testname e.g. ADC_CALIBRATION, chips, moduleName, and ESUM
# Works with anything where the esum has something like
# MODULE_TESTNAME_FE_1_LINK = {IDNUMBER}
# The results come in the form of plots of history of the module
def get_QCtest_results(
    testname, ESUMS, chips, moduleName, analysis_cuts, force_remake_plots
):
    test_parameter_codes = []
    test_parameter_names = []
    test_parameter_descs = []
    test_dict = userdb.QC.tests.find_one({"code": testname})["parameters"]
    for parameter in test_dict:
        test_parameter_codes.append(parameter["code"])
        test_parameter_descs.append(parameter["description"])
        test_parameter_names.append(parameter["name"])
    num_params = len(test_parameter_codes)
    test_datas = [np.zeros((len(ESUMS), len(chips))) for ii in range(num_params)]
    test_figures = []

    analysis_cuts_for_test = analysis_cuts.get(testname, {})

    for i, key in enumerate(ESUMS):
        for j in range(len(chips)):
            keystring = "MODULE_" + testname + "_FE_LINK_" + str(j + 1)
            idlink = ESUMS[key][keystring]
            if not idlink:
                logger.warning(
                    f"There is no valid link in {key}/E_SUMMARY: {keystring}. Skipping."
                )
                continue

            qc_result = localdb.QC.result.find_one({"_id": ObjectId(idlink)})
            if not qc_result:
                logger.warning(
                    f"Could not find link in {key}/E_SUMMARY: {keystring}={idlink}. Skipping."
                )
                continue

            test_result = qc_result.get("results", {})
            for param_index, param_name in enumerate(test_parameter_codes):
                # Temporary: skip lists to be able to plot some Analog Readback parameters
                value = test_result.get(param_name, -99.0)
                if not isinstance(value, list):
                    test_datas[param_index][i, j] = value

    for i in range(num_params):
        test_figures.append(
            grab_plot(
                moduleName,
                testname,
                test_datas[i],
                test_parameter_codes[i],
                ESUMS.keys(),
                chips,
                analysis_cuts_for_test,
                force_remake_plots,
            )
        )

    return test_figures


# Get json from qc-analysis-tools that has analysis cuts in it
# return dict of analysis cuts
def get_analysis_cuts():
    try:
        analysis_cuts = json.loads(datadir.joinpath("analysis_cuts.json").read_text())
    except Exception:
        logger.error(traceback.format_exc())
        analysis_cuts = {}
    return analysis_cuts


# Get ESummary for the Module
def get_ESUMS(moduleId):
    logger.info(f"moduleId = {moduleId}")

    stages_info = userdb.QC.stages.find_one({"code": "MODULE"})
    stages = stages_info["stage_flow"]
    stages_tests_mapping = stages_info["stage_test"]
    ESUMS = {}
    for stage in stages:
        # only look at stages that have E_SUMMARY
        tests_in_stage = stages_tests_mapping[stage]
        if "E_SUMMARY" not in tests_in_stage:
            continue
        qc_record = localdb.QC.module.status.find_one({"component": moduleId})
        result_id = qc_record.get("QC_results", {}).get(stage, {}).get("E_SUMMARY")
        if not result_id or result_id == "-1":
            logger.warning(f"no record for {stage}")
        else:
            result = localdb.QC.result.find_one({"_id": ObjectId(result_id)}).get(
                "results"
            )
            ESUMS[stage] = result

    return ESUMS


# Get Chip Names for the Module
def get_chips(moduleId):
    chips = []

    subcpts = localdb.childParentRelation.find({"parent": moduleId})
    for subcpt in subcpts:
        with contextlib.suppress(Exception):
            subcpt_doc = localdb.component.find_one(
                {
                    "_id": ObjectId(subcpt.get("child")),
                    "componentType": "front-end_chip",
                }
            )

        subcpt_SN = None
        if subcpt_doc is not None:
            subcpt_SN = subcpt_doc.get("serialNumber")
        if subcpt_SN is not None:
            chips += [subcpt_SN]

    return chips


# Similar to function below but gets names of tests rather than codes of tests
# E.g. this returns things like "ADC Calibration" rather than the below function
# that returns things like "ADC_CALIBRATION"
def get_testnames_for_html(codes):
    testnames = {"BAD_PIXEL": "Pixel Failure Analysis"}
    for code in codes:
        testnames[code] = userdb.QC.tests.find_one({"code": code})["name"]
    return testnames


# Get names of tests in ESUM that can be passed to get_QCtest_result
def get_testnames(ESUMS):
    ex_ESUM = ESUMS[next(iter(ESUMS.keys()))] if ESUMS else {}
    # Replace something like "MODULE_ADC_CALIBRATION" with something like "ADC_CALIBRATION"
    key_names = [key.replace("MODULE_", "") for key in ex_ESUM]
    return userdb.QC.tests.distinct("code", {"code": {"$in": key_names}})


# Handles logic to decide whether plot needs to be remade or not
# And creates relevant directory
def grab_plot(
    moduleName, testname, data, ytitle, stages, chips, analysis_cuts, force_remake
):
    # Create Directory:
    dirpath = CACHE_DIR / moduleName / testname
    dirpath.mkdir(parents=True, exist_ok=True)

    # If not forcing remake, check if plot exists
    figname = dirpath / f"{ytitle}.png"
    if not figname.is_file() or force_remake:
        # If forcing remake or if plot exists, make the plot
        fig = make_plot(moduleName, data, ytitle, stages, chips, analysis_cuts)
        fig.savefig(figname)
        plt.close(fig)
    return url_for("static.cache.static", filename=figname.relative_to(CACHE_DIR))


# Given a data array, which should be dim (# of stages) x (# chips in module)
# return a plot that is the history of the module
# Pass things like, module name, stages, chips for plot titles/axis labels
def make_plot(moduleName, data, ytitle, stages, chips, analysis_cuts):
    SMALL_SIZE = 22
    MEDIUM_SIZE = 26
    BIGGER_SIZE = 30

    plt.rc("font", size=SMALL_SIZE)
    plt.rc("axes", titlesize=SMALL_SIZE)
    plt.rc("axes", labelsize=MEDIUM_SIZE)
    plt.rc("xtick", labelsize=SMALL_SIZE)
    plt.rc("ytick", labelsize=SMALL_SIZE)
    plt.rc("legend", fontsize=SMALL_SIZE)
    plt.rc("figure", titlesize=BIGGER_SIZE)
    currtime = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

    fig = plt.figure(figsize=(20, 15))
    plt.subplots_adjust(bottom=0.25)
    xshape = data.shape[0]
    yshape = data.shape[1]
    for j in range(yshape):
        xplot = np.array(range(xshape))
        yplot = np.array(data[:, j])
        mask = yplot != -99
        plt.plot(xplot[mask], yplot[mask], marker="o", markersize=20, label=chips[j])
    stagenames = []
    for stage in stages:
        stagenames.append(stage.replace("MODULE/", ""))

    good_range = analysis_cuts.get(ytitle, {}).get("sel") or analysis_cuts.get(
        ytitle, {}
    ).get("LTwo", {}).get("sel")
    if good_range is not None:
        plt.fill_between(
            range(xshape),
            y1=good_range[0],
            y2=good_range[1],
            alpha=0.2,
            label="Passing Range",
            color="tab:green",
        )

    plt.xticks(range(xshape), stagenames, rotation=45)
    plt.title(ytitle + " for " + moduleName, fontsize=BIGGER_SIZE)
    plt.ylabel(ytitle)
    plt.xlabel("Stage")

    currlim = plt.ylim()
    plt.ylim(currlim[0] - 0.05 * abs(currlim[0]), currlim[1])
    plt.text(
        0, currlim[0], "Plot Made on " + currtime, fontsize=SMALL_SIZE, color="grey"
    )
    plt.grid()

    plt.legend()

    return fig
