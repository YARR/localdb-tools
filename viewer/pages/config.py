import concurrent.futures
import json
import logging
import traceback
from itertools import islice

import matplotlib.pyplot as plt
import numpy as np
from bson import json_util
from bson.objectid import ObjectId
from flask import (
    Blueprint,
    make_response,
    redirect,
    render_template,
    request,
    send_file,
    session,
    url_for,
)
from flask_httpauth import HTTPBasicAuth
from functions.common import (
    THUMBNAIL_DIR,
    TMP_DIR,
    clear_message,
    client,
    create_message,
    download_configs,
    get_pd_client,
    localdb,
    message_exists,
)
from json2html import json2html
from module_qc_database_tools.chip_config_api import ChipConfigAPI
from module_qc_database_tools.utils import (
    chip_uid_to_serial_number,
    get_chip_type_from_config,
)

logger = logging.getLogger("localdb")

auth = HTTPBasicAuth()

config_api = Blueprint("config_api", __name__)


def chunks(data, SIZE=10000):
    it = iter(data)
    for _ in range(0, len(data), SIZE):
        yield {k: data[k] for k in islice(it, SIZE)}


@config_api.route("/show_config/<configId>")
@config_api.route("/show_config/<configId>/<revision>")
def show_config(configId, revision=None):
    msg = f"Calling with configId={configId}, revision={revision}"
    logger.info(msg)

    chip_config_api = ChipConfigAPI(client)
    config_info = chip_config_api.get_info(configId)

    doc = {"configId": configId, "revision": revision, "config_info": config_info}

    config_data = chip_config_api.get_config(configId, revision, True) or {}
    chip_type = get_chip_type_from_config(config_data) if config_data else None
    config_global = config_data[chip_type].get("GlobalConfig", {}) if chip_type else {}
    config_param = config_data[chip_type].get("Parameter", {}) if chip_type else {}
    config_pixel = config_data[chip_type].get("PixelConfig", []) if chip_type else []

    revision_data = (
        localdb.fe_config_revision.find_one({"_id": ObjectId(revision)})
        if ObjectId.is_valid(revision)
        else {"diff": "{}"}
    )
    revision_prev = revision_data.get("parent_revision_id")

    try:
        diff = json.loads(revision_data.get("diff"))
    except Exception:
        diff = revision_data.get("diff")

    config_diff_html = json2html.convert(diff) or "<p>No diff is present.</p>"

    doc.update({"diff_html": config_diff_html, "prev_revision": revision_prev})

    config_global_chunks = chunks(config_global, 25)
    config_param_chunks = chunks(config_param, 11)

    config_global_html = [json2html.convert(chunk) for chunk in config_global_chunks]
    config_param_html = [json2html.convert(chunk) for chunk in config_param_chunks]

    doc.update(
        {
            "config_global_html": config_global_html,
            "config_param_html": config_param_html,
        }
    )

    pixelDataMap = {}
    pixelDataImages = {}

    if config_pixel:
        pixelDataMap = {
            k: [dic[k] for dic in config_pixel] for k in config_pixel[0] if k != "Col"
        }

        for key, data in pixelDataMap.items():
            arr = np.array(data)

            vmin, vmax = 0, 1
            ticks = [0, 1]
            if key.lower() == "tdac":
                vmin, vmax = -15, 15
                ticks = [-15, -10, -5, 0, 5, 10, 15]

            cmap = plt.cm.get_cmap("magma", (vmax - vmin) + 1)

            plt.imshow(arr.T, origin="lower", cmap=cmap, vmin=vmin, vmax=vmax)
            plt.title(key, fontsize=28)
            plt.xticks(fontsize=24)
            plt.yticks(fontsize=24)
            plt.xlabel("Column", fontsize=28)
            plt.ylabel("Row", fontsize=28)
            plt.tight_layout()
            cb = plt.colorbar(label="Value")
            cb.set_ticks(ticks)
            for t in cb.ax.get_yticklabels():
                t.set_fontsize(12)

            tmpfile = f"{revision}_{key}.png"
            thumbfile = f"{revision}_{key}_thumb.png"
            plt.savefig(THUMBNAIL_DIR / tmpfile, dpi=200)
            plt.savefig(THUMBNAIL_DIR / thumbfile, dpi=50)
            plt.clf()

            pixelDataImages[key] = url_for(
                "static.cache.thumbnail.static", filename=tmpfile
            )
            pixelDataImages[key + "_thumb"] = url_for(
                "static.cache.thumbnail.static", filename=thumbfile
            )

    doc["pixelData"] = pixelDataImages

    return render_template("displayConfig.html", m_doc=doc)


@config_api.route("/download_config", methods=["GET", "POST"])
def download_config():
    serialNumber = request.args.get("serialNumber", type=str, default=None)
    configId = request.args.get("configId", type=str, default=None)
    revision = request.args.get("revision", type=str, default=None)

    chip_config_api = ChipConfigAPI(client)

    name = f"{serialNumber}_{revision}.json"

    try:
        config_data = chip_config_api.get_config(configId, revision, True)

        with open(f"{TMP_DIR}/{revision}", "w", encoding="utf-8") as f:
            json.dump(config_data, f)

        return send_file(
            f"{TMP_DIR}/{revision}",
            as_attachment=True,
            mimetype="text/html",
            download_name=name,
        )

    except Exception as e:
        return make_response(
            f"something went wrong: config_id = {configId}, revision = {revision}, cache_path = {TMP_DIR}/{revision}: {e!s}"
        )


@config_api.route("/sync_chip_configs/<serial_number>", methods=["GET"])
def sync_chip_configs(serial_number):
    # first check if we have 'redirect' in url parameters
    # if not, add it in and redirect one more time
    redirect_url = request.args.get("redirect")
    refer = request.headers.get("Referer")

    this_url = url_for(
        request.endpoint,
        **dict(request.view_args, redirect=refer),
    )

    if not redirect_url:
        if not refer:
            return make_response(
                {"error": "Redirect url must be specified as there is no referer."}, 422
            )
        return redirect(this_url, code=307)

    if not session.get("logged_in"):
        return render_template("401.html")

    # check itkdb authentication
    pd_client = get_pd_client()

    if pd_client is None:
        message = "No valid ITkPD client found. Please login."
        return redirect(
            url_for("user_api.itkdb_authenticate", message=message, redirect=this_url)
        )

    if message_exists(component=serial_number, code="START_SYNC_CHIP_CONFIGS"):
        logger.error("Already pulling chip configs for this component.")
        create_message(
            f"The chip configs for component {serial_number} are already being synchronized.",
            function="sync_chip_configs",
            code="ERROR_DUPLICATE",
            component=serial_number,
        )
    else:
        message = f"Synchronization of the chip configs for component {serial_number}. This process should not take too long (no more than a few minutes)."
        ticket_id = create_message(
            message,
            function="download_chip_configs",
            component=serial_number,
            code="START_SYNC_CHIP_CONFIGS",
        )

        def download_done(future):
            clear_message({"_id": ticket_id}, forced=True)
            try:
                future.result()
            except Exception as e:
                logger.error(f"{e}")
                logger.error(traceback.format_exc())
                create_message(
                    "An error occurred. Please contact LocalDB admin.",
                    function="download_chip_configs",
                    code="ERROR_GENERAL",
                    component=serial_number,
                )

        executor = concurrent.futures.ThreadPoolExecutor(
            thread_name_prefix=f"{serial_number}/downloadChipConfigs"
        )
        future = executor.submit(
            download_configs,
            serial_number,
            pd_client,
        )
        future.add_done_callback(download_done)
        executor.shutdown(wait=False)

    return redirect(redirect_url, code=307)


@config_api.route("/identify_config/<config_id>", methods=["GET"])
def identify_config(config_id):
    result = localdb.fe_configs.find_one({"_id": ObjectId(config_id)})
    if result:
        result = json.loads(json_util.dumps(result))
        return make_response({"source": "db.fe_configs", "result": result}, 200)
    result = localdb.QC.result.find_one({"config_id": config_id})
    if result:
        result = json.loads(json_util.dumps(result))
        return make_response({"source": "db.QC.result", "result": result}, 200)
    result = localdb.componentTestRun.find_one(
        {
            "$or": [
                {"config_revision_current": config_id},
                {"config_revision_prev": config_id},
            ]
        }
    )
    if result:
        result = json.loads(json_util.dumps(result))
        return make_response({"source": "db.componentTestRun", "result": result}, 200)
    result = localdb.testRun.find_one(
        {
            "$or": [
                {"siteCfg": config_id},
                {"userCfg": config_id},
                {"scanCfg": config_id},
                {"ctrlCfg": config_id},
                {"dbCfg": config_id},
            ]
        }
    )
    if result:
        result = json.loads(json_util.dumps(result))
        return make_response({"source": "db.testRun", "result": result}, 200)

    return make_response({"source": "unknown", "result": None}, 400)


@config_api.route("/config/<chip_uid>/<mode>", methods=["GET"])
@config_api.route("/config/<chip_uid>/<mode>/<revision>", methods=["GET"])
def get_config(chip_uid, mode, revision=None):
    chip_sn = chip_uid_to_serial_number(chip_uid)
    chip_doc = localdb.component.find_one({"serialNumber": chip_sn})
    qc_status = localdb.QC.module.status.find_one(
        {"component": str(chip_doc.get("_id"))}
    )
    stage = qc_status.get("stage")

    chip_config = localdb.fe_configs.find_one(
        {"serialNumber": chip_sn, "stage": stage, "branch": mode}
    )
    config_id = str(chip_config.get("_id"))
    chip_config_api = ChipConfigAPI(client)

    if not revision:
        revision = "HEAD"

    revision_id = chip_config_api.get_revision_id(config_id, revision)
    config_data = chip_config_api.get_config(config_id, revision_id, True)

    return json.dumps(config_data)
