import concurrent.futures
import hashlib
import logging
import os
import secrets
import shutil
import string
import traceback
from datetime import datetime

from bson.errors import InvalidId
from bson.objectid import ObjectId
from flask import (
    Blueprint,
    abort,
    make_response,
    redirect,
    render_template,
    request,
    session,
    url_for,
)
from flask_httpauth import HTTPBasicAuth
from flask_mail import Mail, Message
from functions.common import (
    TMP_DIR,
    args,
    clear_message,
    create_message,
    format_messages,
    getScanSummary,
    initPage,
    localdb,
    localdb_url,
    message_exists,
    process_request,
    readKey,
    userdb,
)
from functions.itkpd_interface.institution import download_institution
from functions.itkpd_interface.moduleType import download_ModuleType
from functions.workers import download_worker

logger = logging.getLogger("localdb")

auth = HTTPBasicAuth()

user_api = Blueprint("user_api", __name__)
mail = Mail()


@user_api.route("/login", methods=["POST"])
def login():
    pre_url = request.headers.get("Referer")
    pre_url_lastpass = pre_url.split("/")

    query = {"username": request.form["username"]}
    user = userdb.viewer.user.find_one(query)
    query = {}
    if user is None:
        txt = "This user does not exist"
        if pre_url_lastpass[-1] != "login":
            session["pre_url"] = pre_url
        return render_template("error.html", txt=txt)

    if user["password"] != "":
        if (
            hashlib.md5(request.form["password"].encode("utf-8")).hexdigest()
            == user["password"]
        ):
            session["logged_in"] = True
            session["username"] = user["username"]
            session["fullname"] = user["name"]
            session["institution"] = user["institution"]
            session["user_id"] = str(user["_id"])
            url_list = [
                "register_password",
                "register_password_from_admin",
                "login",
                "signup",
            ]
            if pre_url_lastpass[-1] in url_list:
                return redirect(session["pre_url"])

            return redirect(pre_url)

        txt = "This password is not correct"
        if pre_url_lastpass[-1] != "login":
            session["pre_url"] = pre_url
        return render_template("error.html", txt=txt)

    txt = "Password is not registered yet."
    if pre_url_lastpass[-1] != "login":
        session["pre_url"] = pre_url
    return render_template("error.html", txt=txt)


@user_api.route("/user/<string:username>/institution", methods=["GET", "POST"])
def institution(username):
    # first check if we have 'redirect' in url parameters
    # if not, add it in and redirect one more time
    redirect_url = request.args.get("redirect")
    refer = request.headers.get("Referer")
    if not redirect_url:
        if not refer:
            return make_response(
                {"error": "Redirect url must be specified as there is no referer."}, 422
            )
        return redirect(
            url_for(request.endpoint, **dict(request.view_args, redirect=refer)),
            code=307,
        )

    user = userdb.viewer.user.find_one({"username": username})
    if user is None:
        txt = f"This user does not exist: {username}"
        return render_template("error.html", txt=txt), 400

    if not session.get("logged_in", False):
        return render_template("401.html"), 401

    logged_in_user = userdb.viewer.user.find_one({"username": session["username"]})
    if session["username"] != username and logged_in_user["auth"] != "adminViewer":
        return render_template("403.html"), 403

    try:
        tmp = localdb.institution.aggregate(
            [
                {"$match": {"code": {"$exists": True}}},
                {
                    "$project": {
                        "_id": 0,
                        "data": {
                            "$arrayToObject": [[{"k": "$code", "v": "$institution"}]]
                        },
                    }
                },
                {"$replaceRoot": {"newRoot": "$data"}},
            ]
        )

        institutions = {}
        for doc in tmp:  # Iterate properly over the cursor
            institutions.update(doc)  # Merge each document into the dictionary

    except StopIteration:
        institutions = {}

    messages = []
    if request.method == "POST":
        institution_code = request.form.get("institution_code")
        if not institution_code:
            messages.append("ERROR: The institution code must be provided.")
            return render_template(
                "change_institution.html",
                institutions=institutions,
                username=username,
                messages=messages,
                redirect_url=redirect_url,
            )

        institution = institutions.get(institution_code)

        if not institution:
            messages.append(f"The institution code does not exist: {institution_code}")
            return render_template(
                "change_institution.html",
                institutions=institutions,
                username=username,
                messages=messages,
                redirect_url=redirect_url,
            )

        userdb.viewer.user.update_one(
            {"_id": ObjectId(user["_id"])}, {"$set": {"institution": institution}}
        )
        session["institution"] = institution
        return redirect(redirect_url)

    return render_template(
        "change_institution.html",
        institutions=institutions,
        username=username,
        messages=messages,
        redirect_url=redirect_url,
    )


@user_api.route("/logout", methods=["GET", "POST"])
def logout():
    pre_url = request.headers.get("Referer")
    session["logged_in"] = False
    session["username"] = ""
    session["institution"] = ""
    session["user_id"] = ""

    return redirect(pre_url)


@user_api.route("/register_password", methods=["GET", "POST"])
def register_password():
    stage = request.form.get("stage", "input")
    userinfo = request.form.getlist("userinfo")
    if userinfo == []:
        session.pop("registerpass", None)
    if session.get("registerpass"):
        if userdb.viewer.user.count_documents({"username": userinfo[0]}) == 0:
            text = "This username does not exist"
            stage = "input"
            return render_template(
                "register_password.html",
                userInfo=userinfo,
                nametext=text,
                stage=stage,
            )

        query = {"username": userinfo[0]}
        userdata = userdb.viewer.user.find_one(query)
        if userdata["Email"] != userinfo[1]:
            text = "This Email is not correct"
            stage = "input"
            return render_template(
                "register_password.html",
                userInfo=userinfo,
                nametext2=text,
                stage=stage,
            )

        if stage == "input":
            return render_template(
                "register_password.html",
                userInfo=userinfo,
                stage=stage,
            )
        if stage == "confirm":
            if args.fmail:
                return_num = updatePasswordAndMail(userinfo)
                if return_num == 0:
                    return "Mail sending failed. You cannot use the function."
            return render_template(
                "register_password.html",
                userInfo=userinfo,
                stage=stage,
            )

        if userdata["password"] != hashlib.md5(userinfo[2].encode("utf-8")).hexdigest():
            text = "This current password is not correct"
            stage = "confirm"
            return render_template(
                "register_password.html",
                userInfo=userinfo,
                nametext3=text,
                stage=stage,
            )
        if userinfo[3] != userinfo[4]:
            text = "Please make sure your password match"
            stage = "confirm"
            return render_template(
                "register_password.html",
                userInfo=userinfo,
                nametext4=text,
                stage=stage,
            )

        changeUserPassword(userinfo[0], userinfo[3])
        session.pop("registerpass")
        return render_template(
            "register_password.html",
            userInfo=userinfo,
            stage=stage,
        )

    userinfo = ["", "", "", "", ""]
    pre_url = request.headers.get("Referer")
    session["pre_url"] = pre_url

    session["registerpass"] = True
    return render_template(
        "register_password.html",
        userInfo=userinfo,
        stage=stage,
    )


@user_api.route("/itkdb_authenticate", methods=["GET", "POST"])
def itkdb_authenticate():
    # first check if we have 'redirect' in url parameters
    # if not, add it in and redirect one more time
    redirect_url = request.args.get("redirect")
    refer = request.headers.get("Referer")
    additional_message = request.args.get("message")

    if not redirect_url:
        if not refer:
            return make_response(
                {"error": "Redirect url must be specified as there is no referer."}, 422
            )
        return redirect(
            url_for(
                request.endpoint,
                **dict(request.view_args, redirect=refer, message=additional_message),
            ),
            code=307,
        )

    if not session.get("logged_in"):
        return render_template("401.html")

    messages = [additional_message] if additional_message else []

    if request.method == "GET":
        return render_template(
            "itkdb_authenticate.html", messages=messages, redirect_url=redirect_url
        )

    code1 = request.form.get("code1", "")
    code2 = request.form.get("code2", "")

    try:
        _flag, client = process_request(code1, code2)

        if client is None:
            messages += ["Itkdb not authorized. Please input correct codes."]
            return render_template(
                "itkdb_authenticate.html", messages=messages, redirect_url=redirect_url
            )

        logger.info("successfully authenticated")

    except Exception:
        return render_template(
            "itkdb_authenticate.html", messages=messages, redirect_url=redirect_url
        )

    return redirect(redirect_url)


@user_api.route("/download_component", methods=["GET", "POST"])
def download_component():
    if not session.get("logged_in", False):
        return render_template("401.html")

    stage = request.form.get("stage", "input").split("|")[0]
    code1 = request.form.get("code1", "")
    code2 = request.form.get("code2", "")

    component_id = request.form.get("component_id")

    skip_attachments = "skipAtt" in request.form.get("stage", "input").split("|")

    skip_synched = "noSkipSynched" not in request.form.get("stage", "input").split("|")

    if stage == "submit":
        logger.info("start")
        msg = f"stage = {stage}, component_id = {component_id}, skip_attachments = {skip_attachments}, skip_synched = {skip_synched}"
        logger.info(msg)

        flag, client = process_request(code1, code2)

        if flag == 0:
            stage = "input"
            create_message(
                "ITkPD not authorized. Please input correct codes.",
                function="download_component",
                code="ERROR_ITKPD_AUTH",
            )

        elif stage == "submit":
            institution = session["institution"]

            if message_exists(component=component_id, code="START_SYNC_COMPONENT"):
                logger.error("Already pulling this component.")
                stage = "input"
                create_message(
                    f"The component {component_id} is already being pulled.",
                    function="download_component",
                    code="ERROR_DUPLICATE",
                )
            else:
                timestamp = datetime.now()
                ticket_id = create_message(
                    f"Recursive synchronization of the component {component_id} started on {timestamp}. This process will take several minutes to complete.",
                    function="RecursiveComponentsSynchronizer",
                    component=component_id,
                    code="START_SYNC_COMPONENT",
                )

                def download_done(future):
                    clear_message({"_id": ticket_id}, forced=True)
                    try:
                        future.result()
                    except Exception as e:
                        logger.error(f"{e}")
                        logger.error(traceback.format_exc())
                        create_message(
                            "An error occurred. Please contact LocalDB admin.",
                            function="download_component",
                            code="ERROR_GENERAL",
                        )

                executor = concurrent.futures.ThreadPoolExecutor(
                    thread_name_prefix="downloadComponent"
                )
                future = executor.submit(
                    download_worker,
                    component_id,
                    code1,
                    code2,
                    skip_attachments,
                    skip_synched,
                    institution,
                )
                future.add_done_callback(download_done)
                executor.shutdown(wait=False)

    table_docs = {
        "messages": format_messages(
            {
                "function": {
                    "$in": ["RecursiveComponentsSynchronizer", "download_component"]
                }
            }
        )
    }

    # logger.info( "messages to the user: " )
    # logger.info( pprint.pformat( messages ) )

    return render_template(
        "download_component.html", stage=stage, table_docs=table_docs
    )


@user_api.route("/register_chipid", methods=["GET", "POST"])
def register_chipid():
    if not session.get("logged_in", False):
        return render_template("401.html")

    component = {}
    module = request.args.get("module", "")
    stage = request.form.get("stage", "input")
    chipid = request.form.getlist("chipid")
    text = ""

    if stage == "input":
        this_cp = localdb.component.find_one({"name": module})
        if not this_cp:
            text = "The downloading process have not been over. Try again."
        else:
            component["module"] = this_cp
            this_cprs = localdb.childParentRelation.find(
                {"parent": str(this_cp["_id"])}
            )
            chips = []
            for this_cpr in this_cprs:
                this_chip = localdb.component.find_one(
                    {"_id": ObjectId(this_cpr["child"])}
                )
                chips.append(this_chip)
            component["chips"] = chips
            if this_cp["children"] == 4 and this_cp["chipType"] == "RD53A":
                component["picture"] = "/localdb/static/images/RD53A_quad.png"
            else:
                component["picture"] = ""

    elif stage == "complete":
        this_cp = localdb.component.find_one({"name": module})
        component["module"] = this_cp
        this_cprs = localdb.childParentRelation.find({"parent": str(this_cp["_id"])})
        for i, this_cpr in enumerate(this_cprs):
            localdb.component.update_one(
                {"_id": ObjectId(this_cpr["child"])},
                {"$set": {"chipId": int(chipid[i])}},
            )

    return render_template(
        "register_chipid.html",
        stage=stage,
        text=text,
        module=module,
        component=component,
    )


@user_api.route("/check_mail", methods=["GET", "POST"])
def check_mail():
    stage = request.form.get("stage", "input")
    userinfo = request.form.getlist("userinfo")
    if userinfo == []:
        session.pop("mailsending", None)
    if session.get("mailsending"):
        if stage == "input":
            return render_template(
                "check_mail.html",
                userInfo=userinfo,
                stage=stage,
            )
        if stage == "request":
            return_num = sendTestMail(userinfo)
            if return_num == 0:
                return "Mail sending failed. Cannot use the mail-sending function on your network environment."

    userinfo = [""]
    pre_url = request.headers.get("Referer")
    session["pre_url"] = pre_url

    session["mailsending"] = True
    return render_template(
        "check_mail.html",
        userInfo=userinfo,
        stage=stage,
    )


@user_api.route("/set_time", methods=["GET", "POST"])
def set_time():
    session["timezone"] = request.form.get("timezone")

    return redirect(request.headers.get("Referer"))


############
## For Admin

if args.KeyFile:
    keys = readKey(args.KeyFile)
    admin_username = keys["username"]
    admin_password = keys["password"]
else:
    if args.username:
        admin_username = hashlib.md5(args.username.encode("utf-8")).hexdigest()
        admin_password = hashlib.md5(args.password.encode("utf-8")).hexdigest()
    else:
        admin_username = hashlib.md5(b"username").hexdigest()
        admin_password = hashlib.md5(b"password").hexdigest()

users = {admin_username: admin_password}


@auth.get_password
def get_pw(username):
    if hashlib.md5(username.encode("utf-8")).hexdigest() in users:
        return users.get(hashlib.md5(username.encode("utf-8")).hexdigest())
    return None


@auth.hash_password
def hash_pw(password):
    return hashlib.md5(password.encode("utf-8")).hexdigest()


@user_api.route("/admin_toppage", methods=["GET"])
# @auth.login_required
def show_admin_toppage():
    initPage()

    user_dir = "/".join([TMP_DIR, session["uuid"]])
    if os.path.isdir(user_dir):
        shutil.rmtree(user_dir)

    session.pop("signup", None)
    table_docs = {"page": None}

    return render_template(
        "admin_toppage.html",
        mail=args.fmail,
        table_docs=table_docs,
    )


@user_api.route("/signup", methods=["GET", "POST"])
# @auth.login_required
def signup():
    stage = request.form.get("stage", "input")
    userinfo = request.form.getlist("userinfo")
    Address = localdb.institution.find({})
    if userinfo == []:
        session.pop("signup", None)

    if session.get("signup"):
        if userinfo[5] != userinfo[6]:
            text = "Please make sure your Email match"
            stage = "input"
            return render_template(
                "signup.html",
                userInfo=userinfo,
                Address=Address,
                passtext=text,
                stage=stage,
            )
        if userdb.viewer.user.count_documents({"username": userinfo[0]}) == 1:
            text = "This username is already in use, please select an alternative."
            stage = "input"
            return render_template(
                "signup.html",
                userInfo=userinfo,
                Address=Address,
                nametext=text,
                stage=stage,
            )

        username_hash = hashlib.md5(userinfo[0].encode("utf-8")).hexdigest()
        if username_hash == admin_username:
            text = "This is the admin's username. Please use different one."
            stage = "input"
            return render_template(
                "signup.html",
                userInfo=userinfo,
                Address=Address,
                nametext=text,
                stage=stage,
            )

        if userinfo[3] == "other" and userinfo[4] == "":
            text = "Please fill out this brank."
            stage = "input"
            return render_template(
                "signup.html",
                userInfo=userinfo,
                Address=Address,
                nametext3=text,
                stage=stage,
            )

        if stage == "input":
            return render_template(
                "signup.html",
                userInfo=userinfo,
                Address=Address,
                stage=stage,
            )
        if stage == "confirm":
            return render_template(
                "signup.html",
                userInfo=userinfo,
                Address=Address,
                stage=stage,
            )

        if args.fmail:
            return_num = sendSignupMail(userinfo)
            if return_num == 0:
                return "Mail sending Failed. You cannot use the function."

        insertDBUser(userinfo)

        session.pop("signup")
        return render_template(
            "signup.html",
            userInfo=userinfo,
            Address=Address,
            stage=stage,
            mail=args.fmail,
        )

    userinfo = ["", "", "", "", "", ""]
    pre_url = request.headers.get("Referer")
    session["pre_url"] = pre_url
    session["signup"] = True

    return render_template(
        "signup.html",
        userInfo=userinfo,
        Address=Address,
        stage=stage,
    )


@user_api.route("/register_password_from_admin", methods=["GET", "POST"])
# @auth.login_required
def register_password_from_admin():
    stage = request.form.get("stage", "input")
    userinfo = request.form.getlist("userinfo")
    if userinfo == []:
        session.pop("registerpass", None)
    if session.get("registerpass"):
        if stage == "input":
            return render_template(
                "register_password_from_admin.html",
                userInfo=userinfo,
                stage=stage,
            )
        if stage == "request":
            query = {"username": userinfo[0]}
            if userdb.viewer.user.count_documents(query) == 0:
                text = "This username does not exist"
                stage = "input"
                return render_template(
                    "register_password_from_admin.html",
                    userInfo=userinfo,
                    nametext1=text,
                    stage=stage,
                )

            num = 8
            pool = string.ascii_letters
            pool2 = string.digits
            pin_number = "".join([secrets.choice(pool + pool2) for i in range(num)])
            changeUserPassword(userinfo[0], pin_number)
            session.pop("registerpass")
            return render_template(
                "register_password_from_admin.html",
                userInfo=userinfo,
                stage=stage,
                pin_number=pin_number,
            )

    userinfo = ["", "", ""]
    pre_url = request.headers.get("Referer")
    session["pre_url"] = pre_url

    session["registerpass"] = True
    return render_template(
        "register_password_from_admin.html",
        userInfo=userinfo,
        stage=stage,
    )


@user_api.route("/download_pdinfo", methods=["GET", "POST"])
# @auth.login_required
def download_pdinfo():
    stage = request.form.get("stage", "input")
    code1 = request.form.get("code1", " ")
    code2 = request.form.get("code2", " ")
    text = ""

    if stage == "complete":
        flag, _client = process_request(code1, code2)
        if flag == 0:
            stage = "input"
            text = "Not authorized. Something is wrong."
        else:
            try:
                download_institution(code1, code2)
            except Exception as e:
                stage = "input"
                text = f"The error was occurred in the downloading institutions process.: {e}"
            try:
                download_ModuleType(code1, code2)
            except Exception as e:
                stage = "input"
                text = f"The error was occurred in the downloading component types process: {e}"

    return render_template("download_pdinfo.html", stage=stage, text=text)


@user_api.route("/rename_sn", methods=["GET", "POST"])
# @auth.login_required
def rename_sn():
    stage = request.form.get("stage", "input")
    moduleinfo = request.form.getlist("moduleinfo")
    modules = []
    chips = {"old": [], "oldId": [], "new": [], "newId": []}
    change_test_run = []
    text = ""
    if stage == "input":
        components = localdb.component.find()
        modules = [
            item["name"] for item in components if item["componentType"] == "module"
        ]

    if stage == "confirm":
        old_moduleId = str(localdb.component.find_one({"name": moduleinfo[0]})["_id"])
        old_chips = localdb.childParentRelation.find({"parent": old_moduleId})
        old_chips = sorted(old_chips, key=lambda x: x["chipId"])
        for chip in old_chips:
            chip_name = localdb.component.find_one({"_id": ObjectId(chip["child"])})[
                "name"
            ]
            chips["old"].append(chip_name)

        new_moduleId = str(localdb.component.find_one({"name": moduleinfo[1]})["_id"])
        new_chips = localdb.childParentRelation.find({"parent": new_moduleId})
        new_chips = sorted(new_chips, key=lambda x: x["chipId"])
        for chip in new_chips:
            chip_name = localdb.component.find_one({"_id": ObjectId(chip["child"])})[
                "name"
            ]
            chips["new"].append(chip_name)

        if len(new_chips) != len(old_chips):
            stage = "input"
            text = (
                "The number of chips in the new module and the old one are different..."
            )

        query = {"component": old_moduleId}
        entries = localdb.componentTestRun.find(query)
        for run_entry in entries:
            change_test_run.append(getScanSummary(run_entry["testRun"]))

    if stage == "complete":
        old_module_info = localdb.component.find_one({"name": moduleinfo[0]})
        old_moduleId = str(old_module_info["_id"])
        old_chips = localdb.childParentRelation.find({"parent": old_moduleId})
        old_chips = sorted(old_chips, key=lambda x: x["chipId"])
        for chip in old_chips:
            chip_info = localdb.component.find_one({"_id": ObjectId(chip["child"])})
            chips["old"].append(chip_info["name"])
            chips["oldId"].append(chip["child"])

        new_module_info = localdb.component.find_one({"name": moduleinfo[1]})
        new_moduleId = str(new_module_info["_id"])
        new_chips = localdb.childParentRelation.find({"parent": new_moduleId})
        new_chips = sorted(new_chips, key=lambda x: x["chipId"])
        for chip in new_chips:
            chip_info = localdb.component.find_one({"_id": ObjectId(chip["child"])})
            chips["new"].append(chip_info["name"])
            chips["newId"].append(chip["child"])

        module_testRun = localdb.componentTestRun.find(
            {"name": old_module_info["name"]}
        )
        for run in module_testRun:
            pre_sn = run.get("history", [])
            pre_sn.append(old_module_info["name"])
            scan_sn = run.get("scanSN", old_module_info["name"])

            localdb.componentTestRun.update_one(
                {"name": old_module_info["name"], "testRun": run["testRun"]},
                {
                    "$set": {
                        "name": new_module_info["name"],
                        "component": str(new_module_info["_id"]),
                        "scanSN": scan_sn,
                        "history": pre_sn,
                    }
                },
            )
            for i in range(len(old_chips)):
                try:
                    newchipID = str(
                        localdb.chip.find_one({"name": chips["new"][i]})["_id"]
                    )
                except Exception:
                    new_chip_info = localdb.chip.find_one({"name": chips["old"][i]})
                    new_chip_info["name"] = chips["new"][i]
                    new_chip_info.pop("_id")
                    localdb.chip.insert_one(new_chip_info)
                    newchipID = str(
                        localdb.chip.find_one({"name": chips["new"][i]})["_id"]
                    )

                pre_info = localdb.componentTestRun.find_one(
                    {"name": chips["old"][i], "testRun": run["testRun"]}
                )
                pre_sn = pre_info.get("history", [])
                pre_sn.append(chips["old"][i])
                scan_sn = pre_info.get("scanSN", chips["old"][i])

                localdb.componentTestRun.update_one(
                    {"name": chips["old"][i], "testRun": run["testRun"]},
                    {
                        "$set": {
                            "name": chips["new"][i],
                            "scanSN": scan_sn,
                            "history": pre_sn,
                            "component": chips["newId"][i],
                            "chip": newchipID,
                        }
                    },
                )

    return render_template(
        "rename_moduleSN.html",
        stage=stage,
        moduleinfo=moduleinfo,
        modules=modules,
        chips=chips,
        change_test_run=change_test_run,
        text=text,
    )


@user_api.route("/scan_stage", methods=["GET", "POST"])
# @auth.login_required
def scan_stage():
    stage = request.form.get("stage", "input")
    moduleinfo = request.form.getlist("moduleinfo")
    run_ids = request.form.getlist("run_ids")
    modules = []
    stages = []
    table_docs = {"run": []}
    scans = localdb.componentTestRun.find()
    for item in scans:
        if item["chip"] == "module":
            modules.append(item["name"])
    modules = sorted(set(modules))

    if stage == "scans":
        stages = ["Testing"]
        try:
            module_id = localdb.component.find_one({"name": moduleinfo[0]})["_id"]
            query = {"component": str(module_id)}
            qc_module_doc = localdb.QC.module.status.find_one(query)
            stage_list = list(qc_module_doc["QC_results"])
            stages = stages + stage_list
        except Exception:
            stage = "input"

        scans = localdb.componentTestRun.find({"name": moduleinfo[0]})
        for scan in scans:
            run_data = getScanSummary(scan["testRun"])
            run_data["stage"] = localdb.testRun.find_one(
                {"_id": ObjectId(scan["testRun"])}
            )["stage"]
            table_docs["run"].append({"run_data": run_data})

    if stage == "confirm":
        for run_id in run_ids:
            run_data = getScanSummary(run_id)
            run_data["stage"] = localdb.testRun.find_one({"_id": ObjectId(run_id)})[
                "stage"
            ]
            table_docs["run"].append({"run_data": run_data})

    if stage == "complete":
        for run_id in run_ids:
            localdb.testRun.update_one(
                {"_id": ObjectId(run_id)}, {"$set": {"stage": moduleinfo[1]}}
            )
        for run_id in run_ids:
            run_data = getScanSummary(run_id)
            run_data["stage"] = localdb.testRun.find_one({"_id": ObjectId(run_id)})[
                "stage"
            ]
            table_docs["run"].append({"run_data": run_data})

    return render_template(
        "scan_stage.html",
        stage=stage,
        modules=modules,
        stages=stages,
        moduleinfo=moduleinfo,
        table_docs=table_docs,
        run_ids=run_ids,
    )


# TODO: REMOVE ME, i think it is unused entirely
@user_api.route("/upload_result", methods=["GET", "POST"])
# @auth.login_required
def upload_result():
    abort(
        500,
        "Please inform developers that you're executing /upload_result and describe how you got here.",
    )


def changeUserPassword(username, password):
    query = {"username": username}
    userdb.viewer.user.update_one(
        query, {"$set": {"password": hashlib.md5(password.encode("utf-8")).hexdigest()}}
    )
    try:
        localdb.command("dropUser", username)
        localdb.command("dropUser", hashlib.md5(username.encode("utf-8")).hexdigest())
        logger.info("The password is changed.")
    except Exception:
        logger.info("A new password is registered.")

    localdb.command(
        "createUser",
        username,
        pwd=password,
        roles=[
            {"role": "readWrite", "db": "localdb"},
            {"role": "readWrite", "db": "localdbtools"},
        ],
    )
    localdb.command(
        "createUser",
        hashlib.md5(username.encode("utf-8")).hexdigest(),
        pwd=hashlib.md5(password.encode("utf-8")).hexdigest(),
        roles=[
            {"role": "readWrite", "db": "localdb"},
            {"role": "readWrite", "db": "localdbtools"},
        ],
    )

    return 0


def sendTestMail(userinfo):
    msg = Message(
        "Test mail from LocalDB",
        sender="admin-no-reply@localdb.com",
        recipients=[userinfo[0]],
    )
    msg.html = "This is a test mail to check the mail-sending function in LocalDB."
    try:
        mail.send(msg)
    except Exception:
        return 0

    return 1


def sendSignupMail(userinfo):
    msg = Message(
        "Welcome to Local DB! Register your Password",
        sender="admin-no-reply@localdb.com",
        recipients=[userinfo[5]],
    )
    with open("static/text/signup_mail_text.txt", encoding="utf-8") as mail_text:
        contents = mail_text.read()
        msg.html = (
            contents.replace("USERNAME", userinfo[0])
            .replace("ADDRESS", userinfo[5])
            .replace("LOCALDBURL", localdb_url)
            .replace("FIRSTNAME", userinfo[1])
            .replace("LASTNAME", userinfo[2])
        )
    try:
        mail.send(msg)
        return 1
    except Exception:
        return 0


def updatePasswordAndMail(userinfo):
    query = {"username": userinfo[0]}
    num = 8
    pool = string.ascii_letters
    pool2 = string.digits
    pin_number = "".join([secrets.choice(pool + pool2) for i in range(num)])
    msg = Message(
        "Local DB password notice",
        sender="admin-no-reply@localdb.com",
        recipients=[userinfo[1]],
    )
    with open("static/text/pin_mail_text.txt", encoding="utf-8") as mail_text:
        contents = mail_text.read()
        msg.html = contents.replace("PINNUMBER", pin_number)
    try:
        mail.send(msg)
    except Exception:
        return 0

    userdb.viewer.user.update_one(
        query,
        {"$set": {"password": hashlib.md5(pin_number.encode("utf-8")).hexdigest()}},
    )

    return 1


def insertDBUser(userinfo):
    institution = userinfo[3] if userinfo[3] != "other" else userinfo[4]
    thistime = datetime.now()
    userdb.viewer.user.insert_one(
        {
            "sys": {"rev": 0, "cts": thistime, "mts": thistime},
            "username": userinfo[0],
            "name": userinfo[1] + " " + userinfo[2],
            "auth": "readWrite",
            "institution": institution,
            "Email": userinfo[5],
            "password": "",
        }
    )

    return 0


@user_api.route("/delete_message/<message_id>", methods=["POST", "GET"])
def delete_message(message_id):
    if not session.get("logged_in"):
        return render_template("401.html")

    try:
        query = {"_id": ObjectId(message_id)}
    except InvalidId:
        abort(400, "The message ID provided was invalid.")

    clear_message(query)

    pre_url = request.headers.get("Referer")
    return redirect(pre_url)


@user_api.route("/customize_qc", methods=["POST", "GET"])
def customize_qc():
    if not session.get("logged_in"):
        return render_template("401.html")

    input_step = request.form.get("input_step", "select_ctype")

    if input_step == "select_ctype":
        ctypes = [ctype.get("name") for ctype in list(userdb.componentType.find({}))]
        return render_template(
            "customize_qc.html", ctypes=ctypes, input_step=input_step
        )

    if input_step == "user_input":
        selected_ctype = request.form.get("selected_ctype", None)
        stage_doc = userdb.QC.stages.find_one({"name": selected_ctype})
        return render_template(
            "customize_qc.html",
            selected_ctype=selected_ctype,
            stage_doc=stage_doc,
            input_step=input_step,
        )

    if input_step == "submit":
        selected_ctype = request.form.get("selected_ctype", None)
        stage_doc = userdb.QC.stages.find_one({"name": selected_ctype})
        disable_doc = stage_doc.get("disabled_tests", {})

        for stage, tests in stage_doc.get("stage_test").items():
            if request.form.get(f"enable_{stage}") != "on":
                if stage not in disable_doc:
                    disable_doc[stage] = {"disabled": 1, "tests": []}
                else:
                    disable_doc[stage]["disabled"] = 1
            else:
                if stage not in disable_doc:
                    disable_doc[stage] = {"disabled": 0, "tests": []}
                else:
                    disable_doc[stage]["disabled"] = 0

            for test in tests:
                if stage not in disable_doc:
                    disable_doc[stage] = {"disabled": 0, "tests": []}

                if disable_doc[stage]["disabled"] or (
                    1 if request.form.get(f"enable_{stage}_{test}") == "on" else 0
                ):
                    if test in disable_doc[stage]["tests"]:
                        disable_doc[stage]["tests"].remove(test)
                else:
                    if test not in disable_doc[stage]["tests"]:
                        disable_doc[stage]["tests"].append(test)

        userdb.QC.stages.update_one(
            {"_id": stage_doc.get("_id")}, {"$set": {"disabled_tests": disable_doc}}
        )

        return render_template(
            "customize_qc.html",
            selected_ctype=selected_ctype,
            stage_doc=stage_doc,
            input_step="user_input",
        )

    return render_template(
        "error.html", txt="user_api.dustomize_qc(): Invalid state"
    ), 400
