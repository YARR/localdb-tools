import os
from datetime import datetime

from flask import (
    Blueprint,
    render_template,
    request,
    session,
)
from functions.common import (
    dbv,
    localdb,
)

register_api = Blueprint("register_api", __name__)


@register_api.route("/register_chip", methods=["GET", "POST"])
def register_chip():
    stage = request.form.get("stage", "input")
    chipinfo = request.form.getlist("chipinfo")
    address = localdb.institution.find({})
    if chipinfo == []:
        session.pop("chip_register", None)

    if session.get("chip_register"):
        query = {"serialNumber": chipinfo[0]}
        thistime = datetime.utcnow()
        chips_count = localdb.component.count_documents(query)

        if not session.get("logged_in"):
            text = "Please login."
            stage = "input"
            return render_template(
                "register_chip.html",
                chipInfo=chipinfo,
                addresses=address,
                nametext=text,
                stage=stage,
            )
        if chips_count != 0:
            text = "This chipname is already in use."
            stage = "input"
            return render_template(
                "register_chip.html",
                chipInfo=chipinfo,
                addresses=address,
                nametext2=text,
                stage=stage,
            )
        if chipinfo[3] == "other" and chipinfo[4] == "":
            text = "Please fill out this brank."
            stage = "input"
            return render_template(
                "register_chip.html",
                chipInfo=chipinfo,
                addresses=address,
                nametext3=text,
                stage=stage,
            )
        if stage == "input":
            return render_template(
                "register_chip.html",
                chipInfo=chipinfo,
                addresses=address,
                stage=stage,
            )
        if stage == "confirm":
            return render_template(
                "register_chip.html",
                chipInfo=chipinfo,
                addresses=address,
                stage=stage,
            )
        query = {
            "userName": session["username"],
            "description": "viewer",
            "dbVersion": dbv,
        }
        userinfo = localdb.user.find_one(query)
        if userinfo is None:
            add_userinfo(session["username"], session["institution"])
            userinfo = localdb.user.find_one(query)

        addressinfo = chipinfo[3]
        thistime = datetime.utcnow()
        localdb.component.insert_one(
            {
                "sys": {"cts": thistime, "mts": thistime, "rev": 0},
                "serialNumber": chipinfo[0],
                "componentType": "front-end_chip",
                "chipType": chipinfo[1],
                "name": chipinfo[0],
                "chipId": int(chipinfo[2]),
                "address": str(addressinfo),
                "user_id": str(userinfo["_id"]),
                "children": -1,
                "proDB": False,
                "dbVersion": dbv,
            }
        )
        session.pop("chip_register")
        return render_template(
            "register_chip.html",
            chipInfo=chipinfo,
            stage=stage,
        )

    chipinfo = ["", "", "", "", ""]
    pre_url = request.headers.get("Referer")
    session["pre_url"] = pre_url

    session["chip_register"] = True

    return render_template(
        "register_chip.html",
        chipInfo=chipinfo,
        addresses=address,
        stage=stage,
    )


@register_api.route("/register_module", methods=["GET", "POST"])
def register_module():
    query = {"componentType": "front-end_chip", "dbVersion": dbv}
    chip_entries = localdb.component.find(query)
    address = localdb.institution.find({})
    unregistered_chips = []
    for chip in chip_entries:
        query = {"child": str(chip["_id"])}
        registered_chip = localdb.childParentRelation.find_one(query)
        if registered_chip is None:
            unregistered_chips.append(chip)

    stage = request.form.get("stage", "input")
    moduleinfo = request.form.getlist("moduleinfo")
    Row = request.form.get("row")
    Col = request.form.get("col")
    row = Row
    col = Col
    if moduleinfo == []:
        session.pop("module_register", None)

    if session.get("module_register"):
        query = {"serialNumber": moduleinfo[0]}
        thistime = datetime.utcnow()
        modules_count = localdb.component.count_documents(query)
        if int(Row) > int(Col):
            row = Col
            col = Row
        row = int(row)
        col = int(col)
        if not session.get("logged_in"):
            text = "Please login."
            stage = "input"
            return render_template(
                "register_module.html",
                chips=unregistered_chips,
                addresses=address,
                moduleInfo=moduleinfo,
                Row=row,
                Col=col,
                nametext=text,
                stage=stage,
            )
        if modules_count != 0:
            text = "This module is already registered."
            stage = "input"
            return render_template(
                "register_module.html",
                chips=unregistered_chips,
                addresses=address,
                moduleInfo=moduleinfo,
                Row=row,
                Col=col,
                nametext2=text,
                stage=stage,
            )
        if moduleinfo[2] == "other" and moduleinfo[3] == "":
            text = "Please fill out this brank."
            stage = "input"
            return render_template(
                "register_module.html",
                chips=unregistered_chips,
                addresses=address,
                moduleInfo=moduleinfo,
                Row=row,
                Col=col,
                nametext3=text,
                stage=stage,
            )
        if stage == "input":
            return render_template(
                "register_module.html",
                chips=unregistered_chips,
                addresses=address,
                moduleInfo=moduleinfo,
                Row=row,
                Col=col,
                stage=stage,
            )
        if stage == "input2":
            return render_template(
                "register_module.html",
                chips=unregistered_chips,
                addresses=address,
                moduleInfo=moduleinfo,
                Row=row,
                Col=col,
                stage=stage,
            )
        if stage == "confirm":
            chipId_list = []
            for i in range(int(row)):
                for j in range(int(col)):
                    if moduleinfo[2 * i + j + 4] == "":
                        text = "Please fill out the brank."
                        stage = "input2"
                        return render_template(
                            "register_module.html",
                            chips=unregistered_chips,
                            addresses=address,
                            moduleInfo=moduleinfo,
                            Row=row,
                            Col=col,
                            nametext4=text,
                            stage=stage,
                        )

                    this_chip = localdb.component.find_one(
                        {"serialNumber": moduleinfo[2 * i + j + 4]}
                    )
                    chipId_list.append(this_chip["chipId"])
                if len(chipId_list) != len(set(chipId_list)):
                    text = "Please select chips so that each chipId does not overlap."
                    stage = "input2"
                    return render_template(
                        "register_module.html",
                        chips=unregistered_chips,
                        addresses=address,
                        moduleInfo=moduleinfo,
                        Row=row,
                        Col=col,
                        nametext4=text,
                        stage=stage,
                    )
            return render_template(
                "register_module.html",
                chips=unregistered_chips,
                addresses=address,
                moduleInfo=moduleinfo,
                Row=row,
                Col=col,
                stage=stage,
            )

        thistime = datetime.now()
        query = {
            "userName": session["username"],
            "description": "viewer",
            "dbVersion": dbv,
        }
        userinfo = localdb.user.find_one(query)
        if userinfo is None:
            add_userinfo(session["username"], session["institution"])
            userinfo = localdb.user.find_one(query)

        addressinfo = moduleinfo[2]

        localdb.component.insert_one(
            {
                "sys": {"cts": thistime, "mts": thistime, "rev": 0},
                "serialNumber": moduleinfo[0],
                "componentType": "module",
                "chipType": moduleinfo[1],
                "name": moduleinfo[0],
                "chipId": -1,
                "address": str(addressinfo),
                "user_id": str(userinfo["_id"]),
                "children": int(row) * int(col),
                "proDB": False,
                "dbVersion": dbv,
            }
        )

        chipId_list = []
        for i in range(int(row)):
            for j in range(int(col)):
                this_chip = localdb.component.find_one(
                    {"serialNumber": moduleinfo[2 * i + j + 4]}
                )
                query = {
                    "serialNumber": moduleinfo[0],
                    "componentType": "module",
                }
                parent = localdb.component.find_one(query)
                query = {
                    "serialNumber": moduleinfo[2 * i + j + 4],
                    "componentType": "front-end_chip",
                }
                child = localdb.component.find_one(query)
                localdb.childParentRelation.insert_one(
                    {
                        "sys": {"cts": thistime, "mts": thistime, "rev": 0},
                        "parent": str(parent["_id"]),
                        "child": str(child["_id"]),
                        "chipId": this_chip["chipId"],
                        "geomId": {"x": j, "y": i},
                        "status": "active",
                        "dbVersion": dbv,
                    }
                )

            session.pop("module_register")
            return render_template(
                "register_module.html",
                chips=unregistered_chips,
                addresses=address,
                moduleInfo=moduleinfo,
                Row=row,
                Col=col,
                stage=stage,
            )

    moduleinfo = ["", "", "", ""]
    row = ""
    col = ""
    pre_url = request.headers.get("Referer")
    session["pre_url"] = pre_url

    session["module_register"] = True

    return render_template(
        "register_module.html",
        chips=unregistered_chips,
        addresses=address,
        moduleInfo=moduleinfo,
        Row=row,
        Col=col,
        stage=stage,
    )


def add_userinfo(username, institution):
    thistime = datetime.utcnow()
    localdb.user.insert_one(
        {
            "userName": username,
            "institution": institution,
            "description": "viewer",
            "USER": os.environ["USER"],
            "HOSTNAME": str(os.uname()[1]),
            "sys": {"cts": thistime, "mts": thistime, "rev": 0},
            "userType": "readWrite",
            "dbVersion": dbv,
        }
    )
