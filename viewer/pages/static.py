from flask import (
    Blueprint,
)
from functions.common import (
    CACHE_DIR,
    STATIC_DIR,
    THUMBNAIL_DIR,
)

static_api = Blueprint(
    "static",
    __name__,
    url_prefix="/static",
    static_url_path="",
    static_folder=STATIC_DIR,
)

cache = Blueprint(
    "cache",
    __name__,
    url_prefix="/cache",
    static_url_path="",
    static_folder=CACHE_DIR,
)

static_api.register_blueprint(cache)

thumbnail = Blueprint(
    "thumbnail",
    __name__,
    static_url_path="/thumbnail",
    static_folder=THUMBNAIL_DIR,
)

cache.register_blueprint(thumbnail)
