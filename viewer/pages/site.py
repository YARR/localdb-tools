from flask import Blueprint, current_app, render_template, url_for

site = Blueprint("site", __name__)


def has_no_empty_params(rule):
    defaults = rule.defaults if rule.defaults is not None else ()
    arguments = rule.arguments if rule.arguments is not None else ()
    return len(defaults) >= len(arguments)


@site.route("/site-map")
def site_map():
    links = []
    for rule in current_app.url_map.iter_rules():
        # Filter out rules we can't navigate to in a browser
        # and rules that require parameters
        if "GET" in rule.methods:
            if has_no_empty_params(rule):
                url = url_for(rule.endpoint, **(rule.defaults or {}))
            else:
                url = None
            links.append((url, rule.endpoint, str(rule)))

    return render_template("sitemap.html", links=links)
