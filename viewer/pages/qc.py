import concurrent.futures
import contextlib
import copy
import hashlib
import io
import json
import logging
import os
import pprint
import shutil
import statistics
import subprocess
import traceback
import zipfile
from datetime import datetime, timezone
from pathlib import Path
from typing import Any

import arrow
import itkdb
import jsonschema
import module_qc_database_tools
import pymongo
from bson.objectid import ObjectId
from flask import (
    Blueprint,
    abort,
    make_response,
    redirect,
    render_template,
    request,
    send_file,
    session,
    url_for,
)
from functions.checkouter import make_archive_threadsafe
from functions.common import (
    AT_DIR,
    THUMBNAIL_DIR,
    TMP_DIR,
    SN_typeinfo,
    check_and_process,
    client,
    component_type_code_map,
    create_message,
    dbv,
    delim_SN,
    dt_tz_to_timestamp,
    format_messages,
    fs,
    get_component_property_codes,
    get_component_property_value,
    get_localtime_str,
    get_module_children,
    get_module_sensors,
    get_pd_client,
    initPage,
    localdb,
    process_request,
    qcAnalysisVersion,
    upload_chip_config,
    userdb,
)
from functions.workers import upload_worker
from json2html import json2html
from module_qc_database_tools.chip_config_api import ChipConfigAPI
from module_qc_database_tools.core import DPPort, Module
from module_qc_database_tools.iv import fetch_reference_ivs
from module_qc_database_tools.sync_component_stages import sync_component_stages
from module_qc_database_tools.utils import (
    chip_uid_to_serial_number,
    get_chip_type_from_config,
    get_layer_from_serial_number,
)
from pymongo import (
    DESCENDING,
)
from tzlocal import get_localzone

logger = logging.getLogger("localdb")

qc_api = Blueprint("qc_api", __name__)

rawSchema = {
    "definitions": {
        "chip_noSubtype": {
            "type": "object",
            "required": ["serialNumber", "testType", "results", "subtestType"],
            "additionalProperties": True,
            "properties": {
                "serialNumber": {"type": "string"},
                "testType": {"type": "string"},
                "subtestType": {"constant": ""},
            },
        },
        "chip": {
            "type": "object",
            "required": ["serialNumber", "testType", "results", "subtestType"],
            "additionalProperties": True,
            "properties": {
                "serialNumber": {"type": "string"},
                "testType": {"type": "string"},
                "subtestType": {"type": "string", "minLength": 1},
            },
        },
    },
    "type": "array",
    "minItems": 0,
    "items": {
        "oneOf": [
            {
                "type": "array",
                "maxItems": 1,
                "items": {"$ref": "#/definitions/chip_noSubtype"},
            },
            {"type": "array", "minItems": 1, "items": {"$ref": "#/definitions/chip"}},
        ]
    },
}


@qc_api.route("/qc_tests", methods=["GET", "POST"])
def qc_tests():
    search_phrase = request.form.get("keyword", type=str, default=None)
    page_index = request.form.get("page", type=int, default=0)

    logger.info(f"search phrase = {search_phrase}")
    logger.info(f"page index = {page_index}")

    table_docs = {
        "messages": format_messages({"component": None}),
        "search_phrase": search_phrase if search_phrase else "",
    }

    modules = localdb.component.find(
        {"componentType": "module"}, sort=[("sys.cts", DESCENDING)]
    )

    # tests = list( localdb.QC.result.find( {} , sort = [ ('component', DESCENDING), ('sys.cts', DESCENDING) ] ) )

    output_tests = []

    for index, module in enumerate(modules):
        if search_phrase is not None:
            if search_phrase not in module.get("serialNumber"):
                continue
        elif index < page_index * 10 or index >= (page_index + 1) * 10:
            continue

        module["delim_sn"] = delim_SN(module.get("serialNumber"))
        module["typeInfo"] = SN_typeinfo(module.get("serialNumber"))
        module["stage"] = localdb.QC.module.status.find_one(
            {"component": str(module.get("_id"))}
        ).get("stage")

        sub_cpts = gather_subcomponents(str(module.get("_id")), [module])

        tests = {}

        tests["moduleInfo"] = module
        tests["subComponents"] = sub_cpts

        tests["testTypes"] = {}

        testCount = 0

        for subcpt in sub_cpts:
            subcpt_tests = list(
                localdb.QC.result.find(
                    {
                        "component": str(subcpt.get("_id")),
                        "stage": module.get("stage"),
                    },
                    sort=[("component", DESCENDING), ("sys.cts", DESCENDING)],
                )
            )
            for test in subcpt_tests:
                if test.get("testType") not in tests["testTypes"]:
                    tests["testTypes"][test.get("testType")] = 0

                test.update({"table_index": tests["testTypes"][test.get("testType")]})
                test.update(
                    {"timestamp": get_localtime_str(test.get("sys").get("mts"))}
                )

                tests["testTypes"][test.get("testType")] += 1

                testCount += 1

            subcpt.update({"tests": subcpt_tests})

        module["testCount"] = testCount

        output_tests += [tests]

    return render_template("qcTests.html", tests=output_tests, table_docs=table_docs)


def gather_subcomponents(top_id, _list):
    cprs = localdb.childParentRelation.find({"parent": top_id})

    for cpr in cprs:
        subcpt = localdb.component.find_one({"_id": ObjectId(cpr.get("child"))})
        subcpt["delim_sn"] = delim_SN(subcpt.get("serialNumber"))
        subcpt["typeInfo"] = SN_typeinfo(subcpt.get("serialNumber"))

        _list += [subcpt]

        # gather_subcomponents( cpr.get('child'), _list )

    return _list


@qc_api.route("/create_configs", methods=["GET", "POST"])
def create_configs():
    logger.info("called")

    componentId = request.args.get("componentId", type=str, default=None)

    redirect_url = url_for("component_api.show_component", id=componentId)

    try:
        cpt_doc = localdb.component.find_one({"_id": ObjectId(componentId)})
        serialNumber = cpt_doc["serialNumber"]
    except Exception as e:
        logger.error(str(e))
        logger.error(traceback.format_exc())
        logger.error(f"Failed in getting the serialNumber for component {componentId}")
        return redirect(redirect_url)

    # check itkdb authentication
    pd_client = get_pd_client()

    if pd_client is None:
        message = "No valid ITkPD client found. Please login."
        return redirect(
            url_for(
                "user_api.itkdb_authenticate", message=message, redirect=redirect_url
            )
        )

    # Call config generation alg
    try:
        module = Module(pd_client, serialNumber)
        logger.info("Getting layer-dependent config from module SN...")
    except Exception as e:
        logger.error(str(e))
        logger.error(traceback.format_exc())
        raise
        # return redirect( url_for( 'toppage_api.show_toppage' ) )

    try:
        layer_config = get_layer_from_serial_number(serialNumber)
    except Exception as e:
        logger.error(str(e))
        logger.error(traceback.format_exc())
        return redirect(url_for("component_api.show_component", id=componentId))

    chip_template_path = (
        module_qc_database_tools.data / "YARR" / "chip_template.json"
    ).resolve()
    os.makedirs(str(chip_template_path.parent), exist_ok=True)
    chip_template = json.loads(chip_template_path.read_text())

    for suffix in ["warm", "cold", "LP"]:
        generated_configs = module.generate_config(
            chip_template, layer_config, DPPort.A, suffix=suffix, version="latest"
        )

        revise_pd_record = False

        if len(generated_configs.get("chips")) == 0:
            logger.warning(
                f'Fetching latest config failed for "{suffix}". Try the fallback option with TESTONWAFER...'
            )
            generated_configs = module.generate_config(
                chip_template,
                layer_config,
                DPPort.A,
                suffix=suffix,
                version="TESTONWAFER",
                reverse=False,
            )
            revise_pd_record = True

        chip_config_client = ChipConfigAPI(client)

        for chip_spec in generated_configs["chips"]:
            chip_serial_number = chip_uid_to_serial_number(
                chip_spec.get(get_chip_type_from_config(chip_spec))
                .get("Parameter")
                .get("Name")
            )

            chip_doc = localdb.component.find_one({"serialNumber": chip_serial_number})
            if not chip_doc:
                logger.error(
                    f"Failed in getting the chip component information for serialNumber {chip_serial_number}"
                )
                return redirect(redirect_url)

            config_id = chip_config_client.create_config(
                chip_serial_number, "MODULE/INITIAL_WARM", branch=suffix
            )
            revision_id = chip_config_client.commit(
                config_id,
                chip_spec,
                "initial generation from module-qc-database-tools",
            )
            logger.info(
                f"new chip config {config_id} with a new revision {revision_id} saved to mongodb."
            )

            if revise_pd_record:
                upload_chip_config(
                    chip_doc["proID"], config_id, revision_id, layer_config
                )

    return redirect(
        url_for(
            "component_api.show_component",
            id=componentId,
            config_create_processed=True,
        )
    )


@qc_api.route("/qc_uploader", methods=["GET", "POST"])
def upload_qcTests():
    if not session.get("logged_in", False):
        return render_template("401.html")

    page_docs = {"page": None, "title": "QC Test Results Uploading Page"}

    if "fileUpload" not in request.files:
        return render_template("qc/upload_qcTests.html", page_docs=page_docs)

    try:
        inputJson = json.load(request.files["fileUpload"])
    except Exception as e:
        logger.error(traceback.format_exc())
        page_docs["message"] = "Input file is not a valid JSON: " + str(e)

        return render_template("qc/upload_qcTests.html", page_docs=page_docs)

    page_docs["testData"] = json2html.convert(inputJson)

    try:
        upload_qcTests_core(inputJson, page_docs)

    except Exception as e:
        logger.error(traceback.format_exc())
        page_docs["message"] = str(e)

        return render_template("qc/upload_qcTests.html", page_docs=page_docs), 400

    page_docs["message_done"] = "QC TestRun submission was processed successfully!"

    return render_template("qc/upload_qcTests.html", page_docs=page_docs)


@qc_api.route("/qc_uploader_post", methods=["POST"])
def upload_qcTests_post():
    page_docs = {"page": None, "title": "QC Test Results Uploading Page"}
    inputJson = request.get_json(force=True)

    try:
        anaResults = upload_qcTests_core(inputJson, page_docs)

    except Exception as e:
        return json.dumps({"ERROR": str(e), "traceback": traceback.format_exc()}), 400

    for a in anaResults:
        if a is None:
            continue

        # Keep ID
        a["id"] = str(a["_id"])
        if ObjectId.is_valid(a["address"]):
            institution = localdb.institution.find_one({"_id": ObjectId(a["address"])})
            if institution:
                a["address"] = institution["code"]

        for key in ["_id", "raw_id", "sys", "gridfs_attachments"]:
            a.pop(key)

    logger.info(pprint.pformat(anaResults))

    return json.dumps(anaResults)


def upload_qcTests_core(inputJson, page_docs):
    jsonschema.validate(inputJson, rawSchema)

    logger.info(f"inputJson is valid: contains {len(inputJson)} results")

    anaResults = []

    for index, tests in enumerate(inputJson):
        # here, each "test" is an array and it may contain several measurement objects
        logger.info(f"inputJson Result-#{index}")

        # check the validity of the serialNumber
        serialNumber = None
        for test in tests:
            serialNumber = test["serialNumber"]
            logger.info(f"serialNumber = {serialNumber}")

            # reject if serialNumber is not found
            component = localdb.component.find_one({"serialNumber": serialNumber})
            if component is None:
                msg = f"Serial number {serialNumber} was not found in LocalDB ==> You need to pull the component first."
                raise KeyError(msg)

            logger.info(f'component = {component["_id"]}')

            # acquire the stage
            qcStatus = localdb.QC.module.status.find_one(
                {"component": str(component["_id"])}
            )
            if qcStatus is None:
                msg = f"Input test type {test['testType']} is not listed in the current stage {qcStatus['stage']}"
                raise ValueError(msg)

            test["stage"] = qcStatus["stage"]

            if test["testType"] not in qcStatus["QC_results"][qcStatus["stage"]]:
                # it could be the case that the stage definition has changed.
                # as a fallback,Check the latest stage structure

                logger.info(f'componentType = {component["componentType"]}')

                ctype_code = component_type_code_map.get(component["componentType"])
                if ctype_code is None:
                    msg = "Cannot process QC measurement for component type: component['componentType']"
                    raise ValueError(msg)

                stage_tests = (
                    userdb.QC.stages.find_one({"code": ctype_code})
                    .get("stage_test")
                    .get(qcStatus["stage"])
                ) or []

                if test["testType"] not in stage_tests:
                    msg = f"Input test type {test['testType']} is not a valid test of the current stage {qcStatus['stage']}"
                    raise ValueError(msg)

        # at this point, the schema is valid in termf os general format.
        # if this passes the testType-specific JSON schema, then record it to LocalDB as RAW.

        # schema validation is done.
        # register this test as RAW, then analyze it

        anaResult, already_registered = save_raw_and_analyze(tests, component)

        if already_registered:
            anaResult["_id"] = "Skipped to record"

        anaResults += [anaResult]

        for test in tests:
            test["Component info in LocalDB"] = component

    page_docs["testData"] = json2html.convert(inputJson)
    page_docs["testDataAnaResult"] = json2html.convert(anaResults)

    return anaResults


@qc_api.route("/recycle_analysis", methods=["GET", "POST"])
def recycle_analysis():
    test_run_id = request.form.get("test_run_id", type=str, default=None)

    component_id, new_test_run_id = recycle_analysis_core(test_run_id)

    return redirect(
        url_for(
            "component_api.show_component",
            id=component_id,
            test="qctest",
            runId=new_test_run_id,
        )
    )


def recycle_analysis_core(test_run_id):
    prev_test_run = localdb.QC.result.find_one({"_id": ObjectId(test_run_id)})

    component_id = prev_test_run["component"]
    component = localdb.component.find_one({"_id": ObjectId(component_id)})

    if component is None:
        msg = f"component doc for component ID {component_id} was not found. Recycling aborts for this TestRun {test_run_id}"
        logger.error(msg)
        create_message(
            f"ERROR: {msg}",
            function="recycle_analysis_core",
            component=component_id,
            code="ERROR_RECYCLE_ANALYSIS_CORE",
        )
        return str(component_id), str(prev_test_run.get("_id"))

    serial_number = component["serialNumber"]

    raw_id = prev_test_run.get("raw_id")
    raw_obj = localdb.QC.testRAW.find_one({"_id": ObjectId(raw_id)})

    if raw_obj is None:
        logger.warning(f"no RAW data associated to TestRun {test_run_id}")
        create_message(
            "WARNING: failure in reycle_analysis(): no RAW data associated to TestRun",
            function="recycle_analysis",
            component=serial_number,
            code="WARNING_RECYCLE_ANALYSIS",
        )
        return redirect(
            url_for(
                "component_api.show_component",
                id=str(component.get("_id")),
                test="qctest",
                runId=str(prev_test_run.get("_id")),
            )
        )

    raw = localdb.QC.testRAW.find_one({"_id": ObjectId(raw_id)}).get("raw")

    component_type = component.get("componentType")

    module_serial_number = None

    for raw_elem in raw:
        if component_type == "front-end_chip":
            if "Metadata" not in raw_elem["results"]:
                raw_elem["Metadata"] = {}

            if "ModuleSN" not in raw_elem["results"]["Metadata"]:
                if not module_serial_number:
                    logger.info(
                        '"ModuleSN" was not found in raw metadata. seeking the parent module from LocalDB...'
                    )
                    for cpr in localdb.childParentRelation.find(
                        {"child": str(component.get("_id"))}
                    ):
                        parent_id = cpr.get("parent")
                        parent = localdb.component.find_one(
                            {"_id": ObjectId(parent_id)}
                        )
                        if parent is None:
                            continue
                        if parent.get("componentType") == "module":
                            module_serial_number = parent.get("serialNumber")
                            logger.info(
                                f"module serial number was identified to be {module_serial_number}"
                            )
                            break

                raw_elem["results"]["Metadata"]["ModuleSN"] = module_serial_number

            if "Institution" not in raw_elem["results"]:
                try:
                    inst_id = prev_test_run.get("address")
                    inst = localdb.institution.find_one({"_id": ObjectId(inst_id)}).get(
                        "code"
                    )
                    raw_elem["results"]["Metadata"]["Institution"] = inst
                except Exception:
                    with contextlib.suppress(Exception):
                        inst = (
                            prev_test_run.get("prodDB_record")
                            .get("institution")
                            .get("code")
                        )
                        raw_elem["results"]["Metadata"]["Institution"] = inst

        for var in ["address", "component"]:
            if var in raw_elem:
                raw_elem[var] = str(raw_elem[var])

    test = copy.deepcopy(raw)
    for t in test:
        t.pop("sys")

    try:
        new_test_run, already_registered = analyze_measurement(
            test, raw, component, raw_id, update_config=False, recycle=True
        )
    except Exception as e:
        logger.error(str(e))
        logger.error(traceback.format_exc())
        create_message(
            f"ERROR: failure in reycle_analysis() <pre>{traceback.format_exc()}</pre>",
            function="recycle_analysis",
            component=serial_number,
            code="ERROR_RECYCLE_ANALYSIS",
        )
        return str(component.get("_id")), str(prev_test_run.get("_id"))

    create_message(
        f'INFO: Analysis for {new_test_run.get("testType")} was recycled successfully',
        function="recycle_analysis",
        component=serial_number,
        code="INFO_RECYCLE_ANALYSIS_DONE",
    )

    return str(component.get("_id")), str(new_test_run.get("_id"))


@qc_api.route("/recycle_complex_analysis", methods=["GET", "POST"])
def recycle_complex_analysis():
    """
    Recycling mqat analysis for MHT, TUN, PFA
    """

    test_run_id = request.form.get("test_run_id", type=str, default=None)

    component_id, new_test_run_id = recycle_complex_analysis_core(test_run_id)

    return redirect(
        url_for(
            "component_api.show_component",
            id=component_id,
            test="qctest",
            runId=new_test_run_id,
        )
    )


def recycle_complex_analysis_core(test_run_id):
    prev_test_run = localdb.QC.result.find_one({"_id": ObjectId(test_run_id)})

    test_type = prev_test_run.get("testType")

    component_id = prev_test_run["component"]
    component = localdb.component.find_one({"_id": ObjectId(component_id)})

    if component is None:
        msg = f"component doc for component ID {component_id} was not found. Recycling complex aborts for this TestRun {test_run_id}"
        logger.error(msg)
        create_message(
            f"ERROR: {msg}",
            function="recycle_complex_analysis_core",
            component=component_id,
            code="ERROR_RECYCLE_COMPLEX_ANALYSIS_CORE",
        )
        return str(component_id), str(prev_test_run.get("_id"))

    serial_number = component["serialNumber"]

    stage_alt = prev_test_run.get("stage").replace("/", "__")
    test_type_hyphened = test_type.replace("_", "-")

    work_dir = Path(f"{AT_DIR}/{serial_number}_{stage_alt}_{test_type_hyphened}")
    os.makedirs(str(work_dir), exist_ok=True)

    anaResult = None
    # Dump the attachment zip file and expand
    for filename, oid in prev_test_run.get("gridfs_attachments").items():
        if not filename.lower().find("zip") >= 0:
            continue

        os.makedirs(THUMBNAIL_DIR, exist_ok=True)

        ext = filename.split(".")[-1]

        cache_file = f"{THUMBNAIL_DIR}/{oid!s}.{ext}"

        if os.path.exists(cache_file):
            pass
        else:
            try:
                data = fs.get(oid).read()

                with open(cache_file, "wb") as f:
                    f.write(data)
            except Exception as e:
                logger.error(str(e))
                logger.error(traceback.format_exc())
                logger.error(f"Failed in dumping attachment to {cache_file}")

        logger.info(f"attachment dumped to cache {cache_file}")

        with zipfile.ZipFile(cache_file, "r") as zfile:
            zfile.extractall(str(work_dir))

        info_file = Path(work_dir / "info.json")

        with info_file.open("r", encoding="utf-8") as f:
            info_json = json.load(f)

        info_json["datadir"] = str(work_dir)

        if any(key for key in ["YARR_VERSION", "TimeStart"]) not in info_json:
            testRun_paths = Path(work_dir).glob("*/testRun.json")

            yarr_versions = set()
            start_times = []
            for tr_path in testRun_paths:
                with open(tr_path) as tr_fp:
                    tr_data = json.load(tr_fp)
                yarr_versions.add(tr_data["yarr_version"]["git_tag"])
                start_times.append(tr_data["startTime"]["$date"])

            if "YARR_VERSION" not in info_json:
                if len(yarr_versions) > 1:
                    create_message(
                        f"Multiple yarr versions were identified for recycling: {yarr_versions}. Taking the first one as the YARR_VERSION.",
                        function="recycle_complex_analysis",
                        component=serial_number,
                        code="WARNING",
                    )

                yarr_version = "unknown"
                if not yarr_versions:
                    create_message(
                        "Unable to identify any yarr versions. Will set a placeholder of 'unknown'.",
                        function="recycle_complex_analysis",
                        component=serial_number,
                        code="WARNING",
                    )
                else:
                    yarr_version = yarr_versions.pop()

                info_json["YARR_VERSION"] = yarr_version

            if "TimeStart" not in info_json:
                info_json["TimeStart"] = sorted(start_times)[0]

        with info_file.open("w", encoding="utf-8") as f:
            json.dump(info_json, f)

        out_dir = str(Path(work_dir) / "output")
        shutil.rmtree(out_dir)

        command = [
            f"analysis-{test_type}".replace("_", "-"),
            "-i",
            str(info_file),
            "-o",
            out_dir,
        ]

        try:
            subprocess.check_call(command)
        except subprocess.CalledProcessError as exc:
            msg = f"module-qc-analysis-tools did not run successfully. Tried to run the command: {command}"
            logger.error(msg)
            raise RuntimeError(msg) from exc
        except FileNotFoundError as exc:
            msg = f"Is module-qc-analysis-tools installed? Tried to run the command: {command}"
            logger.error(msg)
            raise RuntimeError(msg) from exc

        output = None
        output_others = []
        for curdir, _dirs, files in os.walk(f"{work_dir!s}/output"):
            for nested_filename in files:
                if nested_filename.endswith(".json"):
                    output = os.path.join(curdir, nested_filename)
                else:
                    output_others += [os.path.join(curdir, nested_filename)]

        if output is not None:
            with open(output, encoding="utf-8") as f:
                anaResult = json.load(f)[0]

        # gather attachment files
        output_others = []
        try:
            # seek the analysis result file inside the output path
            for curdir, _dirs, files in os.walk(f"{work_dir!s}/output"):
                for nested_filename in files:
                    if "json" in nested_filename:
                        continue

                    output_others += [os.path.join(curdir, nested_filename)]

            logger.info(f"subsidiary files: {output_others}")

        except Exception as e:
            logger.error(str(e))
            logger.error(traceback.format_exc())
            msg = f"Null output file is detected: the analysis command\n\n{command}\n\nmay have an issue"
            raise RuntimeError(msg) from e

        # attach other files first
        attachments = {}
        for attachment in output_others:
            if any(
                attachment.endswith(ext)
                for ext in [
                    ".jpg",
                    "jpeg",
                    "png",
                    ".JPG",
                    ".JPEG",
                    ".PNG",
                    ".pdf",
                    ".log",
                ]
            ):
                with open(attachment, "rb") as f:
                    binary = f.read()
                ret = fs.put(binary)
                item = {str(Path(attachment).name): ret}

                attachments.update(item)
                logger.info(f"submitted {attachment} to localdb gridfs: {item}")

        logger.info("attachments = " + pprint.pformat(attachments))

        # zipping the raw input
        logger.info("zipping the input files...")
        make_archive_threadsafe(f"{work_dir!s}.zip", str(work_dir))

        zfile = Path(str(work_dir) + ".zip")
        with zfile.open("rb") as f:
            binary = f.read()
            md5 = hashlib.md5(binary).hexdigest()

            if localdb.fs.files.find_one({"md5": md5}) is None:
                archive_id = fs.put(binary)
                logger.info(
                    f"the zip file {zfile.name} is new, uploaded to gridfs. md5 = {md5}"
                )
            else:
                logger.info(
                    f"identical binary was detected in gridfs, not pushing. md5 = {md5}"
                )
                archive_id = localdb.fs.files.find_one({"md5": md5}).get("_id")

            if archive_id is not None:
                attachments[zfile.name] = archive_id
                logger.info(
                    f"registered zip archive as gridfs_attachment of TestRun. md5 = {md5}"
                )
            else:
                logger.warning(
                    f"archive_id is None! not possible to register the binary as a gridfs_attachment. md5 = {md5}"
                )

    if anaResult is None:
        msg = "There does not seem to be an attachment zip-file that contains the raw data to recycle the analysis"
        logger.error(msg)
        raise RuntimeError(msg)

    new_test_run = copy.deepcopy(prev_test_run)
    new_test_run.pop("_id")
    for k, v in anaResult.items():
        new_test_run[k] = v

    new_test_run["gridfs_attachments"] = attachments

    new_test_run["sys"] = {
        "mts": datetime.now(timezone.utc),
        "cts": datetime.now(timezone.utc),
        "rev": 0,
    }

    new_test_run_id = localdb.QC.result.insert_one(new_test_run).inserted_id

    return str(component.get("_id")), str(new_test_run_id)


@qc_api.route("/recycle_summary", methods=["GET", "POST"])
def recycle_summary():
    """
    Recycling analyses for E_SUMMARY as a bulk
    """

    test_run_id = request.form.get("test_run_id", type=str, default=None)

    prev_test_run = localdb.QC.result.find_one({"_id": ObjectId(test_run_id)})

    serial_number = prev_test_run.get("serialNumber")

    if not serial_number:
        serial_number = request.form.get("serial_number", type=str, default=None)

    logger.info(f"serial_number = {serial_number}, test_run_id = {test_run_id}")

    component = localdb.component.find_one({"serialNumber": serial_number})

    new_test_run = copy.deepcopy(prev_test_run)
    new_test_run.pop("_id")
    if "prodDB_record" in new_test_run:
        new_test_run.pop("prodDB_record")

    summaryEvals = {}

    for parameter, value in prev_test_run.get("results").items():
        if parameter.find("FE_LINK") >= 0:
            fe_test_run_id = value
            fe_test_run = localdb.QC.result.find_one({"_id": ObjectId(fe_test_run_id)})

            summaryEvals.update(
                {
                    parameter: (
                        fe_test_run.get("passed") if fe_test_run is not None else None
                    )
                }
            )

            if fe_test_run is None:
                continue

            fe_component_id = fe_test_run.get("component")

            if any(
                parameter.find(complex_test) >= 0
                for complex_test in [
                    "MIN_HEALTH_TEST",
                    "TUNING",
                    "PIXEL_FAILURE_ANALYSIS",
                ]
            ):
                logger.info(f"{parameter} is complex")

                try:
                    fe_component_id, new_fe_test_run_id = recycle_complex_analysis_core(
                        fe_test_run_id
                    )
                except Exception:
                    msg = f"Unable to recycle complex test {fe_test_run_id}"
                    logger.exception(msg)
                    new_fe_test_run_id = fe_test_run_id

            else:
                try:
                    fe_component_id, new_fe_test_run_id = recycle_analysis_core(
                        fe_test_run_id
                    )
                except Exception:
                    msg = f"Unable to recycle test {fe_test_run_id}"
                    logger.exception(msg)
                    new_fe_test_run_id = fe_test_run_id

            new_test_run["results"][parameter] = new_fe_test_run_id

    # Parameter Loop-2: collect bad pixels
    for parameter in prev_test_run.get("results"):
        # print( f'######## loop2: {parameter}' )

        if "ELECTRICALLY_BAD_PIXEL_NUMBER_FE" in parameter:
            feIndex = parameter[-1]

            try:
                referenceResultId = new_test_run["results"][
                    f"MODULE_PIXEL_FAILURE_ANALYSIS_FE_LINK_{feIndex}"
                ]

                if referenceResultId:
                    referenceResult = localdb.QC.result.find_one(
                        {"_id": ObjectId(referenceResultId)}
                    )

                    value = referenceResult.get("results").get(
                        "PIXEL_FAILURE_ELECTRICALLY_FAILED"
                    )
                    if value >= 0:
                        new_test_run["results"][parameter] = value
                        logger.info(f"inserted {value} to {parameter}")
                    else:
                        new_test_run["results"][parameter] = None

                else:
                    logger.info(
                        f"reference result for {parameter} is missing, skipped to evaluate"
                    )

            except Exception:
                msg = f"failure in evaluating {parameter}"
                logger.exception(msg)

        elif "DISCONNECTED_PIXEL_NUMBER_FE" in parameter:
            feIndex = parameter[-1]

            try:
                referenceResultId = new_test_run["results"][
                    f"MODULE_PIXEL_FAILURE_ANALYSIS_FE_LINK_{feIndex}"
                ]

                if referenceResultId:
                    referenceResult = localdb.QC.result.find_one(
                        {"_id": ObjectId(referenceResultId)}
                    )

                    value = referenceResult.get("results").get(
                        "PIXEL_FAILURE_DISCONNECTED_PIXELS"
                    )
                    if value >= 0:
                        new_test_run["results"][parameter] = value
                        logger.info(f"inserted {value} to {parameter}")
                    else:
                        new_test_run["results"][parameter] = None

                else:
                    logger.info(
                        f"reference result for {parameter} is missing, skipped to evaluate"
                    )

            except Exception:
                msg = f"failure in evaluating {parameter}"
                logger.exception(msg)

        elif "MODULE_DISABLED_COLUMNS_NUMBER" in parameter:
            feIndex = parameter[-1]

            try:
                referenceResultId = new_test_run["results"][
                    f"MODULE_PIXEL_FAILURE_ANALYSIS_FE_LINK_{feIndex}"
                ]

                if referenceResultId:
                    referenceResult = localdb.QC.result.find_one(
                        {"_id": ObjectId(referenceResultId)}
                    )

                    value = referenceResult.get("results").get(
                        "PIXEL_FAILURE_COLUMN_DISABLED"
                    )
                    if value >= 0:
                        new_test_run["results"][parameter] = value
                        logger.info(f"inserted {value} to {parameter}")
                    else:
                        new_test_run["results"][parameter] = None

                else:
                    logger.info(
                        f"reference result for {parameter} is missing, skipped to evaluate"
                    )

            except Exception:
                msg = f"failure in evaluating {parameter}"
                logger.exception(msg)

        elif "BAD_PIXEL_NUMBER_FE" in parameter:
            feIndex = parameter[-1]

            try:
                referenceResultId = new_test_run["results"][
                    f"MODULE_PIXEL_FAILURE_ANALYSIS_FE_LINK_{feIndex}"
                ]

                if referenceResultId:
                    referenceResult = localdb.QC.result.find_one(
                        {"_id": ObjectId(referenceResultId)}
                    )

                    value = referenceResult.get("results").get(
                        "PIXEL_FAILURE_FAILING_PIXELS"
                    )
                    if value >= 0:
                        new_test_run["results"][parameter] = value
                        logger.info(f"inserted {value} to {parameter}")
                    else:
                        new_test_run["results"][parameter] = None

                else:
                    logger.info(
                        f"reference result for {parameter} is missing, skipped to evaluate"
                    )

            except Exception:
                msg = f"failure in evaluating {parameter}"
                logger.exception(msg)

    # Fill the rest parameters
    summaryVarList = [
        "MODULE_ELECTRICALLY_BAD_PIXEL_NUMBER",
        "MODULE_DISCONNECTED_PIXEL_NUMBER",
        "MODULE_DISABLED_COLUMNS_NUMBER",
        "MODULE_BAD_PIXEL_NUMBER",
    ]

    # Sum up all FEs
    for var in summaryVarList:
        elems = [
            new_test_run["results"][f"{var}_FE_{feIndex}"]
            for feIndex in range(1, 5)
            if new_test_run["results"][f"{var}_FE_{feIndex}"] is not None
        ]
        if len(elems) == 0:
            new_test_run["results"][var] = None
        else:
            new_test_run["results"][var] = sum(elems)

    # Eval flag for each test
    feTests = [
        "ADC_CALIBRATION",
        "ANALOG_READBACK",
        "SLDO",
        "VCAL_CALIBRATION",
        "INJECTION_CAPACITANCE",
        "DATA_TRANSMISSION",
        "LP_MODE",
        "OVERVOLTAGE_PROTECTION",
        "UNDERSHUNT_PROTECTION",
        "MIN_HEALTH_TEST",
        "TUNING",
        "PIXEL_FAILURE_ANALYSIS",
    ]

    qcEvals = []

    cprs = localdb.childParentRelation.find({"parent": str(component["_id"])})

    fes = []
    for cpr in cprs:
        subcpt = localdb.component.find_one({"_id": ObjectId(cpr["child"])})
        if subcpt["componentType"] == "front-end_chip":
            fes += [str(subcpt["_id"])]

    logger.info(f"#FEs = {len(fes)}: {fes}")

    for feTest in feTests:
        keyname = "_".join(["MODULE", feTest])

        passFails = []

        # The case for "not-evaluated" in the original analysis is dismissed.
        if new_test_run.get(keyname) == -1:
            continue

        for parameter, value in new_test_run["results"].items():
            # the code should contain the feTest string and LINK
            if not (parameter.find(feTest) >= 0 and parameter.find("LINK") >= 0):
                continue

            result = None
            if value:
                result = localdb.QC.result.find_one({"_id": ObjectId(value)})

            if result is not None:
                try:
                    passFails += [result["passed"]]
                except Exception:
                    # skipped case --> True
                    passFails += [None]
            else:
                passFails += [None]

            # print( feTest, code, result )

        # Take AND of the FE QC flags up to the last non-blank FE, but for blank input, always False
        isAllNone = all(f is None for f in passFails[: len(fes)])
        qcFlag = all(passFails[: len(fes)]) if not isAllNone else None

        new_test_run["results"][keyname] = int(qcFlag) if qcFlag is not None else -1

        qcEvals += [qcFlag]

    new_test_run["passed"] = all(qcEvals)

    new_test_run["sys"] = {
        "mts": datetime.now(timezone.utc),
        "cts": datetime.now(timezone.utc),
        "rev": 0,
    }
    new_test_run["results"]["properties"]["ANALYSIS_VERSION"] = qcAnalysisVersion

    new_test_run_id = str(localdb.QC.result.insert_one(new_test_run).inserted_id)

    return redirect(
        url_for(
            "component_api.show_component",
            id=str(component.get("_id")),
            test="qctest",
            runId=new_test_run_id,
        )
    )


@qc_api.route("/qc_input_manual", methods=["GET", "POST"])
def input_qcTests():
    if not session.get("logged_in", False):
        return render_template("401.html")

    page_docs = {"page": None, "title": "QC Test Results Manual Input Page"}

    componentID = request.args.get("componentID") or request.form.get("componentID")
    serialNumber = localdb.component.find_one({"_id": ObjectId(componentID)})["name"]

    cpt_type_raw = request.args.get("componentType") or request.form.get(
        "componentType"
    )
    componentType = component_type_code_map.get(cpt_type_raw, cpt_type_raw)

    stage = request.args.get("stage") or request.form.get("stage")
    testType = request.args.get("testType") or request.form.get("testType")
    mode = request.form.get("mode", "input")

    testFormat = userdb.QC.tests.find_one(
        {"componentType.code": componentType, "code": testType}
    )

    page_docs.update(
        {
            "componentID": componentID,
            "serialNumber": serialNumber,
            "componentType": componentType,
            "stage": stage,
            "testType": testType,
            "testFormat": testFormat,
            "testFormatHTML": json2html.convert(testFormat),
            "mode": mode,
            "properties": {},
            "parameters": {},
            "anaResult": None,
            "already_registered": False,
        }
    )

    for key in ["properties", "parameters"]:
        for item in testFormat[key]:
            page_docs[key][item["code"]] = request.form.get(item["code"])

    if mode == "confirm":
        page_docs.update(
            {
                "TimeStart": request.form["TimeStart"],
                "TimeStart_tz": request.form["TimeStart-tz"],
                "TimeEnd": request.form["TimeEnd"],
                "TimeEnd_tz": request.form["TimeEnd-tz"],
            }
        )
    elif mode == "submit":
        testRun = {
            "serialNumber": serialNumber,
            "testType": testType,
            "results": {
                "property": {
                    prop["code"]: parse_manual_input(
                        page_docs["properties"][prop["code"]],
                        prop["code"],
                        prop["dataType"],
                    )
                    for prop in testFormat["properties"]
                },
                "comment": "",
                "Measurements": {
                    param["code"]: parse_manual_input(
                        page_docs["parameters"][param["code"]],
                        param["code"],
                        param["dataType"],
                    )
                    for param in testFormat["parameters"]
                },
                "Metadata": {
                    "LocalDB version": qcAnalysisVersion,
                    "Institution": localdb.institution.find_one(
                        {"institution": session["institution"]}
                    )["code"],
                    "ModuleSN": serialNumber,
                    "TimeStart": dt_tz_to_timestamp(
                        request.form["TimeStart"], tzinfo=request.form["TimeStart-tz"]
                    ),
                    "TimeEnd": dt_tz_to_timestamp(
                        request.form["TimeEnd"], tzinfo=request.form["TimeEnd-tz"]
                    ),
                },
            },
            "stage": stage,
        }
        logger.info("submit testRun: " + pprint.pformat(testRun))

        component = localdb.component.find_one({"serialNumber": serialNumber})

        page_docs["anaResult"], page_docs["already_registered"] = save_raw_and_analyze(
            [testRun], component
        )
        page_docs["anaResultHTML"] = json2html.convert(page_docs["anaResult"])

    return render_template(
        "qc/input_qcTests.html",
        **page_docs,
        local_timezone=str(get_localzone()),
        datetime_local_now=arrow.now().format("YYYY-MM-DDTHH:MM"),
    )


def parse_manual_input(value, code, datatype):
    isTimestamp = (
        datatype in ["string", "integer"] and code.startswith("DATE_")
    ) or code.endswith("_DATE")

    if isTimestamp:
        return value

    try:
        if datatype == "integer":
            return int(value)

        if datatype == "float":
            return float(value)

        if datatype == "boolean":
            boolean_mapping = {"true": True, "false": False, "N/A": None}
            return boolean_mapping.get(value)

    except (TypeError, ValueError):
        return None

    return value


def save_raw_and_analyze(test, component):
    logger.info("begin")

    raw = copy.deepcopy(test)
    rawHash = hashlib.md5(json.dumps(test).encode()).hexdigest()

    for r in raw:
        r["component"] = component["_id"]
        r["dbVersion"] = dbv
        r["address"] = component["address"]
        with contextlib.suppress(Exception):
            r["user"] = session["username"]
        r["sys"] = {
            "mts": datetime.now(timezone.utc),
            "cts": datetime.now(timezone.utc),
            "rev": 0,
        }

    stage = test[0]["stage"]

    rawRecord = localdb.QC.testRAW.find_one({"rawHash": rawHash, "stage": stage})

    if rawRecord is None:
        raw_id = localdb.QC.testRAW.insert_one(
            {"raw": raw, "rawHash": rawHash, "stage": stage}
        ).inserted_id
    else:
        logger.info(
            "identical rawHash was detected on LocalDB: skipping to register RAW"
        )

        rawRecord["serialNumber"] = raw[0]["serialNumber"]
        rawRecord["component"] = component["_id"]
        rawRecord["dbVersion"] = dbv
        rawRecord["address"] = component["address"]
        with contextlib.suppress(Exception):
            rawRecord["user"] = session["username"]
        rawRecord["sys"] = {
            "mts": datetime.now(timezone.utc),
            "cts": datetime.now(timezone.utc),
            "rev": 0,
        }

        raw = [rawRecord]
        raw_id = raw[0]["_id"]

    logger.info("analyzing")

    try:
        return analyze_measurement(test, raw, component, raw_id)
    except Exception as e:
        localdb.QC.testRAW.delete_one({"rawHash": rawHash, "stage": stage})
        raise e


def analyze_measurement(
    test, raw, component, raw_id, update_config=True, recycle=False
):
    logger.info("begin")

    testTypeHyphened = None
    for t in test:
        testTypeHyphened = t["testType"].replace("_", "-")

    rawHash = hashlib.md5(json.dumps(test).encode()).hexdigest()
    tmpInputPath = f"{TMP_DIR}/{rawHash}/input"
    tmpOutputPath = f"{TMP_DIR}/{rawHash}/output"

    with contextlib.suppress(Exception):
        shutil.rmtree(tmpInputPath)
        shutil.rmtree(tmpOutputPath)

    os.makedirs(tmpInputPath, exist_ok=True)
    os.makedirs(tmpOutputPath, exist_ok=True)

    # write the RAW result to a temporary place
    try:
        with open(f"{tmpInputPath}/measurement.json", "w", encoding="utf-8") as f:
            logger.info(f"opened {tmpInputPath}/measurement.json")
            f.write(json.dumps([test]))
    except Exception as e:
        logger.error(str(e))
        logger.error(traceback.format_exc())

    logger.info(f"testTypeHyphened = {testTypeHyphened}")

    site_option = ""

    elecTests = [
        "ADC-CALIBRATION",
        "ANALOG-READBACK",
        "SLDO",
        "VCAL-CALIBRATION",
        "INJECTION-CAPACITANCE",
        "DATA-TRANSMISSION",
        "LP-MODE",
        "OVERVOLTAGE-PROTECTION",
        "UNDERSHUNT-PROTECTION",
        "MIN-HEALTH-TEST",
        "TUNING",
        "PIXEL-FAILURE-ANALYSIS",
        "IV-MEASURE",
        "FLATNESS",
        "LONG-TERM-STABILITY-DCS",
    ]

    try:
        raw_metadata = raw[0].get("results", {}).get("Metadata")
        if raw_metadata:
            if (
                raw_metadata.get("Institution") is None
                and testTypeHyphened in elecTests
            ):
                try:
                    site_option = " --site " + component.get("address").get("code")
                except Exception:
                    site_doc = localdb.institution.find_one(
                        {"_id": ObjectId(component.get("address"))}
                    )
                    if site_doc:
                        site_code = site_doc.get("code")
                        site_option = " --site " + site_code
        else:
            if testTypeHyphened in elecTests:
                try:
                    site_option = " --site " + component.get("address").get("code")
                except Exception:
                    site_doc = localdb.institution.find_one(
                        {"_id": ObjectId(component.get("address"))}
                    )
                    if site_doc:
                        site_code = site_doc.get("code")
                        site_option = " --site " + site_code
    except Exception as e:
        logger.error(str(e))
        logger.error(traceback.format_exc())

    depl_volt_option = ""
    if (
        testTypeHyphened == "IV-MEASURE"
        and component.get("componentType") != "sensor_tile"
    ):
        # fetch the sensor property
        sensors = get_module_sensors(component)

        assert sensors, f"Component sn={component.get('serialNumber')}, id={component.get('_id')} has no sensor tiles. IV measurement cannot be uploaded for this."

        # logger.info( pprint.pformat( [ sensor.get('serialNumber') for sensor in sensors ] ) )
        logger.info(pprint.pformat(get_component_property_codes(sensors[0])))

        sensor_depl_info = list(
            filter(
                lambda x: x is not None,
                [
                    get_component_property_value(sensor, "V_FULLDEPL")
                    for sensor in sensors
                ],
            )
        )

        if len(sensor_depl_info) > 0:
            vdepl_average = statistics.mean(sensor_depl_info)
            depl_volt_option = f"--vdepl {vdepl_average}"

    command = f"analysis-{testTypeHyphened} -i {tmpInputPath}/measurement.json -o {tmpOutputPath} {site_option} {depl_volt_option}"

    # Special treatment for IV where previous measurement result is referred.

    if testTypeHyphened in [
        "IV-MEASURE",
        "BARE-MODULE-SENSOR-IV",
    ]:
        previous_iv_data = fetch_reference_ivs(localdb, component["serialNumber"])

        command = f"{command} -r {tmpInputPath}/aux.json"

        with open(f"{tmpInputPath}/aux.json", "w", encoding="utf-8") as f:
            logger.info(f"opened {tmpInputPath}/aux.json")
            json.dump(previous_iv_data, f, indent=4)

    logger.info(command)

    try:
        subout = subprocess.run(command, shell=True, capture_output=True, check=False)

        logger.info(subout.stdout.decode())
        logger.info(subout.stderr.decode())

        # seek the analysis result file inside the output path
        output = None
        output_others = []
        for curdir, _dirs, files in os.walk(f"{TMP_DIR}/{rawHash}/output"):
            for filename in files:
                if filename.endswith(".json"):
                    output = os.path.join(curdir, filename)
                    outdir = curdir
                else:
                    output_others += [os.path.join(curdir, filename)]

        if not output:
            msg = f"Null output from {testTypeHyphened} analysis!"
            raise RuntimeError(msg)

        logger.info("output path: " + output)
        logger.info(f"subsidiary files: {output_others}")

        with open(output, encoding="utf-8") as f:
            anaResult = json.load(f)[0]

        # revise the FE config with the analysis output
        serialNumber = raw[0]["serialNumber"]
        cpt_doc = localdb.component.find_one({"serialNumber": serialNumber})

    except Exception as e:
        logger.error(str(e))
        logger.error(traceback.format_exc())
        logger.error(subout.stderr.decode())
        msg = f"Null output file is detected: the analysis command\n\n{command}\n\nmay have an issue\n\n\nstdout:\n\n{subout.stdout.decode()}\n\nstderr:\n\n{subout.stderr.decode()}"
        raise RuntimeError(msg) from e

    chip_api = ChipConfigAPI(client)

    try:
        if update_config and cpt_doc.get("componentType") == "front-end_chip":
            qcStatus = localdb.QC.module.status.find_one(
                {"component": str(cpt_doc.get("_id"))}
            )
            stage = qcStatus.get("stage")

            warm_list = [
                "WARM",
                "PARYLENE_UNMASKING",
                "WIREBOND_PROTECTION",
                "THERMAL_CYCLES",
                "LONG_TERM_STABILITY_TEST",
                "QC_CROSSCHECK",
                "RECEPTION",
            ]
            cold_list = ["COLD"]

            if testTypeHyphened == "LP-MODE":
                branch = "LP"
            elif any(phrase in stage for phrase in warm_list):
                branch = "warm"
            elif any(phrase in stage for phrase in cold_list):
                branch = "cold"
            else:
                msg = f"Trying to update configs for {testTypeHyphened} but not sure what branch to use"
                raise RuntimeError(msg)

            logger.info(f"config: {serialNumber}, {stage}, {branch}")

            config_id = chip_api.checkout(serialNumber, stage, branch)

            updated_config_data = None

            if config_id:
                config_data = chip_api.get_config(config_id, None, True)

                config_dir = f"{TMP_DIR}/{rawHash}/config"
                os.makedirs(config_dir, exist_ok=True)
                with open(
                    f"{TMP_DIR}/{rawHash}/config/{serialNumber}_{branch}.json",
                    "w",
                    encoding="utf-8",
                ) as f:
                    json.dump(config_data, f, indent=0)

                update_command = (
                    f"analysis-update-chip-config -i {outdir} -c {config_dir} | tee"
                )

                logger.info(update_command)
                subout = subprocess.run(update_command, shell=True, check=False)

                try:
                    with open(
                        f"{outdir}/{serialNumber}_{branch}.json.after", encoding="utf-8"
                    ) as f:
                        updated_config_data = json.load(f)
                except Exception as e:
                    logger.error(str(e))
                    logger.error(traceback.format_exc())

    except Exception as e:
        logger.error(str(e))
        logger.error(traceback.format_exc())
        msg = f"Null output file is detected: the analysis command\n\n{command}\n\nmay have an issue\n\n\nstdout:\n\n{subout.stdout}\n\n`stderr:\n\n{subout.stderr}"
        raise RuntimeError(msg) from e

    # identity check with the existing records
    # -- if the raw input hash and the analysis version are both identical,
    #    skip to record a new instance. Otherwise, insert one.

    already_registered = False

    prev_results = localdb.QC.result.find_one(
        {
            "raw_id": ObjectId(raw_id),
            "results.property.ANALYSIS_VERSION": anaResult.get("results")
            .get("property")
            .get("ANALYSIS_VERSION"),
        }
    )
    logger.info(
        f'queried existing TestRuns with {{ "raw_id" : {ObjectId(raw_id)}, "results.property.ANALYSIS_VERSION" : {anaResult.get("results").get("property").get("ANALYSIS_VERSION")} }}'
    )

    if prev_results and not recycle:
        already_registered = True

        logger.info(
            f"duplicated record identified (ObjectId {prev_results['_id']}): : not inserting"
        )
        anaResult = localdb.QC.result.find_one(
            {
                "raw_id": ObjectId(raw_id),
                "results.property.ANALYSIS_VERSION": anaResult.get("results")
                .get("property")
                .get("ANALYSIS_VERSION"),
            }
        )

    else:
        logger.info("no duplicated records: inserting a new analysis")

        # attach other files first
        attachments = {}
        for attachment in output_others:
            if any(
                attachment.endswith(ext)
                for ext in [
                    ".jpg",
                    "jpeg",
                    "png",
                    ".JPG",
                    ".JPEG",
                    ".PNG",
                    ".pdf",
                    ".log",
                ]
            ):
                item = {str(Path(attachment).name): submit_binary(attachment)}
                attachments.update(item)
                logger.info(f"submitted {attachment} to localdb gridfs: {item}")

        # acquire the output format from the test bank
        try:
            outFormat = userdb.QC.tests.find_one({"code": test[0]["testType"]})
            if outFormat is None:
                msg = f"ERROR: testType {test[0]['testType']} is not registered in LocalDB."
                raise ValueError(msg)

            for key in ["user", "address", "stage", "testType", "sys"]:
                with contextlib.suppress(Exception):
                    anaResult[key] = raw[0][key]

            # Format skeleton
            anaResult["component"] = str(raw[0]["component"])
            anaResult["raw_id"] = ObjectId(raw_id)
            anaResult["gridfs_attachments"] = attachments

        except Exception as e:
            logger.error(str(e))
            logger.error(traceback.format_exc())

        localdb.QC.result.insert_one(anaResult)

        # attach the record in QC status
        qcStatus = localdb.QC.module.status.find_one(
            {"component": str(component["_id"])}
        )
        if qcStatus is None:
            msg = f"ERROR: internal glitch in LocalDB: QC status of {component['_id']} is not found!"
            raise RuntimeError(msg)

        localdb.QC.module.status.update_one(
            {"component": str(component["_id"])},
            {
                "$set": {
                    f"QC_results.{test[0]['stage']}.{test[0]['testType']}": str(
                        anaResult["_id"]
                    )
                }
            },
        )

        try:
            if (
                update_config
                and cpt_doc.get("componentType") == "front-end_chip"
                and all(
                    [chip_api, config_id, updated_config_data, raw_id, anaResult["_id"]]
                )
            ):
                revision_prev = chip_api.get_info(config_id).get("current_revision_id")
                revision_current = chip_api.commit(
                    config_id,
                    updated_config_data,
                    f'config revision by {testTypeHyphened} with RAW data {raw_id} and analysis result {anaResult["_id"]!s}',
                )

                localdb.QC.result.update_one(
                    {"_id": anaResult["_id"]},
                    {
                        "$set": {
                            "config_id": config_id,
                            "config_revision_prev": str(revision_prev),
                            "config_revision_current": str(revision_current),
                        }
                    },
                )

        except Exception as e:
            logger.error(str(e))
            logger.error(traceback.format_exc())

    # clean workplace
    shutil.rmtree(f"{TMP_DIR}/{rawHash}")

    # we are returning this result back to the user, inject the serial number back in, instead
    anaResult["component"] = component["serialNumber"]

    return anaResult, already_registered


def submit_binary(path):
    with open(path, "rb") as f:
        binary = f.read()
    return fs.put(binary)


@qc_api.route("/create_summary", methods=["GET", "POST"])
def create_summary():
    if not session.get("logged_in", False):
        return render_template("401.html")

    component = request.args.get("component") or request.form.get("component")
    stage = request.args.get("stage") or request.form.get("stage")
    testType = request.args.get("test") or request.form.get("test")

    mode = request.form.get("mode", "default")

    formData = request.form

    msg = f"component = {component}, stage = {stage}, testType = {testType}, mode = {mode}"
    logger.debug(msg)
    logger.debug(pprint.pformat(formData))

    if not component:
        abort(500, "No component was found.")

    cpt = localdb.component.find_one({"_id": ObjectId(component)})

    testFormat = userdb.QC.tests.find_one(
        {"code": testType, "componentType.code": cpt["componentType"].upper()}
    )

    title = "Create Summary Test"

    pageDocs = {
        "mode": mode,
        "title": title,
        "component": cpt,
        "stage": stage,
        "testType": testType,
        "testFormat": testFormat,
        "formatRaw": json2html.convert(testFormat),
        "componentRaw": json2html.convert(cpt),
        "input": formData,
    }

    logger.debug(f"pageDocs = {pprint.pformat(pageDocs)}")

    callbacks = {
        "default": processCreateSummaryDefault,
        "input": processCreateSummaryInput,
        "done": processCreateSummaryDone,
    }

    callback = callbacks[mode]

    if callback:
        return callback(pageDocs)

    return None


def processCreateSummaryDefault(pageDocs):
    component = pageDocs["component"]
    stage = pageDocs["stage"]
    testType = pageDocs["testType"]
    testFormat = pageDocs["testFormat"]

    resultCandidates = {}

    # List of supported test types
    processFunctions = {"E_SUMMARY": process_E_SUMMARY}

    # Loop over parameters
    for parameter in testFormat["parameters"]:
        code = parameter["code"]

        processFunctions[testType](
            component, stage, code, resultCandidates, pageDocs["mode"]
        )

    pageDocs.update({"resultCandidates": resultCandidates})
    pageDocs.update({"resultCandidatesHTML": json2html.convert(resultCandidates)})

    return render_template("qc/create_summaryTest.html", **pageDocs)


def processCreateSummaryInput(pageDocs):
    component = pageDocs["component"]
    stage = pageDocs["stage"]
    testType = pageDocs["testType"]
    testFormat = pageDocs["testFormat"]

    pageDocs["mode"] = "confirm"

    resultCandidates = {}

    # List of supported test types
    processFunctions = {"E_SUMMARY": process_E_SUMMARY}

    # Loop-1: gather all inputs
    for parameter in testFormat["parameters"]:
        code = parameter["code"]

        processFunctions[testType](
            component, stage, code, resultCandidates, pageDocs["mode"]
        )

    pageDocs.update({"resultCandidates": resultCandidates})
    pageDocs.update({"resultCandidatesHTML": json2html.convert(resultCandidates)})

    if pageDocs["mode"] == "default":
        return render_template("qc/create_summaryTest.html", **pageDocs)

    ###########################################################
    # Input: create other field values from user's input

    cprs = localdb.childParentRelation.find({"parent": str(component["_id"])})

    fes = []
    for cpr in cprs:
        subcpt = localdb.component.find_one({"_id": ObjectId(cpr["child"])})
        if subcpt["componentType"] == "front-end_chip":
            fes += [str(subcpt["_id"])]

    logger.info(f"#FEs = {len(fes)}: {fes}")

    stage_doc = userdb.QC.stages.find_one({"code": "FE_CHIP"})
    FE_testInfo = stage_doc.get("stage_test")
    disabled_doc = stage_doc.get("disabled_tests")

    try:
        feTests = FE_testInfo[stage]
        logger.info(f"feTests = {feTests}")
    except Exception:
        logger.error(
            f"stage {stage} was not found in the FE_CHIP info of userdb.QC.stages"
        )

    if len(feTests) == 0:
        feTests = [
            "ADC_CALIBRATION",
            "SLDO",
            "VCAL_CALIBRATION",
            "ANALOG_READBACK",
            "LP_MODE",
            "OVERVOLTAGE_PROTECTION",
            "INJECTION_CAPACITANCE",
            "MIN_HEALTH_TEST",
            "TUNING",
            "PIXEL_FAILURE_ANALYSIS",
            "BAD_PIXEL_NUMBER",
            "UNDERSHUNT_PROTECTION",
            "DATA_TRANSMISSION",
        ]

    # logger.info( pprint.pformat(pageDocs['input'] ) )

    summaryEvals = {}
    qcEvals = []

    for feTest in feTests:
        passFails = []

        for parameter in testFormat["parameters"]:
            code = parameter["code"]

            # the code should contain the feTest string and LINK
            if not (code.find(feTest) >= 0 and code.find("LINK") >= 0):
                continue

            if feTest in disabled_doc.get(stage, {}).get("tests", {}):
                continue

            # here, pageDocs['input'] is ImmutableMultiDict, use get(key)
            candidateId = pageDocs["input"].get(code)

            if candidateId == "on":
                candidateId = "skipped"

            result = None
            if candidateId is not None and candidateId != "skipped":
                result = localdb.QC.result.find_one({"_id": ObjectId(candidateId)})
            elif candidateId == "skipped":
                result = "skipped"

            if result is not None:
                try:
                    passFails += [result["passed"]]
                except Exception:
                    # skipped case --> True
                    passFails += [None]
            else:
                passFails += [None]

            # logger.info( feTest, code, result )

        # Take AND of the FE QC flags up to the last non-blank FE, but for blank input, always False
        isAllNone = all(f is None for f in passFails[: len(fes)])
        qcFlag = all(passFails[: len(fes)]) if not isAllNone else None

        # logger.info( f'Test = {feTest}, isAllNone = {isAllNone}, qcFlag = {qcFlag}, passFails = {passFails[:len(fes)]}' )

        summaryEvals["_".join(["MODULE", feTest])] = qcFlag
        qcEvals += [qcFlag]

    pageDocs.update({"summaryEvals": summaryEvals})
    pageDocs.update({"qcPassed": all(qcEvals)})

    return render_template("qc/create_summaryTest.html", **pageDocs)


def getCandidateResults(componentId, stage, testType):
    results = []

    # logger.info( pprint.pformat( { 'component':componentId, 'stage':stage, 'testType':testType } ) )

    rs1 = localdb.QC.result.find(
        {
            "$and": [
                {"stage": stage},
                {"component": componentId},
                {"testType": testType},
            ]
        }
    ).sort("_id", 1)

    dropKeys = [
        "_id",
        "user",
        "address",
        "stage",
        "testType",
        "raw_id",
        "component",
        "sys",
        "prodDB_record",
        "dbVersion",
    ]

    for r in rs1:
        r_copy = copy.deepcopy(r)
        r_copy["Time Stamp"] = r["sys"]["mts"]
        for key in dropKeys:
            if key in r_copy:
                r_copy.pop(key)

        if "results" in r_copy:
            r_copy["Result Data"] = r_copy.pop("results")

        r["resultsHTML"] = json2html.convert(r_copy)
        r["Time Stamp"] = get_localtime_str(r["sys"]["mts"])
        r["componentId"] = componentId
        r["analysis_version"] = (
            r["results"]["properties"]["ANALYSIS_VERSION"]
            if "properties" in r["results"]
            else r["results"]["property"]["ANALYSIS_VERSION"]
        )
        results.append(r)

    return results


def process_E_SUMMARY(component, stage, code, resultCandidates, _mode):
    if component["componentType"].upper() == "OB_LOADED_MODULE_CELL":
        cprs_obmodule = localdb.childParentRelation.find(
            {"parent": str(component["_id"])}
        )
        for cpr in cprs_obmodule:
            childId = cpr["child"]
            child = localdb.component.find_one({"_id": ObjectId(childId)})
            if child["componentType"].upper() == "MODULE":
                component = child

    cprs = localdb.childParentRelation.find({"parent": str(component["_id"])})

    chips = []
    for cpr in cprs:
        childId = cpr["child"]
        child = localdb.component.find_one({"_id": ObjectId(childId)})
        if child["componentType"].upper() == "FRONT-END_CHIP":
            chips += [child]

    tokens = code.split("_")

    tmpList1 = []
    tmpList2 = []

    tmpFlag = False
    for tok in tokens[1:]:
        if tok == "FE":
            tmpFlag = True

        if not tmpFlag:
            tmpList1 += [tok]
        else:
            tmpList2 += [tok]

    paramTestType = "_".join(tmpList1)
    fieldName = "_".join(tmpList2)

    # if fieldName != '':
    #    logger.info( f'componentCode = {component["componentType"]}, paramTestType = {paramTestType}, fieldName = {fieldName}' )
    # else:
    #    logger.info( f'componentCode = {component["componentType"]}, paramTestType = {paramTestType}' )

    for componentType in ["MODULE", "PCB", "BARE_MODULE", "SENSOR_TILE", "FE"]:
        if fieldName.find(componentType) != 0:
            continue

        for chipOrder in range(4):
            if len(chips) < chipOrder + 1:
                continue

            if fieldName == f"{componentType}_LINK_{chipOrder + 1}":
                resultCandidates[code] = getCandidateResults(
                    str(chips[chipOrder]["_id"]), stage, paramTestType
                )


def processCreateSummaryDone(pageDocs):
    component = pageDocs["component"]
    stage = pageDocs["stage"]
    testType = pageDocs["testType"]

    pageDocs["mode"] = "done"

    # acquire the output format from the test bank

    outFormat = userdb.QC.tests.find_one({"code": testType})

    if outFormat is None:
        msg = f"ERROR: testType {testType} is not registered in LocalDB."
        raise RuntimeError(msg)

    # logger.debug( pprint.pformat( outFormat ) )

    out = {}
    out["component"] = str(component["_id"])
    out["address"] = component["address"]

    try:
        out["user"] = session["username"]
    except Exception:
        out["user"] = None

    out["sys"] = {
        "mts": datetime.now(timezone.utc),
        "cts": datetime.now(timezone.utc),
        "rev": 0,
    }
    out["stage"] = stage
    out["testType"] = testType
    out["raw_id"] = None

    out["results"] = {"properties": {}}

    summaryEvals = []

    # Parameter Loop-1: embedding user's inputs
    for parameter in outFormat["parameters"]:
        code = parameter["code"]
        dataType = parameter["dataType"]

        required = parameter["required"]
        result = pageDocs["input"].get(code)

        if result is None and not required:
            logger.info(
                f"dismissed non-required parameter { code } (dataType: { dataType }"
            )
            out["results"][code] = None
            continue

        if dataType == "boolean":
            out["results"][code] = (result or "false").lower() in ["true", "1"]
        elif dataType == "float":
            try:
                out["results"][code] = float(result)
            except ValueError:
                logger.warning(f"parameter { code }  failed in formatting to float")
                out["results"][code] = None
        elif dataType == "int":
            try:
                out["results"][code] = int(result)
            except ValueError:
                logger.warning(f"parameter { code }  failed in formatting to int")
                out["results"][code] = None
        elif dataType == "codeTable":
            if result == "True":
                out["results"][code] = 1
                summaryEvals += [True]
            elif result == "False":
                out["results"][code] = 0
                summaryEvals += [False]
            else:
                out["results"][code] = -1
        else:
            out["results"][code] = result if result != "on" else None
            if out["results"][code] is None:
                logger.warning(f"parameter { code } was not filled (None)")

    # Parameter Loop-2: collect bad pixels
    for parameter in outFormat["parameters"]:
        code = parameter["code"]
        dataType = parameter["dataType"]

        if "ELECTRICALLY_BAD_PIXEL_NUMBER_FE" in code:
            feIndex = code[-1]

            try:
                referenceResultId = out["results"][
                    f"MODULE_PIXEL_FAILURE_ANALYSIS_FE_LINK_{feIndex}"
                ]

                if referenceResultId:
                    referenceResult = localdb.QC.result.find_one(
                        {"_id": ObjectId(referenceResultId)}
                    )

                    value = referenceResult.get("results").get(
                        "PIXEL_FAILURE_ELECTRICALLY_FAILED"
                    )
                    out["results"][code] = value

                else:
                    logger.info(
                        f"reference result for {code} is missing, skipped to evaluate"
                    )

            except Exception as e:
                logger.error(str(e))
                logger.error(traceback.format_exc())
                logger.error(f"failure in evaluating {code}")

        elif "DISCONNECTED_PIXEL_NUMBER_FE" in code:
            feIndex = code[-1]

            try:
                referenceResultId = out["results"][
                    f"MODULE_PIXEL_FAILURE_ANALYSIS_FE_LINK_{feIndex}"
                ]

                if referenceResultId:
                    referenceResult = localdb.QC.result.find_one(
                        {"_id": ObjectId(referenceResultId)}
                    )

                    value = referenceResult.get("results").get(
                        "PIXEL_FAILURE_DISCONNECTED_PIXELS"
                    )
                    out["results"][code] = value

                else:
                    logger.info(
                        f"reference result for {code} is missing, skipped to evaluate"
                    )

            except Exception as e:
                logger.error(str(e))
                logger.error(traceback.format_exc())
                logger.error(f"failure in evaluating {code}")

        elif "BAD_PIXEL_NUMBER_FE" in code:
            feIndex = code[-1]

            try:
                referenceResultId = out["results"][
                    f"MODULE_PIXEL_FAILURE_ANALYSIS_FE_LINK_{feIndex}"
                ]

                if referenceResultId:
                    referenceResult = localdb.QC.result.find_one(
                        {"_id": ObjectId(referenceResultId)}
                    )

                    value = referenceResult.get("results").get(
                        "PIXEL_FAILURE_FAILING_PIXELS"
                    )
                    if value >= 0:
                        out["results"][code] = value
                    else:
                        out["results"][code] = None

                else:
                    logger.info(
                        f"reference result for {code} is missing, skipped to evaluate"
                    )

            except Exception as e:
                logger.error(str(e))
                logger.error(traceback.format_exc())
                logger.error(f"failure in evaluating {code}")

    # Fill the rest parameters
    summaryVarList = [
        "MODULE_ELECTRICALLY_BAD_PIXEL_NUMBER",
        "MODULE_DISCONNECTED_PIXEL_NUMBER",
        "MODULE_BAD_PIXEL_NUMBER",
    ]

    for var in summaryVarList:
        elems = [
            out["results"][f"{var}_FE_{feIndex}"]
            for feIndex in range(1, 5)
            if out["results"][f"{var}_FE_{feIndex}"] is not None
        ]
        if len(elems) == 0:
            out["results"][var] = None
        else:
            out["results"][var] = sum(elems)

    for prop in outFormat["properties"]:
        if prop["code"] == "ANALYSIS_VERSION":
            out["results"]["properties"][prop["code"]] = qcAnalysisVersion

        if prop["code"] == "MODULE_TEMPERATURE":
            if stage.lower().find("warm") >= 0:
                out["results"]["properties"][prop["code"]] = 20
            elif stage.lower().find("cold") >= 0:
                out["results"]["properties"][prop["code"]] = -15
            else:
                out["results"]["properties"][prop["code"]] = -99

    qcPassed = all(summaryEvals)
    out["passed"] = qcPassed

    # logger.info( pprint.pformat( out ) )

    # Insert TestRun
    localdb.QC.result.insert_one(out)

    # attach the record in QC status
    logger.info("attach the record in QC status")
    qcStatus = localdb.QC.module.status.find_one({"component": str(component["_id"])})
    if qcStatus is None:
        msg = f"ERROR: internal glitch in LocalDB: QC status of {component['_id']} is not found!"
        raise RuntimeError(msg)

    localdb.QC.module.status.update_one(
        {"component": str(component["_id"])},
        {"$set": {f"QC_results.{out['stage']}.{out['testType']}": str(out["_id"])}},
    )

    logger.info(f'finding cprs: parent_id = {component["_id"]}')
    _cprs = localdb.childParentRelation.find({"parent": str(component["_id"])})
    FEs = []
    for cpr in _cprs:
        child = localdb.component.find_one({"_id": ObjectId(cpr["child"])})
        if child["componentType"] != "front-end_chip":
            continue
        FEs += [str(child["_id"])]

    # loop over test parameters and update the QC.module.status of the FE chips accordingly
    logger.info(
        "loop over test parameters and update the QC.module.status of the FE chips accordingly"
    )

    for parameter in outFormat["parameters"]:
        code = parameter["code"]

        if not code.find("FE_LINK") >= 0:
            continue

        feTestCode = "_".join(code.split("_")[1:-3])
        feIndex = int(code.split("_")[-1]) - 1

        # logger.info( f'{code} --> {feTestCode}, {feIndex}' )

        if feIndex >= len(FEs):
            continue

        fe_cpt_id = FEs[feIndex]

        logger.info(
            f'updating FE chip {fe_cpt_id} QC_results.{out["stage"]}.{feTestCode} --> {out["results"][ code ]}'
        )
        if out["results"][code] is not None:
            localdb.QC.module.status.update_one(
                {"component": fe_cpt_id},
                {
                    "$set": {
                        f'QC_results.{out["stage"]}.{feTestCode}': out["results"][code]
                    }
                },
            )
        else:
            localdb.QC.module.status.update_one(
                {"component": fe_cpt_id},
                {"$set": {f'QC_results.{out["stage"]}.{feTestCode}': "-1"}},
            )

    return redirect(
        url_for(
            "component_api.show_component",
            id=str(component["_id"]),
        )
    )


@qc_api.route("/select_test", methods=["GET", "POST"])
def select_test():
    initPage()

    page_docs = {"run": [], "text": ""}
    page_docs["mode"] = request.form.get("mode", "input")

    page_docs["page"] = "select_test"
    page_docs["title"] = "Sign off QC test in this stage"

    if not session.get("logged_in", False):
        return render_template("401.html")

    try:
        session.get("logged_in", False)
    except Exception:
        page_docs["mode"] = "input"
        page_docs["text"] = "Please log in before the selection."
        return render_template("localSignoff.html", **page_docs)

    componentId = request.args.get("componentId")
    page_docs["componentId"] = componentId

    thisComponent = localdb.component.find_one(
        {"_id": ObjectId(page_docs["componentId"])}
    )
    page_docs["componentName"] = str(thisComponent["name"])

    # logger.debug( 'select_test(): thisComponent = '+pprint.pformat( thisComponent ) )

    componentType = component_type_code_map[thisComponent["componentType"]]
    page_docs["componentType"] = componentType

    thisComponentQCStatus = localdb.QC.module.status.find_one(
        {"component": str(thisComponent["_id"])}
    )

    current_stage = thisComponentQCStatus["stage"]

    page_docs["stage"] = current_stage
    try:
        page_docs["testTypes"] = list(
            thisComponentQCStatus["QC_results"][page_docs["stage"]]
        )
    except Exception:
        page_docs["testTypes"] = []
    page_docs["tests"] = list(
        localdb.QC.result.find(
            {"component": componentId, "stage": current_stage},
        )
    )

    # Augment test results
    for test in page_docs["tests"]:
        test["resultHTML"] = json2html.convert(test["results"])

        test["timestamp"] = get_localtime_str(test["sys"]["mts"])
        # if "ANALYSIS_VERSION" is missing, set it to None
        test["analysis_version"] = (
            test["results"]["properties"].get("ANALYSIS_VERSION")
            if "properties" in test["results"]
            else test["results"]["property"].get("ANALYSIS_VERSION")
        )

        selected = request.form.get(f"{test['testType']}_id")

        if selected == str(test["_id"]):
            test["selected"] = True

    # unselected flags (indicating blank submission of the test)
    page_docs["unselected"] = []
    entitledTests = {}  # { testName : testID }
    for testType in page_docs["testTypes"]:
        selected = request.form.get(f"{testType}_id")
        if selected == "":
            page_docs["unselected"] += [testType]
            entitledTests[testType] = "-1"
        else:
            entitledTests[testType] = selected

    if page_docs["mode"] == "complete" or page_docs["mode"] == "complete_sync":
        localdb.QC.module.status.update_one(
            {"component": str(thisComponent["_id"])},
            {
                "$set": {
                    ("QC_results." + page_docs["stage"]): entitledTests,
                    ("upload_status." + page_docs["stage"]): "-1",
                }
            },
        )

        for testname in entitledTests:
            localdb.QC.module.status.update_one(
                {"component": str(thisComponent["_id"])},
                {"$set": {(f'QC_results_pdb.{page_docs["stage"]}.{testname}'): "-1"}},
            )

        for testType in page_docs["testTypes"]:
            entitledTestId = entitledTests[testType]

            test = None
            for t in page_docs["tests"]:
                if str(t["_id"]) == entitledTestId:
                    test = t
                    break

            if test is None:
                continue

            for fieldCode in test["results"]:
                value = test["results"][fieldCode]

                # logger.debug( f'{fieldCode}, {value}' )

                # Rule: tests with a test link should have LINK in the name
                if fieldCode.find("LINK") < 0:
                    continue

                resultId = value

                if resultId is None:
                    continue

                result = localdb.QC.result.find_one({"_id": ObjectId(resultId)})

                if result is None:
                    logger.warning(
                        f"ResultId {resultId} is requested but not found in DB!"
                    )
                    continue

                # logger.info( pprint.pformat( result ) )
                subComponentId = result["component"]
                subComponentStage = result["stage"]
                subComponentTestType = result["testType"]

                localdb.QC.module.status.update_one(
                    {"component": subComponentId},
                    {
                        "$set": {
                            f"QC_results.{subComponentStage}.{subComponentTestType}": resultId,
                            ("upload_status." + page_docs["stage"]): "-1",
                        }
                    },
                )

                subComponentQCStatus = localdb.QC.module.status.find_one(
                    {"component": subComponentId}
                )
                logger.debug(pprint.pformat(subComponentQCStatus))

            # endfor fieldCode
        # endfor testType

        stageInfo = userdb.QC.stages.find_one({"code": componentType})
        next_stage_index = min(
            len(stageInfo["stage_flow"]) - 1,
            stageInfo["stage_flow"].index(current_stage) + 1,
        )
        if request.form.get("transitionToAlternative") == "enable":
            next_stages_required = (
                stage for stage in stageInfo["stage_flow"][next_stage_index:]
            )
        else:
            next_stages_required = (
                stage
                for stage in stageInfo["stage_flow"][next_stage_index:]
                if stage not in stageInfo["alternatives"]
            )
        try:
            next_stage = next(next_stages_required)
        except StopIteration:
            next_stage = current_stage

        changed_components = sync_component_stages(
            localdb,
            thisComponent["serialNumber"],
            next_stage,
            userdb=userdb,
            ignore_types=["module_carrier"],
        )
        msg = f"status of changing component stages recursively: {changed_components}"
        logger.info(msg)

        if page_docs["mode"] == "complete":
            return redirect(
                url_for(
                    "component_api.show_component",
                    id=str(thisComponent["_id"]),
                )
            )

        if page_docs["mode"] == "complete_sync":
            return redirect(
                url_for(
                    "qc_api.result_transceiver",
                    module=thisComponent["name"],
                    mode="upload_input",
                )
            )

    return render_template("localSignoff.html", **page_docs)


@qc_api.route("/switch_stage/<serial_number>", methods=["GET", "POST"])
@qc_api.route(
    "/switch_stage/<serial_number>/<any(expert,previous,alternative):mode>",
    methods=["GET", "POST"],
)
def switch_stage(serial_number, mode="previous"):
    if not session.get("logged_in", False):
        return render_template("401.html")

    destStage = request.form.get("destStage")
    touch = request.form.get("touch")

    text = ""

    module_doc = localdb.component.find_one({"name": serial_number})
    componentId = str(module_doc["_id"])

    componentType = module_doc.get("componentType")
    if componentType is None:
        logger.warning(
            f"componentType for component {serial_number} is not defined in LocalDB!"
        )

    qcStagesDoc = userdb.QC.stages.find_one(
        {"code": component_type_code_map[componentType]}
    )

    stageListAll = qcStagesDoc["stage_flow"]
    alternativeList = qcStagesDoc["alternatives"]
    stageList = [
        stage
        for stage in stageListAll
        if stage not in alternativeList
        and not qcStagesDoc.get("disabled_tests", {})
        .get(stage, {})
        .get("disabled", False)
    ]

    # When operating QC for a sub-component, the stage should not go beyond
    # post-assembly stages
    exclusion_keys = []
    if componentType == "ob_loaded_module_cell":
        pass
    elif componentType == "module":
        exclusion_keys.append("OB_LOADED_MODULE_CELL/")
    else:
        exclusion_keys.append("OB_LOADED_MODULE_CELL/")
        exclusion_keys.append("MODULE/")

    # Exclusion applies to both alternativeList and stageList
    alternativeList = [
        stage
        for stage in alternativeList
        if not any(stage.startswith(key) for key in exclusion_keys)
    ]
    stageList = [
        stage
        for stage in stageList
        if not any(stage.startswith(key) for key in exclusion_keys)
    ]

    qc_module_doc = localdb.QC.module.status.find_one({"component": componentId})
    currentStage = qc_module_doc.get("stage", stageList[0])

    if mode == "previous":
        try:
            stageList = [
                stage
                for i, stage in enumerate(stageList)
                if i <= stageList.index(currentStage)
            ]
        except Exception:
            return switch_stage(serial_number, mode="expert")

    if destStage is None:
        text = "Please select stage from the pull-down menu..."
        doc = {
            "_id": componentId,
            "serial_number": serial_number,
            "stage": currentStage,
            "nstage": destStage,
            "text": text,
            "stages": stageList,
            "alternatives": alternativeList,
            "mode": mode,
        }
        return render_template("switchStage.html", doc=doc)

    if request.method == "POST":
        logger.debug(f"attempting to switch stage from {currentStage} to {destStage}")

        changed_components = sync_component_stages(
            localdb,
            serial_number,
            destStage,
            userdb=userdb,
            ignore_types=["module_carrier"],
        )
        msg = f"status of changing component stages recursively: {changed_components}"
        logger.debug(msg)

        changed_components_html = ""
        for comp_serial_number, (orig_stage, was_changed) in changed_components.items():
            changed_components_html += (
                f"<li>{comp_serial_number}: <code>{orig_stage}</code>"
            )
            if was_changed:
                changed_components_html += f" to <code>{destStage}</code>"
            else:
                changed_components_html += " (unchanged)"
            changed_components_html += "</li>"

        create_message(
            f"The following components were handled: <br /><ul>{changed_components_html}</ul>",
            function="switch_stage",
            component=serial_number,
            code="INFO",
        )

        if not touch and currentStage in alternativeList:
            localdb.QC.module.status.update_one(
                {"_id": qc_module_doc["_id"]},
                {"$set": {f"upload_status.{currentStage}": "alternative"}},
            )
            logger.debug("Untouching alternative stage")
            create_message(
                f"This component was previously in an alternative stage ({currentStage}) and it has been untouched.",
                function="switch_stage",
                component=serial_number,
                code="INFO",
            )

        mode = "local_input"
        text = "The current stage was switched to " + destStage + "."

        return redirect(url_for("component_api.show_component", id=componentId))

    doc = {
        "_id": componentId,
        "serial_number": serial_number,
        "stage": currentStage,
        "nstage": destStage,
        "text": text,
        "stages": stageList,
        "alternatives": alternativeList,
        "mode": mode,
    }
    return render_template("switchStage.html", doc=doc)


@qc_api.route("/result_transceiver", methods=["GET", "POST"])
def result_transceiver():
    if not session.get("logged_in", False):
        return render_template("401.html")

    doc = {}
    module = request.args.get("module", "")
    doc["_id"] = request.args.get("id", "")
    mode = request.args.get("mode") or request.form.get("mode", "select")
    text = request.args.get("text", "")

    query = {"name": module}
    component_doc = localdb.component.find_one(query)

    doc = {
        "_id": str(component_doc["_id"]),
        "mname": module,
        "mode": mode,
        "text": text,
    }

    msg = f"result_transceiver(): mode = {doc['mode']}"
    logger.debug(msg)

    ####################
    ## Upload results ##
    if doc["mode"] == "select":
        doc["upload_summary"] = {}
        status_doc = localdb.QC.module.status.find_one({"component": doc["_id"]})
        doc["upload_summary"] = status_doc.get("upload_status", {})

    if doc["mode"] == "upload_input":
        summary = {}
        uploaded_stage = []
        status_doc = localdb.QC.module.status.find_one({"component": doc["_id"]})

        doc["stageInfo"] = userdb.QC.stages.find_one(
            {"code": status_doc["componentType"]}
        )
        stage_flow = doc["stageInfo"]["stage_flow"]

        # logger.debug( pprint.pformat( status_doc ) )
        # logger.debug( pprint.pformat( status_doc["stage"] ) )

        currentIndex = stage_flow.index(status_doc["stage"])

        for index, stage in enumerate(stage_flow):
            if stage == status_doc["stage"]:
                break

            # skip alternative stage if it is not the previous stage
            if stage in doc["stageInfo"]["alternatives"] and index != currentIndex - 1:
                continue

            try:
                if status_doc["upload_status"][stage] == "-1":
                    summary[stage] = status_doc["QC_results"][stage]
                else:
                    uploaded_stage.append(stage)
            except Exception:
                logger.warning(
                    f"Stage {stage} not found in the QC status doc. Perhaps the stage definition has changed on the PDB and the local record does not reflect it yet. Please pull the module from ITkPD and re-synchronize."
                )

        doc["upload_summary"] = summary
        doc["uploaded_stage"] = uploaded_stage

    if doc["mode"] == "upload_complete":
        code1 = request.form.get("code1")
        code2 = request.form.get("code2")
        institution = session["institution"]

        if not check_and_process(code1, code2, institution):
            return redirect(
                url_for(
                    "qc_api.result_transceiver",
                    module=doc["mname"],
                    mode="upload_input",
                    text="ITkPD client authentication failed. Please re-check your credentials, and your institution, and try again.",
                )
            )

        executor = concurrent.futures.ThreadPoolExecutor()
        executor.submit(upload_worker, module, get_pd_client())
        executor.shutdown(wait=False)

        return redirect(url_for("component_api.show_component", id=doc["_id"]))

    return render_template("result_tranceiver.html", doc=doc)


@qc_api.route("/site_qualification", methods=["GET", "POST"])
def site_qualification():
    # if not session.get("logged_in", False):
    #    return render_template( "401.html" )

    mode = request.form.get("mode")

    logger.info(f"mode = {mode}")

    doc = {}

    if mode is None:
        doc["mode"] = "init"
        return render_template("site_qualification.html", doc=doc)

    if mode == "select":
        code1 = request.form.get("code1")
        code2 = request.form.get("code2")

        flag, site_qualification.client = process_request(code1, code2)

        if flag == 0:
            doc["mode"] = "init"
            doc["message"] = "Failed in ITkPD authentication: Please retry."
            return render_template("site_qualification.html", doc=doc)

        site_qualification.sites = list(
            site_qualification.client.get(
                "listComponents", json={"project": "P", "componentType": "MODULESQ"}
            )
        )

        doc["sites"] = site_qualification.sites

        doc["mode"] = mode

        return render_template("site_qualification.html", doc=doc)

    if mode == "submit":
        site = request.form.get("selectedSite")

        site_qualification.institution_code = None

        for s in site_qualification.sites:
            if s["serialNumber"] == site:
                site_qualification.institution_code = s["institution"]["code"]

        logger.info(f"Site code == { site }")

        doc["site"] = site
        doc["localdb_version"] = qcAnalysisVersion

        fetch_sysdata(doc)
        fetch_dbdata(doc)

        doc["dataHTML"] = json2html.convert(doc)
        doc["mode"] = mode

        return render_template("site_qualification.html", doc=doc)

    if mode == "complete":
        site = request.form.get("selectedSite")

        logger.info(f"Site code == { site }")

        doc["site"] = site
        doc["localdb_version"] = qcAnalysisVersion

        fetch_sysdata(doc)
        fetch_dbdata(doc)

        doc["mode"] = mode

        site_qualification.client.post(
            "setComponentStage", json={"component": site, "stage": "DB"}
        )

        for test in ["DB_SETUP", "DB_ACC", "DB_NON", "DB_HELP"]:
            tr = site_qualification.client.get(
                "generateTestTypeDtoSample",
                json={"project": "P", "componentType": "MODULESQ", "code": test},
            )

            hashid = hashlib.md5(
                json.dumps(doc, cls=MongoJSONEncoder).encode()
            ).hexdigest()[:30]

            tr.update(
                {
                    "component": site,
                    "institution": site_qualification.institution_code,
                    "date": datetime.now(timezone.utc).strftime("%Y-%m-%dT%H:%MZ"),
                    "runNumber": hashid,
                    "properties": {"SQ_STATUS": "0"},
                }
            )

            logger.info(pprint.pformat(tr))

            res = site_qualification.client.post("uploadTestRunResults", json=tr)

            logger.info(pprint.pformat(res))

            with io.TextIOWrapper(
                io.BytesIO(),
                encoding="utf-8",
                line_buffering=True,
            ) as attachment_fpointer:
                json.dump(doc, attachment_fpointer, cls=MongoJSONEncoder)
                attachment_fpointer.seek(0, 0)

                files = {
                    "data": itkdb.utils.get_file_components(
                        {"data": (f"{hashid}.json", attachment_fpointer)}
                    )
                }

                res = site_qualification.client.post(
                    "createTestRunAttachment",
                    data={
                        "testRun": res["testRun"]["id"],
                        "type": "file",
                        "title": "server_info.json",
                        "url": f"{hashid}.json",
                        "description": "server_info.json",
                    },
                    files=files,
                )

                logger.info(pprint.pformat(res))

        return render_template("site_qualification.html", doc=doc)

    msg = "This should not have happened."
    raise RuntimeError(msg)


class MongoJSONEncoder(json.JSONEncoder):
    def default(self, o: Any) -> Any:
        if isinstance(o, ObjectId):
            return str(o)
        if isinstance(o, datetime):
            return str(o)
        return json.JSONEncoder.default(self, o)


def fetch_sysdata(doc):
    subout = subprocess.run(
        "uname -a", shell=True, capture_output=True, text=True, check=False
    )
    doc["uname"] = subout.stdout

    if doc["uname"].find("Linux") == 0:
        subout = subprocess.run(
            "lscpu", shell=True, capture_output=True, text=True, check=False
        )
        doc["cpu"] = subout.stdout.split("\n")

        subout = subprocess.run(
            "lsmem", shell=True, capture_output=True, text=True, check=False
        )
        doc["memory"] = subout.stdout.split("\n")

    elif doc["uname"].find("Darwin") == 0:
        subout = subprocess.run(
            "system_profiler SPSoftwareDataType SPHardwareDataType",
            shell=True,
            capture_output=True,
            text=True,
            check=False,
        )
        doc["sys_profile"] = subout.stdout.split("\n")

    subout = subprocess.run(
        "df -h", shell=True, capture_output=True, text=True, check=False
    )
    doc["disk_space"] = subout.stdout.split("\n")

    subout = subprocess.run(
        "mongod --version", shell=True, capture_output=True, text=True, check=False
    )
    doc["mongod"] = subout.stdout.split("\n")

    subout = subprocess.run(
        "influxd version", shell=True, capture_output=True, text=True, check=False
    )
    doc["influxd"] = subout.stdout.split("\n")


def fetch_dbdata(doc):
    doc["localdb"] = {}
    doc["localdbtools"] = {}

    for collection in localdb.list_collection_names():
        if collection in ["fs.chunks", "fs.files"]:
            continue

        doc["localdb"][collection] = localdb[collection].find_one(
            {}, sort=[("_id", DESCENDING)]
        )

    for collection in userdb.list_collection_names():
        doc["localdbtools"][collection] = userdb[collection].find_one(
            {}, sort=[("_id", DESCENDING)]
        )


@qc_api.route("/compare_tests", methods=["GET", "POST"])
def compare_tests():
    module_sn = request.form.get("module_sn", type=str, default=None)
    subcpt = request.form.get("subcpt", type=str, default=None)
    test_type = request.form.get("test_type", type=str, default=None)
    stage1 = request.form.get("stage1", type=str, default=None)
    stage2 = request.form.get("stage2", type=str, default=None)

    state_info = {
        "module_sn": module_sn,
        "subcpt": subcpt,
        "test_type": test_type,
        "stage1": stage1,
        "stage2": stage2,
    }

    if not module_sn:
        modules = sorted(
            [
                doc.get("serialNumber")
                for doc in localdb.component.find(
                    {"componentType": "module"}, {"serialNumber": 1}
                )
            ]
        )
        state_info.update({"module_list": modules})

        state_info.update({"step": "init"})

        return render_template("compare_tests.html", state=state_info)

    if not subcpt:
        state_info.update({"step": "subcpt"})

        component_types = list(userdb.componentType.find({}, {"name": 1, "code": 1}))
        state_info.update({"cpt_list": component_types})

        return render_template("compare_tests.html", state=state_info)

    if not test_type:
        state_info.update({"step": "test_type"})

        test_types = list(
            userdb.QC.tests.find(
                {"componentType.code": subcpt, "state": "active"}
            ).sort([("code", pymongo.ASCENDING)])
        )

        # In order for a comparison to work, the test is defined in at least 2 stages
        stage_tests = userdb.QC.stages.find_one({"code": subcpt}).get("stage_test")

        test_types_valid = []

        for test in test_types:
            n_tests = sum(
                (test.get("code") in test_list)
                for _stage, test_list in stage_tests.items()
            )
            if n_tests >= 2:
                test_types_valid.append(test)

        state_info.update({"test_list": test_types_valid})

        return render_template("compare_tests.html", state=state_info)

    if not stage1 or not stage2:
        # Limit the stages only to those which have the specified test type
        stage_info = userdb.QC.stages.find_one({"code": subcpt})
        stage_tests = stage_info.get("stage_test")
        stage_flow = stage_info.get("stage_flow")

        module_doc_id = str(
            localdb.component.find_one({"serialNumber": module_sn}).get("_id")
        )
        current_stage = localdb.QC.module.status.find_one(
            {"component": module_doc_id}
        ).get("stage")

        msg = f"currentStage: {current_stage}"
        logger.info(msg)

        # Up to the current stage
        stage_tests_valid = []

        for st in stage_flow:
            tests = stage_tests.get(st)
            if test_type in tests:
                logger.info(st)
                stage_tests_valid.append(st)

            if st == current_stage:
                break

        state_info.update({"step": "stage"})
        state_info.update({"stage_list": stage_tests_valid})

        return render_template("compare_tests.html", state=state_info)

    # ---------------------------------
    # Display comparison

    state_info.update({"step": "display"})

    compare_data = []

    for stage in [stage1, stage2]:
        # list matching components recursively
        module_doc = localdb.component.find_one({"serialNumber": module_sn})

        valid_child_docs = [
            child_doc
            for child_doc in get_module_children(module_doc)
            if child_doc.get("componentType") == component_type_code_map.get(subcpt)
        ]

        data = []

        for child_doc in valid_child_docs:
            data_obj = {}

            child_sn = child_doc.get("serialNumber")
            qc_status = localdb.QC.module.status.find_one(
                {"component": str(child_doc.get("_id"))}
            ).get("QC_results")

            test_id = qc_status.get(stage).get(test_type)

            try:
                test_data = localdb.QC.result.find_one({"_id": ObjectId(test_id)})
                for key in [
                    "prodDB_record",
                    "component",
                    "user",
                    "address",
                    "sys",
                    "testType",
                    "pdb_runNumber",
                    "stage",
                    "_id",
                    "dbVersion",
                ]:
                    test_data.pop(key)

                if "raw_id" in test_data:
                    raw_data = localdb.QC.testRAW.find_one(
                        {"_id": test_data.get("raw_id")}
                    )
                    test_data["raw_id"] = json2html.convert(raw_data)

                if "gridfs_attachments" in test_data:
                    attachments = dump_cache(test_data["gridfs_attachments"])
                    test_data["gridfs_attachments"] = attachments

                data_obj = {
                    "stage": stage,
                    "serialNumber": child_sn,
                    "result": test_data,
                }

            except Exception:
                data_obj = {"stage": stage, "serialNumber": child_sn, "result": None}

            data.append(data_obj)

        compare_data.append(data)

    state_info["compare_data"] = compare_data
    return render_template("compare_tests.html", state=state_info)


def dump_cache(attachments):
    att_data = {}

    for filename, oid in attachments.items():
        os.makedirs(THUMBNAIL_DIR, exist_ok=True)

        ext = filename.split(".")[-1]

        cache_file = f"{THUMBNAIL_DIR}/{oid!s}.{ext}"

        if os.path.exists(cache_file):
            pass
        else:
            try:
                data = fs.get(oid).read()

                with open(cache_file, "wb") as f:
                    f.write(data)
            except Exception as e:
                logger.error(str(e))
                logger.error(traceback.format_exc())
                logger.error(f"Failed in dumping attachment to {cache_file}")

        att_data.update({cache_file[cache_file.find("static") :]: [filename, ext]})

        logger.info(f"attachment dumped to cache {cache_file}")

    return att_data


@qc_api.route("/download_testrun_raw", methods=["GET", "POST"])
def download_testRun_raw():
    testRun_id = request.args.get("testRun", type=str, default=None)

    testRun = localdb.QC.result.find_one({"_id": ObjectId(testRun_id)})
    raw_id = testRun.get("raw_id")
    if not raw_id:
        return make_response(f"no raw for testRun with id {testRun_id}.")

    component = localdb.component.find_one({"_id": ObjectId(testRun["component"])})

    try:
        raw = localdb.QC.testRAW.find_one({"_id": ObjectId(raw_id)})

        name = f"{component['serialNumber']}_{testRun['testType']}_{testRun['stage'].replace('/','-')}_RAW.json"

        raw_contents = raw.get("raw")
        for key in [
            "_id",
            "address",
            "component",
            "raw_id",
            "sys",
            "user",
        ]:
            for raw_content in raw_contents:
                raw_content.pop(key, None)

        raw_buffer = io.BytesIO(json.dumps(raw_contents, indent=4).encode())
        raw_buffer.seek(0, 0)

        return send_file(
            raw_buffer,
            as_attachment=True,
            mimetype="application/json",
            download_name=name,
        )

    except Exception as e:
        return make_response(
            f"something went wrong: testRun_id = {testRun_id}, raw_id = {raw_id}: {e!s}"
        )
