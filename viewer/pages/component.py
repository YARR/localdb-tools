from __future__ import annotations

import ast
import base64  # Base64 encoding scheme
import concurrent.futures
import contextlib
import copy
import io
import json
import logging

# import module for metrology
import os
import pickle
import pprint
import time
import traceback
import zipfile
from datetime import datetime
from pathlib import Path

import filetype
import gridfs  # gridfs system
import itkdb
import matplotlib.pyplot as plt
import numpy as np
from bson.objectid import ObjectId
from flask import (
    Blueprint,
    abort,
    flash,
    make_response,
    redirect,
    render_template,
    request,
    send_file,
    session,
    url_for,
)
from functions.checkouter import ScanCheckoutManager
from functions.common import (
    CACHE_DIR,
    THUMBNAIL_DIR,
    TMP_DIR,
    VIEWER_DIR,
    SN_typeinfo,
    cleanDir,
    clear_message,
    client,
    component_type_code_map,
    create_message,
    dbv,
    delim_SN,
    format_messages,
    fs,
    get_localtime_str,
    get_pd_client,
    initPage,
    localdb,
    message_exists,
    mqat_version,
    plotting_mutex_lock,
    plotting_mutex_lock_timestamp,
    proddbv,
    qcAnalysisVersion,
    setTime,
    update_message,
    userdb,
)
from functions.plot_root import setPlots
from functions.SensorIV import layout_ModuleQC

#######################
### route functions ###
#######################
from json2html import json2html
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.colors import LogNorm
from module_qc_database_tools.chip_config_api import ChipConfigAPI
from module_qc_database_tools.sync_component_stages import sync_component_stages
from module_qc_database_tools.utils import (
    chip_serial_number_to_uid,
)
from PIL import Image
from rich.console import Console
from rich.table import Table
from tzlocal import get_localzone

logger = logging.getLogger("localdb")

component_api = Blueprint("component_api", __name__)


########################
# display component page
@component_api.route("/component/<serialNumber>", methods=["GET"])
@component_api.route("/component", methods=["GET", "POST"])
def show_component(serialNumber=None):
    global plotting_mutex_lock

    initPage()

    session["code"] = request.args.get("code", "")
    session["test"] = request.args.get("test", "")
    session["runId"] = request.args.get("runId")
    session["unit"] = "front-end_chip"  ### TODO just temporary coding
    session["logged_in"] = session.get("logged_in", False)
    session["regeneratePlots"] = request.form.get(
        "regeneratePlots", type=bool, default=False
    )
    session["expandPlots"] = request.form.get("expandPlots", type=bool, default=False)

    config_create_processed = request.args.get("config_create_processed", False)

    if serialNumber:
        # component info
        cpt_doc = localdb.component.find_one({"serialNumber": serialNumber})
        session["this"] = str(cpt_doc.get("_id"))
        this_component_doc = setComponentInfo(session.get("this"))
        # print(this_component_doc)
        this_component_doc["typeinfo"] = SN_typeinfo(serialNumber, verbose=1)

    else:
        session["this"] = request.args.get("id")
        # component info
        this_component_doc = setComponentInfo(session.get("this"))
        # print(this_component_doc)
        this_component_doc["typeinfo"] = SN_typeinfo(
            this_component_doc.get("serialNumber"), verbose=1
        )

        serialNumber = this_component_doc.get(
            "serialNumber", this_component_doc.get("name", "NONAME")
        )

    # comment
    comments = setComments(
        session.get("this"), session.get("runId"), this_component_doc
    )

    # check test items for the current stage
    latest_tests = setLatestQCSummary(session.get("this"))
    latest_props = setLatestPropSummary(session.get("this"))

    # make status of current stage
    selection_result = [item["runId"] for item in latest_tests.get("results", [])]
    selection_prop = [item["runId"] for item in latest_props.get("results", [])]

    # set summary
    summary = setQCSummary(session.get("this"))
    summary_pdb = setQCSummaryAtProdDB(session.get("this"))

    latest_qc_status = localdb.QC.module.status.find_one(
        {"component": session.get("this")}
    )

    upload_status = None
    flow_version = None
    if latest_qc_status is not None:
        upload_status = latest_qc_status.get("upload_status")
        flow_version = latest_qc_status.get("stageVersion", "RD53A")

    component_type = this_component_doc.get("type")
    component_typeinfo = this_component_doc.get("typeinfo", "")
    code = component_type_code_map.get(component_type)
    stageInfo = None
    qcmenu = None
    test_items = None
    qctest_item_list = None

    if code:
        stageInfo = userdb.QC.stages.find_one({"code": code})
        qcmenu = stageInfo["stage_test"]
        test_items = stageInfo["test_items"]

        supported_qc_tests = {
            "module_pcb": f"{VIEWER_DIR}/json-lists/supported_test_pcb.json",
            "bare_module": f"{VIEWER_DIR}/json-lists/supported_test_bare.json",
            "sensor_tile": f"{VIEWER_DIR}/json-lists/supported_test_sensor.json",
            "module": f"{VIEWER_DIR}/json-lists/supported_test.json",
            "ob_loaded_module_cell": f"{VIEWER_DIR}/json-lists/supported_test.json",
        }

        qctest_file_path = supported_qc_tests.get(component_type)
        qctest_item_list = (
            json.loads(Path(qctest_file_path).read_text()) if qctest_file_path else {}
        )

    ##############################Trying Things#######
    msg = f"This Session Id: {session['this']}"
    logger.debug(msg)
    chip_configs = localdb.fe_configs.find({"serialNumber": serialNumber}) or []

    stages = []
    for cfg in chip_configs:
        if cfg.get("stage") not in stages:
            stages += [cfg.get("stage")]

    for stage in reversed(stages):
        stageIndex = stages.index(stage)
        msg = f"stage Name: {stage}, stageIndex: {stageIndex}"
        logger.debug(msg)
    ##################################################

    logger.debug(pprint.pformat(session))
    qcresult_index = setQCResultIndex(session["this"])
    qcresults, tr_ser = setQCResults(
        session["this"],
        session["runId"],
        session["regeneratePlots"],
        session["expandPlots"],
    )
    qctest = {
        "test_item": qctest_item_list,
        "resultIndex": qcresult_index,
        "results": qcresults,
        "menu": qcmenu,
        "test_items": test_items,
    }

    dict_E_SUMMARY = {}
    qcresultresult_copy = {}
    fe_summary_dict = {}
    isQuad = (
        component_type == "module" and "Quad" in component_typeinfo
    ) or component_type == "ob_loaded_module_cell"
    bgcolors = {1: "green", 0: "red", -1: "grey", None: "#ffc107"}
    # TODO: Following lists should be imported from somwehre else, but where?
    tests_E_SUMMARY = [
        "MODULE_ADC_CALIBRATION",
        "MODULE_SLDO",
        "MODULE_VCAL_CALIBRATION",
        "MODULE_ANALOG_READBACK",
        "MODULE_LP_MODE",
        "MODULE_OVERVOLTAGE_PROTECTION",
        "MODULE_INJECTION_CAPACITANCE",
        "MODULE_MIN_HEALTH_TEST",
        "MODULE_TUNING",
        "MODULE_PIXEL_FAILURE_ANALYSIS",
        "MODULE_UNDERSHUNT_PROTECTION",
        "MODULE_DATA_TRANSMISSION",
    ]
    results_E_SUMMARY = [
        "MODULE_BAD_PIXEL_NUMBER",
        "MODULE_HIGHEST_NUMBER_BAD_PIXELS_CLUSTER",
        "MODULE_ELECTRICALLY_BAD_PIXEL_NUMBER",
        "MODULE_DISCONNECTED_PIXEL_NUMBER",
    ]

    for key, value in qcresults.get("results", {}).items():
        # set results for QC tests
        if isQuad:  # E_SUMMARY visualization only implemented for quads
            is_E_SUMMARY = False
            if key in tests_E_SUMMARY:
                qcresultresult_copy["QUAD-" + key] = {
                    "bgcolor": bgcolors.get(value, "white"),
                    "fe_results": [],
                }
                dict_E_SUMMARY[key] = {}
                is_E_SUMMARY = True
            else:
                for cached_key, cached_item in dict_E_SUMMARY.items():
                    if key.startswith(cached_key):
                        cached_item[key[-1]] = value
                        is_E_SUMMARY = True
                        break
            for prefix_pixel_summary in results_E_SUMMARY:
                prefix_pixel_summary_fe = prefix_pixel_summary + "_FE_"
                if key.startswith(prefix_pixel_summary_fe):
                    try:
                        fe_id = int(key[len(prefix_pixel_summary_fe) :])
                        quad_key = f"quad-{fe_id}"
                        if quad_key not in fe_summary_dict:
                            fe_summary_dict[quad_key] = {}
                        fe_summary_dict[quad_key][prefix_pixel_summary[7:]] = (
                            value
                            if value is not None
                            else '<span class="badge badge-warning">N/A</span>'
                        )
                        is_E_SUMMARY = True
                    except ValueError:
                        logger.warning(
                            f"The format of the bad pixel summary {key} is not expected."
                        )
            if is_E_SUMMARY:
                continue
        if (key.find("_LINK") > 0 and value is not None) or (
            "LINK_TO_SENSOR_IV" in key and value is not None
        ):
            try:
                result = localdb.QC.result.find_one({"_id": ObjectId(value)})
                component = localdb.component.find_one(
                    {"_id": ObjectId(result["component"])}
                )

                passed = "Passed2" if result["passed"] else "Failed"
                ana_version = (
                    result["results"]["property"]["ANALYSIS_VERSION"]
                    if "property" in result["results"]
                    else result["results"]["properties"]["ANALYSIS_VERSION"]
                )
                url = url_for(
                    "component_api.show_component",
                    id=result["component"],
                    test="qctest",
                    runId=value,
                )

                if passed == "Passed2":
                    url_string = f"<a href=\"{url}\" class=\"badge badge-success\">{component['name']} : {passed} (mqat v{ana_version})</a>"
                else:
                    url_string = f"<a href=\"{url}\" class=\"badge badge-dark\">{component['name']} : {passed} (mqat v{ana_version})</a>"

                qcresultresult_copy[key] = url_string
            except Exception as e:
                logger.error(str(e))
                logger.error(traceback.format_exc())
                logger.error(
                    f"failure in result rendering for linking subcomponent TestRuns: item = {key},{value}"
                )

        elif value is None:
            qcresultresult_copy[key] = '<span class="badge badge-warning">N/A</span>'
        else:
            qcresultresult_copy[key] = value

    # loop for E_SUMMARY
    if isQuad:  # E_SUMMARY visualization only implemented for quads
        for key_test, value_test in dict_E_SUMMARY.items():
            for key_chip, value in value_test.items():
                result = localdb.QC.result.find_one({"_id": ObjectId(value)})
                if result:
                    url = url_for(
                        "component_api.show_component",
                        id=result["component"],
                        test="qctest",
                        runId=value,
                    )
                    color = "green" if result["passed"] else "red"
                else:
                    msg = f"No valid link for E_SUMMARY item: (key_test={key_test}, value_test={value_test}), (key_chip={key_chip}, value={value})"
                    logger.debug(msg)
                    url = None
                    color = "gray"

                qcresultresult_copy["QUAD-" + key_test]["fe_results"].append(
                    {"text": key_chip, "color": color, "link": url}
                )
    qctest["results"]["results"] = qcresultresult_copy

    mode = request.form.get("mode")
    if mode == "updateProperties":
        properties_orig = this_component_doc["properties"]

        for index, prop in enumerate(properties_orig):
            code = prop["code"]
            value_orig = prop["value"]

            value_input = request.form.get(code)

            if prop["readonly"]:
                logger.warning(
                    "attempt to change read-only property %s from %r to %r. will ignore.",
                    code,
                    value_orig,
                    value_input,
                )
                continue

            if not value_input or value_input == "N/A":
                if value_orig is not None:
                    logger.debug(
                        f'- Property change for code {code} : value_orig = "{value_orig}" value_input = "{value_input}"'
                    )
                    prop["value"] = None

                    localdb.component.update_one(
                        {"_id": ObjectId(session["this"])},
                        {"$set": {f"properties.{index}": prop}},
                    )
                continue

            if prop["dataType"] == "boolean":
                if value_input == "true":
                    value_input = True
                elif value_input == "false":
                    value_input = False

            elif prop["dataType"] == "float":
                value_input = float(value_input)

            elif prop["dataType"] == "int":
                value_input = int(value_input)

            if value_input != value_orig:
                logger.debug(
                    f'- Property change for code {code} : value_orig = "{value_orig}" value_input = "{value_input}"'
                )
                prop["value"] = value_input

                localdb.component.update_one(
                    {"_id": ObjectId(session["this"])},
                    {"$set": {f"properties.{index}": prop}},
                )

    # In case of module/ob_loaded_module_cell, check if chip config created or not
    if component_type in ["module", "ob_loaded_module_cell"]:
        chips = []

        if component_type == "module":
            subcpts = localdb.childParentRelation.find({"parent": session["this"]})
        elif component_type == "ob_loaded_module_cell":
            cprs_obmodule = localdb.childParentRelation.find(
                {"parent": session["this"]}
            )
            for cpr in cprs_obmodule:
                childId = cpr["child"]
                child = localdb.component.find_one({"_id": ObjectId(childId)})
                if child["componentType"] == "module":
                    subcpts = localdb.childParentRelation.find(
                        {"parent": str(child["_id"])}
                    )
                    break

        for subcpt in subcpts:
            with contextlib.suppress(Exception):
                subcpt_doc = localdb.component.find_one(
                    {
                        "_id": ObjectId(subcpt.get("child")),
                        "componentType": "front-end_chip",
                    }
                )

            if subcpt_doc is not None:
                subcpt_SN = subcpt_doc.get("serialNumber")
                if subcpt_SN is not None:
                    chips += [subcpt_SN]

        branches = ["warm", "cold", "LP"]

        is_config_created = True
        is_valid_rd53_module = False
        with contextlib.suppress(AssertionError):
            is_valid_rd53_module = [chip_serial_number_to_uid(chip) for chip in chips]

        for chip in chips:
            for branch in branches:
                cfg = localdb.fe_configs.find_one(
                    {
                        "serialNumber": chip,
                        "stage": this_component_doc.get("stage"),
                        "branch": branch,
                    }
                )
                if not cfg:
                    logger.warning(
                        f'  ==> cfg not found for ({chip}, {this_component_doc.get("stage")}, {branch})'
                    )
                    is_config_created = False
                    continue

                if not cfg.get("current_revision_id"):
                    logger.warning("  ==> current_revision_id not found")
                    is_config_created = False

        localdb.component.update_one(
            {"_id": ObjectId(session["this"])},
            {"$set": {"isConfigGenerated": is_config_created}},
        )
        this_component_doc["is_config_created"] = is_config_created

        if (
            this_component_doc.get("stage") == "MODULE/INITIAL_WARM"
            and not is_config_created
            and not config_create_processed
            and is_valid_rd53_module
        ):
            return redirect(
                url_for("qc_api.create_configs", componentId=session.get("this"))
            )

        if message_exists(component=serialNumber, code="DOWNLOAD_CONFIG_INFO"):
            msg = f"Chip configs are still downloading for {serialNumber}."
            flash(msg, "info")

        elif not is_config_created and is_valid_rd53_module:
            if not message_exists(
                component=serialNumber, code="WARNING_FE_CONFIG_NOT_READY"
            ):
                if this_component_doc.get("stage") in [
                    "MODULE/WIREBONDING",
                    "MODULE/ASSEMBLY",
                ]:
                    create_message(
                        f"WARNING: For the current stage ({this_component_doc.get('stage')}), FE configs are not present, please do not upload YARR scans to LocalDB",
                        function="show_components",
                        component=serialNumber,
                        code="WARNING_FE_CONFIG_NOT_READY",
                        can_dismiss=False,
                    )
                else:
                    uri_sync_chip_configs = url_for(
                        "config_api.sync_chip_configs",
                        serial_number=serialNumber,
                        redirect=request.url,
                    )
                    create_message(
                        f"WARNING: FE Chip configs for the current stage ({this_component_doc.get('stage')}) is not ready. You can consider trying to <a href='{uri_sync_chip_configs}'>sync the FE chip configs</a> from production DB.",
                        function="show_components",
                        component=serialNumber,
                        code="WARNING_FE_CONFIG_NOT_READY",
                        can_dismiss=False,
                    )

        else:
            clear_message(
                {"component": serialNumber, "code": "WARNING_FE_CONFIG_NOT_READY"},
                forced=True,
            )
            logger.debug("all needed configs are stored!")

    # In case of fe-chips, create config revision index
    config_api = ChipConfigAPI(client)

    config_revisions = {}

    if component_type == "front-end_chip":
        chip_configs = list(localdb.fe_configs.find({"serialNumber": serialNumber}))

        stages = []
        for cfg in chip_configs:
            if cfg.get("stage") not in stages:
                stages += [cfg.get("stage")]

        for stage in reversed(stages):
            config_revisions[stage] = {}

            stageIndex = stages.index(stage)

            branches = []
            for cfg in chip_configs:
                if stage != cfg.get("stage"):
                    continue

                if cfg.get("branch") not in branches:
                    branches += [cfg.get("branch")]

            for branch in branches:
                config_revisions[stage][branch] = {
                    "prev_stage": stages[stageIndex - 1] if stageIndex > 0 else None
                }

                config_id = config_api.checkout(serialNumber, stage, branch)

                if not config_id:
                    continue

                config_api.info(config_id)

                try:
                    revision_history = config_api.get_revision_history(config_id)

                except Exception as e:
                    logger.error(f"branch {branch}: {e!s}")
                    logger.error(traceback.format_exc())
                    continue

                for revision_id in revision_history:
                    try:
                        revision = localdb.fe_config_revision.find_one(
                            {"_id": ObjectId(revision_id)}
                        )
                    except Exception:
                        logger.error(traceback.format_exc())
                        continue

                    revision.pop("config_data")
                    revision.pop("diff")
                    revision.pop("pix_config")
                    if "timestamp" in revision:
                        revision["timestamp"] = get_localtime_str(revision["timestamp"])
                    else:
                        revision["timestamp"] = ""
                    revision["configId"] = config_id
                    revision["serialNumber"] = serialNumber
                    revision["stage"] = stage
                    revision["branch"] = branch
                    config_revisions[stage][branch][revision_id] = revision

    # set results for electrical scans
    scan_result_index = setResultIndex(session["this"])
    scan_results = setResults(session["this"], session["runId"])
    # plots = setPlots(session["this"], session["runId"], selection = True, dumpPlots = True, regeneratePlots = session['regeneratePlots'] , expandPlots = session['expandPlots'] )

    try:
        plots_light = set_light_plots(
            session["this"],
            component_type,
            session["runId"],
            session["regeneratePlots"],
        )
    except Exception as e:
        logger.error(traceback.format_exc())
        plotting_mutex_lock = False
        raise e

    dcs = setDCS(session["runId"])
    electrical = {
        "resultIndex": scan_result_index,
        "results": scan_results,
        "dcs": dcs,
        "plots_light": plots_light,
    }
    # print(electrical)

    # helper link for OB_LOADED_MODULE_CELL <-> FE_CHIP
    if component_type == "ob_loaded_module_cell":
        chips = []

        cprs_obmodule = localdb.childParentRelation.find({"parent": session["this"]})
        for cpr in cprs_obmodule:
            childId = cpr["child"]
            child = localdb.component.find_one({"_id": ObjectId(childId)})
            if child["componentType"] == "module":
                subcpts = localdb.childParentRelation.find(
                    {"parent": str(child["_id"])}
                )
                break

        for subcpt in subcpts:
            with contextlib.suppress(Exception):
                subcpt_doc = localdb.component.find_one(
                    {
                        "_id": ObjectId(subcpt.get("child")),
                        "componentType": "front-end_chip",
                    }
                )

            if subcpt_doc is not None:
                chips += [subcpt_doc]

        this_component_doc["children"] = this_component_doc["children"] + chips

    elif component_type == "front-end_chip":
        cprs_chip = localdb.childParentRelation.find({"child": session["this"]})
        for cpr in cprs_chip:
            parentId = cpr["parent"]
            parent = localdb.component.find_one({"_id": ObjectId(parentId)})
            if parent["componentType"] == "module":
                cprs_module = localdb.childParentRelation.find(
                    {"child": str(parent["_id"])}
                )
                for cpr_module in cprs_module:
                    grandparentId = cpr_module["parent"]
                    grandparent = localdb.component.find_one(
                        {"_id": ObjectId(grandparentId)}
                    )
                    if grandparent["componentType"] == "ob_loaded_module_cell":
                        this_component_doc["parents"] = [
                            grandparent
                        ] + this_component_doc["parents"]
                        break

    component = {
        "_id": session.get("this"),
        "test": session["test"],
        "runId": session["runId"],
        "info": this_component_doc,
        "comments": comments,
        "property": {},
        "electrical": electrical,
        "qctest": qctest,
        "tr_ser": tr_ser,
        "summary": summary,
        "summary_pdb": summary_pdb,
        "upload_status": upload_status,
        "flow_version": flow_version,
        "latest_tests": latest_tests,
        "latest_props": latest_props,
        "selection_result": selection_result,
        "selection_prop": selection_prop,
        "stageInfo": stageInfo,
        "config_revision": config_revisions,
        "fe_summary_dict": fe_summary_dict,
    }

    # messages
    messages = format_messages({"component": serialNumber})
    table_docs = {"messages": messages}

    # mqat version
    table_docs.update({"mqat_version": mqat_version})
    table_docs.update({"qc_analysis_version": qcAnalysisVersion})

    return render_template(
        "component.html",
        component=component,
        table_docs=table_docs,
    )


@component_api.route("/nomodule_result", methods=["GET", "POST"])
def nomodule_result():
    initPage()

    session["code"] = request.args.get("code", "")
    session["test"] = request.args.get("test", "")
    session["runId"] = request.args.get("runId")
    session["unit"] = "front-end_chip"  ### TODO just temporary coding

    scan_results = setResults("", session["runId"])
    plots = setPlots("", session["runId"])
    dcs = setDCS(session["runId"])
    electrical = {"results": scan_results, "dcs": dcs, "plots": plots}

    component = {"electrical": electrical}

    return render_template("nomodule_result.html", component=component)


###################
# show summary plot
@component_api.route("/show_plot", methods=["GET"])
def show_plot():
    name = request.args.get("name")
    tr_oid = request.args.get("runId")

    filepath = f"{CACHE_DIR}/{tr_oid}/{name}_root.png"

    image = Image.open(filepath)
    filename = f"{session['uuid']}_{tr_oid}_{name}_root.png"
    image.save(f"{THUMBNAIL_DIR}/{filename}")
    url = url_for("static.cache.thumbnail.static", filename=filename)

    return redirect(url)


###################
# show summary plot
@component_api.route("/show_summary", methods=["GET"])
def show_summary():
    initPage()

    # get from args
    code = request.args.get("code")
    scan = request.args.get("scan")
    stage = request.args.get("stage")

    query = {"_id": ObjectId(code)}
    data = localdb.fs.files.find_one(query)
    if "png" not in data["filename"]:
        filePath = (
            f"{THUMBNAIL_DIR}/{session['uuid']}/{stage}_{scan}_{data['filename']}.png"
        )
    else:
        filePath = (
            f"{THUMBNAIL_DIR}/{session['uuid']}/{stage}_{scan}_{data['filename']}"
        )

    thum_dir = "/".join([THUMBNAIL_DIR, session["uuid"]])
    cleanDir(thum_dir)

    binary = fs.get(ObjectId(code)).read()
    image_bin = io.BytesIO(binary)
    image = Image.open(image_bin)
    image.save(filePath)
    if "png" not in data["filename"]:
        url = url_for(
            "static.cache.thumbnail.static",
            filename=f"{session['uuid']}/{stage}_{scan}_{data['filename']}.png",
        )
    else:
        url = url_for(
            "static.cache.thumbnail.static",
            filename=f"{session['uuid']}/{stage}_{scan}_{data['filename']}",
        )

    return redirect(url)


#######################
# Display JSON/DAT Data
@component_api.route("/display_data", methods=["GET"])
def display_data():
    initPage()

    code = request.args.get("code")
    data_format = request.args.get("format")

    logger.debug(f"code = {code}, data_format = {data_format}")

    filesystem = gridfs.GridFS(localdb)

    if data_format == "pickle":
        try:
            fs_data = filesystem.get(ObjectId(code))

            if not fs_data:
                return make_response(f"data is not found on gridfs code = {code}")

            pickle_data = fs_data.read()
            data = pickle.load(io.BytesIO(pickle_data))

            if data.get("Type") == "Histo1d":
                bins = np.linspace(
                    data.get("x").get("Low"),
                    data.get("x").get("High"),
                    data.get("x").get("Bins"),
                )

                plt.hist(bins, len(bins), weights=data.get("Data"))

            elif data.get("Type") == "Histo2d":
                arr = np.array(data.get("Data"))
                plt.imshow(arr.T, origin="lower", cmap="turbo")
                cb = plt.colorbar(label=data.get("z").get("AxisTitle"))
                for t in cb.ax.get_yticklabels():
                    t.set_fontsize(12)

            else:
                uri = url_for(request.endpoint, **dict(request.view_args))
                msg = f"data of type {data.get('Type')} is unhandled: {uri}"
                raise ValueError(msg)

            plt.title(data.get("Name"), fontsize=22)
            plt.xlabel(data.get("x").get("AxisTitle"), fontsize=18)
            plt.ylabel(data.get("y").get("AxisTitle"), fontsize=18)
            plt.tight_layout()

            tmpfile = f"{TMP_DIR}/{code}.png"
            plt.savefig(tmpfile, dpi=100)

            with open(tmpfile, "rb") as f:
                image = f.read()

                response = make_response(image)

                response.headers.set("Content-Type", "image/png")
                response.headers.set("Content-Disposition", "inline")

            plt.clf()

        except Exception as e:
            if "Data" in data:
                data.pop("Data")
                response = make_response(
                    f"Plot rendering error: {e!s}; metadata = {data}"
                )
            else:
                response = make_response(f"Plot rendering error: {e!s}")
            return response

    else:
        try:
            pickle_data = filesystem.get(ObjectId(code)).read()
        except gridfs.errors.NoFile:
            abort(400, f"data is not found on gridfs code = {code}")

        data = json2html.convert(pickle.load(io.BytesIO(pickle_data)))

        logger.debug(pprint.pformat(data))

        response = make_response(data)
        response.headers.set("Content-Type", "text/html")

    return response


@component_api.route("/download_each_data", methods=["GET"])
def download_each_data():
    initPage()

    code = request.args.get("code")
    data_format = request.args.get("format")
    name = request.args.get("name")

    logger.debug(f"code = {code}, data_format = {data_format}")

    filesystem = gridfs.GridFS(localdb)

    fs_data = filesystem.get(ObjectId(code))
    if not fs_data:
        return make_response(f"data is not found on gridfs code = {code}")

    if data_format == "pickle":
        try:
            pickle_data = fs_data.read()
            data = pickle.load(io.BytesIO(pickle_data))

            with open(f"{TMP_DIR}/{code}", "w", encoding="utf-8") as f:
                json.dump(data, f)

            return send_file(
                f"{TMP_DIR}/{code}",
                as_attachment=True,
                mimetype="text/html",
                download_name=name,
            )

        except Exception as e:
            logger.error(traceback.format_exc())
            return make_response(
                f"something went wrong: gridfs code = {code}, cache_path = {TMP_DIR}/{code}: {e!s}"
            )

    else:
        logger.debug(f"data_format = {data_format}")

        try:
            pickle_data = filesystem.get(ObjectId(code)).read()
            data = pickle.load(io.BytesIO(pickle_data))

            if "_id" in data:
                data.pop("_id")

            with open(f"{TMP_DIR}/{code}", "w", encoding="utf-8") as f:
                json.dump(data, f)

            return send_file(
                f"{TMP_DIR}/{code}",
                as_attachment=True,
                mimetype="text/html",
                download_name=name,
            )
        except Exception as e:
            logger.error(traceback.format_exc())
            return make_response(
                f"something went wrong: gridfs code = {code}, cache_path = {TMP_DIR}/{code}: {e!s}"
            )


########################
# Download JSON/DAT Data
@component_api.route("/download_data", methods=["GET", "POST"])
def download_data():
    initPage()
    cmp_name = request.form.get("cmp")
    run_number = request.form.get("run", "data")

    zip_name = f"{cmp_name}_{run_number}" if cmp_name else run_number

    zip_dir = "/".join([TMP_DIR, session["uuid"]])
    cleanDir(zip_dir)
    zip_path = f"{TMP_DIR}/{session['uuid']}/{zip_name}.zip"
    with zipfile.ZipFile(zip_path, "a") as zip_file:
        entries = request.form.getlist("data")
        for entry in entries:
            this = ast.literal_eval(entry)
            filename = f"{this['type']}.{this['format']}"
            if this["serialNumber"] != "":
                filename = f"{this['serialNumber']}_{filename}"
            filepath = f"{zip_dir}/{filename}"
            with open(filepath, "wb") as f:
                f.write(fs.get(ObjectId(this["code"])).read())
            zip_file.write(filepath, filename)

    return send_file(zip_path, as_attachment=True, mimetype="application/zip")


##################
# Add/Edit Comment
@component_api.route("/edit_comment", methods=["GET", "POST"])
def edit_comment():
    oid = request.args.get("id", -1)
    tr_oid = request.args.get("runId", -1)

    thistime = datetime.utcnow()
    info = {
        "sys": {"rev": 0, "cts": thistime, "mts": thistime},
        "component": oid,
        "runId": tr_oid,
        "comment": request.form.get("text").replace("\r\n", "<br>"),
        "componentType": session["unit"],
        "user_id": session["user_id"],
        "name": request.form.get("text2"),
        "institution": request.form.get("text3"),
        "datetime": thistime,
    }

    localdb.comments.insert_one(info)

    return redirect(request.headers.get("Referer"))


@component_api.route("/upload_flag", methods=["GET", "POST"])
def upload_flag():
    initPage()
    table_docs = {"run": []}
    table_docs["_id"] = request.args.get("id", "")
    table_docs["mname"] = str(
        localdb.component.find_one({"_id": ObjectId(table_docs["_id"])})["name"]
    )
    table_docs["stage"] = request.args.get("stage", "")
    table_docs["test"] = request.args.get("test", "")
    table_docs["text"] = ""
    table_docs["pstage"] = request.form.get("pstage", "input")
    table_docs["flag"] = request.form.get("flag", "")

    if not session["logged_in"]:
        table_docs["pstage"] = "input"
        table_docs["text"] = "Please log in before the selection."
    else:
        if table_docs["pstage"] == "input":
            pass

        elif table_docs["pstage"] == "complete":
            institute = userdb.viewer.user.find_one({"username": session["username"]})[
                "institution"
            ]
            result_para = {}
            if "ADC_CALIBRATION" in table_docs["test"]:
                result_para["flag"] = table_docs["flag"]
            elif "TUNING" in table_docs["test"]:
                result_para["flag"] = table_docs["flag"]
                temp = (
                    table_docs["test"]
                    .split("_")[1]
                    .replace("MINUS", "-")
                    .replace("DEG", "")
                )
                result_para["setting_temp"] = int(temp)

            new_flag_doc = {
                "component": table_docs["_id"],
                "user": session["username"],
                "address": institute,
                "stage": table_docs["stage"],
                "sys": {"mts": datetime.now(), "cts": datetime.now(), "rev": 0},
                "dbVersion": dbv,
                "testType": table_docs["test"],
                "results": result_para,
            }
            localdb.QC.result.insert_one(new_flag_doc)

            ### script for updating a result
            # old_result_flag = localdb.QC.result.find_one({ "component": table_docs["_id"], "stage": table_docs["stage"], "testType": table_docs["test"] })
            # if old_result_flag:
            #    localdb.QC.result.update_one({"_id": old_result_flag["_id"]}, { "$set": new_flag_doc})
            # else:
            #    localdb.QC.result.insert_one(new_flag_doc)

    return render_template(
        "upload_flag.html",
        table_docs=table_docs,
    )


@component_api.route("/set_stage", methods=["GET", "POST"])
def set_stage():
    if not session.get("logged_in", False):
        return render_template("401.html")

    module = request.args.get("module", "")
    nstage = request.form.get("nstage", "")
    pstage = request.form.get("pstage", "input")
    cstage = ""
    text = ""

    query = {"name": module}
    module_doc = localdb.component.find_one(query)

    query = {"component": str(module_doc["_id"]), "proddbVersion": proddbv}
    qc_module_doc = localdb.QC.module.status.find_one(query)
    stage_list = list(qc_module_doc["QC_results"])
    cstage = qc_module_doc["stage"]
    if pstage == "complete":
        if not nstage:
            text = "Please select stage from pull-down menu..."
            pstage = "input"
            doc = {
                "_id": str(module_doc["_id"]),
                "module": module,
                "pstage": pstage,
                "cstage": cstage,
                "nstege": nstage,
                "text": text,
                "stages": stage_list,
            }
            return render_template("set_stage.html", doc=doc)
        localdb.QC.module.status.update_one(query, {"$set": {"stage": nstage}})
        pstage = "input"
        text = "The current stage was switched to " + nstage + "."

    doc = {
        "_id": str(module_doc["_id"]),
        "module": module,
        "pstage": pstage,
        "cstage": cstage,
        "nstege": nstage,
        "text": text,
        "stages": stage_list,
    }
    return render_template("set_stage.html", doc=doc)


@component_api.route("/download_rootfile", methods=["GET", "POST"])
def download_rootfile():
    root_path = request.form.get("path", "")
    return send_file(root_path, as_attachment=True, mimetype="application/root")


@component_api.route("/download_json", methods=["GET", "POST"])
def download_json():
    path = request.form.get("path", "")
    logger.debug(f"download_json(): path = {path}")
    return send_file(path, as_attachment=True, mimetype="application/json")


@component_api.route("/download_file", methods=["GET"])
def download_file():
    path = request.args.get("path")
    filename = request.args.get("download_name")
    mimetype = request.args.get("mimetype")

    if Path(path).is_file():
        return send_file(
            path, as_attachment=True, mimetype=mimetype, download_name=filename
        )

    return make_response({"error": f"Path '{path}' not found."}, 404)


@component_api.route("/download/test_run/<test_run_id>/result", methods=["GET"])
def download_test_run_result(test_run_id):
    test_run = localdb.QC.result.find_one({"_id": ObjectId(test_run_id)})
    if not test_run:
        return make_response(
            {"error": f"Test run record with ID '{test_run_id}' not found."}, 404
        )

    serialNumber = localdb.component.find_one({"_id": ObjectId(test_run["component"])})[
        "serialNumber"
    ]
    stage = test_run["stage"]

    filename = f"{serialNumber}-{test_run['testType']}-{stage}.json"
    buffer = io.BytesIO(json.dumps(test_run["results"]).encode("utf-8"))
    return send_file(
        buffer, as_attachment=True, mimetype="application/json", download_name=filename
    )


#######################
### other functions ###
#######################


###########################
# Set Component Information
# i_oid: ObjectId of this component/chip document
def setComponentInfo(i_oid):
    if not i_oid:
        return {}

    ### component
    this = localdb.component.find_one({"_id": ObjectId(i_oid)}) or {}
    if not this:
        return {}

    ### parent
    parents = setCpr(i_oid, "child", "parent")
    ### child
    children = setCpr(i_oid, "parent", "child")

    qc_doc = localdb.QC.module.status.find_one({"component": i_oid}) or {}

    session["unit"] = this.get("componentType", "front-end_chip").lower()

    ctype_mapping = {"front-end_chip": "fe_chip", "module_pcb": "pcb"}
    ctype_code = ctype_mapping.get(this["componentType"]) or this["componentType"]
    # set properties that impact SN
    ctype = userdb.componentType.find_one({"code": ctype_code.upper()})
    ctype_props = {}
    flag_list = []
    if ctype:
        ctype_props = {prop["code"]: prop for prop in ctype["properties"]}
        flag_list = ctype.get("flags", []) or []

    comp_props = {prop["code"]: prop for prop in this.get("properties", [])}
    comp_flags = this.get("flags", []) or []

    for prop_code, prop in comp_props.items():
        # see setComponentInfo() for definition of readonly (means "snPosition" is defined, mapping to SN)
        prop["readonly"] = ctype_props.get(prop_code, {}).get("snPosition") is not None

    if flag_list:
        for index, flag in enumerate(flag_list):
            flag_list[index]["status"] = flag.get("code") in comp_flags

        flag_list = sorted(flag_list, key=lambda flag: flag["name"])
    else:
        flag_list = [
            {
                "code": "FLAGS_NOT_DOWNLOADED",
                "name": "Flags not downloaded. Resync to make flags appear",
                "status": True,
            }
        ]

    flags = {flag["code"]: flag for flag in flag_list}

    return {
        "serialNumber": this["serialNumber"],
        "name": this["name"],
        "component": i_oid,
        "stage": qc_doc["stage"],
        "parents": parents,
        "children": children,
        "type": this["componentType"].lower(),
        "chipType": this["chipType"],
        "qc": this.get("proDB", False),
        "proID": this.get("proID", False),
        "properties": comp_props.values(),
        "flags": flags,
    }


###########################
# Set Child Parent Relation
# i_oid: ObjectId of this component/chip document
# i_cp_in: set this component to child or parent
# i_cp_out: get child or parent of this component
def setCpr(i_oid, i_cp_in, i_cp_out):
    docs = []
    entries = localdb.childParentRelation.find({i_cp_in: i_oid}) or []
    for entry in entries:
        oid = entry[i_cp_out]
        this = localdb.component.find_one({"_id": ObjectId(oid)}) or {}
        docs.append({"_id": str(oid), "name": this.get("name")})
    return docs


def setQCSummary(i_oid):
    summary = {}
    doc = localdb.QC.module.status.find_one({"component": i_oid})

    if doc is None:
        return summary

    # Adaptation to old version
    try:
        qcDoc = userdb.QC.stages.find_one({"code": doc["componentType"]})
    except Exception:
        qcDoc = userdb.QC.stages.find_one({"code": "MODULE"})
    stage_flow = qcDoc["stage_flow"]

    cstage = doc["stage"]
    for stage in stage_flow:
        if stage == cstage:
            break

        summary[stage] = doc["QC_results"].get(stage, {})

    return summary


def setQCSummaryAtProdDB(i_oid):
    summary = {}
    doc = localdb.QC.module.status.find_one({"component": i_oid})

    if doc is None:
        return summary

    try:
        qcDoc = userdb.QC.stages.find_one({"code": doc["componentType"]})
    except Exception:
        qcDoc = userdb.QC.stages.find_one({"code": "MODULE"})

    stage_flow = qcDoc["stage_flow"]

    cstage = doc["stage"]
    for stage in stage_flow:
        if stage == cstage:
            break
        summary[stage] = doc["QC_results_pdb"].get(stage, -1)

    return summary


def setLatestQCSummary(i_oid):
    tests = {}
    doc = localdb.QC.module.status.find_one({"component": i_oid})
    if doc is None:
        return tests

    mname = localdb.component.find_one({"_id": ObjectId(i_oid)})["name"]
    cstage = doc["stage"]
    tests["stage"] = cstage
    tests["results"] = []
    etests = ScanCheckoutManager(mname, cstage).test_scan_map

    with contextlib.suppress(Exception):
        logger.debug("setLatestQCSummary()")
        logger.debug(pprint.pformat(doc["QC_results"][cstage]))

        cpt_doc = localdb.component.find_one({"_id": ObjectId(i_oid)})
        cpt_type = cpt_doc.get("componentType")
        stage_doc = userdb.QC.stages.find_one(
            {"code": component_type_code_map[cpt_type]}
        )

        disable_doc = stage_doc.get("disabled_tests", {}) if stage_doc else {}

        for test in doc["QC_results"][cstage]:
            test_docs = None

            if cstage in disable_doc and test in disable_doc.get(cstage, {}).get(
                "tests", []
            ):
                continue

            if (
                localdb.QC.result.count_documents(
                    {
                        "$and": [
                            {"component": i_oid},
                            {"stage": cstage},
                            {"testType": test},
                        ]
                    }
                )
                > 0
            ):
                test_docs = (
                    localdb.QC.result.find(
                        {
                            "$and": [
                                {"component": i_oid},
                                {"stage": cstage},
                                {"testType": test},
                            ]
                        }
                    )
                    .sort([("$natural", -1)])
                    .limit(1)
                )
            else:
                # instance is not present, but at least a dummy slot is needed.
                test_docs = [
                    {
                        "component": i_oid,
                        "stage": cstage,
                        "dbVersion": dbv,
                        "testType": test,
                        "results": {},
                    }
                ]

            logger.debug(f"test_docs = {test_docs}")

            test_doc = {}
            for doc in test_docs:
                test_doc = doc

            tests["results"].append(
                {
                    "name": test,
                    "runId": (
                        str(test_doc["_id"]) if test_doc and "_id" in test_doc else ""
                    ),
                    "scan_selection": test in etests,
                }
            )
    return tests


def setLatestPropSummary(i_oid):
    tests = {}
    doc = ["RD53A_PULL-UP_RESISTOR", "IREFTRIM_FE", "ORIENTATION"]
    if doc is None:
        return tests

    tests["results"] = []
    for test in doc:
        test_docs = (
            localdb.QC.module.prop.find(
                {"$and": [{"component": i_oid}, {"testType": test}]}
            )
            .sort([("$natural", -1)])
            .limit(1)
        )
        test_doc = {}
        for item in test_docs:
            test_doc = item
        tests["results"].append(
            {
                "name": test,
                "runId": str(test_doc["_id"]) if test_doc != {} else "",
            }
        )
    return tests


#################
# Set Result List
# i_oid: ObjectId of this component/chip document
def setResultIndex(i_oid):
    docs = []
    query = {"component": i_oid, "dbVersion": dbv}
    entries = localdb.componentTestRun.find(query)
    oids = []
    for entry in entries:
        oids.append(str(entry["_id"]))
    for oid in oids:
        query = {"_id": ObjectId(oid)}
        this_ctr = localdb.componentTestRun.find_one(query)
        query = {"_id": ObjectId(this_ctr["testRun"])}
        this_tr = localdb.testRun.find_one(query)

        ### result data
        result = (this_tr["plots"] != [] and this_ctr["chip"] == "module") or (
            this_ctr["attachments"] != [] and this_ctr["chip"] != "module"
        )
        ### user
        query = {"_id": ObjectId(this_tr["user_id"])}
        this_user = localdb.user.find_one(query)
        ### site
        query = {"_id": ObjectId(this_tr["address"])}
        this_site = localdb.institution.find_one(query)
        ### score
        count = setCount()

        ### tags
        tag_docs = userdb.viewer.tag.docs.find({"runId": str(this_tr["_id"])})
        tags = [tag["name"] for tag in tag_docs]

        docs.append(
            {
                "_id": str(this_tr["_id"]),
                "runNumber": this_tr["runNumber"],
                "datetime": get_localtime_str(this_tr["startTime"]),
                "testType": this_tr["testType"],
                "result": result,
                "stage": this_tr.get("stage", "N/A"),
                "user": this_user["userName"],
                "site": this_site["institution"],
                "rate": count.get("module", {}).get("rate", "-"),
                "score": count.get("module", {}).get("score"),
                "values": count.get("module", {}).get("parameters", {}),
                "summary": this_tr.get("summary"),
                "tags": tags,
            }
        )

    return sorted(docs, key=lambda x: (x["datetime"]), reverse=True)


##############################
# Set QC Result List
# i_oid: ObjectId of this component/chip document
def setQCResultIndex(i_oid):
    docs = []
    query = {"component": i_oid}
    entries = localdb.QC.result.find(query)
    oids = []

    for entry in entries:
        oids.append(str(entry["_id"]))

    for oid in oids:
        query = {"_id": ObjectId(oid)}
        this_tr = localdb.QC.result.find_one(query)
        this_status = localdb.QC.module.status.find_one(
            {"component": this_tr["component"]}
        )

        QC_passed = this_tr.get("passed")

        try:
            QC_result = (
                oid == this_status["QC_results"][this_tr["stage"]][this_tr["testType"]]
            )
        except Exception:
            QC_result = None

        stage = this_tr.get("stage")
        if stage is None:
            stage = this_tr.get("stage", "N/A")

        properties = this_tr.get("results", {}).get("property", {}) or this_tr.get(
            "results", {}
        ).get("properties", {})
        analysis_version = properties.get("ANALYSIS_VERSION", "")

        ### result data of non-electrical test
        docs.append(
            {
                "component_id": str(this_tr["component"]),
                "_id": str(this_tr["_id"]),
                "testType": this_tr["testType"],
                "user": this_tr.get("user"),
                "institute": this_tr["address"],
                "datetime": get_localtime_str(this_tr["sys"]["mts"]),
                "stage": stage,
                "QC_passed": QC_passed,
                "QC_result": QC_result,
                "analysis_version": analysis_version,
            }
        )

    return sorted(docs, key=lambda x: (x["datetime"]), reverse=True)


###############################
# Set Result Information & Data
# i_oid: ObjectId of this component/chip document
# i_tr_oid: ObjectId of this testRun document
def setResults(_i_oid, i_tr_oid):
    if not i_tr_oid:
        return {}

    if not ObjectId.is_valid(i_tr_oid):
        return {}

    this_tr = localdb.testRun.find_one({"_id": ObjectId(i_tr_oid)})
    if not this_tr:
        return {}

    ### output data
    outputs = {}
    outputs = setOutput(this_tr, outputs)
    ctr_entries = localdb.componentTestRun.find({"testRun": i_tr_oid})
    cmps = []
    for this_ctr in ctr_entries:
        outputs = setOutput(this_ctr, outputs)
        cmps.append(
            {
                "name": this_ctr["name"],
                "_id": this_ctr["component"],
            }
        )
    ### basic info
    info = setTestRunInfo(this_tr, cmps)

    return {"runId": i_tr_oid, "info": info, "output": outputs}


##############################################
# Set QC Result Information & Data
# i_old: ObjectId of this component/chip document
# i_tr_old: ObjectId of this testRun document
def setQCResults(_i_oid, i_tr_oid, _regeneratePlots, _expandPlots):
    if not i_tr_oid:
        return {}, {}

    try:
        query = {"_id": ObjectId(i_tr_oid)}
    except Exception as e:
        logger.error(str(e))
        logger.error(traceback.format_exc())
        return {}, {}

    this_tr = localdb.QC.result.find_one(query)
    if this_tr is None:
        return {}, {}
    query = {"_id": ObjectId(this_tr["component"])}

    raw = None
    if "raw_id" in this_tr:
        raw = localdb.QC.testRAW.find_one({"_id": ObjectId(this_tr["raw_id"])})
    else:
        this_tr["raw_id"] = None

    passed = None
    with contextlib.suppress(Exception):
        passed = this_tr["passed"]

    # cache testRun
    os.makedirs(CACHE_DIR, exist_ok=True)
    ana_out = f"{CACHE_DIR}/{i_tr_oid}.json"

    tr_ser = copy.deepcopy(this_tr)
    for key in [
        "_id",
        "address",
        "component",
        "raw_id",
        "sys",
        "user",
        "gridfs_attachments",
    ]:
        if key in tr_ser:
            tr_ser.pop(key)

    with open(ana_out, "w", encoding="utf-8") as f:
        f.write(json.dumps([tr_ser], indent=4))

    logger.debug(f"dumped TestRun: {ana_out}")

    # cache attachment files

    attachments = {}

    if this_tr.get("gridfs_attachments") is not None:
        for filename, oid in this_tr.get("gridfs_attachments").items():
            THUMBNAIL_DIR.mkdir(parents=True, exist_ok=True)

            ext = Path(filename).suffix.lstrip(".")

            cache_file = THUMBNAIL_DIR / f"{oid!s}.{ext}"

            if cache_file.exists():
                pass
            else:
                try:
                    data = fs.get(oid).read()

                    with cache_file.open("wb") as f:
                        f.write(data)
                except Exception as e:
                    logger.error(str(e))
                    logger.error(traceback.format_exc())
                    logger.error(f"Failed in dumping attachment to {cache_file}")

            with cache_file.open() as fp:
                mimetype = itkdb.utils.get_mimetype(cache_file, fp)
            uri = {}
            uri["download_uri"] = url_for(
                "component_api.download_file",
                path=str(cache_file),
                download_name=filename,
                mimetype=mimetype,
            )
            if ext in ["png", "jpg"]:
                uri["thumbnail_uri"] = url_for(
                    "static.cache.thumbnail.static", filename=cache_file.name
                )

            attachments.update({filename: uri})

            logger.debug(f"attachment dumped to cache {cache_file}")

    # cache visual inspection images
    vi_doc = {}

    if this_tr.get("testType") == "VISUAL_INSPECTION":
        raw_id = this_tr.get("raw_id")

        try:
            raw = localdb.QC.testRAW.find_one({"_id": raw_id}).get("raw")[0]
            metadata = raw.get("results", {}).get("metadata", {})
            metadata.update(raw.get("results", {}).get("Metadata", {}))

            front_image = metadata.get("front_image")
            back_image = metadata.get("back_image")

            front_defect_images = metadata.get("front_defect_images")
            back_defect_images = metadata.get("back_defect_images")

            for name, image_id in {"front": front_image, "back": back_image}.items():
                cache_file, ext = dump_image(image_id)

                if ext:
                    with cache_file.open() as fp:
                        mimetype = itkdb.utils.get_mimetype(cache_file, fp)
                    vi_doc.update(
                        {
                            name + "_image": {
                                "download_uri": url_for(
                                    "component_api.download_file",
                                    path=str(cache_file),
                                    download_name=f"{name}_image.{ext}",
                                    mimetype=mimetype,
                                )
                            }
                        }
                    )

            if front_defect_images:
                for tile, image_id in front_defect_images.items():
                    cache_file, ext = dump_image(image_id)

                    defects = metadata.get("front_defects").get(tile)
                    comment = metadata.get("front_comments").get(tile)

                    if ext:
                        with cache_file.open() as fp:
                            mimetype = itkdb.utils.get_mimetype(cache_file, fp)
                        vi_doc.update(
                            {
                                "front_tile" + tile: {
                                    "download_uri": url_for(
                                        "component_api.download_file",
                                        path=str(cache_file),
                                        download_name=f"front_tile{tile}.{ext}",
                                        mimetype=mimetype,
                                    ),
                                    "thumbnail_uri": url_for(
                                        "static.cache.thumbnail.static",
                                        filename=cache_file.name,
                                    ),
                                    "defects": defects,
                                    "comment": comment,
                                }
                            }
                        )

            if back_defect_images:
                for tile, image_id in back_defect_images.items():
                    cache_file, ext = dump_image(image_id)

                    defects = metadata.get("back_defects").get(tile)
                    comment = metadata.get("back_comments").get(tile)

                    if ext:
                        with cache_file.open() as fp:
                            mimetype = itkdb.utils.get_mimetype(cache_file, fp)
                        vi_doc.update(
                            {
                                "back_tile" + tile: {
                                    "download_uri": url_for(
                                        "component_api.download_file",
                                        path=str(cache_file),
                                        download_name=f"back_tile{tile}.{ext}",
                                        mimetype=mimetype,
                                    ),
                                    "thumbnail_uri": url_for(
                                        "static.cache.thumbnail.static",
                                        filename=cache_file.name,
                                    ),
                                    "defects": defects,
                                    "comment": comment,
                                }
                            }
                        )

        except Exception as e:
            logger.error(str(e))
            logger.error(traceback.format_exc())
            logger.error("Failed in acquiring RAW data for visula inspection")

    results = {
        "testType": this_tr["testType"],
        "stage": this_tr["stage"],
        "component": this_tr["component"],
        "datetime": get_localtime_str(this_tr["sys"]["mts"]),
        "user": this_tr.get("user"),
        "institute": this_tr["address"],
        "runId": i_tr_oid,
        "rawId": this_tr["raw_id"],
        "id": str(this_tr["_id"]),
        "attachments": attachments,
        "vi_doc": vi_doc,
        "passed": passed,
        "prodDB_record": (
            json2html.convert(this_tr["prodDB_record"])
            if "prodDB_record" in this_tr
            else {}
        ),
        "raw_record": json2html.convert(raw) if raw is not None else {},
        "result_json": ana_out,
    }

    if this_tr["results"] == {}:
        return results, tr_ser

    ## For electrical tests
    # checkouter = ScanCheckoutManager( this_cp["name"], this_tr["stage"] )

    results.update({"results": this_tr["results"]})

    return results, tr_ser


def dump_image(image_id) -> (Path, str | None):
    THUMBNAIL_DIR.mkdir(parents=True, exist_ok=True)
    cache_file = THUMBNAIL_DIR / image_id

    if not cache_file.exists():
        try:
            data = fs.get(ObjectId(image_id)).read()
            with open(cache_file, "wb") as f:
                f.write(data)
        except Exception as e:
            logger.error(str(e))
            logger.error(traceback.format_exc())
            logger.error(f"Failed in dumping attachment to {cache_file}")
            return cache_file, None

    ext = filetype.guess(cache_file).extension

    return cache_file, ext


########################
# Set Result Information
# i_tr: this testRun document
# i_cmps: component list of this testRun
def setTestRunInfo(i_tr, i_cmps):
    info = {
        "runNumber": None,
        "testType": None,
        "stage": None,
        "component": i_cmps,
        "startTime": None,
        "finishTime": None,
        "user": None,
        "site": None,
        "targetCharge": None,
        "targetTot": None,
    }
    for key in i_tr:
        if (
            "Cfg" in key
            or key == "_id"
            or key == "sys"
            or key == "chipType"
            or key == "dbVersion"
            or key == "timestamp"
        ):
            continue

        if key in ["startTime", "finishTime"]:
            info.update({key: get_localtime_str(i_tr[key])})
        elif key == "user_id" and i_tr[key] != "...":
            query = {"_id": ObjectId(i_tr[key])}
            this_user = localdb.user.find_one(query)
            info.update({"user": this_user["userName"]})
        elif key == "address" and i_tr[key] != "...":
            query = {"_id": ObjectId(i_tr[key])}
            this_site = localdb.institution.find_one(query)
            info.update({"site": this_site["institution"]})
        elif isinstance(i_tr[key], list):
            value = ""
            for i in i_tr[key]:
                value.join(f"{i}<br>")
            info.update({key: value})
        elif isinstance(i_tr[key], dict):
            value = ""
            for i in i_tr[key]:
                value += f"{i}: {i_tr[key][i]}<br>"
            info.update({key: value})
        else:
            info.update({key: i_tr[key]})

    return info


#################
# Set Result Data
# i_docs: this testRun/componentTestRun document
# i_files: result data file list
def setOutput(i_docs, i_files):
    for key in i_docs:
        if "Cfg" in key and i_docs[key] != "...":
            if key not in i_files:
                i_files.update({key: []})
            query = {"_id": ObjectId(i_docs[key])}
            this = localdb.config.find_one(query)
            i_files[key].append(
                {
                    "type": key,
                    "code": this["data_id"],
                    "serialNumber": i_docs.get("serialNumber", ""),
                    "format": "json",
                }
            )

    # attachments
    for this in i_docs.get("attachments", []):
        if this["title"] not in i_files:
            i_files.update({this["title"]: []})
        i_files[this["title"]].append(
            {
                "type": this["title"],
                "code": this["code"],
                "serialNumber": i_docs.get("serialNumber", ""),
                "format": this["contentType"],
            }
        )

    # chip config
    for cfg in ["feCfg.before", "feCfg.after"]:
        if cfg not in i_files:
            i_files[cfg] = []

        doc = {"serialNumber": i_docs.get("serialNumber")}

        if "before" in cfg:
            for var in ["config_id", "config_revision_prev"]:
                if var in i_docs:
                    doc[var.replace("_prev", "")] = i_docs.get(var)

        if "after" in cfg:
            for var in ["config_id", "config_revision_current"]:
                if var in i_docs:
                    doc[var.replace("_current", "")] = i_docs.get(var)

        if "config_id" in doc:
            i_files[cfg].append(doc)

    return i_files


##############
# Set DCS Plot
def setDCS(i_tr_oid):
    docs = {}
    if not i_tr_oid:
        return {}

    try:
        query = {"_id": ObjectId(i_tr_oid)}
    except Exception as e:
        logger.error(str(e))
        logger.error(traceback.format_exc())
        return {}

    this_tr = localdb.testRun.find_one(query)
    if this_tr is None:
        return docs

    docs.update({"dcs_data_exist": False, "environment": "..."})
    if not i_tr_oid:
        return docs
    if this_tr.get("environment", False):
        docs.update({"dcs_data_exist": True})
    return docs


###########
# Set Score
def setCount():  # TODO ./dev/component.py
    return {}


##############
# Set Comments
# i_oid: ObjectId of this component/chip document
# i_tr_oid: ObjectId of this testRun document
# i_info: component information
def setComments(i_oid, i_tr_oid, i_info):
    docs = []
    query = []
    if i_tr_oid:
        query.append({"runId": i_tr_oid})
    if i_oid:
        query.append({"component": i_oid})
        for child in i_info.get("children", []):
            query.append({"component": child["_id"]})
        for parent in i_info.get("parents", []):
            query.append({"component": parent["_id"]})

    if not query:
        return docs

    entries = localdb.comments.find({"$or": query})
    for entry in entries:
        entry["datetime"] = datetime.fromisoformat(str(entry["datetime"])).strftime(
            "%Y-%m-%d %H:%M:%S"
        )
        docs.append(entry)
        componentId = entry["component"]
        entry["componentInfo"] = localdb.component.find_one(
            {"_id": ObjectId(componentId)}
        )

    return docs


### Others ###


def getResultIds(mname, stage):
    run_ids = []
    this_cp = localdb.component.find_one({"name": mname})
    if this_cp is not None:
        results = localdb.QC.result.find(
            {"$and": [{"component": str(this_cp["_id"])}, {"stage": stage}]},
            {"_id": 1},
        )
        run_ids = [str(result["_id"]) for result in results]
    return run_ids


def checkQCTestItems(c_oid, stage, test_list):
    with open(f"{VIEWER_DIR}/json-lists/supported_test.json", encoding="utf-8") as f:
        supported_test = json.load(f)
    for testType in test_list:
        test_doc = localdb.QC.result.find_one(
            {
                "$and": [
                    {"component": c_oid},
                    {"stage": stage},
                    {"testType": testType},
                ]
            }
        )
        if testType in supported_test["test"] and not test_doc:
            return False

    return True


def createQCtestSummary(run_ids):
    list_ = []

    nrun_entries = len(run_ids)
    if nrun_entries != 0:
        # for i in run_num_list:
        for i in range(nrun_entries):
            if run_ids[i] is None or run_ids[i] == "":
                list_.append({"run_data": {}})
                continue

            query = {"_id": ObjectId(run_ids[i])}
            this_run = localdb.QC.result.find_one(query)

            ### user
            user_name = this_run["user"]

            ### site
            site_name = this_run["address"]

            ### component
            this_cp = localdb.component.find_one(
                {"_id": ObjectId(this_run["component"])}
            )
            component = this_cp["name"]

            ### tags
            query = {}
            testRun_tag_candidates = userdb.viewer.tag.categories.find(query)
            tag_candidate = []
            for testRun_tag in testRun_tag_candidates:
                tag_candidate.append(testRun_tag)
            ### put tags
            query = {"runId": run_ids[i]}
            testRun_tags = userdb.viewer.tag.docs.find(query)
            tag = []
            for testRun_tag in testRun_tags:
                tag.append(testRun_tag)

            run_data = {
                "_id": run_ids[i],
                "datetime": setTime(
                    this_run["sys"]["cts"],
                    session.get("timezone", str(get_localzone())),
                ),
                "testType": this_run["testType"],
                "runNumber": 0,
                "stage": this_run["stage"],
                "component": component,
                "user": user_name,
                "site": site_name,
                "testRun_tag_candidate": tag_candidate,
                "testRun_tag": tag,
            }

            list_.append({"run_data": run_data})

    # list_ = sorted( list_, key=lambda x: x["run_data"]["datetime"], reverse=True )
    return list_


def createPropSummary(run_ids):
    list_ = []
    nrun_entries = len(run_ids)
    if nrun_entries != 0:
        # for i in run_num_list:
        for i in range(nrun_entries):
            if run_ids[i] is None or run_ids[i] == "":
                list_.append({"run_data": {}})
                continue

            query = {"_id": ObjectId(run_ids[i])}
            this_run = localdb.QC.module.prop.find_one(query)

            ### user
            user_name = this_run["user"]

            ### site
            site_name = this_run["address"]

            ### component
            this_cp = localdb.component.find_one(
                {"_id": ObjectId(this_run["component"])}
            )
            component = this_cp["name"]

            ### tags
            query = {}
            testRun_tag_candidates = userdb.viewer.tag.categories.find(query)
            tag_candidate = []
            for testRun_tag in testRun_tag_candidates:
                tag_candidate.append(testRun_tag)
            ### put tags
            query = {"runId": run_ids[i]}
            testRun_tags = userdb.viewer.tag.docs.find(query)
            tag = []
            for testRun_tag in testRun_tags:
                tag.append(testRun_tag)

            run_data = {
                "_id": run_ids[i],
                "datetime": setTime(
                    this_run["sys"]["cts"],
                    session.get("timezone", str(get_localzone())),
                ),
                "testType": this_run["testType"],
                "runNumber": 0,
                "stage": this_run["stage"],
                "component": component,
                "user": user_name,
                "site": site_name,
                "testRun_tag_candidate": tag_candidate,
                "testRun_tag": tag,
            }

            list_.append({"run_data": run_data})

    # list_ = sorted( list_, key=lambda x: x["run_data"]["datetime"], reverse=True )
    return list_


def getNextStage(module_id, stage):
    qc_status = localdb.QC.module.status.find_one({"component": str(module_id)})
    stage_list = list(qc_status["QC_results"])
    return stage_list[stage_list.index(stage) + 1]


def skipNextStage(module_id, stage):
    qc_status = localdb.QC.module.status.find_one({"component": str(module_id)})
    stage_list = list(qc_status["QC_results"])

    try:
        nextStages = [
            stage_list[stage_list.index(stage) + 1],
            stage_list[stage_list.index(stage) + 2],
        ]
    except Exception:
        logger.error(traceback.format_exc())
        nextStages = []
    return nextStages


####################################
# results for Sensor_IV & SLDO_VI
def SensorIV_graph(sensorIVresult):
    graph = layout_ModuleQC(sensorIVresult, "Sensor_IV")

    pngImage = io.BytesIO()
    FigureCanvas(graph).print_png(pngImage)

    # Encode PNG image to base64 string
    pngImageB64String = "data:image/png;base64,"
    pngImageB64String += base64.b64encode(pngImage.getvalue()).decode("utf8")

    return pngImageB64String


def SensorIV_units(sensorIVresult):
    units = {}
    try:
        units["V_step"] = sensorIVresult["results"]["unit"]["Voltage"]
        units["Step_duration"] = sensorIVresult["results"]["unit"]["Time"]
        units["N_measurements_per_step"] = ""
        units["I_Compliance"] = sensorIVresult["results"]["unit"]["Current"]
        units["Temperature"] = sensorIVresult["results"]["unit"].get(
            "Temperature", "no unit"
        )
        units["Humidity"] = sensorIVresult["results"]["unit"].get("Humidity", "no unit")
        units["comment"] = ""
        return units
    except Exception as exc:
        logger.error(exc)
        logger.error(traceback.format_exc())
        return units


def SLDOVI_graph(sensorIVresult):
    graph = layout_ModuleQC(sensorIVresult, "SLDO_VI")

    pngImage = io.BytesIO()
    FigureCanvas(graph).print_png(pngImage)

    # Encode PNG image to base64 string
    pngImageB64String = "data:image/png;base64,"
    pngImageB64String += base64.b64encode(pngImage.getvalue()).decode("utf8")

    return pngImageB64String


def SLDOVI_units(sensorIVresult):
    units = {}
    try:
        units["I_step"] = sensorIVresult["results"]["unit"]["Current"]
        units["N_iteration"] = ""
        units["N_measurements_per_step"] = ""
        units["Voltage_limit"] = sensorIVresult["results"]["unit"]["Voltage"]
        units["Temperature"] = sensorIVresult["results"]["unit"].get(
            "Temperature", "no unit"
        )
        units["Humidity"] = sensorIVresult["results"]["unit"].get("Humidity", "no unit")
        units["comment"] = ""
        return units
    except Exception as exc:
        logger.error(exc)
        logger.error(traceback.format_exc())
        return units


def set_light_plots(component_id, component_type, test_run_id, refresh_cache=False):
    global plotting_mutex_lock
    global plotting_mutex_lock_timestamp

    start_time = datetime.now()

    filesystem = gridfs.GridFS(localdb)

    plots = {}

    cpt = localdb.component.find_one({"_id": ObjectId(component_id)})

    serial_number = cpt.get("serialNumber")

    test_run = localdb.testRun.find_one({"_id": ObjectId(test_run_id)})

    if not test_run:
        return plots

    logger.debug(
        f"set_light_plots(): component_id = { component_id }, component_type = { component_type }, test_run_id = { test_run_id }"
    )

    ctrs = {
        ctr.get("geomId"): ctr
        for ctr in localdb.componentTestRun.find(
            {"testRun": test_run_id, "componentType": "front-end_chip"}
        )
    }

    plot_data = []

    serialNumbers = []

    for geomId in range(4):
        ctr = ctrs.get(geomId)

        if not ctr:
            continue

        serialNumbers += [ctr.get("name")]

        for index, att in enumerate(ctr.get("attachments")):
            if att.get("title") in plot_data:
                continue

            if att.get("contentType") != "pickle":
                continue

            if len([d for d in plot_data if d.get("title") == att.get("title")]) == 0:
                plot_data.append({"index": index, "title": att.get("title")})

    for plot in plot_data:
        logger.debug(f'plot-{plot.get("index")} : {plot.get("title")}')
        plot["data"] = {}

        tmpfile = THUMBNAIL_DIR / f"{test_run_id}_{plot.get('title')}.png"

        if tmpfile.is_file() and refresh_cache is False:
            plots[plot.get("title")] = url_for(
                "static.cache.thumbnail.static", filename=tmpfile.name
            )
            continue

        if tmpfile.is_file():
            os.remove(tmpfile)

        if (datetime.now() - plotting_mutex_lock_timestamp).seconds < 60:
            while plotting_mutex_lock:
                logger.debug("waiting for other thread to unlock...")
                time.sleep(0.1)

        plotting_mutex_lock_timestamp = datetime.now()
        plotting_mutex_lock = True

        for geomId in range(4):
            if not ctrs.get(geomId):
                continue

            try:
                if plot.get("index") < len(ctrs.get(geomId).get("attachments")):
                    plot_doc = None
                    for att in ctrs.get(geomId).get("attachments"):
                        if att.get("title") == plot.get("title"):
                            plot_doc = att
                            break
                else:
                    plot["data"].update({geomId: np.zeros((400, 384))})
                    continue
            except Exception as e:
                logger.error(str(e))
                logger.error(traceback.format_exc())
                logger.error(f"geomId {geomId}: attachment data not found")
                plot["data"].update({geomId: np.zeros((400, 384))})
                continue

            if plot_doc is None:
                logger.warning(
                    f'no good plot_doc was found for plot title {plot.get("title")} for geomId {geomId}. Skipping this FE'
                )
                continue

            if plot_doc.get("title") != plot.get("title"):
                logger.warning(
                    f'plot_doc title {plot_doc.get("title")} did not match with plot title {plot.get("title")} for geomId {geomId}. Skipping this FE'
                )
                continue

            pickle_data = filesystem.get(ObjectId(plot_doc.get("code"))).read()
            data = pickle.load(io.BytesIO(pickle_data))

            plot["type"] = data.get("Type")

            if len(plot["data"]) == 0:
                for var in ["x", "y", "z"]:
                    if var in data:
                        plot[var] = data.get(var)

            if plot.get("type") == "Histo2d":
                data_arr = np.array(data.get("Data"))
                if None in data_arr:
                    chip_hexSN = ctrs.get(geomId).get("hexSN")
                    plot_title = plot.get("title")
                    chip_serial_number = ctrs.get(geomId).get("name") or ctrs.get(
                        geomId
                    ).get("serialNumber")
                    msg = f"The component {chip_serial_number} ({chip_hexSN}) appears to have null data for {plot_title}. Will replace with 0's."
                    logger.warning(msg)
                    flash(msg, "warning")
                    data_arr[np.equal(data_arr, None)] = 0
                plot["data"].update({geomId: data_arr})
            else:
                plot["data"].update({geomId: data})

        if plot.get("type") == "Histo2d" and plot.get("title") != "InjVcalDiff":
            # debug rotation
            # for ig in range(0, geomId+1):
            #    for iy in range(0,4):
            #        for ix in range(0,100):
            #            for k in range(0, ig+1):
            #                plot.get("data")[ig][ix, k*10+iy] = -100

            if any(
                XXYY in serial_number
                for XXYY in [
                    "PIMS",
                    "PIM0",
                    "PIM5",
                    "PIR6",
                    "PIR7",
                    "PIR8",
                    "PIX3",
                    "PIX4",
                    "PIX5",
                ]
            ):
                plt.figure(figsize=(18, 6))
                ax = plt.gca()
                ax.tick_params(axis="both", which="major", labelsize=14)

                module = np.concatenate(
                    (
                        plot.get("data")[0].T,
                        plot.get("data")[1].T,
                        plot.get("data")[2].T,
                    ),
                    axis=1,
                )

                if None in module:
                    chip_hexSN = ctrs.get(geomId).get("hexSN")
                    plot_title = plot.get("title")
                    chip_serial_number = ctrs.get(geomId).get("name") or ctrs.get(
                        geomId
                    ).get("serialNumber")
                    msg = f"The component {chip_serial_number} ({chip_hexSN}) appears to have null data for {plot_title}. Will replace with 0's."
                    logger.warning(msg)
                    flash(msg, "warning")
                    module[np.equal(module, None)] = 0

                module = np.array(module, dtype=float)
                ma = np.ma.MaskedArray(module, module <= 0)
                min_nonzero_value = np.ma.min(ma)
                max_nonzero_value = np.ma.max(ma)
                min_max_ratio = max_nonzero_value / (min_nonzero_value + 1.0e-19)

                set_log_scale = min_nonzero_value > 0 and min_max_ratio > 100

                if set_log_scale:
                    module += min_nonzero_value * 1.0e-2
                    plt.imshow(
                        module,
                        origin="lower",
                        cmap="turbo",
                        norm=LogNorm(min_nonzero_value * 0.5, max_nonzero_value * 2),
                    )
                else:
                    plt.imshow(module, origin="lower", cmap="turbo")

                plt.title(
                    "Triplet Module: " + serial_number + " " + plot.get("title"),
                    fontsize=18,
                )
                ax.text(
                    200,
                    -20,
                    "FE1: " + delim_SN(serialNumbers[0]),
                    horizontalalignment="center",
                    verticalalignment="top",
                    size=10,
                    color="blue",
                )
                ax.text(
                    600,
                    -20,
                    "FE2: " + delim_SN(serialNumbers[1]),
                    horizontalalignment="center",
                    verticalalignment="top",
                    size=10,
                    color="blue",
                )
                ax.text(
                    1000,
                    -20,
                    "FE3: " + delim_SN(serialNumbers[2]),
                    horizontalalignment="center",
                    verticalalignment="top",
                    size=10,
                    color="blue",
                )

                if module.shape == (384, 1200):
                    ax.set_xticks(np.arange(0, 1200 + 1, 400))
                    ax.set_yticks(np.arange(0, 384 + 1, 384))
                    ax.set_xticks(np.arange(0, 1200 + 1, 16), minor=True)
                    ax.set_yticks(np.arange(0, 384 + 1, 16), minor=True)

                    ax.grid(
                        which="major", color="black", linestyle="solid", linewidth=0.5
                    )

                plt.xlabel("Column", fontsize=18)
                plt.ylabel("Row", fontsize=18)
                cb = plt.colorbar(label=data.get("z").get("AxisTitle"))
                for t in cb.ax.get_yticklabels():
                    t.set_fontsize(12)

            else:
                up = np.zeros((400, 768))
                try:
                    up = np.concatenate(
                        (
                            np.flip(np.flip(plot.get("data")[0], axis=0), axis=1),
                            plot.get("data")[3],
                        ),
                        axis=1,
                    )
                except Exception:
                    try:
                        up = np.concatenate(
                            (
                                np.flip(np.flip(plot.get("data")[0], axis=0), axis=1),
                                np.zeros((400, 384)),
                            ),
                            axis=1,
                        )
                    except Exception:
                        with contextlib.suppress(Exception):
                            up = np.concatenate(
                                (np.zeros((400, 384)), plot.get("data")[3]), axis=1
                            )

                down = np.zeros((400, 768))
                try:
                    down = np.concatenate(
                        (
                            np.flip(np.flip(plot.get("data")[1], axis=0), axis=1),
                            plot.get("data")[2],
                        ),
                        axis=1,
                    )
                except Exception:
                    try:
                        down = np.concatenate(
                            (
                                np.flip(np.flip(plot.get("data")[1], axis=0), axis=1),
                                np.zeros((400, 384)),
                            ),
                            axis=1,
                        )
                    except Exception:
                        with contextlib.suppress(Exception):
                            down = np.concatenate(
                                (np.zeros((400, 384)), plot.get("data")[1]), axis=1
                            )

                try:
                    module = np.concatenate((down, up), axis=0)
                except Exception as e:
                    logger.error(traceback.format_exc())
                    plotting_mutex_lock = False
                    logger.warning(f"Plotting for {plot.get('title')} failed: str{e}")
                    continue

                # note: sometimes module is an 'object' array, so convert it back to float
                # (Giordon): I think it's because of null values from yarr
                module = module.astype("float")
                ma = np.ma.MaskedArray(module, module <= 0)
                min_nonzero_value = np.ma.min(ma)
                max_nonzero_value = np.ma.max(ma)
                min_max_ratio = max_nonzero_value / (min_nonzero_value)

                set_log_scale = min_nonzero_value > 0 and min_max_ratio > 100

                plt.figure(figsize=(12, 9))

                if set_log_scale:
                    module += min_nonzero_value * 1.0e-2
                    plt.imshow(
                        module,
                        origin="lower",
                        cmap="turbo",
                        extent=[-384, 384, -400, 400],
                        norm=LogNorm(min_nonzero_value * 0.5, max_nonzero_value * 2),
                    )
                else:
                    plt.imshow(
                        module,
                        origin="lower",
                        cmap="turbo",
                        extent=[-384, 384, -400, 400],
                    )

                ax = plt.gca()
                ax.tick_params(axis="both", which="major", labelsize=10)

                # assert( module.shape == (800, 768) )
                ax.set_yticks(np.arange(-400, 400 + 1, 400))
                ax.set_xticks(np.arange(-384, 384 + 1, 384))
                ax.set_yticks(np.arange(-400, 400 + 1, 16), minor=True)
                ax.set_xticks(np.arange(-384, 384 + 1, 16), minor=True)

                ax.grid(which="major", color="black", linestyle="solid", linewidth=0.5)

                ax.text(
                    -415,
                    200,
                    "FE1: " + delim_SN(serialNumbers[0]),
                    horizontalalignment="center",
                    verticalalignment="center",
                    size=12,
                    color="blue",
                    rotation=-90,
                )
                ax.text(
                    -415,
                    -200,
                    "FE2: " + delim_SN(serialNumbers[1]),
                    horizontalalignment="center",
                    verticalalignment="center",
                    size=12,
                    color="blue",
                    rotation=-90,
                )
                ax.text(
                    415,
                    -200,
                    "FE3: " + delim_SN(serialNumbers[2]),
                    horizontalalignment="center",
                    verticalalignment="center",
                    size=12,
                    color="blue",
                    rotation=90,
                )
                ax.text(
                    415,
                    200,
                    "FE4: " + delim_SN(serialNumbers[3]),
                    horizontalalignment="center",
                    verticalalignment="center",
                    size=12,
                    color="blue",
                    rotation=90,
                )

                plt.title(
                    "Quad Module: " + serial_number + " " + plot.get("title"),
                    fontsize=18,
                )
                plt.xlabel("Row", fontsize=18)
                plt.ylabel("Column", fontsize=18)
                cb = plt.colorbar(label=data.get("z").get("AxisTitle"))
                for t in cb.ax.get_yticklabels():
                    t.set_fontsize(12)

        elif plot.get("type") == "Histo2d" and plot.get("title") == "InjVcalDiff":
            try:
                module = np.sum([d for k, d in plot.get("data").items()], axis=0)
            except Exception as e:
                logger.error(traceback.format_exc())
                plotting_mutex_lock = False
                logger.error(f"Plotting for {plot.get('title')} failed: str{e}")
                continue

            ma = np.ma.MaskedArray(module, module <= 0)
            min_nonzero_value = np.ma.min(ma)
            max_nonzero_value = np.ma.max(ma)
            min_max_ratio = max_nonzero_value / (min_nonzero_value + 1.0e-19)

            set_log_scale = min_nonzero_value > 0 and min_max_ratio > 100

            plt.figure(figsize=(12, 9))
            if set_log_scale:
                module += min_nonzero_value * 1.0e-2
                plt.imshow(
                    module.T,
                    origin="lower",
                    cmap="turbo",
                    norm=LogNorm(min_nonzero_value * 0.5, max_nonzero_value * 2),
                )
            else:
                plt.imshow(module.T, origin="lower", cmap="turbo")

            ax = plt.gca()
            ax.tick_params(axis="both", which="major", labelsize=10)

            plt.title(
                "Quad Module: " + serial_number + " " + plot.get("title"), fontsize=18
            )
            plt.xlabel(xlabel=data.get("x").get("AxisTitle"), fontsize=18)
            plt.ylabel(ylabel=data.get("y").get("AxisTitle"), fontsize=18)
            cb = plt.colorbar(label=data.get("z").get("AxisTitle"))
            for t in cb.ax.get_yticklabels():
                t.set_fontsize(12)

        elif plot.get("type") == "Histo1d":
            plt.figure(figsize=(12, 9))
            plt.title("Module: " + serial_number + " " + plot.get("title"), fontsize=18)

            for geomId, d in plot.get("data").items():
                try:
                    bins = np.linspace(
                        d.get("x").get("Low"),
                        d.get("x").get("High"),
                        d.get("x").get("Bins"),
                    )

                    plt.hist(
                        bins,
                        len(bins),
                        weights=d.get("Data"),
                        histtype="step",
                        label=f"FE{geomId+1}: " + delim_SN(serialNumbers[geomId]),
                    )
                    plt.legend(loc="upper right", fontsize=10)
                except AttributeError:
                    logger.error(traceback.format_exc())
                    logger.error(
                        f'{plot.get("title")}, geomId-{geomId}: skipped to render (likely the chip is disabled)'
                    )
                    plotting_mutex_lock = False
                    continue

                with contextlib.suppress(AttributeError):
                    plt.xlabel(d.get("x").get("AxisTitle"), fontsize=18)
                plt.ylabel(d.get("y").get("AxisTitle"), fontsize=18)

        plt.tight_layout()

        if not tmpfile.is_file():
            plt.savefig(tmpfile, dpi=100)
            plt.clf()
            plt.close()

        plots[plot.get("title")] = url_for(
            "static.cache.thumbnail.static", filename=tmpfile.name
        )

        plotting_mutex_lock = False

    end_time = datetime.now()
    process_time = (end_time - start_time).total_seconds()
    logger.info("set_light_plots: process time = " + pprint.pformat(process_time))

    return plots


@component_api.route("/component/<identifier>/stage", methods=["GET"])
def get_component_stage(identifier):
    if ObjectId.is_valid(identifier):
        component = localdb.component.find_one({"_id": ObjectId(identifier)})
    else:
        component = localdb.component.find_one({"serialNumber": identifier})

    if not component:
        abort(400, f"there is no component with {identifier}")

    component_id = str(component["_id"])

    stage = (localdb.QC.module.status.find_one({"component": component_id}) or {}).get(
        "stage"
    )

    if not stage:
        abort(500, f"there is no QC status for {identifier}")

    return make_response({"stage": stage}, 200)


@component_api.route("/component/<identifier>/stage/<stage>", methods=["POST"])
def set_component_stage(identifier, stage):
    redirect_url = request.args.get("redirect")
    refer = request.headers.get("Referer")
    if not redirect_url:
        if not refer:
            return make_response(
                {"error": "Redirect url must be specified as there is no referer."}, 422
            )
        return redirect(
            url_for(request.endpoint, **dict(request.view_args, redirect=refer)),
            code=307,
        )

    if not session.get("logged_in", False):
        return render_template("401.html"), 401

    # check itkdb authentication
    pd_client = get_pd_client()

    if pd_client is None:
        message = "No valid ITkPD client found. Please login."
        return redirect(
            url_for(
                "user_api.itkdb_authenticate", message=message, redirect=redirect_url
            )
        )

    if ObjectId.is_valid(identifier):
        component = localdb.component.find_one({"_id": ObjectId(identifier)})
    else:
        component = localdb.component.find_one({"serialNumber": identifier})

    if not component:
        abort(400, f"there is no component with {identifier}")

    serial_number = component["serialNumber"]

    if message_exists(component=serial_number, code="SYNC_COMPONENT_STAGE"):
        msg = f"Already setting the stage for component {serial_number}."
        logger.error(msg)
        create_message(
            f"The component {serial_number} is already syncing stages.",
            function="set_component_stage",
            component=serial_number,
            code="ERROR_DUPLICATE",
        )
    else:
        ticket_id = create_message(
            f"Recursively setting the stage to {stage} for {serial_number} and its children in ITkPD and LocalDB. Feel free to refresh but do not switch the stage of this component in the meantime.",
            function="set_component_stage",
            component=serial_number,
            code="SYNC_COMPONENT_STAGE",
            can_dismiss=False,
        )

        def set_component_stage_worker(pd_client, serial_number, stage):
            try:
                changed_components_itkpd = sync_component_stages(
                    pd_client, serial_number, stage, ignore_types=["MODULE_CARRIER"]
                )
            except Exception:
                msg = f"There was an error in setting the stage to {stage} for {serial_number} and its children in ITkPD.<br /><pre>{traceback.format_exc()}</pre>"
                update_message(ticket_id, message=msg, can_dismiss=True)
                return

            try:
                changed_components_localdb = sync_component_stages(
                    localdb,
                    serial_number,
                    stage,
                    userdb=userdb,
                    ignore_types=["module_carrier"],
                )
            except Exception:
                msg = f"There was an error in setting the stage to {stage} for {serial_number} and its children in LocalDB.<br /><pre>{traceback.format_exc()}</pre>"
                update_message(ticket_id, message=msg, can_dismiss=True)
                return

            def make_table(changed_components, title_prefix):
                table = Table(title=f"{title_prefix} Components Changed")
                table.add_column("Component", style="red bold")
                table.add_column("Old Stage", justify="center", style="blue")
                table.add_column("New Stage", justify="center", style="green")
                table.add_column("New?", justify="center")

                for current_serial_number, (
                    current_stage,
                    changed,
                ) in changed_components.items():
                    table.add_row(
                        current_serial_number,
                        current_stage,
                        stage,
                        ":new:" if changed else "",
                    )

                return table

            table_itkpd = make_table(changed_components_itkpd, "ITkPD")
            table_localdb = make_table(changed_components_localdb, "LocalDB")

            with open(os.devnull, "w") as devnull:
                console = Console(record=True, file=devnull)

                console.print(table_itkpd)
                exported_html_itkpd = console.export_html(
                    inline_styles=True, code_format="<pre>{code}</pre>"
                )

                console.print(table_localdb)
                exported_html_localdb = console.export_html(
                    inline_styles=True, code_format="<pre>{code}</pre>"
                )

            msg = f"Finished setting the stage to {stage} for {serial_number} and its children in ITkPD and LocalDB.<br />{exported_html_itkpd}<br />{exported_html_localdb}"
            update_message(ticket_id, message=msg, can_dismiss=True)

        def set_component_stage_done(future):
            try:
                future.result()
            except Exception as e:
                logger.error(f"{e}")
                logger.error(traceback.format_exc())
                create_message(
                    "An error occurred. Please contact LocalDB admin.",
                    function="set_component_stage",
                    component=serial_number,
                    code="ERROR_GENERAL",
                )

        executor = concurrent.futures.ThreadPoolExecutor(
            thread_name_prefix="setComponentStage"
        )
        future = executor.submit(
            set_component_stage_worker,
            pd_client,
            serial_number,
            stage,
        )
        future.add_done_callback(set_component_stage_done)
        executor.shutdown(wait=False)

    return redirect(redirect_url)
