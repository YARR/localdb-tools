from pages.checkout import checkout_api
from pages.component import component_api
from pages.config import config_api
from pages.error import error_api
from pages.history import history_api
from pages.picture_data import picture_data_api
from pages.plot_data import plot_data_api
from pages.plot_dcs import plot_dcs_api
from pages.qc import qc_api
from pages.register import register_api
from pages.retrieve import retrieve_api
from pages.site import site as site_api
from pages.static import static_api
from pages.sync_statistics import sync_statistics_api
from pages.tag import tag_api
from pages.toppage import toppage_api
from pages.user import user_api

__all__ = (
    "checkout_api",
    "component_api",
    "config_api",
    "error_api",
    "history_api",
    "picture_data_api",
    "plot_data_api",
    "plot_dcs_api",
    "qc_api",
    "register_api",
    "retrieve_api",
    "site_api",
    "static_api",
    "sync_statistics_api",
    "tag_api",
    "toppage_api",
    "user_api",
)
