# Archive Tool

The Archive Tool creates a/some archive tar.gz files to back-up Local DB.

## Usage

### 0. Pre Requirement

- MongoDB is running
- python3
- ROOT CXX Library

### 1. Setup

```
$ ./setup_archive_tool.sh
```

It will create ./bin/localdbtool-archive.sh and `my_archive_configure.yml`, and open to edit `my_archive_configure.yml` automatically. 
Then type below to see what will be happened.

### 2. Keep backup to .tar.gz file

```
$ ./bin/localdbtool-archive.sh -f my_archive_configure.yml
```

### 3. Restore DB from .tar.gz file

```
$ cd localdb-tools/archive-tool
$ tar -zxvf archive-mongo-data/dump.tar.gz
$ mongorestore --db localdb dump/localdb
$ mongorestore --db localdbtools dump/localdbtools
$ rm -rf dump
```

## Config file

```yaml
# MongoDB database port. Default is 27017
db_port: 27017

# MongoDB database port. Default is localdb
db_name: localdb

# Path want to put archives
archive_path: ./archive-mongo-data

# Path to log file
log_path: ./log

# # of archives want to keep
n_archives: 2
```
