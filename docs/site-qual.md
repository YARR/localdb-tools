# Site Qualification Quick Guide

We equipped an automated site-qualification documentation within the LocalDB
service.

## Pre-requisites

- You need to pull initial data from ITkPD from Top Page (with sign-in) ->
  Administrate LocalDB / Initial data sync with ITkPD.
- You need to download an example tutorial module `20UPGXM2000013` from Top Page
  (with sign-in) -> Administrate LocalDB / Request site qualification Block 2

# Request site qualification Block 2

After signing-in as an admin user, click "Request site qualification Block 2"
![Guide 1](images/sq/SQ1.png)

## Step-1: ITkPD authentication

Input 2 access codes and press "Next"

![Guide 1](images/sq/SQ1.png)

## Step-2: Select your cluster

Here, it is assumed that you have already created a site qualification instance
of your cluster on the production DB for e.g. Lab Infrastructure qualification
(Block 1). If you do not find your institute, you are guided to go through
[Kenny's webapp](https://uk-itk-pdb-webapp-pixels.web.cern.ch) for initializing
the SQ process.

- Further reading:
  [Site Qualification WebApp instruction slide](https://indico.cern.ch/event/1258215/contributions/5285054/attachments/2598717/4487552/WP13_23_2_23_KGW.pdf)

![Guide 2](images/sq/SQ2.png)

Select your cluster and press "Next"

## Step-3: Confirmation

Several functions are called to collect server's information as well as contents
of data within mongoDB. It is displayed in the page as a table for the following
items:

- site serial number
- LocalDB version
- OS (`uname`)
- CPU/memory of the service machine
- Disk profile of the server machine
- mongod/influxd services infos (if servers are on the same host)
- Glance of last document of each collection in `localdb` and `localdbtools`
  mongo databases

![Guide 3](images/sq/SQ3.png)

Press "Submit"

## Step-4: Completion

You are also guided to fill the Google Form
[questionnaire for server admins](https://docs.google.com/forms/d/e/1FAIpQLSdyJS2zKJ7-vVVUzD6VRvzgwXmHT93BlB3JrawC8njlbYOsPg/viewform)
Submission of the Form is a part of requirement.

![Guide 4](images/sq/SQ4.png)
