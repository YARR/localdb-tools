# Demo

[This YouTube playlist](https://youtube.com/playlist?list=PLGHE-1Vk0R9yEiXJ_X_l3H0PeZuV0vOIo)
provides various demos about LocalDB.

## LocalDB v2.0 Demo Part1: Initial Setup and Site Qualification

<iframe width="560" height="315" src="https://www.youtube.com/embed/HSx1bt1oMj4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## LocalDB v2.0 Demo Part2: Uploading an electrical test via browser

<iframe width="560" height="315" src="https://www.youtube.com/embed/yBG5sYXKsDo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## LocalDB v2.0 Part 4: Switching Stage of the Module

<iframe width="560" height="315" src="https://www.youtube.com/embed/6utf8uxLPfM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## LocalDB v2.0 Demo Part3: Uploading an electrical test via CLI

<iframe width="560" height="315" src="https://www.youtube.com/embed/7slQacDis1I" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## LocalDB v2.1 Preview Part1: `module-qc-nonelec-gui`: Visual Inspection

<iframe width="560" height="315" src="https://www.youtube.com/embed/dEOBEBgRZLI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## LocalDB v2.1 Preview Part 2: Non-electrical QC Simple Input Example (Mass Measurement)

<iframe width="560" height="315" src="https://www.youtube.com/embed/Bt4NqEDZmJ0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
