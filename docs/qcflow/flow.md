# Module QC Demo

This page provides a demonstration of a module QC flow using emulated data for a
dummy module.

## Pre-requisites

- In order to go through this page, it is assumed that all installation
  procedure in the [Installation Guide](../install.md) is complete.
- On top of that, you will need to have an account on ITk production database
  (two passcodes authentication). You will generate a virtual serial number for
  a virtual module.

!!! attention If you don't have an account for the production DB. Please sign up
following the link below:<br>
[Tutorial page for ITkPD ](https://gitlab.cern.ch/jpearkes/itkpd_tutorial/blob/master/README.md)<br>

!!! important ITk production DB web login system provides various logins,
including CERN single sign-on (SSO), Google account etc. However, the `LocalDB`
system relies on the backend `itkdb` python package which uses the two passcodes
authentication. Please get familiar with this sign-in method.

## Goal

Through this course, you will be able to gain experience on:

- Learn how to register a new component to ITk production DB
- (Virtually) assemble a bare module
- (Virtually) assemble a module
- Carry out a virtual QC of the "BareToPCB" stage
- Sign-off the

## Download institutions and module types info from ITkPD

So far we assume the LocalDB is freshy new. Before you assemble a new module,
first the LocalDB needs to be aware of the list of module types and the list of
possible institutes (sites). Let's download them.

!!! note This process needs to be done only once, unless there is addition of
new institutes or new module types defined in the production DB.

1. Sign-in in the viewer webapp as `admin`
1. Click "Download institution and module types info from ITkPD
1. Input two passcodes authentication and press "Download" blue button

![download pdinfo](../images/qc-flow/download_pdinfo.png)

!!! hint If you fail in downloading, it may be the case that your PC time is
desynchronized from the global time more than 2 seconds. If this would be the
case, you will need to adjust the PC's clock using e.g. `chrony`.

## Book your dummy tutorial components (only for Demo)

In this demo, we will use dummy serial numbers for `tutorial components`, which
are all surrogate and not associated to any real physical component. Polluting
LocalDB or Production DB by using these dummy serial numbers do not damage real
operation. However, you will need to occupy these components "for your use
only".

Please input your name in any blank row of
[this spreadsheet](https://docs.google.com/spreadsheets/d/1OqOUhGu_tBYJXXhzD7B5GtiS1nLSQiVCOWlQe9BCIIM/edit?usp=sharing).
You will use the corresponding serial numbers of:

- a PCB
- a carrier
- a FE chip (we will assemble a dummy single-chip module)
- sensor manufacturer

## Register new components to ITkPD

We can register a module from LocalDB and a bare module from QC-helper. Before
registering these components, we need to register children for these. We will
register components using web page for
[ITkPD](https://itkpd-test.unicorncollege.cz/myComponents). We can register a
new component from `My Components`:

![Register_Module](../images/qc-flow/register_comp_itkpd.png)<br>

You will need to repeat the above steps for all four items:

- a PCB
  - FE chip version: `ITkpix_v1.1`
  - Other fields can be left empty.
  - ATLAS SN: follow
    [the spreadsheet table](https://docs.google.com/spreadsheets/d/1OqOUhGu_tBYJXXhzD7B5GtiS1nLSQiVCOWlQe9BCIIM/edit?usp=sharing)
- a carrier
  - Type: `0`
  - Usgae countdown: `1`
  - ATLAS SN: follow
    [the spreadsheet table](https://docs.google.com/spreadsheets/d/1OqOUhGu_tBYJXXhzD7B5GtiS1nLSQiVCOWlQe9BCIIM/edit?usp=sharing)
- a FE chip (we will assemble a dummy single-chip module)
  - ATLAS SN: follow
    [the spreadsheet table](https://docs.google.com/spreadsheets/d/1OqOUhGu_tBYJXXhzD7B5GtiS1nLSQiVCOWlQe9BCIIM/edit?usp=sharing)
- a sensor tile
  - Main vendor: `0`
  - Sensor Type or Test Structure: `0`
  - Version of component: `prototype`
  - ATLAS SN: follow
    [the spreadsheet table](https://docs.google.com/spreadsheets/d/1OqOUhGu_tBYJXXhzD7B5GtiS1nLSQiVCOWlQe9BCIIM/edit?usp=sharing)

!!! Warning You will need to have an appropriate privilege of managing
components at your institute in the ITk production DB. If you encounter problems
in the above step, contact someone who have `Authority` `Executive`
`Component Manager` privilege of your institute to assign you an appropriate
privilege.

!!! Note You may belong to multiple institutes in the world of ITk production
DB. Your privilege can be different by institutes.

!!! Warning If you can not find component type, you need to add component type
as one of the components handled by your institute. Please go to your institute
page and fix.

<br>
After registering children, we can register a new bare module and module to DB!

## Register a new bare module to ITkPD

Now we do a virtual "flip-chip" and register a new bare-module. We use QCHelper
in this step.

**This step using QCHelper is broken. To Be fixed.**

!!! Warning Current temporary solution using ITk PD website:

    1. Take the common least 5 significant digits as PCB, carrier, FE.
    1. `20U`XX``YY`: `XX` should be `PG` (Pixel General). `YY` should be `XB`.
    1. The next digit after `YY` is `2` for ITkPix_v1.1. The 2nd next digit after `YY` is `0` for "Thin".
    1. As a result, the SN should be `20UPGXB20?????` where `?????` is the common 5 digits.

```bash title="bash"
cd /path/to/qc-heler-install
qc-helper &
```

![choose option](../images/qc-flow/register_bare_1.png)

First, QCHelper requests to sign-in ITkPD using two passcodes. Input them and
press "Next".

Then, input bare module information. In the first panel, you fill:

- Bare Module Type: Tutorial bare module
- FE chip version: ITkPix_v1.1
- FE chip thickness:

![input bare info](../images/qc-flow/register_bare_2.png)

### 4. Assemble `chip` and `sensor tile` to the `bare module`

![assemble chip and sensor](../images/qc-flow/register_bare_3.png)

!!! warning QC-helper can not assemble single chip to bare module. So for this
tutorial, we assemble a sensor tile with QC-helper and a chip with web page for
ITkPD: [My Components](https://itkpd-test.unicorncollege.cz/myComponents)

After registering a bare module to ITkPD, we can register a new module to ITkPD.

## Register a new module to DB

We can register a new module using web page of LocalDB. It register a module to
ITkPD and LocalDB.<br>

Please go to the top page in LocalDB and follow the instruction below. <br>

![Register_Module](../images/qc-flow/register_module.png)

<br>
You can check the registered module in my components of ITkPD below:<br>
[https://itkpd-test.unicorncollege.cz/myComponents](https://itkpd-test.unicorncollege.cz/myComponents)<br>

You can find the registered module list in the page of the LocalDB.<br>
[http://localhost:5000/localdb/components](http://localhost:5000/localdb/components)<br>
or<br> http://IPADRESS:5000/localdb/components

## Upload QC-test results for Bare to PCB

We will upload QC-test results using GUI for QC-helper. QC-helper cafully
support operations.<br> If you check the detail for QC-helper, please check it:
[Instruction for QC-helper](https://gitlab.cern.ch/atlas-itk/sw/db/pixels/qc-viz-tools-dev/qc-helper/-/tree/master/doc/Instruction)

## Test Items for Bare to PCB

![demo scan](../images/qc-flow/stage_bare.png)

### Bare to PCB

- Visual Inspection
- Metrology (<span style="color: red; ">Old format</span>)
- Mass
- Glue Flex Attach information

!!! warning Stages and test items are not current version. We are improving the
SW to match newest version of QC flow.<br>

## QC-helper

QCHelper is GUI to upload QC results to localDB.

!!! warning Before stating to upload QC results, you need to pull the reference
module's picture in `WorkDir/qc-helper/each_test/qc-vi`. If you can not find
`golden_module` in `qc-vi`, please download it from
[indico](https://indico.cern.ch/event/1093086/).<br>

### 1. Start GUI

```bash
$ cd Workdir/qc-helper
$ python3 main.py
```

<br>

### 2. Operating procedure

![demo scan](../images/qc-flow/qchelper_bare.png)

<br>

When we are asked to input some file, please use the files we downloaded from
[indico](https://indico.cern.ch/event/1093086/).

Most of the results are easy to input, but visual inspection is a little
complicated. If you are confusing for visual inspection, please check below:

#### Optical Inspection (Visual Inspection)

![demo optical](../images/qc-flow/qchelper_visual.png)

### 3. Check the uploaded results in LocalDB viewer

We can see the uploaded results in LocalDB viewer:<br>
[http://127.0.0.1:5000/localdb](http://127.0.0.1:5000/localdb)<br> or <br>
https://IPADRESS:5000/localdb.

#### Go to the module's toppage following the instruction below.

![demo scan](../images/qc-flow/goto_module_toppage.png)

You can see the uploaded results in the table of "QC Test" in the page as below.
You can go to the result page for each test by clicking the ids in the table.

![demo scan](../images/qc-flow/check_results_localdb.png)

After uploading all required test results to LocalDB, we can proceed a stage.

# Sign-off Bare to PCB

We will sign off the QC results using LocalDB viewer. The production stage is
automatically changed to the next after the sign-off. Please follow the
instruction below after sign-in on your viewer to click "Sign-in" at the top
right.

!!! warning We need to upload all test results listed in the table. We can not
push the button before store all results.<br>

![demo scan](../images/qc-flow/sign_off_results.png)

After the sign-off, we will upload the selected results to ITkPD.

# Upload selected results to ITkPD

You can upload selected QC test results to ITkPD.<br> Please follow the
instruction below after sign-in on your viewer to click "Sign-in" at the top
right.<br>

![Upload_Results_To_ITkPD](../images/qc-flow/upload_results_itkpdbare.png)<br>

You can check the results at web page for ITkPD:<br>
[https://itkpd-test.unicorncollege.cz/myTests](https://itkpd-test.unicorncollege.cz/myTests)

After uploading the results, we will start QC test for Wirebonding.

# Download QC-test results from ITkPD

For this tutorial, we use a module: `ATLAS SN: 20UPGXM0000002`. We prepared this
module as the target to download results.<br>
[Component Page for 20UPGXM0000002](https://itkpd-test.unicorncollege.cz/componentView?code=453953baba941fff9dbe41c6c9b6a0fe)

## Download a module information from ITkPD

We need to download a module information from ITkPD. We will download a module
using commands below:

```bash
$ cd ~/localdb-tools/viewer/itkpd-interface
$ source authenticate.sh
Input Access Code 1 for ITkPD:
Input Access Code 2 for ITkPD:
You have signed in as Satoshi Kinoshita. Your token expires in 1799s.
$ ./bin/ModuleIdDownloader.py --component_id 20UPGXM0000002
```

Then, we can see a module in LocalDB viewer:<br>
[http://localhost:5000/localdb/components](http://localhost:5000/localdb/components)<br>
or<br> http://IPADRESS:5000/localdb/components

## Download results from ITkPD

You can download QC test results stored in ITkPD. <br> Please follow the
instruction below after sign-in on your viewer to click "Sign-in" at the top
left conner.<br>
![Download_Results_From_ITkPD](../images/qc-flow/download_results_itkpd.png)<br>

### 5. Upload QC-test results for Wirebonding

- [Upload QC-test results for Wirebonding](nonelectricalwire.md)
- [Upload Scan results to LocalDB](scanconsole.md)
- [Select Scans test results](upload_resultwire.md)

### 6. Sign-off each stage and push results to ITkPD

- [Sign-off each stage](signoffwire.md)
- [Push the list of signed off results](upload_itkpdwire.md)

### 6. Change a stage after Wirebonding

- [Change a stage after Wirebonding](change_stage.md)

<hr>

## Reference

1. Document of "Traveling
   module"[(https://moduledaqdb.readthedocs.io/en/latest/)](https://moduledaqdb.readthedocs.io/en/latest/)
2. Yarr
   docs[(https://yarr.readthedocs.io/en/latest/)](https://yarr.readthedocs.io/en/latest/)
3. LocalDB
   docs[(https://localdb-docs.readthedocs.io/en/master/)](https://localdb-docs.readthedocs.io/en/master/)
4. QC-helper docs[(https://grafana.com)](https://grafana.com)
5. Tutorial page for ITk production
   DB[(https://gitlab.cern.ch/jpearkes/itkpd_tutorial/blob/master/README.md)](https://gitlab.cern.ch/jpearkes/itkpd_tutorial/blob/master/README.md)
6. Module QC
   documentation[(https://cds.cern.ch/record/2702738/files/ATL-COM-ITK-2019-045.pdf?)](https://cds.cern.ch/record/2702738/files/ATL-COM-ITK-2019-045.pdf?)
7. MongoDB web[(https://www.mongodb.com)](https://www.mongodb.com)

## Contact

- Please send questions or comments to: kinoshita at hep.phys.titech.ac.jp
- Also a dedicated mattermost
  channel:[https://mattermost.web.cern.ch/yarr/channels/qc-demonstration](https://mattermost.web.cern.ch/yarr/channels/qc-demonstration)
