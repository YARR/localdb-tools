# Previous documentations

- [v1.6.x](https://localdb-docs.readthedocs.io/en/1.6/)
- [v1.5](https://localdb-docs.readthedocs.io/en/1.5/)
- [v1.4](https://localdb-docs.readthedocs.io/en/1.4/)
- [v1.3](https://localdb-docs.readthedocs.io/en/1.3/)
